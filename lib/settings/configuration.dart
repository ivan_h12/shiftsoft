import 'package:flutter/material.dart';
import 'package:shiftsoft/models/muser.dart';

class Configuration extends InheritedWidget {
    int buildCode = 1;
    String company = "shiftsoft";

    String serverProd = "http://shiftsoft.org:8000";
    String serverDev = "http://shiftsoft.org:8000";
    
    int companyId = 1;
    String serverProdCompany = "shift";
    String serverProdHash = "myhash";
    String oneSignalAppId = "2b373a13-de7d-478d-a3ce-f3c470d09e1c";
    int apiLimit = 10;

    String initialRoute = "login";
      
    bool featureRestriction = false;
    bool resetOnStart = false;
    bool testing = false;
    bool multipleCompany = false;
    bool triggerCompanyList = true;

    User user;
    
    Color colorGreen = Color.fromRGBO(80, 150, 100, 1.0);
    Color colorRed = Colors.red;
    Color colorGrey = Colors.grey;

    String get baseUrlAPI => serverProd + "/" + serverProdCompany + "/api";
    String get baseUrl => serverProd + "/" + serverProdCompany;

    String currency = "Rp";

    Configuration({Key key, Widget child }):super(key: key, child: child);
    
    bool updateShouldNotify(oldWidget) => true;

    static Configuration of(BuildContext context) {
      return (context.inheritFromWidgetOfExactType(Configuration) as Configuration);
    }

    Color topBarBackgroundColor = Color(0xFF334455);
    Color topBarTextColor = Color(0xFFFFFFFF);
    Color topBarIconColor = Color(0xFFFFFFFF);

    Color bottomBarBackgroundColor = Color(0xFFF0F0F0);
    Color bottomBarTextColor = Color(0xFF333333);
    Color bottomBarIconColor = Color(0xFF333333);

    Color bodyBackgroundColor = Color(0xFFF7F7F7);


    Color primaryColor = Colors.red;
    Color primaryTextColor = Colors.white;
    Color secondaryColor = Colors.orange;
    Color secondaryTextColor = Color(0xFF333333);
    Color grayColor = Color(0xFF777777);
    Color lightGrayColor = Color(0xFFBBBBBB);
    Color lighterGrayColor = Color(0xFFDDDDDD);

    

    // Color topBarBackgroundColor = Color(0xffb74093);
    // Color topBarTextColor = Color(0xffff00ff);
    // Color topBarIconColor = Colors.white;
    // Color primaryColor = Color(0xffb74093);
    // Color primaryTextColor = Color(0xffff00ff);
    // Color secondaryColor = Color(0xffaaffaa);
    // Color secondaryTextColor = Color(0xffffaaaa);
}

final c = Configuration();