import 'package:flutter/material.dart';
import 'package:shiftsoft/DemoLocalizations.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../widgets/button.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'dart:convert';

Future<SharedPreferences> _prefs;

String messages(context, List<String> keys, [String separator = " "]) {
  List<String> tempString = [];
  int index = 0;
  keys.map((item) {
    tempString.add(DemoLocalizations.of(context).trans(item));
    // capitalize first character
    if (index == 0) {
      tempString[0] = tempString[0][0].toUpperCase() + tempString[0].substring(1);
      index++;
    }
  }).toList();
  return tempString.join(separator);
}

void Alert({context, String title, Widget content, List<Widget> actions, VoidCallback defaultAction, bool cancel = true, String type = "warning"}) {
  if (actions == null) {
    actions = [];
  }

  Widget icon;
  if (type == "success") {
    icon = Icon(
      Icons.check_circle,
      color: Colors.green,
      size: 30.0,
    );
  } else if (type == "warning") {
    icon = Icon(
      Icons.warning,
      color: Colors.orange,
      size: 30.0,
    );
  } else {
    icon = Icon(
      Icons.error,
      color: Colors.red,
      size: 30.0,
    );
  }
  
  showDialog (
    context: context,
    barrierDismissible: false,
    builder: (context){
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(7.5)),
        ),
        title: 
        // kalau titlenya gak null, judulnya ada
        title != null ?
          // kalau titlenya kosongan, brarti gk ada judulnya
          title == "" ?
          null
          :
          Row(
            children: <Widget>[
              icon,
              Padding(padding: EdgeInsets.symmetric(horizontal: 10.0)),
              Text(title),
            ],
          )
        :
        Row(
          children: <Widget>[
            icon,
            Padding(padding: EdgeInsets.symmetric(horizontal: 10.0)),
            Text(messages(context, [type])),
          ],
        ),
        content: content == null ? null:content,
        // kalau actions nya kosong akan otomatis mengeluarkan tombol ok untuk menutup alert
        actions: actions.length == 0 ?
        [
          defaultAction != null && cancel ?
          Button(
            key: Key("cancel"),
            child: Text(messages(context, ["cancel"]), style: TextStyle(color: Colors.black),),
            fill: false,
            border: false,
            onTap: () {
              Navigator.of(context).pop();
            },
          ) : Container(),
          Button(
            key: Key("ok"),
            child: Text(messages(context, ["ok"]), style: TextStyle(color: Colors.black),),
            fill: false,
            border: false,
            onTap: () {
              Navigator.of(context).pop();
              defaultAction();
            },
          ),
          // kalau ada default action akan otomatis menampilkan tombol cancel, jadi akan muncul ok dan cancel
        ]
        :
        [
          // kalau ada pilihan tombol lain, akan otomatis mengeluarkan tulisan cancel
          Button(
            key: Key("cancel"),
            child: Text(messages(context, ["cancel"])),
            fill: false,
            border: false,
            onTap: () {
              Navigator.of(context).pop();
            },
          )
        ]..addAll(actions)
      );
    }
  );    
}

String APIUrl(String url, {String parameter = "", bool godMode = false, useToken:false, bool print = false, BuildContext context}) {
  Configuration config;
  if (context != null) {
    config = Configuration.of(context);
  } else {
    config = c;
  }

  String link = config.baseUrlAPI + "/" + url + "?h=" + config.serverProdHash + (parameter == "" ? "" : "&" + parameter);
  link += (godMode ? "&GodMode=true":"") + (useToken ? "&useToken=true":"");
  if(print)
    debugPrint(link);
  return link;
}

String Url(String url, [String parameter = "" ]) {
    return c.baseUrlAPI + "/" + url + "?h=" + c.serverProdHash + (parameter == "" ? "" : "&" + parameter) ;
}

String CompanyAsset(String page, String link, {BuildContext context}) {
  Configuration config;
  if (context != null) {
    config = Configuration.of(context);
  } else {
    config = c;
  }

  if (link == "") {
    return config.serverProd + "/public/img/no-image.png";
  }

  if (page == "") {
    return link;
  }
  return config.serverProd + "/public/c/" + config.serverProdCompany + "/" + page + "/" + link;
}

Future<bool> checkRolePrivileges(String module, String feature, [String package = "Backend"]) async {
  _prefs = SharedPreferences.getInstance();
  if (c.featureRestriction == false) {
    return true;
  }
  
  final SharedPreferences prefs = await _prefs;
  List<String> tempPrivileges = prefs.getStringList("rolePrivileges");

  bool status = tempPrivileges.contains("$package$module.$feature");

  return status;
}

Future<bool> checkPermission(final context, String module, String privilege, mustLogin) async {
  Configuration config = Configuration.of(context);
  if (config.featureRestriction == false)
		return true;
  
  _prefs = SharedPreferences.getInstance();
  final SharedPreferences prefs = await _prefs;
  List<String> loginUserList = prefs.getStringList("loginUserList") ?? [];
  List<String> tempModules = prefs.getStringList("modules") ?? [];
  List<String> tempPrivileges = prefs.getStringList("rolePrivileges") ?? [];

  bool result = false;

  result = tempModules.contains(module);

  if (result == false) {
    return result;
  }


  if (mustLogin) {
    result = false;
    bool isLogin = false;
    
    loginUserList.map((item) {
      User user = User.fromJson(json.decode(item));
      if (config.companyId == user.companyId) {
        isLogin = true;
      }
    }).toList();

    // kalau dia udah login akan ngecek privileges
    if (isLogin) {
      result = tempPrivileges.contains(privilege);
    }
  }

  return result;
}

String numberFormat(int number, String currency) {
  var f = NumberFormat.decimalPattern(); 
  return "$currency. " + f.format(number);
}

Future<String> getToken() async {
  _prefs = SharedPreferences.getInstance();
  final SharedPreferences prefs = await _prefs;
  String token = prefs.getString("token");
  return token;
}

String getBase64Image(File img){
  List<int> imagesBytes = img.readAsBytesSync();
  String base64Image = base64Encode(imagesBytes);
  return base64Image;  
}