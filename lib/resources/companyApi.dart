import 'dart:convert';
import 'package:shiftsoft/models/mcompany.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/companyMock.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';

class CompanyApi {
  Client client = Client();

  Future<List<Company>> getCompanyList(int page, {String parameter = ""}) async {
     if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Company> companyList = [];
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("company/list/$page", parameter:parameter, print: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          companyList.add(Company.fromJson(item));
        }).toList();
      }

      return companyList;
    } else {
      return null;
    }
  }

  Future<Company> getCompany(final context, int id) async {
    Company company;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("company/$id-id", context: context, parameter: "with[0]=Package.Modules", print: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);


      if (parsedJson["success"] == 1) {

        print(parsedJson["data"]["SpecialAttrs"]);

        List<String> tempId = [];
        company = Company.fromJson(parsedJson["data"]);
        
        print(parsedJson["data"]["SpecialAttrs"]["BannerMustLogin"]);

        if (parsedJson["data"]["Package"]["Modules"].length > 0) {
          parsedJson["data"]["Package"]["Modules"].map((item) {
            tempId.add(item["ID"]);
          }).toList();
        }

        Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
        final SharedPreferences prefs = await _prefs;
        await prefs.setStringList("modules", tempId);
      }

      return company;
    } else {
      return null;
    }
  }
}
final companyApi = CompanyApi();