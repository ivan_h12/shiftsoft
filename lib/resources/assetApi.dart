import 'dart:convert';
import 'package:shiftsoft/models/masset.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/models/massetrequest.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/assetMock.dart';
import 'dart:io';

class AssetApi {
  Client client = Client();

  Future<List<Asset>> getAssetList(final context) async {
     if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Asset> assetList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("asset/list", context:context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          assetList.add(Asset.fromJson(item));
        }).toList();
      }

      return assetList;
    } else {
      return null;
    }
  }

  Future<Asset> getAssets(final context, int id) async {
    Asset asset;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("asset/$id-id", context:context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          asset = Asset.fromJson(parsedJson["data"]);
      }

      return asset;
    } else {
      return null;
    }
  }

  Future<List<AssetRequest>> getAssetRequestDetail(final context, int id, int page) async{
    List<AssetRequest> assetRequestDetail = [];
    String token = await getToken();

    final response = await client.get(
      APIUrl("assetrequest/list/$page", context:context, parameter: "&with[0]=Asset&with[1]=User.Circles&with[2]=Company&order=start_at-asc&filters[]=asset_id|=|$id|and|god&inactive=true&filters[]=status|<>|-1|and"),
      headers: {HttpHeaders.authorizationHeader: "Basic $token"}
    );

    if(token != "no token found, please login again to get new tokens"){
      final parsedJson = jsonDecode(response.body);

      if(parsedJson["success"] != false){
        parsedJson["data"].map((item){
          AssetRequest tempAssetRequestDetail = AssetRequest.fromJson(item);

          tempAssetRequestDetail.user = User.fromJson(item["User"]);
          tempAssetRequestDetail.asset = Asset.fromJson(item["Asset"]);

          assetRequestDetail.add(tempAssetRequestDetail);
        }).toList();
      }
      return assetRequestDetail;
    } else {
      return null;
    }
  }

  Future<bool> approveAssetRequest(final context, String idEncrypt) async{
    String token = await getToken();

    final response = await client.patch(
      APIUrl("assetrequest/$idEncrypt", context:context, parameter: "GodMode=true"),
      headers: {HttpHeaders.authorizationHeader: "Basic $token"}
    );

    final parsedJson = jsonDecode(response.body);

    if(parsedJson["success"] == 1){
      return true;
    }
    
    return false;
  }

  Future<bool> disapproveAssetRequest(final context, String idEncrypt) async{
    String token = await getToken();

    final response = await client.patch(
      APIUrl("disapproveassetrequest/$idEncrypt", context:context, parameter: "GodMode=true"),
      headers: {HttpHeaders.authorizationHeader: "Basic $token"}
    );

    final parsedJson = jsonDecode(response.body);

    if(parsedJson["success"] == 1){
      return true;
    }
    
    return false;
  }

  Future<bool> deleteAssetRequest(final context, String idEncrypt) async{
    String token = await getToken();

    final response = await client.delete(
      APIUrl("assetrequest/$idEncrypt", context:context, parameter: "GodMode=true"),
      headers: {HttpHeaders.authorizationHeader: "Basic $token"}
    );

    final parsedJson = jsonDecode(response.body);

    if(parsedJson["success"] == 1){
      return true;
    }
    
    return false;
  }

  Future<List<AssetRequest>> getAssetCalendar(final context, int id) async{
    List<AssetRequest> assetCalendar = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("assetrequest/list", context:context, parameter: "filters[]=asset_id|=|$id|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          //assetCalendar.add(AssetRequest.fromJson(item));

          AssetRequest tempAssetRequestDetail = AssetRequest.fromJson(item);

          //tempAssetRequestDetail.user = User.fromJson(item["User"]);
          tempAssetRequestDetail.asset = Asset.fromJson(item["Asset"]);

          assetCalendar.add(tempAssetRequestDetail);
        }).toList();
      }

      return assetCalendar;
    } else {
      return null;
    }
  }

  Future<Result> createAssetRequest(final context, String place, String content, int assetid, int userid, String startAt, String endAt, String action, String idEncrypt) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "place" : "$place",
        "content" : "$content",
        "assetid" : assetid,
        "userid" : userid,
        "StartAt" : "$startAt",
        "EndAt" : "$endAt",
        "action" : "$action",
        "assetrequestidencrypt":"$idEncrypt"
      });
      
      final response = await client.post(
        APIUrl("assetrequest", context:context),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: body
      );
      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
      return result;
    } else {
      return result;
    }
  }
}
final assetApi = AssetApi();