import 'dart:convert';
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/models/mproductreview.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class ProductApi {
  Client client = Client();

  Future<List<Product>> getProductList(final context, int index,int page, [String filter = ""]) async {
    List<Product> productList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      var response;
      if(filter != ""){
        response = await client.get(
        APIUrl("product/list", context: context, parameter: "&filters[]=category_id|=|$index|and|god&$filter"),
          headers: {HttpHeaders.authorizationHeader: "Basic $token"}
        );
      }else{
        response = await client.get(
        APIUrl("product/list/$page", context: context, parameter: "&filters[]=category_id|=|$index|and|god&$filter"),
          headers: {HttpHeaders.authorizationHeader: "Basic $token"}
        );
      }
      
      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productList.add(Product.fromJson(item));
        }).toList();
      }

      return productList;
    }else {
      return null;
    }
  }

  Future<List<Product>> getProductSearchList(final context, int index, [String filter = ""]) async {
    List<Product> productList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
      APIUrl("product/list", context: context, parameter: "&filters[]=category_id|=|$index|and|god&$filter"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );
      print("product/list&filters[]=category_id|=|$index|and|god&$filter");
      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productList.add(Product.fromJson(item));
        }).toList();
      }

      return productList;
    }else {
      return null;
    }
  }

  Future<Product> getProduct(final context, int id) async {
    Product product;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("product/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          product = Product.fromJson(parsedJson["data"]);
      }

      return product;
    }else {
      return null;
    }
  }

  
  Future<String> addWishlist(final context, int productId, int userId) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("productwishlist", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "ProductID": "$productId",
          "UserID": "$userId",
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];
    }else {
      return null;
    }    
  }

  Future<String> addCart(final context, int productId, int userId, int quantity, String description ) async {
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("productcart", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "ProductID": "$productId",
          "UserID": "$userId",
          "Quantity": quantity,
          "Description": "$description",
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];
    } else {
      return null;
    }
  }

  Future<bool> checkWishList(final context, int productId, int userId) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productwishlist/list", context: context, parameter: "&filters[]=product_id|=|$productId|and|god&filters[]=user_id|=|$userId|and|god&with[0]=Product"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      ); 

      final parsedJson = jsonDecode(response.body);
      if (parsedJson["success"] == 1) {
          return true;
      }
      else{
        return false;
      }
    } else {
      return null;
    }
  }

  Future<bool> checkCart(final context, int productId, int userId) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcart/list", context: context, parameter: "&filters[]=product_id|=|$productId|and|god&filters[]=user_id|=|$userId|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      if (parsedJson["success"] == 1) {
          return true;
      }
      else{
        return false;
      }
    } else {
      return null;
    }
  }

  Future <List<ProductReview>> getProductReviewList(final context, int userId, int productId) async{
    List<ProductReview> productReviewList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productreview/list", context: context, parameter: "&filters[]=user_id|=|$userId|and|god&filters[]=product_id|=|$productId|and|god&with[0]=Product&with[1]=User"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productReviewList.add(ProductReview.fromJSON(item));
        }).toList();
      }

      return productReviewList;
    }else {
      return null;
    }
  } 

  Future<String> addReview(final context, int productId, int userId, double rating, String description, String image, String imageName) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("productreview", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "ProductID": "$productId",
          "UserID": "$userId",
          "Rating": rating,
          "Description": "$description",
          "Pic": "$image",
          "PicName": "$imageName",
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];
    }else {
      return null;
    }    
  }

  Future<String> removeWishlist(final context, int wishlistId) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.delete(
        APIUrl("productwishlist/$wishlistId", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];    
    } else {
      return null;
    }
  }
}

final productApi = ProductApi();