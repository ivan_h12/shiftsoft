import 'dart:convert';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import '../resources/mock/downloadMock.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';
import 'package:shiftsoft/models/mdownload.dart';

class DownloadApi {
  Client client = Client();

  Future<List<Download>> getDownload(final context, int page, {parameter = ""}) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Download> downloadList = [];
    String token = await getToken();

    if(token != 'no token found, please login again to get new tokens'){
      final response = await client.get(APIUrl("download/list", context:context, parameter:parameter));
      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          downloadList.add(Download.fromJson(item));
        }).toList();
      }
      return downloadList;
    } else {
      return null;
    }
  }
}

final downloadApi = DownloadApi();