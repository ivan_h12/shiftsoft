import 'dart:convert';
import 'dart:io';
import 'package:shiftsoft/models/mcategory.dart';
import 'package:shiftsoft/models/masset.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;

class CategoryApi {
  Client client = Client();

  Future<List<Category>> getCategoryListWithAsset(final context) async {
    List<Category> category =[];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("assetcategory/list", context: context, parameter: "with[0]=Assets"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          Category tempCategory = Category.fromJson(item);
          List<Asset> tempAssetList = [];
          item["Assets"].map((item) {
            tempAssetList.add(Asset.fromJson(item));
          }).toList();
          tempCategory.assetList = tempAssetList;
          category.add(tempCategory);
        }).toList();
      }
    
      return category;
    } else {
      return null;
    }
  }
}

final categoryApi = CategoryApi();