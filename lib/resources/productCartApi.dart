import 'dart:convert';
import 'package:shiftsoft/models/mproductcart.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class ProductCartApi {
  Client client = Client();

  Future<List<ProductCart>> getProductCartList(final context, int index) async {
    List<ProductCart> productCartList = [];
    
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcart/list", context: context, parameter: "&filters[]=user_id|=|1926|and|god&with[0]=Product"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productCartList.add(ProductCart.fromJson(item));
        }).toList();
      }

      return productCartList;
    } else {
      return null;
    }
  }

  Future<ProductCart> getProductCart(final context, int id) async {
    String token = await getToken();
    ProductCart productCart;

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcart/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        productCart = ProductCart.fromJson(parsedJson["data"]);
      }

      return productCart;
    }else {
      return null;
    }
  }

  Future<Result> checkoutCart(final context, int userId) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("productmastertransaction", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "UserID": "$userId",
        })
      );
      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
      result.message = parsedJson["message"] + "|" + parsedJson["data"]["ID"].toString();
    }

    return result;
  }

  Future<Result> checkProductCartQuantity(final context, String productCartID, int quantity) async {
    String token = await getToken();
    Result result;

    if (token != "no token found, please login again to get new tokens") {
      final response = await client.patch(
        APIUrl("ProductCartQuantity/$productCartID", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "Quantity":quantity,
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      result = Result.fromJson(parsedJson);
    }
    return result;
  }

  Future<Result> deleteCart(final context, int productCartID) async {
    String token = await getToken();
    Result result;
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.delete(
        APIUrl("ProductCart/$productCartID", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
      );
      var parsedJson = jsonDecode(response.body);
      
      result = Result.fromJson(parsedJson);
    }
    return result;
  }
}
final productCartApi = ProductCartApi();