import 'dart:convert';
import 'package:shiftsoft/models/mcircle.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/circleMock.dart';
import 'dart:io';

class CircleApi {
  Client client = Client();

  Future<List<Circle>> getCircleListByUser(final context, int userId) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<Circle> circle =[];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("user/$userId-id", context:context, parameter: "with[0]=Circles.Members&with[1]=Circles.Childs"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"]["Circles"].map((item) {
          circle.add(Circle.fromJson(item));
        }).toList();
      }
    
      return circle;
    } else {
      return null;
    }
  }

Future<List<Circle>> getCircleListByParentId(final context, int circleId) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<Circle> circle =[];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("circle/list", context:context, parameter: "&filters[]=parent_id|=|$circleId|and|god&with[0]=Members"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          circle.add(Circle.fromJson(item));
        }).toList();
      }
    
      return circle;
    } else {
      return null;
    }
  }

  Future<Result> updateCircle(final context, int id, String circleName, String circlePlace, String circleSchedule, String gps) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("circle/$id", context:context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "name" : "$circleName",
          "place" : "$circlePlace",
          "gps" : "$gps",
          "schedule" : "$circleSchedule"
        })
      );
      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
    }

    return result;
  }

  Future<String> changeCircleImage(final context, int id, String image, String imageName) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("changeCircleImage/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "Pic": "$image",
          "PicName": "$imageName",
        })
      );

      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }
}

final circleApi = CircleApi();