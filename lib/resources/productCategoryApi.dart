import 'dart:convert';
import 'package:shiftsoft/models/mproductcategory.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/productCategoryMock.dart';
import 'dart:io';

class ProductCategoryApi{
  Client client = Client();
  
  Future<List<ProductCategory>> getProductCategoryList(final context, int page) async {
    if(c.testing){
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<ProductCategory> productCategoryList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcategory/list/$page", context: context, parameter: "&filters[]=parent_id|=|0|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productCategoryList.add(ProductCategory.fromJSON(item));
        }).toList();
      }

      return productCategoryList;
    }else{
      return null;
    }
  }

  Future<List<ProductCategory>> getProductCategoryDetailList(final context, int index, int page) async {
    if(c.testing){
      client = MockClient((request) async {
        return Response(json.encode(mockDetail), 200);
      });
    }
    List<ProductCategory> productCategoryList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcategory/list/$page", context: context, parameter: "&filters[]=parent_id|=|$index|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productCategoryList.add(ProductCategory.fromJSON(item));
        }).toList();
      }

      return productCategoryList;
    }else {
      return null;
    }
  }

}
final productCategoryApi = ProductCategoryApi();