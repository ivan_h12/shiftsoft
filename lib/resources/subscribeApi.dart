import 'dart:convert';
import 'package:shiftsoft/models/msubscribe.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/subscribeMock.dart';
import 'dart:io';

class SubscribeApi {
  Client client = Client();

  Future<String> addSubscribe(final context, String paidAt, String period, String userId, int value, String desc, String pic, String picname) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("subscription", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "PaidAt": "$paidAt",
          "Period": "$period",
          "UserID": "$userId",
          "Value": value,
          "Description": "$desc",
          "Pic": "$pic",
          "PicName": "$picname",
        })
      );
      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }

  Future<List<Subscribe>> getSubscribeList(final context, int page,[String filter=""]) async {
    if(c.testing){
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Subscribe> subscribeList = [];
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("subscription/list/$page", context: context, parameter:"$filter"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          subscribeList.add(Subscribe.fromJson(item));
        }).toList();
      }

      return subscribeList;
    } else {
      return null;
    }
  }

  Future<Subscribe> getSubscribe(final context, int id) async {
    Subscribe subscribe;
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("subscription/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          subscribe = Subscribe.fromJson(parsedJson["data"]);
      }

      return subscribe;
    } else {
      return null;
    }
  }
}

final subscribeApi = SubscribeApi();