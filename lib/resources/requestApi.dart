import 'package:flutter/material.dart';

import 'dart:convert';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import '../resources/mock/requestMock.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';
import 'package:shiftsoft/models/mrequest.dart';
import 'package:shiftsoft/models/mrequestType.dart';
import 'package:shiftsoft/models/mresult.dart';

class RequestApi {
  Client client = Client();

  Future<List<Request>> getRequestList(final context, int page, {parameter = ""}) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Request> requestList = [];
    String token = await getToken();

    if(token != 'no token found, please login again to get new tokens'){
      final response = await client.get(APIUrl("visitationrequest/list", context:context, parameter:parameter));
      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          Request tempRequest = Request.fromJson(item);
          tempRequest.requestType = RequestType.fromJson(item["Visitation"]);
          requestList.add(tempRequest);
        }).toList();
      }
      return requestList;
    } else {
      return null;
    }
  }

  Future<Result> create(final context, String userId, String visitationId, String name, String content, String pic, String picName) async {
    String token = await getToken();
    Result tempResult;
    
    
    debugPrint(picName);

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "UserID" : "$userId",
        "VisitationID" : "$visitationId",
        "Name" : "$name",
        "Content" : "$content",
        "Pic" : "$pic",
        "PicName" : "$picName",
      });

      print(body);

      final response = await client.post(
        APIUrl("visitationrequest", context: context, parameter: "GodMode=true"),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"  
        },
        body: body
      );
      var parsedJson = jsonDecode(response.body);
      tempResult = Result.fromJson(parsedJson);
      return tempResult;
    } else {
      return tempResult;
    }
  }
}

final requestApi = RequestApi();