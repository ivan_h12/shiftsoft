import 'dart:convert';
import 'package:shiftsoft/models/meventservices.dart';
import 'package:shiftsoft/models/meventservicestype.dart';
import 'package:shiftsoft/models/meventservicesskill.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class EventServicesApi {
  Client client = Client();

  Future<List<EventService>> getEventServicesList(final context, int id) async {
    List<EventService> eventServiceList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("eventservice/list", context: context, parameter: "&eventID=$id&with[0]=User"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] == 1) {
        parsedJson["data"].map((item) {
          EventService tempEventService = EventService.fromJson(item);
          tempEventService.user = User.fromJson(item["User"]);
          eventServiceList.add(tempEventService);
        }).toList();
      }

      return eventServiceList;
    } else {
      return null;
    }
  }

  Future<List<EventServiceType>> getEventServiceTypeList(final context) async {
    List<EventServiceType> eventServiceTypeList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("service/list", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      if(parsedJson["success"] == 1){
        parsedJson["data"].map((item) {
          EventServiceType tempEventServiceType = EventServiceType.fromJson(item);
          eventServiceTypeList.add(tempEventServiceType);
        }).toList();
      }
      return eventServiceTypeList;
    } else {
      return null;
    }
  }


  Future<List<EventServiceSkill>> getEventServiceSkillList(final context, String id) async {
    List<EventServiceSkill> eventServiceSkillList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("service/list", context: context, parameter: "&with[0]=Skills&filters[]=id|=|$id|and|god", print: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      if(parsedJson["success"] == 1) {
        if (parsedJson["data"][0]["Skills"] != null) {
          parsedJson["data"][0]["Skills"].map((item) {
            EventServiceSkill tempEventServiceSkill = EventServiceSkill.fromJson(item);
            eventServiceSkillList.add(tempEventServiceSkill);
          }).toList();
        }
      }
      return eventServiceSkillList;
    } else {
      return null;
    }
  }

  Future<List<User>> getEventServiceSkillUserList(final context, String skillId) async {
    List<User> eventServiceSkillList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("skill/$skillId-id", context: context, parameter: "&with[0]=Users&filters[]=id|=|$skillId|and|god", print: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      if(parsedJson["success"] == 1){
        if (parsedJson["data"]["Users"] != null) {
          parsedJson["data"]["Users"].map((item) {
            User tempEventServiceSkill = User.fromJson(item);
            eventServiceSkillList.add(tempEventServiceSkill);
          }).toList();
        }
      }
      return eventServiceSkillList;
    } else {
      return null;
    }
  }

  Future<Result> createEventService(final context, String eventId, String userId, String skillId) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "EventID" : "$eventId",
        "UserID" : "$userId",
        "SkillID" : "$skillId",
      });
      
      final response = await client.post(
        APIUrl("eventservice", context: context, parameter: "GodMode=true"),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: body
      );
      
      var parsedJson = jsonDecode(response.body);
      
      result = Result.fromJson(parsedJson);
    }
    return result;
  }

  Future<String> removeEventService(final context, int id) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.delete(
        APIUrl("eventservice/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];
    } else {
      return null;
    }
  }
}

final eventServicesApi = EventServicesApi();