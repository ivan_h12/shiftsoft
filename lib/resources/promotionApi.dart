import 'dart:convert';
import 'package:shiftsoft/models/mpromotion.dart';
import 'package:shiftsoft/tools/functions.dart';
import '../resources/mock/promotionMock.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';

class PromotionApi {
  Client client = Client();

  Future<List<Promotion>> getPromotionList(final context, int page) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    
    String token = await getToken();;
    List<Promotion> promotionList = [];

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("promotion/list/$page", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          promotionList.add(Promotion.fromJson(item));
        }).toList();
      }

      return promotionList;
    } else {
      return null;
    }
  }

  Future<Promotion> getPromotion(final context, int id) async {
    Promotion promotion;
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    String token = await getToken();;

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("promotion/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          promotion = Promotion.fromJson(parsedJson["data"]);
      }

      return promotion;
    } else {
      return null;
    }
  }
}

final promotionApi = PromotionApi();