import 'dart:convert';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import '../resources/mock/requestTypeMock.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';
import 'package:shiftsoft/models/mrequestType.dart';

class RequestTypeApi {
  Client client = Client();

  Future<List<RequestType>> getRequestTypeList(final context, int page, {parameter = ""}) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<RequestType> requestTypeList = [];
    String token = await getToken();

    if(token != 'no token found, please login again to get new tokens'){
      final response = await client.get(APIUrl("visitation/list", context:context, parameter:parameter));
      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map(
          (item) {
          requestTypeList.add(RequestType.fromJson(item));
        }).toList();
      }
      return requestTypeList;
    } else {
      return null;
    }
  }
}

final requestTypeApi = RequestTypeApi();