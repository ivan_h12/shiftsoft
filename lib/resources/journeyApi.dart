import 'dart:convert';
import 'package:shiftsoft/models/mjourney.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/journeyMock.dart';
import 'dart:io';

class JourneyApi{
  Client client = Client();

  Future<List<Journey>> getJourneyList(final context, int idUser, int page) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    
    List<Journey> journeyList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("levelhistories/list/$page", context: context, parameter: "&order=created_at-desc&filters[]=user_id|=|$idUser|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          journeyList.add(Journey.fromJson(item));
        }).toList();
      }

      return journeyList;
    } else {
      return null;
    }
  }

  Future<String> addJourney(final context, String userId, String journeyName , String startAt, String journeyContent) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("levelhistories", context: context),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "UserID" : "$userId",
          "Name" : "$journeyName",
          "StartAt" : "$startAt",
          "Description" : "$journeyContent"
        })
      );
      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }

}

final journeyApi = JourneyApi();