import 'dart:convert';
import 'package:shiftsoft/models/mproductcomment.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class ProductCommentApi {
  Client client = Client();

  Future<List<ProductComment>> getProductCommentList(final context, int productId) async {
    List<ProductComment> productCommentList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcomment/list", context: context, parameter:"&filters[]=product_id|=|$productId|and|god&with[0]=User&with[1]=Product"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          productCommentList.add(ProductComment.fromJson(item));
        }).toList();
      }

      return productCommentList;
    }else {
      return null;
    }
  }

  Future<ProductComment> getProductComment(final context, int id) async {
    ProductComment productComment;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productcomment/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          productComment = ProductComment.fromJson(parsedJson["data"]);
      }

      return productComment;
    }else {
      return null;
    }
  }

  
  Future<String> addComment(final context, int productId, int userId, String description) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("productcomment", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "ProductID" : "$productId",
          "UserID" : "$userId",
          "Description" : "$description",
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];    
    } else {
      return null;
    }
  }

  Future<bool> checkComment(final context, int productId, int userId) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productwishlist/list", context: context, parameter: "&filters[]=product_id|=|$productId|and|god&filters[]=user_id|=|$userId|and|god&with[0]=Product"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      if (parsedJson["success"] == 1) {
          return true;
      }
      else{
        return false;
      }
    } else {
      return null;
    }
  }
}

final productCommentApi = ProductCommentApi();