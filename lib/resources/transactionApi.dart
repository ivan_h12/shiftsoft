import 'dart:convert';
import 'package:shiftsoft/models/mdetailtransaction.dart';
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/models/mtransaction.dart';
import 'package:shiftsoft/models/mtransactionhistory.dart';
import 'package:shiftsoft/tools/functions.dart';
import '../resources/mock/transactionMock.dart';
import '../resources/mock/transactionDetailMock.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';

class TransactionApi {
  Client client = Client();

  Future<List<Transaction>> getTransactionList(final context, int userId, int page) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Transaction> transactionList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productmastertransaction/list/$page", context: context, parameter:"&filters[]=user_id|=|$userId|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          transactionList.add(Transaction.fromJson(item));
        }).toList();
      }

      return transactionList;
    } else {
      return null;
    }
  }

  Future<List<TransactionHistory>> getTransactionHistory(final context, int transactionId) async {
    List<TransactionHistory> transactionHistory = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("ProductTransactionHistory/list", context: context, parameter:"&filters[]=product_master_transaction_id|=|$transactionId|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          transactionHistory.add(TransactionHistory.fromJson(item));
        }).toList();
      }

      return transactionHistory;
    } else {
      return null;
    }
  }

  Future<List<DetailTransaction>> getTransactionDetailList(final context, int transactionId) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mocks), 200);
      });
    }
    List<DetailTransaction> detailTransactionList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      
      final response = await client.get(
        APIUrl("productmastertransaction/list", context: context, parameter:"&with[0]=DetailTransaction.Product&filters[]=id|=|$transactionId|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );
      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] == 1) {
        parsedJson["data"][0]["DetailTransaction"].map((item) {
          DetailTransaction tempDetailTransactionDetail;
          tempDetailTransactionDetail = DetailTransaction.fromJson(item);
          tempDetailTransactionDetail.product = Product.fromJson(item["Product"]);
          detailTransactionList.add(tempDetailTransactionDetail);
        }).toList();
      }
      return detailTransactionList;
    } else {
      return null;
    }
  }

  Future<Transaction> getTransaction(final context, int id) async {
    Transaction transaction;
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productmastertransaction/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          transaction = Transaction.fromJson(parsedJson["data"]);
      }

      return transaction;
    } else {
      return null;
    }
  }

  Future<String> uploadCofirmationImage(final context, int id, String image, String imageName) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens") {
      final response = await client.patch(
        APIUrl("productmastertransactionphoto/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "Pic": "$image",
          "PicName": "$imageName",
        })
      );

      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }
}

final transactionApi = TransactionApi();