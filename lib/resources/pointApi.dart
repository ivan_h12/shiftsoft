// packages
import 'dart:convert';
import 'package:shiftsoft/models/mpoint.dart';
import 'package:shiftsoft/models/mbalance.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'dart:io';

// resources
import 'mock/pointMock.dart';
import 'mock/balanceMock.dart' as balance;

// tools
import 'package:shiftsoft/settings/configuration.dart';

class PointApi{
  Client client = Client();

  Future<PointHeader> getPointList(final context, int userId, int page, [String filter=""]) async {
    if(c.testing){
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    PointHeader mPointHeader;
    
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("point/list/$page", context: context, parameter:"&user=$userId&$filter", godMode: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        PointHeader tempPointHeader = PointHeader.fromJson(parsedJson);
        List<Point> tempPointList = [];
        parsedJson["data"].map((item) {
          Point tempPoint;
          tempPoint = Point.fromJson(item);
          tempPointList.add(tempPoint);
        }).toList();
        mPointHeader = tempPointHeader;
        mPointHeader.pointList = tempPointList;
      }
      return mPointHeader;
    }else {
      return null;
    }
    
  }

  Future<List<Balance>> getBalanceList(final context, int userId,int page, [String filter=""]) async {
    if(c.testing){
      client = MockClient((request) async {
        return Response(json.encode(balance.mock), 200);
      });
    }
    List<Balance> balanceList = [];
    
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("balance/list/$page", context: context, parameter: "&filters[]=user_Id|=|$userId|and|god&$filter"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          balanceList.add(Balance.fromJson(item));
        }).toList();
      }
      return balanceList;
    } else {
      return null;
    }
  }
}
final pointApi = PointApi();