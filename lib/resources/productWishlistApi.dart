import 'dart:convert';
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/models/mproductwishlist.dart';
import 'mock/productWishlistMock.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';

class ProductWishlistApi{
  Client client = Client();
  
  Future<List<Productwishlist>> getProductWishlistList(final context) async {
    // if (c.testing) {
    //   client = MockClient((request) async {
    //     return Response(json.encode(mock), 200);
    //   });
    // }
    List<Productwishlist> productWishlistList = [];
    String token = await getToken();;

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("productwishlist/list", context: context, parameter:"&filters[]=user_id|=|1926|and|god&with[0]=Product"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          Productwishlist tempProductWishlist;
          tempProductWishlist = Productwishlist.fromJSON(item);
          tempProductWishlist.product = Product.fromJson(item["Product"]);
          productWishlistList.add(tempProductWishlist);
        }).toList();
      }

      return productWishlistList;
    } else {
      return null;
    }
  }

  Future<Productwishlist> getProductWishlist(final context, int productId, int userId) async {
    // if (c.testing) {
    //   client = MockClient((request) async {
    //     return Response(json.encode(mock), 200);
    //   });
    // }
    Productwishlist productWishlist;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
      APIUrl("productwishlist/list", context: context, parameter:"&filters[]=product_id|=|$productId|and|god&filters[]=user_id|=|$userId|and|god&with[0]=Product"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          Productwishlist tempProductWishlist;
          tempProductWishlist = Productwishlist.fromJSON(item);
          tempProductWishlist.product = Product.fromJson(item["Product"]);
          productWishlist = tempProductWishlist;
        }).toList();
        // productWishlist = Productwishlist.fromJSON(parsedJson["data"]);
        // productWishlist.product = Product.fromJson(parsedJson["Product"]);
      }
      return productWishlist;
    } else {
      return null;
    }
  }
}
final productWishlistApi = ProductWishlistApi();