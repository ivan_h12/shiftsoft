import 'dart:convert';
import 'package:shiftsoft/models/mshout.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class ShoutApi {
  Client client = Client();

  Future<String> updateStatus(final context, int id) async {
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("shout/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "status": 3,
        })
      );

      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    }else {
       return null;
    }
  }

  Future<List<Shout>> getShoutList(final context, int userId) async {
    String token = await getToken();
    List<Shout> shoutList = [];

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("shout/list", context: context, parameter:"&filters[]=user_id|=|$userId|and|god&order=created_at-desc"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          shoutList.add(Shout.fromJson(item));
        }).toList();
      }

      return shoutList;
    } else {
      return null;
    }
  }

  Future<Shout> getShout(final context, int id) async {
    Shout shout;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("shout/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          shout = Shout.fromJson(parsedJson["data"]);
      }

      return shout;
    } else {
      return null;
    }
  }
}

final shoutApi = ShoutApi();