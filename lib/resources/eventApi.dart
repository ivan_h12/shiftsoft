import 'dart:convert';
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/eventMock.dart';
import 'dart:io';

class EventApi {
  Client client = Client();

  Future<List<Event>> getEventList(final context, int page) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<Event> eventList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("event/list/$page", context: context, parameter: "order[]=start_at-desc"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          eventList.add(Event.fromJson(item));
        }).toList();
      }

      return eventList;
    } else {
      return null;
    }
  }

  Future<List<Event>> getEventHistoryList(final context, int userId, int page) async {
    List<Event> eventHistoryList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("eventhistories/list/$page",context: context, parameter:"&userId=$userId"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          eventHistoryList.add(Event.fromJson(item));
        }).toList();
      }

      return eventHistoryList;
    } else {
      return null;
    }
  }

  Future<Event> getEvents(final context, int id) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mockdetail), 200);
      });
    }

    Event event;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("event/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          event = Event.fromJson(parsedJson["data"]);
      }

      return event;
    } else {
      return null;
    }
  }

  Future<Result> addEvent(final context, String name, String place, String content, String idCircle, String startAt, String endAt, String pic) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "name" : "$name",
        "place" : "$place",
        "content" : "$content",
        "CircleID" : "$idCircle",
        "StartAt" : "$startAt",
        "EndAt" : "$endAt",
        "Pic" : "$pic",
        "PicName" : "test.jpg"
      });

      final response = await client.post(
        APIUrl("event", context: context, parameter: "GodMode=true"),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"  
        },
        body: body
      );

      var parsedJson = jsonDecode(response.body);

      result = Result.fromJson(parsedJson);

    }
    return result;
  }

  Future<Result> updateEvent(final context, String idEncrypt, String name, String place, String content, String idCircle, String startAt, String endAt) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "Name" : "$name",
        "Place" : "$place",
        "Content" : "$content",
        "CircleID" : "$idCircle",
        "StartAt" : "$startAt",
        "EndAt" : "$endAt"
      });

      final response = await client.patch(
        APIUrl("event/$idEncrypt", context: context, parameter: "GodMode=true"),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: body
      );
      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
    }
    return result;
  }

  Future<Result> submitEvent(final context, String idEncrypt, String name, String place, String content, int offering, int envelope, String gps, int status, String startAt, String endAt) async {
    String token = await getToken();
    Result result;
    
    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "Name" : "$name",
        "Place" : "$place",
        "Content" : "$content",
        "Offering" : "$offering",
        "Envelope" : "$envelope",
        "gps" : "$gps",
        "Status" : "$status",
        "StartAt" : "$startAt",
        "EndAt" : "$endAt"
      });

      final response = await client.patch(
        APIUrl("event/$idEncrypt", context: context),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: body
      );
      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
    }
    return result;
  }

  Future<String> changeEventImage(final context, int id, String image, String imageName) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("changeEventImage/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "Pic": "$image",
          "PicName": "$imageName",
        })
      );

      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }
}

final eventApi = EventApi();