import 'dart:convert';
import 'package:shiftsoft/models/mnews.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/models/mcomment.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/newsMock.dart';
import 'mock/commentMock.dart';
import 'dart:io';

class NewsApi {
  Client client = Client();

  Future<List<News>> getNewsList(final context, int page) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<News> newsList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("notification/list/$page", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          newsList.add(News.fromJson(item));
        }).toList();
      }

      return newsList;
    } else {
      return null;
    }
  }

  Future<List<News>> getNewsSearchList(final context, String search) async {
    List<News> newsList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("notification/list", context: context, parameter: "filter=subject&key=*$search*"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          newsList.add(News.fromJson(item));
        }).toList();
      }

      return newsList;
    } else {
      return null;
    }
  }

  Future<News> getNews(final context, int id) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock2), 200);
      });
    }

    News news;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("notification/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          news = News.fromJson(parsedJson["data"]);
      }

      return news;
    } else {
      return null;
    }
  }

  Future<List<Comment>> getCommentList(final context, int id) async{
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(commentmock), 200);
      });
    }

    List<Comment> commentsList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("notificationcomment/list", context: context, parameter: "&filter=notification_id&key=$id&order[0]=created_at-desc&with[0]=User"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );
      final parsedJson = jsonDecode(response.body);
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          Comment tempComment = Comment.fromJson(item);
          tempComment.user = User.fromJson(item["User"]);

          commentsList.add(tempComment);
        }).toList();
      }
      return commentsList;
    } else {
      return null;
    }
  }

  Future<String> createComment(final context, String newsId, String userId, String content) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("notificationcomment", context: context, godMode: true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "NotificationID" : "$newsId",
          "UserID" : "$userId",
          "Content" : "$content",
        })
      );

      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return "no token";
    }
  }

  Future<List<News>> getTagsList(final context) async {
    List<News> newsList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("tags/list", context: context, print: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          newsList.add(News.fromJson(item));
        }).toList();
      }

      return newsList;
    } else {
      return null;
    }
  }

  Future<List<News>> getTagsDetailList(final context, String search) async {
    List<News> newsList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("tags/list", context: context, parameter: "with[0]=Notifications&filters[]=content|=|$search|and"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        if(parsedJson["data"][0]["Notifications"] != null){
          parsedJson["data"][0]["Notifications"].map((item) {
            newsList.add(News.fromJson(item));
          }).toList();
        }
      }

      return newsList;
    } else {
      return null;
    }
  }

  Future<int> getLike(final context, String id) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("notificationlike/count", parameter: "filters[]=notification_id|=|$id|and|god", context: context, print: true),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      return parsedJson;
    } else {
      return null;
    }
  }

  Future<Result> createLike(final context, String newsId, String userId) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("notificationlike", context: context, godMode: true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "NotificationID" : "$newsId",
          "UserID" : "$userId",
        })
      );

      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
      return result;
    } else {
      return result;
    }
  }

  Future<Result> createRecipient(final context, String newsId, String userId) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "NotificationID" : "$newsId",
        "UserID" : "$userId",
      });

      final response = await client.post(
        APIUrl("notificationRecipient", context: context, godMode: true, print: true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: body
      );

      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
      return result;
    } else {
      return result;
    }
  }
}

final newsApi = NewsApi();