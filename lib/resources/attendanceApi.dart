import 'dart:convert';
import 'package:shiftsoft/models/mattendance.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/attendanceMock.dart';
import 'dart:io';

class AttendanceApi {
  Client client = Client();

  Future<List<Attendance>> getAttendanceList(final context, int id) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<Attendance> attendanceList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("attendance/list", context: context, parameter: "with[0]=User&filters[]=event_id|=|$id|and|god"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
        attendanceList.add(Attendance.fromJson(item));
        }).toList();
      }

      return attendanceList;
    } else {
      return null;
    }
  }

  Future<Result> addAttendance(final context, String eventID, String userID, int status) async {
    String token = await getToken();
    Result result;

    if(token != "no token found, please login again to get new tokens"){
      final body = jsonEncode({
        "eventID" : "$eventID",
        "userID" : "$userID",
        "status" : status
      });

      final response = await client.post(
        APIUrl("attendance", context: context),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
          },
        body: body
      );
      var parsedJson = jsonDecode(response.body);
      result = Result.fromJson(parsedJson);
      
      return result;
    } else {
      return null;
    }
  }
}

final attendanceApi = AttendanceApi();