import 'dart:convert';
import 'package:shiftsoft/models/mdashboard.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class DashboardApi {
  Client client = Client();

  Future<List<MDashboard>> getDashboardList(final context) async {
    List<MDashboard> dashboardList = [];

    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("banner/list", useToken: false, context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          dashboardList.add(MDashboard.fromJson(item));
        }).toList();
      }

      return dashboardList;
    }else {
      return null;
    }
  }

  Future<MDashboard> getDashboard(final context, int id) async {
    MDashboard dashboard;

    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("banner/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          dashboard = MDashboard.fromJson(parsedJson["data"]);
      }

      return dashboard;
    } else {
      return null;
    }
  }
}

final dashboardApi = DashboardApi();