import 'dart:convert';
import 'dart:io';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shiftsoft/settings/configuration.dart';
Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

class UserApi {
  Client client = Client();

  Future<List<User>> getUserList(final context, String search) async {
    List<User> userList = [];
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("user/list", context: context, parameter:"guest=true&key=$search&with[0]=Circles"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          userList.add(User.fromJson(item));
        }).toList();
      }

      return userList;
    }else{
      return null;
    }
  }

  Future<User> getUser(final context, int id, {String parameter = ""}) async{
    User user;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("user/$id/id", context: context, parameter: parameter),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        user = User.fromJson(parsedJson["data"]);
        await saveRolePrivilege(parsedJson["data"]["Role"]["Privileges"] ?? []);
      }
      return user;
    } return null;
  }

  Future<String> addUser(final context, String email, String name, String password, String phone1, String address, String city) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("user", context: context),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"  
        },
        body: jsonEncode({
          "Email": "$email",
          "Name": "$name",
          "Password": "$password",
          "Phone1": "$phone1",
          "Address": "$address",
          "City": "$city" 
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];    
    } else{
      return null;
    }
  }

  Future<String> forgetPassword(final context, String email) async {
    final response = await client.post(
      APIUrl("forgetpassword", context: context),
      headers: {"Content-Type": "application/json",},
      body: jsonEncode({
        "Email": "$email",
      })
    );
    var parsedJson = jsonDecode(response.body);
    
    return parsedJson["message"];
  }

  Future<String> changePassword(final context, int userId,String oldPassword, String newPassword) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){    
      final response = await client.patch(
        APIUrl("changepassword/$userId", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json"},
        body: jsonEncode({
          "OldPassword": "$oldPassword",
          "Password": "$newPassword"
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];
    } else {
      return null;
    }
  }
  Future<String> editProfile(final context, int userId, String email, String name, String birthday, String address, String city, String phone) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("user/$userId", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"  
        },
        body: jsonEncode({
          "Email": "$email",
          "Name": "$name",
          "Birthday": "$birthday",
          "Address": "$address",
          "City": "$city",
          "Phone1": "$phone",
        })
      );
      var parsedJson = jsonDecode(response.body);
      
      if(parsedJson["success"] == 0){
        return parsedJson["message"];
      }else if(parsedJson["success"] == 1){
        return "Update Profile Succeed";
      }
    }else {
      return null;
    }
  }
  Future<String> changeProfileImage(final context, int id, String image, String imageName) async {
    String token = await getToken();
    
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("changeProfileImage/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "Pic": "$image",
          "PicName": "$imageName",
        })
      );

      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    }else{
      return null;
    }
  }
  
  Future<Result> login(final context, String playerId, String username, String password, int status) async {
    Result result;
    final response = await client.post(
      APIUrl("login", context: context, parameter:"with[0]=Role.Privileges"),
      headers: {"Content-Type": "application/json"},
      body: jsonEncode({
        "username":username,
        "password":password,
        "one_signal_player_id":"$playerId",
        "status":status
      })
    );

    var parsedJson = jsonDecode(response.body);
    result = Result.fromJson(parsedJson);

    if(result.success == 1){
      await saveRolePrivilege(parsedJson["data"]["Role"]["Privileges"] ?? []);

      final SharedPreferences prefs = await _prefs;
      await prefs.setString("token", parsedJson["message"]);

      Configuration config = Configuration.of(context);

      List<String> loginUserList = prefs.getStringList("loginUserList") ?? [];
      User user = User.fromJson(parsedJson["data"]);
      loginUserList.add(json.encode(user.toJson()));
      await prefs.setStringList("loginUserList", loginUserList);

      await saveToLocalStorage(user);
      config.user = user;
    }

    return result;
  }

  saveToLocalStorage(User user) async {
    final SharedPreferences prefs = await _prefs;
    await prefs.setString("user", json.encode(user.toJson()));
  }

  Future<User> loadFromLocalStorage() async {
    final SharedPreferences prefs = await _prefs;
    final tempString = prefs.getString("user") ?? "";
    User user;

    if (tempString != "") {
      user = User.fromJson(json.decode(tempString));
    }
    return user;
  }

  saveRolePrivilege(var privileges) async {
    if (privileges.length > 0) {
      List<String> tempId = [];
      privileges.map((item) {
        tempId.add(item["ID"].toString());
      }).toList();

      final SharedPreferences prefs = await _prefs;
      await prefs.setStringList("rolePrivileges", tempId);
    }
  }
}

final userApi = UserApi();