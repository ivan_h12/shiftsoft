import 'dart:convert';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/mbudget.dart';
import 'package:shiftsoft/models/mbudgetdetail.dart';
import 'mock/budgetListMock.dart';
import 'dart:io';

class BudgetApi {
  Client client = Client();

  Future<List<Budget>> getBudgetList(final context, int page) async{
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<Budget> budgetList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("budget/list/$page", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          budgetList.add(Budget.fromJson(item));
        }).toList();
      }

      return budgetList;
    } else {
      return null;
    }
  }

  Future<List<BudgetDetails>> getBudget(final context, int id, int type, int page) async{
    List<BudgetDetails> budgetDetails = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("budgetdetail/list/$page", context:context, parameter: "&budgetID=$id&type=$type&with[0]=Circle"),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );
      
      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
          parsedJson["data"].map((item) {
            budgetDetails.add(BudgetDetails.fromJson(item));
        }).toList();
      }
      return budgetDetails;
    } else {
      return null;
    }
  }
  

  Future<String> addBudgetSubmission(final context, String budgetId, String userId, String circleId, String name, int harga, int jumlah, int total, String content, String pic, String picName) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("budgetsubmission", context:context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "BudgetID" : "$budgetId",
          "UserID" : "$userId",
          "CircleID" : "$circleId",
          "Name" : "$name",
          "Price" : "$harga",
          "Quantity" : "$jumlah",
          "Total" : "$total",
          "Description" : "$content",
          "Pic" : "$pic",
          "PicName" : "$picName"
        })
      );
      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }

  Future<String> addBudgetRealization(final context, String budgetId, String userId, String circleId, String name, int harga, int jumlah, int total, int pajak, String content, String pic, String picName) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("budgetrealization", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "BudgetID" : "$budgetId",
          "UserID" : "$userId",
          "CircleID" : "$circleId",
          "Name" : "$name",
          "Price" : "$harga",
          "Quantity" : "$jumlah",
          "Tax" : "$pajak",
          "Total" : "$total",
          "Description" : "$content",
          "Pic" : "$pic",
          "PicName" : "$picName"
        })
      );
      var parsedJson = jsonDecode(response.body);
      return parsedJson["message"];
    } else {
      return null;
    }
  }

  Future<String> disapproveBudgetDetail(final context, int id) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("disapprovebudgetdetail/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];    
    } else {
      return null;
    }
  }

  Future<String> approveBudgetDetail(final context, int id) async {
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.patch(
        APIUrl("approvebudgetdetail/$id", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
      );
      var parsedJson = jsonDecode(response.body);
      
      return parsedJson["message"];    
    } else {
      return null;
    }
  }
}
final budgetApi = BudgetApi();