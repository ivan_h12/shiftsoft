import 'dart:convert';
import 'package:shiftsoft/models/mnearme.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart' show Client;
import 'dart:io';

class NearMeApi {
  Client client = Client();

  Future<List<Nearme>> getNearMeList(final context, int page) async {
    List<Nearme> nearMeList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("circle/list/$page", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          nearMeList.add(Nearme.fromJson(item));
        }).toList();
      }

      return nearMeList;
    } else {
      return null;
    }
  }
}

final nearMeApi = NearMeApi();