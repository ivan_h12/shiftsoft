import 'dart:convert';
import 'dart:io';
import 'package:shiftsoft/models/mbasicpage.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'mock/basicPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

class BasicPageApi {
  Client client = Client();

  Future<List<MBasicPage>> getBasicPageList(final context) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }

    List<MBasicPage> basicPageList = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString("token");

    final response = await client.get(
      APIUrl("page/list", context: context),
      headers: {HttpHeaders.authorizationHeader: "Basic $token"}
    );

    final parsedJson = jsonDecode(response.body);

    if (parsedJson["success"] != false) {
      parsedJson["data"].map((item) {
        basicPageList.add(MBasicPage.fromJson(item));
      }).toList();
    }

    return basicPageList;
  }

  Future<MBasicPage> getBasicPage(final context, int id) async {
    MBasicPage basicPage;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString("token");

    final response = await client.get(
      APIUrl("page/$id-id", context: context),
      headers: {HttpHeaders.authorizationHeader: "Basic $token"}
    );

    final parsedJson = jsonDecode(response.body);

    if (parsedJson["success"] != false) {
        basicPage = MBasicPage.fromJson(parsedJson["data"]);
    }

    return basicPage;
  }
}

final basicPageApi = BasicPageApi();