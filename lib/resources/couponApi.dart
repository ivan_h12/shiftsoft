import 'dart:convert';
import 'package:shiftsoft/models/mcoupon.dart';
import 'package:shiftsoft/tools/functions.dart';
import '../resources/mock/promotionMock.dart';
import 'package:http/testing.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:http/http.dart' show Client, Response;
import 'dart:io';

class CouponApi {
  Client client = Client();

  Future<List<Coupon>> getCouponList(final context, int page) async {
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    List<Coupon> couponList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("promotion/list/$page", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          couponList.add(Coupon.fromJson(item));
        }).toList();
      }

      return couponList;
    } else {
      return null;
    }
  }

  Future<Coupon> getCoupon(final context, int id) async {
    Coupon coupon;
    if (c.testing) {
      client = MockClient((request) async {
        return Response(json.encode(mock), 200);
      });
    }
    
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("promotion/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
        coupon = Coupon.fromJson(parsedJson["data"]);
      }

      return coupon;
    } else {
      return null;
    }
  }

  Future<String> validateCoupon(final context, int promotionId, int userId) async {
    String token = await getToken();
    
    final response = await client.post(
      APIUrl("promotionvalidate", context:context, godMode:true),
      headers: {
        "Content-Type": "application/json",
        HttpHeaders.authorizationHeader: "Basic $token"
      },
      body: jsonEncode({
        "promotionId": promotionId,
        "userId": userId,
      })
    );
    var parsedJson = jsonDecode(response.body);
    
    return parsedJson["message"];
  }

  Future<bool> checkCouponValidate(final context, int promotionId, int userId) async {
    String token = await getToken();
    if(token != "no token found, please login again to get new tokens"){
      final response = await client.post(
        APIUrl("promotioncheck", context: context, godMode:true),
        headers: {
          "Content-Type": "application/json",
          HttpHeaders.authorizationHeader: "Basic $token"
        },
        body: jsonEncode({
          "promotionId": promotionId,
          "userId": userId,
        })
      );
      var parsedJson = jsonDecode(response.body);

      if(parsedJson["success"] == 1){
        return true;
      } else {
        return false;
      }
    }else {
      return null;
    }
  }
}

final couponApi = CouponApi();