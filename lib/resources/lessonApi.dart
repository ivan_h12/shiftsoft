import 'dart:convert';
import 'package:shiftsoft/models/mlesson.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:http/http.dart';
import 'dart:io';

class LessonApi{
  Client client = Client();

  Future<List<Lesson>> getLessonList(final context, int page) async {
    List<Lesson> lessonList = [];
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("lesson/list/$page", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);
      
      if (parsedJson["success"] != false) {
        parsedJson["data"].map((item) {
          lessonList.add(Lesson.fromJson(item));
        }).toList();
      }

      return lessonList;
    } else {
      return null;
    }
  }

  Future<Lesson> getLesson(final context, int id) async {
    Lesson lesson;
    String token = await getToken();

    if(token != "no token found, please login again to get new tokens"){
      final response = await client.get(
        APIUrl("lesson/$id-id", context: context),
        headers: {HttpHeaders.authorizationHeader: "Basic $token"}
      );

      final parsedJson = jsonDecode(response.body);

      if (parsedJson["success"] != false) {
          lesson = Lesson.fromJson(parsedJson["data"]);
      }

      return lesson;
    } else {
      return null;
    }
  }
}

final lessonApi = LessonApi();