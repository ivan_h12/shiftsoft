import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DemoLocalizations {  
  DemoLocalizations(this.locale);  
  
  final Locale locale;  
  
  static DemoLocalizations of(BuildContext context) {  
    return Localizations.of<DemoLocalizations>(context, DemoLocalizations);  
  }  
  
  Map<String, String> _sentences;  
  
  Future<bool> load([String language]) async {
    language = language ?? this.locale.languageCode;
    
    String data = await rootBundle.loadString('lib/settings/generalLanguages/$language.json');
    String dataCompany = await rootBundle.loadString('lib/settings/companies/active/$language.json');
    
    Map<String, dynamic> _result = json.decode(data);
    Map<String, dynamic> _resultCompany = json.decode(dataCompany);

    print("Load $language");

    this._sentences = new Map();

    // load general language
    _result.forEach((String key, dynamic value) {
      this._sentences[key] = value.toString();
    });

    // load spesific language
    _resultCompany.forEach((String key, dynamic value) {
      this._sentences[key] = value.toString();
    });

    return true;
  }
  
  String trans(String key) {  
    return this._sentences[key];  
  }  
}