// packages
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

// screens

// widgets
import '../widgets/itemDetail.dart';

// resources
import 'package:shiftsoft/resources/basicPageApi.dart';

// models
import 'package:shiftsoft/models/mbasicpage.dart';


class BasicPage extends StatefulWidget{
  int id, mode;
  MBasicPage model;
  BasicPage({Key key, this.id, this.mode, this.model}) : super(key:key);

  createState(){
    return BasicPageState();
  }
}

class BasicPageState extends State<BasicPage>{
  MBasicPage mbasicPage;
  bool basicPageLoading = true;

  @override
  void initState(){
    if(widget.mode == 1) {
      initBasicPage();
    } else if (widget.mode == 3) {
      mbasicPage = widget.model;
      setState(() {
        mbasicPage = mbasicPage;
        basicPageLoading = false;
      });
    }
  }

  initBasicPage() async {
    setState(() {
      basicPageLoading = true;
    });
    
    mbasicPage = await basicPageApi.getBasicPage(context, widget.id);

    setState(() {
      mbasicPage = mbasicPage;
      basicPageLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initBasicPage();
    return null;
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: basicPageLoading ?
        Column(
          children: <Widget>[
            Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: Container(height:MediaQuery.of(context).size.width/16*9, width: MediaQuery.of(context).size.width, color: Colors.grey[200]),
            ),
            Padding(
              padding: EdgeInsets.all(5),
            ),
            Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: Container(height:MediaQuery.of(context).size.width, width: MediaQuery.of(context).size.width, color: Colors.grey[200]),
            )
          ],
        )
        :
        ItemDetail(
          appBar: true,
          module: "page",
          title: mbasicPage.title,
          content: Html(
            data: ""
              "${mbasicPage.content}"
            "",
            onLinkTap: (url) async {
              if (await canLaunch(url)) {
                await launch(url);
              } else {
                throw 'Could not launch $url';
              }
            },
          ),
          imageList: mbasicPage.pic.split(","),
        ),
      )
    );
  }

}