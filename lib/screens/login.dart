// packages
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'package:shiftsoft/settings/configuration.dart';

// screens
import 'package:shiftsoft/screens/dashboard.dart';
import 'package:shiftsoft/screens/forgetPassword.dart';
import 'package:shiftsoft/screens/signUp.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/SSText.dart';

// resources
import 'package:shiftsoft/resources/userApi.dart';

// models
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/models/muser.dart';

class Login extends StatefulWidget {
  createState() {
    return LoginState();
  }
}
class LoginState extends State<Login> {
  bool signUpLoading = false;
  bool unlockPassword = true;
  Map<String, String> message=new Map();
  List<String> messageList = ["login","register","forgotPassword","signUp","enterName",
  "enterPassword","enterEmail","enterPhone","enterAddress","enterCity","createAccount",
  "loginFailed", "alert"
  ];

  final FocusNode usernameFocus = FocusNode();  
  final FocusNode passwordFocus = FocusNode();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  bool pageLoading = true;

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final newPasswordController = TextEditingController();
  final phoneController = TextEditingController();
  final addressController = TextEditingController();
  final cityController = TextEditingController();

  final FocusNode nameFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();
  final FocusNode newPasswordFocus = FocusNode();
  final FocusNode phoneFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();
  final FocusNode cityFocus = FocusNode();

  final _formKey = GlobalKey<FormState>();

  List <String> listTabBar = ["login","sign up"];

  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    if (pageLoading)
      checkLogin();
  }

  checkLogin() async {
    Configuration config = Configuration.of(context);
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    List<User> loginUserListObject = [];
    List<String> loginUserList = prefs.getStringList("loginUserList") ?? [];
    loginUserList.map((item) {
      User tempUser = User.fromJson(json.decode(item));
      loginUserListObject.add(tempUser);
    }).toList();
    
    User tempUser = User(id: 0);
    loginUserListObject.map((user) {
      if (user.companyId == config.companyId) {
        tempUser = user;
      }
    }).toList();

    if (tempUser.id != 0) {
      config.user = tempUser;
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Dashboard())
      );
      setState(() {
        pageLoading = false;
      });
    }

    setState(() {
      pageLoading = false;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: SSText(
          text : c.company,
          type : 1
        ),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        color: config.bodyBackgroundColor,
        padding: EdgeInsets.only(left: 30, right: 30, top:30, bottom:30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: TextFields(
                type: 1,
                key: Key("username"),
                controller: usernameController,
                focusNode: usernameFocus,
                textInputAction: TextInputAction.next,                        
                hintText: "Username",
                labelText: "Username",
                onSubmitted: (value) {
                  passwordFocus.unfocus();
                  FocusScope.of(context).requestFocus(passwordFocus);
                },
                prefixIcon: IconButton(
                  icon: Icon(Icons.person),
                  onPressed: (){},
                ),
              )
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: TextFields(
                type: 1,
                key: Key("password"),
                obscureText: unlockPassword,
                controller: passwordController,
                focusNode : passwordFocus,
                textInputAction: TextInputAction.done,                        
                hintText: "Password",
                labelText: "Password",
                prefixIcon: IconButton(
                  icon: Icon(Icons.lock),
                  onPressed: (){},
                ),
                suffixIcon: IconButton(
                  icon: Icon(Icons.remove_red_eye),
                  onPressed: () {
                    setState(() {
                      unlockPassword = !unlockPassword;
                    });
                  }
                ),
              )
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Button(
                backgroundColor: config.primaryColor,
                rounded: true,
                child: SSText(
                  text : message["login"],
                  type : 3
                ),
                onTap: () async {
                  await login();
                },
              ),
            ),
            Container(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.only(bottom:20,top:20),
              child: InkWell(
                child: Center(
                  child: SSText(
                    text : message["forgotPassword"]+"?",
                    type : 5
                  )
                ),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => ForgetPassword())
                  );
                },
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: InkWell(
                child: Center(
                  child: SSText(
                    text : message["register"],
                    type : 6
                  ),
                ),
                onTap: () async {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SignUp())
                  );
                },
              ),
            ),
          ],
        )
      ),
    );
  }

  login([int status = 1]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String playerId = await prefs.getString('oneSignalPlayerId');
    Result result = await userApi.login(context, playerId, usernameController.text, passwordController.text, status);
    if (result.success == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Dashboard())
      );
    } else if (result.success == 1) {
      Alert(
        context: context,
        title: message["alert"],
        content: Text(result.message),
        defaultAction: () {
          login(1);
        }
      );
    } else {
      Alert(
        context: context,
        title: message["alert"],
        content: Text(message["loginFailed"]),
        cancel: false,
      );
    }
  }
}