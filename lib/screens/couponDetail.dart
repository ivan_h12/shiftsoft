// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/couponApi.dart';

// models
import 'package:shiftsoft/models/mcoupon.dart';
import 'package:shiftsoft/settings/configuration.dart';

class CouponDetail extends StatefulWidget{
  String name;
  int id, mode;
  Coupon model;
  createState(){
    return CouponDetailState();
  }
  CouponDetail({Key key, this.id, this.mode, this.model}) : super(key:key);
}

class CouponDetailState extends State<CouponDetail>{
  Coupon couponDetail;
  bool couponDetailLoading = true;
  bool checkValidate = false;
  bool checkValidateLoading = true;
  Map<String, String> message = Map();
  List<String> messageList = ["use","couponUsed","usingCoupon"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }
  void initState(){
    super.initState();
    if(widget.mode == 1) {
      initCouponDetail();
      initCouponCheck();
    } else if (widget.mode == 3) {
      couponDetail = widget.model;
      setState(() {
        couponDetailLoading = false;
        couponDetail = couponDetail;
      });
      initCouponCheck();
    }
  }

  initCouponCheck() async {
    if(couponDetail.type == 3){
      checkValidate = await couponApi.checkCouponValidate(context, couponDetail.id, 1926);
      if(checkValidate == null){
        checkValidate = false;
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
    }
    
    setState(() {
      checkValidate = checkValidate;
      checkValidateLoading = false;
    });
  }

  initCouponDetail() async {
    setState(() {
      couponDetailLoading = true;
    });

    couponDetail = await couponApi.getCoupon(context, widget.id);
    if(couponDetail == null){
      couponDetail = null;
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      couponDetail = couponDetail;
      couponDetailLoading = false;
    });
  }

  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(couponDetail.name, style: TextStyle(fontSize: 18, color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: couponDetailLoading ?
        Container(
          color: Colors.black12,
          padding: EdgeInsets.all(5),
          child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context, index){
              return Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Card(
                              color: Colors.grey[200],
                              child: Center(
                                child: Container(width: 300,height: 300,),
                              ),
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Card(
                              color: Colors.grey[200],
                              child: Center(
                                child: Container(width: 300,height: 50, margin: EdgeInsets.only(top: 6)),
                              ),
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Card(
                              color: Colors.grey[200],
                              child: Center(
                                child: Container(width: 300,height: 50, margin: EdgeInsets.only(top: 6)),
                              ),
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Card(
                              color: Colors.grey[200],
                              child: Center(
                                child: Container(width: 300,height: 50, margin: EdgeInsets.only(top: 6)),
                              ),
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Card(
                              color: Colors.grey[200],
                              child: Center(
                                child: Container(width: 300,height: 50, margin: EdgeInsets.only(top: 6)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ),
                ],
              );
            },
          ),
        )
        : Container(
          color: Colors.black12,
          padding: EdgeInsets.all(5),
          child: ListView.builder(
            itemCount: 1,
            itemBuilder: (context, index){
              return Column(
                children: <Widget>[
                  Card(
                    child: Container(
                      padding: EdgeInsets.all(10),
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Center(
                            child: ImageBox(CompanyAsset("promotion", couponDetail.pic), 300),
                          ),
                          Center(
                            child: Container(
                              margin: EdgeInsets.only(top: 5, bottom: 5),
                              child: Text(couponDetail.description,style: TextStyle(color: Colors.black)),
                            ),
                          ),
                          Text(message["usingCoupon"]+": ",style: TextStyle(color: Colors.grey),),
                          Text("- Kupon Buy 1 get 1 berlaku untuk setiap pembelian minuman Spesial Winter",style: TextStyle(color: Colors.grey)),
                          Text("- Berlaku untuk semua ukuran",style: TextStyle(color: Colors.grey)),
                          Text("- Tunjukkan kupon ini kepada kasir Starbucks",style: TextStyle(color: Colors.grey)),
                          Text("- Berlaku 1 kali di tanggal 7 Januari 2019",style: TextStyle(color: Colors.grey)),
                          Text("- Berlaku untuk semua minuman kecuali Minuman Special Price, Manual Brew, minuman Reserve dan Nitro Cold Brew",style: TextStyle(color: Colors.grey)),
                          Text("- Tidak dapat digabungkan dengan promo lainnya",style: TextStyle(color: Colors.grey)),
                        ],
                      ),
                    )
                  ),
                ],
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: checkValidateLoading ?
      Padding(
        padding: EdgeInsets.only(top:15, bottom:15),
        child:Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircularProgressIndicator(),
          ]
        )
      )
      :
      Container(
        width: MediaQuery.of(context).size.width,
        color: checkValidate ? Colors.grey : Colors.green,
        child: FlatButton(
          child: checkValidate ? Text(message["couponUsed"], style: TextStyle(color: Colors.white)): Text(message["use"], style: TextStyle(color: Colors.white)),
          onPressed: checkValidate ? (){}
          :() async {
            final message = await couponApi.validateCoupon(context, couponDetail.id, 1926);
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context){
                return AlertDialog(
                  title: Text("Alert"),
                  content: Text(message),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("OK"),
                      onPressed: (){
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
              }
            );
          },
        ),
      ),
    );
  }
  Future<Null> _refresh() async {
    await initCouponDetail();
    return null;
  }
}