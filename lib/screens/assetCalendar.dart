// package
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:shiftsoft/settings/configuration.dart';

// model
import '../models/massetrequest.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// resources
import '../resources/assetApi.dart';

class AssetCalendar extends StatefulWidget {
  int mode, id;
  AssetCalendar({Key key, this.mode, this.id}) : super(key:key);

  @override
  _AssetCalendarState createState() => _AssetCalendarState();
  void handleNewDate(date) {
    print("handleNewDate ${date}");
  }
}
class _AssetCalendarState extends State<AssetCalendar>{
  List<AssetRequest> _assetCalendar = [];
  List<AssetRequest> _dateAssetCalendar = [];
  bool assetCalendarLoading = true;

  DateTime dateNow;

  List<String> _hours = [];
  List<String> _minutes = [];
  List<String> _times = [];
  List<String> tempList = [];
  List<bool> isassetCalendarTime = [];

  int diffMins;

  Map<String, String> message = new Map();
  List<String> messageList = ["assetCalendar"];

  @override
  void initState(){
    dateNow = DateTime.now();

    initAssetCalendar();

    setState(() {
      for(int i=0; i<24; i++){
        String a = "";
        if(i < 10) a = "0";
        _hours.add(a+i.toString());
      }
      for(int i = 0 ; i<4 ; i++){
        int n = 15;
        int temp;
        temp = n*i;
        if(temp == 0) _minutes.add("00");
        else _minutes.add(temp.toString());
      }
      for(int i = 0 ; i<24; i++){
        for(int j = 0 ; j<4; j++){
          if(j == 0)
            _times.add(_hours[i] + "." + _minutes[j]);
          else
            _times.add("." + _minutes[j]);
        }
      }
    });
  }

  Widget showAssetCalendars(AssetRequest assetCalendar){
    if(assetCalendar.startAt.day == dateNow.day && assetCalendar.startAt.month == dateNow.month && assetCalendar.startAt.year == dateNow.year){
      return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, top: 20),
            width: 500.0,
            child: assetCalendar.asset.name == "" ? null : Text(assetCalendar.asset.name, style: TextStyle(fontSize: 16.0)),
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0),
            width: 500.0,
            child: Text(DateFormat('kk:mm').format(assetCalendar.startAt) + " - " + DateFormat('kk:mm').format(assetCalendar.endAt),
              style: TextStyle(fontSize: 15.0)),
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0, bottom: 20.0),
            width: 500.0,
            child: assetCalendar.asset.content == "" ? null : Text(assetCalendar.asset.content, style: TextStyle(fontSize: 15.0)),
          ),
          Divider(
            height: 10,
            color: Colors.black26,
          )
        ],
      );
    }else{
      return Text("No assetCalendar");
    }
  }

  int countEvent(List<AssetRequest> assetCalendar){
    _dateAssetCalendar.clear();
    int count = 0;
    for(int i = 0 ; i<assetCalendar.length ; i++){
      if(assetCalendar[i].startAt.day == dateNow.day && assetCalendar[i].startAt.month == dateNow.month && assetCalendar[i].startAt.year == dateNow.year){
        count++;
        _dateAssetCalendar.add(assetCalendar[i]);
      }  
    }
    return count;
  }

  void dateClicked(date){
    setState(() {
      dateNow = date;      
    });
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  initAssetCalendar() async {
    setState(() {
      assetCalendarLoading = true;
    });
    
    _assetCalendar = await assetApi.getAssetCalendar(context, widget.id);

    if(_assetCalendar == null){
      _assetCalendar = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _assetCalendar = _assetCalendar;
      assetCalendarLoading = false;
    });

    hitung(DateTime.now());
  }

  void hitung(DateTime time){
    tempList.clear();
    isassetCalendarTime.clear();
    int startHour, startMinute, endHour, endMinute;

    print(time);
    for(int index=0; index<_assetCalendar.length; index++){
      if((DateFormat('yyyy-M-dd').format(_assetCalendar[index].startAt)).toString() == (DateFormat('yyyy-M-dd').format(time))){

        startHour = int.parse((DateFormat('kk').format(_assetCalendar[index].startAt)));
        startMinute = int.parse((DateFormat('mm').format(_assetCalendar[index].startAt)));
        
        endHour = int.parse((DateFormat('kk').format(_assetCalendar[index].endAt)));
        endMinute = int.parse((DateFormat('mm').format(_assetCalendar[index].endAt)));

        int startMins = startHour * 60 + startMinute;
        int endMins = endHour * 60 + endMinute;
        diffMins = ((endMins - startMins) / 15).round();

        for(int i = 0 ; i<24 ; i++){
          for(int j = 0 ; j<60 ; j+= 15){
            isassetCalendarTime.add(false);
            tempList.add("");
          }
        }

        bool isDone = false;
        int counter = 0;
        for(int i = 0 ; i<24 ; i++){
          for(int j = 0 ; j<4 ; j++){
            if(i >= startHour && j*15 >= startMinute &&!isDone){
              for(int x = 0 ; x<diffMins ; x++){
                isassetCalendarTime[counter + x - 1] = true;
                tempList[counter + x - 1] =  _assetCalendar[index].content;
              }
              isDone = true;
            }
            counter++;
          }
        }
      } else {
        for(int i = 0 ; i<24 ; i++){
          for(int j = 0 ; j<60 ; j+= 15){
            isassetCalendarTime.add(false);
            tempList.add("");
          }
        }
      }
    }

    setState(() {
      isassetCalendarTime = isassetCalendarTime;
      tempList = tempList;
    });
  }

   @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["assetCalendar"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 5.0,
          vertical: 10.0,
        ),
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Calendar(
              onSelectedRangeChange: (range) =>
                  print("Range is ${range.item1}, ${range.item2}"),
              isExpandable: false,
              onDateSelected: (date){
                hitung(date);
                dateClicked(date);
              },
              
            ),
            Divider(
              height: 25.0,
              color: Colors.black,
            ),
            Container(
              height: 400,
              width: 1000,
              child: assetCalendarLoading ?
              Center(
                child: CircularProgressIndicator(),
              )
              :
              ListView.builder(
                itemCount: _times.length,
                itemBuilder: (BuildContext context, int index){
                  return ListTile(
                    contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                    trailing: Container(
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            height: 100.0,
                            width: 50.0,
                            child: Container(
                              alignment: Alignment.topRight,
                              child: Text(_times[index]),
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  margin: EdgeInsets.only(left: 10.0),
                                  child: Divider(
                                    color: Colors.black,
                                  )
                                ),
                                isassetCalendarTime[index] ?
                                SizedBox(
                                  height: 76.0,
                                  child: Container(
                                    color: Colors.blue,
                                    margin: EdgeInsets.only(left: 10.0),
                                    alignment: Alignment.topLeft,
                                    child: Text(tempList[index]),
                                  ),
                                )
                                :
                                SizedBox(
                                  height: 76.0,
                                  child: Container(
                                    color: Colors.white,
                                    margin: EdgeInsets.only(left: 10.0),
                                    alignment: Alignment.topLeft,
                                    child: Text(""),
                                  ),
                                )
                              ],
                            ) 
                          ),
                        ],
                      ),
                    ),
                  ); 
                }
              )

              // ListView.builder(
              //   itemCount: countEvent(_assetCalendar),
              //   itemBuilder: (BuildContext context, int index){
              //     return Container(
              //       child: Column(
              //         children: <Widget>[
              //           showAssetCalendars(_dateAssetCalendar[index]),
              //         ],
              //       ),
              //     );
              //   }
              // )
            )
          ],
        ),
      ),
    );
  }
}

 