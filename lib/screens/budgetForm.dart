// package
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

// screens
import './login.dart';

//widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/circleApi.dart';
import 'package:shiftsoft/resources/budgetApi.dart';

// model
import 'package:shiftsoft/models/mcircle.dart';
import 'package:shiftsoft/models/mbudgetdetail.dart';
import 'package:shiftsoft/settings/configuration.dart';

class BudgetForm extends StatefulWidget {
  int mode, id, check, type;
  BudgetDetails model;
  BudgetForm({Key key, this.mode, this.id, this.model, this.check, this.type}) : super(key:key);

  @override
  _BudgetFormState createState() => _BudgetFormState();
}

class _BudgetFormState extends State<BudgetForm> {
  final _formKey = GlobalKey<FormState>();

  int circleId;

  List data = ["-"];
  List budgetType = ["Submission", "Realization"];

  Map<String, String> message = new Map();
  List<String> messageList = ["budgetForm", "budgetName", "budgetType", "price", "circle", "quantity", "tax", "total", "description", "image", "submit"];

  List<DropdownMenuItem<String>> _dropDownMenuItems, _dropDownMenuItems2;
  List<File> temp = [];
  List<String> tempImage = [];
  List<TempPhoto> tempPhoto = [];
  String _current, _current2;

  final budgetNameController = TextEditingController();
  final budgetPriceController = TextEditingController();
  final budgetQuantityController = TextEditingController();
  final budgetTaxController = TextEditingController();
  final budgetTotalController = TextEditingController();
  final budgetContentController = TextEditingController();

  bool budgetFormSubmitLoading = false;

  bool circleListLoading = true;
  List<Circle> circleList = [];

  int counter = 0;

  int check;
  BudgetDetails budgetDetail;

  @override
  void initState(){
    initCircleList();

    _dropDownMenuItems = getDropDownMenuItems();
    _dropDownMenuItems2 = getDropDownMenuItems2();
    _current = _dropDownMenuItems[0].value;
    _current2 = _dropDownMenuItems2[0].value;

    budgetPriceController.addListener(hitung);
    budgetQuantityController.addListener(hitung);
    budgetTaxController.addListener(hitung);

    check = widget.check;
    // check == 1 -> EditForm, check == 0 -> newForm
    if(check == 1){
      budgetDetail = widget.model;
      budgetNameController.text = budgetDetail.name;
      budgetPriceController.text = budgetDetail.price.toString();
      budgetTotalController.text = budgetDetail.total.toString();
      budgetQuantityController.text = budgetDetail.qty.toString();
      budgetTaxController.text = budgetDetail.tax.toString();
      budgetContentController.text = budgetDetail.content;

      String budgetType;
      if(widget.type == 1){
        budgetType = "Realization";
      }else{
        budgetType = "Submission";
      }
      for(int i = 0 ; i<_dropDownMenuItems.length ; i++){
        if(_dropDownMenuItems[i].value == budgetType){
          _current = _dropDownMenuItems[i].value;
        }
      }
      
      var tempPic = budgetDetail.pic.split(",");
      for(int i = 0 ; i<tempPic.length ; i++){
        tempPhoto.add(TempPhoto(type: 1, src: tempPic[i]));
      }
      counter = tempPhoto.length;
      
    }

    super.initState();
  }

  initCircleList() async{
    setState(() {
      circleListLoading = true;    
    });   
    
    int id = 1926;
    circleList = await circleApi.getCircleListByUser(context, id);

    if(circleList == null){
      circleList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    circleList.map((item) {
      _dropDownMenuItems2.add(DropdownMenuItem(
          key: Key(item.name),
          value: item.name,
          child: Text(item.name)
      ));
    }).toList();

    setState(() {
      _dropDownMenuItems2 = _dropDownMenuItems2;
      for(int i = 0 ; i<_dropDownMenuItems2.length ; i++){
        if(_dropDownMenuItems2[i].value == budgetDetail.circleName){        
          _current2 = _dropDownMenuItems2[i].value;
        }
      }
      circleList = circleList;
      circleListLoading = false;    
    });
  }

  @override
  void dispose() {
    budgetNameController.dispose();
    budgetPriceController.dispose();
    budgetQuantityController.dispose();
    budgetTaxController.dispose();
    budgetTotalController.dispose();
    budgetContentController.dispose();
    super.dispose();
  }

  File image;
  picker() async{
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img!=null){
      setState(() {
        image = img;
        counter++;
        temp.add(image);
        tempPhoto.add(TempPhoto(type: 0, file: image));
      });
    } 
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems() {
    List<DropdownMenuItem<String>> items = List();
    for (String da in budgetType) {
      items.add(DropdownMenuItem(
          key: Key(da),
          value: da,
          child:Text(da)
      ));
    }
    return items;
  }

  List<DropdownMenuItem<String>> getDropDownMenuItems2() {
    List<DropdownMenuItem<String>> items = List();
    for (String da in data) {
      items.add(DropdownMenuItem(
          key: Key(da),
          value: da,
          child:Text(da)
      ));
    }
    return items;
  }

  void changedDropDownItem(String selected) {
    setState(() {
      _current = selected;
    });
  }
  void changedDropDownItem2(String selected) {
    setState(() {
      _current2 = selected;
    });
    for(int i = 0 ; i<circleList.length ; i++){
      if(_current2 == circleList[i].name){
        circleId = circleList[i].id;
      }
    }
  }

  void submitBudgetSubmission() async{
    if(_formKey.currentState.validate()){
      setState(() {
        budgetFormSubmitLoading = true;      
      });
      
      String budgetId = widget.id.toString();
      String userId = "1926";
      String name = budgetNameController.text;
      int harga = int.parse(budgetPriceController.text);
      int jumlah = int.parse(budgetQuantityController.text);
      int total = int.parse(budgetTotalController.text);
      String content = budgetContentController.text;
      String pic = getBase64Image(image);
      String picName = "picname.jpg";

      final message = await budgetApi.addBudgetSubmission(context, budgetId, userId, circleId.toString(), name, harga, jumlah, total, content, pic, picName);

      setState(() {
        budgetFormSubmitLoading = false;      
      });

      if(message == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  key: Key("ok"),
                  onPressed: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      }
    }
    
  }

  void submitBudgetRealization() async{
    if(_formKey.currentState.validate()){
      setState(() {
        budgetFormSubmitLoading = true;      
      });
      
      String budgetId = "1";
      String userId = "1926";
      String name = budgetNameController.text;
      int harga = int.parse(budgetPriceController.text);
      int jumlah = int.parse(budgetQuantityController.text);
      int total = int.parse(budgetTotalController.text);
      int pajak = int.parse(budgetTaxController.text);
      String content = budgetContentController.text;
      String pic = getBase64Image(image);
      String picName = "picname.jpg";

      final message = await budgetApi.addBudgetRealization(context, budgetId, userId, circleId.toString(), name, harga, jumlah, total, pajak, content, pic, picName);

      setState(() {
        budgetFormSubmitLoading = false;      
      });

      if(message == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      }
    }    
    
  }

  String getBase64Image(File img){
    List<int> imagesBytes = img.readAsBytesSync();
    String base64Image = base64Encode(imagesBytes);
    return base64Image;  
  }

  void hitung(){
    int tax;
    if(budgetPriceController.text != "" && budgetQuantityController.text != ""){
      if( budgetTaxController.text != ""){
        tax = int.parse(budgetTaxController.text);
      }else{
        tax = 0;
      }
      int price = int.parse(budgetPriceController.text);
      int qty = int.parse(budgetQuantityController.text);
      int total = (price * qty + (price * qty * tax) / 100).toInt();
      budgetTotalController.text = total.toString();
    }
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["budgetForm"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5.0),        
        child: Form(
          key: _formKey,
          child: ListView(
            key: Key("scroll"),
            children: <Widget>[
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["budgetName"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("name"),
                  controller: budgetNameController,
                  keyboardType: TextInputType.text,
                  hintText: message["budgetName"],
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter Your Budget Name";
                    }
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: Text(message["budgetType"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: DropdownButton(
                  key: Key("tipe"),
                  isExpanded: true,
                  value: _current,
                  items: _dropDownMenuItems,
                  onChanged: changedDropDownItem,
                )
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: Text(message["circle"], style: TextStyle(fontSize: 18.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: DropdownButton(
                  key: Key("circle"),
                  isExpanded: true,
                  value: _current2,
                  items: _dropDownMenuItems2,
                  onChanged: changedDropDownItem2,
                )
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["price"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  key: Key("price"),
                  controller: budgetPriceController,
                  keyboardType: TextInputType.number,
                  hintText: message["price"],
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter the Price";
                    }
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["quantity"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("qty"),
                  controller: budgetQuantityController,
                  keyboardType: TextInputType.number,
                  hintText: message["quantity"],
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter the Quantity";
                    }
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["tax"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("journeyname"),
                  controller: budgetTaxController,
                  enabled: _current == "Realization" ? true : false,
                  keyboardType: TextInputType.number,
                  hintText: message["tax"],
                  validator: (String value){
                    if(value.isEmpty && _current == "Realization"){
                      return "Please Enter the Tax";
                    }
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["total"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  controller: budgetTotalController,
                  hintText: message["total"],
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["description"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("content"),
                  controller: budgetContentController,
                  keyboardType: TextInputType.multiline,
                  maxLines: 3,
                  hintText: message["description"],
                ),
              ),
              Row(
                children: <Widget>[
                  Container(
                    width: 60.0,
                    margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                    child: Text(message["image"]+" :", style: TextStyle(fontSize: 18.0, color: Colors.grey))
                  ),
                ],
              ),
              
              Container(
                height: 75.0,       
                child: ListView.builder(
                  itemCount: counter + 1,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index){
                    return Container(
                      child: Card(
                        child: Container(
                          width: 75,
                          child: index == counter ? 
                          IconButton(
                            key: Key("image"),
                            icon: Icon(Icons.add_a_photo),
                            onPressed: () => picker(),
                          )
                          :
                          tempPhoto[index].type == 0 ?
                          Image.file(tempPhoto[index].file)
                          :
                          Image.network(CompanyAsset("budgetdetail", tempPhoto[index].src))
                        ),
                      ),
                    );
                  },
                )
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0, top: 10.0),
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: Button(
                  child: budgetFormSubmitLoading ? 
                  CircularProgressIndicator()
                  :
                  Text(message["submit"], style: TextStyle(fontSize: 20.0, color: config.primaryTextColor),),
                  key: Key("submit"),
                  rounded: true,
                  backgroundColor:config.primaryColor ,
                  onTap: (){
                    if(widget.mode == 0){
                      if(_current == "Submission"){
                        submitBudgetSubmission();
                      }else if(_current == "Realization"){
                        submitBudgetRealization();
                      }
                    } else {
                      if(_current == "Submission"){
                        //updateBudgetSubmission();
                      }else if(_current == "Realization"){
                        //updateBudgetRealization();
                      }
                    }
                  }
                ),
              )
            ]
          ),
        ),
      )
    );
  }

}

class TempPhoto {
  int type; // 0, 1
  File file;
  String src;

  TempPhoto({this.type, this.file, this.src});    
}