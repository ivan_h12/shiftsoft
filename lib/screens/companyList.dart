// packages
import 'package:flutter/material.dart';

// screens
import 'package:shiftsoft/screens/login.dart';
import 'package:shiftsoft/screens/splashscreen.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/templateIndex.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/companyApi.dart';

// models
import 'package:shiftsoft/models/mcompany.dart';
import 'package:shiftsoft/settings/configuration.dart';


class CompanyList extends StatefulWidget {
  createState() {
    return CompanyListState();
  }
}
class CompanyListState extends State<CompanyList> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  final searchCompanyController = TextEditingController();

  List<Company> companyList = [];

  bool searchMode = false;
  String searchText = "";
  final _textController = TextEditingController();
  ScrollController _controller;

  bool companyListLoading = true;
  int page = 1;

  void initState() {
    super.initState();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);

    initCompanyList();
  }

  _scrollListener() {
    // print("offset : ${_controller.offset}");
    // print("maxScrollExtent : ${_controller.position.maxScrollExtent}");
    // print("apakah out of range : ${_controller.position.outOfRange}");
    
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(companyList.length%c.apiLimit == 0){
        initCompanyList();
      }
    }
  }
  
  initCompanyList({String parameter = ""}) async {
    if (page == 1) {
      setState(() {
        companyList = [];
      });
    }

    setState(() {
        companyListLoading = true;
    });

    companyList.addAll(await companyApi.getCompanyList(page, parameter: parameter));
    
    setState(() {
      page = page + 1;
      companyList = companyList;
      companyListLoading = false;
    });
  }

  // makeAnimation() async {
  //   final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
  //   if (offsetFromBottom < 50) {
  //     await _controller.animateTo(
  //       _controller.offset - (50 - offsetFromBottom),
  //       duration: Duration(milliseconds: 500),
  //       curve: Curves.easeOut,
  //     );
  //   }
  // }

  Widget build(context) {
    final config = Configuration.of(context);
    List<Widget> widgetList = [];
    
    companyList.map((item) {
      if (item.name.toLowerCase().contains(searchText.toLowerCase())) {
        widgetList.add(
          InkWell(
            child: TemplateIndex(
              mHeight: 150.0,
              mText: item.name,
            ),
            onTap: () {
              config.triggerCompanyList = false;
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => SplashScreen(companyId: item.id))
              );
            },
          )
        );
      }
    }).toList();

    return Scaffold(
      key: _scaffoldKey,
      // appBar: AppBar(
      //   backgroundColor: config.topBarBackgroundColor,
      //   iconTheme: IconThemeData(
      //     color: config.topBarIconColor
      //   ),
      //   title: searchMode ?
      //   TextField(
      //     cursorColor: Colors.green,
      //     controller: _textController,
      //     autofocus: true,
      //     style: TextStyle(
      //       color: config.topBarTextColor
      //     ),
      //     decoration: InputDecoration(
      //       hintText: "Cari Perusahaan",
      //       hintStyle: TextStyle(
      //         color: config.topBarTextColor
      //       ),
      //       suffixIcon: searchText.length > 0 ?
      //       IconButton(
      //         icon: Icon(Icons.close),
      //         onPressed: () {
      //           _textController.text = "";
      //           setState(() {
      //             searchText = "";
      //           });
      //         }
      //       )
      //       :
      //       null
      //     ),
      //     onChanged: (text) {
      //       setState(() {
      //         searchText = text;
      //       });
      //     },
      //   )
      //   :
      //   Text(
      //     "Company List",
      //     style: TextStyle(
      //       color: config.primaryTextColor
      //     ),
      //   ),
      //   actions: <Widget>[
      //     IconButton(
      //       icon: Icon(
      //         Icons.search, color: config.primaryTextColor,
      //       ),
      //       onPressed: () {
      //         setState(() {
      //           searchMode = !searchMode;
      //         });
      //       },
      //     ),
      //   ]
      // ),
      // body: RefreshIndicator(
      //   onRefresh: _refresh,
      //   child: CustomScrollView(
      //     controller: _controller,
      //     slivers: <Widget>[
      //       companyList.length == 0 ?
      //       SliverList(
      //         delegate: SliverChildBuilderDelegate((context, index) =>
      //           Container(
      //             child: PlaceholderList(type: "company"),
      //           ),
      //           childCount: 1,
      //         )
      //       )
      //       :
      //       SliverGrid.count(
      //         crossAxisCount: 2,
      //         children: widgetList,
      //       ),
      //       SliverList(
      //         delegate: SliverChildBuilderDelegate((context, index) =>
      //           Padding(
      //             padding: EdgeInsets.symmetric(vertical: 25),
      //             child: Center(
      //                 child: CircularProgressIndicator()
      //               ),
      //           ),
      //           childCount: 1,
      //         )
      //       )
      //     ]
      //   ),
      // )
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Find company from our application"),
              Container(
                margin: EdgeInsets.only(left: 20.0, right: 20.0, top: 15.0, bottom: 12.0),
                child: TextFields(
                  type: 1,
                  controller: searchCompanyController,
                  key: Key("content-textfield"),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.search,
                  onSubmitted: (value) {
                    page = 1;
                    initCompanyList(parameter: "filters[]=name|like|*${searchCompanyController.text}*|and");
                  },
                  hintText: "deskripsi",
                ),
              ),
              Button(
                backgroundColor: config.primaryColor,
                child: Text(
                  "Search",
                  style: TextStyle(
                    color: config.primaryTextColor
                  ),
                ),
                onTap: () {
                  page = 1;
                  initCompanyList(parameter: "filters[]=name|like|*${searchCompanyController.text}*|and");
                },
              ),
              Container(
                height: 400,
                child: CustomScrollView(
                  controller: _controller,
                  slivers: <Widget>[
                    companyList.length == 0 && companyListLoading == false ?
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) =>
                        Container(
                          child: PlaceholderList(type: "company"),
                        ),
                        childCount: 1,
                      )
                    )
                    :
                    SliverGrid.count(
                      crossAxisCount: 2,
                      children: widgetList,
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) =>
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 25),
                          child: Center(
                              child: companyListLoading ?
                              CircularProgressIndicator()
                              :
                              Button(
                                backgroundColor: config.primaryColor,
                                child: Text(
                                  "Load more",
                                  style: TextStyle(
                                    color: config.primaryTextColor
                                  ),
                                ),
                                onTap: () {
                                  initCompanyList();
                                },
                              )
                            ),
                        ),
                        childCount: 1,
                      )
                    )
                  ]
                ),
              )
            ],
          ),
        )
      ),
    );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initCompanyList();
    return null;
  }
}