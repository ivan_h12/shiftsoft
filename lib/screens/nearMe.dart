// package
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widget
import '../widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import '../widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/nearMeApi.dart';

// models
import 'package:shiftsoft/models/mnearme.dart';
import 'package:shiftsoft/settings/configuration.dart';

class NearMe extends StatefulWidget {
  @override
  _NearMeState createState() => _NearMeState();
}

class _NearMeState extends State<NearMe> {
  List<Nearme> _nearMeList = [];
  bool nearMeListLoading = true;
  GoogleMapController mapController;
  double long, lat;
  bool toogle = false;
  int tempIndex = -1;
  double minLat = 99999.99, maxLat = -99999.0;
  double minLong = 99999.99, maxLong = -999999.0;
  double alpha = 1;

  Marker _selectedMarker;
  double _selectedLat, _selectedLong;

  ScrollController _controller;
  bool isLoading = false;
  int newsListMaxLength;
  int page = 1;

  Map<String, String> message = Map();
  List<String> messageList = ["nearMe","nearbyCircle"];

  @override
  void initState(){
    _nearMeList.clear();
    initNearMeList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_nearMeList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _nearMeList.addAll(await nearMeApi.getNearMeList(context, page));
      if(_nearMeList == null){
        _nearMeList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initNearMeList() async {
    setState(() {
      nearMeListLoading = true;
    });
    
    _nearMeList = await nearMeApi.getNearMeList(context, 1);
    page++;

    if(_nearMeList == null){
      _nearMeList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _nearMeList = _nearMeList;
      nearMeListLoading = false;
      page = page;
    });

    for(int i=0; i<_nearMeList.length; i++){
      var temp = _nearMeList[i].gps.split(",");
      if(double.parse(temp[0]) > maxLat){
        maxLat = double.parse(temp[0]);
      }
      if(double.parse(temp[0]) < minLat){
        minLat = double.parse(temp[0]);
      }
      if(double.parse(temp[1]) > maxLong){
        maxLong = double.parse(temp[1]);
      }
      if(double.parse(temp[1]) < minLong){
        minLong = double.parse(temp[1]);
      }
    }
  }

  showLocation(bool toogle, double lat, double long){
    if(toogle){
      return mapController.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(this.lat, this.long), 
            zoom: 15.0,
          )
        ),
      );
    } else {
      showCircle();
    }
  }

  showCircle(){
    return mapController.animateCamera(
      CameraUpdate.newLatLngBounds(
        LatLngBounds(
          southwest: LatLng(minLat, minLong),
          northeast: LatLng(maxLat, maxLong),
        ),
        32.0,
      ),
    );
  }

  void showMarker(){
    for(int i=0; i<_nearMeList.length; i++){
      var temp = _nearMeList[i].gps.split(",");
      mapController.addMarker(
        MarkerOptions(
          position: LatLng(double.parse(temp[0]), double.parse(temp[1])),
          infoWindowText: InfoWindowText(_nearMeList[i].name, _nearMeList[i].schedule),
          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
          alpha: alpha
        )
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message["nearMe"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 2 - 100.0,
                  width: MediaQuery.of(context).size.width,
                  child: GoogleMap(
                    onMapCreated: (GoogleMapController controller) {
                      mapController = controller;
                      showCircle();
                      showMarker();
                    },
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10.0),
            child: Text(message["nearbyCircle"], style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
          ),
          Expanded(
            child: Container(
              child: ListView.builder(
                itemCount: _nearMeList.length != 0 ? _nearMeList.length : 1,
                itemBuilder: (BuildContext context, int index){
                  if(_nearMeList.length != 0){
                    return index < _nearMeList.length ? Column(
                      children: <Widget>[
                        ListTile(
                          key: Key("nearme$index"),
                          contentPadding: EdgeInsets.all(25.0),
                          leading: ImageBox(CompanyAsset("circle", _nearMeList[index].pic), 100),
                          title: Text(_nearMeList[index].name, style: TextStyle(fontWeight: FontWeight.bold)),
                          subtitle: Text("Place : " + _nearMeList[index].place + "\nSchedule : " + _nearMeList[index].schedule),
                          onTap: (){
                            var temp = _nearMeList[index].gps.split(",");
                            lat = double.parse(temp[0]);
                            long = double.parse(temp[1]);

                            if(index!=tempIndex){
                              toogle = true;
                              tempIndex = index;
                            } else {
                              if(toogle){
                                toogle = false;
                              } else {
                                toogle = true;
                              }
                            }
                            showLocation(toogle, lat, long);
                            
                            mapController.clearMarkers();

                            if(toogle){
                              alpha = 0.5;
                              showMarker();
                              mapController.addMarker(
                                MarkerOptions(
                                  position: LatLng(lat, long),
                                  infoWindowText: InfoWindowText(_nearMeList[index].name, _nearMeList[index].schedule),
                                  //icon: BitmapDescriptor.defaultMarkerWithHue(HSVColor.fromColor(c.primaryColor).hue),
                                  icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
                                ),
                              );
                              
                            } else {
                              alpha = 1;
                              showMarker();
                            }
                          },
                        ),
                        Divider()
                      ],
                    )
                    :
                    Center(
                      child: Opacity(
                        opacity: isLoading ? 1.0 : 0.0,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  } else {
                    return PlaceholderList(type: "nearme", paddingTop: 80);
                  }
                },
              )
            )
          )
        ],
      ),
    );
  }
}