//packages
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart' as map;

//screens
import './login.dart';

// resources
import 'package:shiftsoft/resources/circleApi.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/settings/configuration.dart';

// models
import '../models/mcircle.dart';
import '../models/mresult.dart';

class CircleForm extends StatefulWidget {
  int mode, id;
  Circle model;
  CircleForm({Key key, this.mode, this.id, this.model}) : super(key:key);

  @override
  _CircleFormState createState() => _CircleFormState();
}

class _CircleFormState extends State<CircleForm> with TickerProviderStateMixin{
  final _formKey = GlobalKey<FormState>();
  final circleNameController = TextEditingController();
  final circlePlaceController = TextEditingController();
  final circleScheduleController = TextEditingController();
  final circleGpsController = TextEditingController();

  bool showMap = false;
  static map.LatLng myLocation = map.LatLng(-7.3413573, 112.7417109);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  Circle circleUpdate;
  bool circleUpdateLoading = true;
  bool circleSubmitLoading = false;
  File image;

  double lat, lng;

  var temp, temp1;

  Map<String, String> message = new Map();
  List<String> messageList = ["circleName", "choosepicture", "circleForm", "circle", "place", "submit", "schedule", "cancel", "success", "loginFailed", "warning"];

  @override
  void initState() {
    super.initState();
    circleUpdate = widget.model;
    var tempGps = circleUpdate.gps.split(",");
    
    // lat = double.parse(tempGps[0]);
    // lng = double.parse(tempGps[1]);

    // print(lat);
    // print(lng);

    // map.LatLng(51.5, -0.09);

    setState(() {
      circleUpdateLoading = false;
      circleUpdate = circleUpdate;
      // myLocation.latitude = lat;
      // myLocation.longitude = lng;
    });
  
    circleNameController.text = widget.model.name;
    circlePlaceController.text = widget.model.place;
    circleScheduleController.text = widget.model.schedule;
    circleGpsController.text = widget.model.gps;
  }

  // initCircleUpdate() async {
  //   setState(() {
  //     circleUpdateLoading = true;
  //   });
    
  //   circleUpdate = await circleApi.;

  //   setState(() {
  //     circleUpdate = circleUpdate;
  //     newsDetailLoading = false;
  //   });
  // }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  void submitCircle() async {
    if(_formKey.currentState.validate()){
      int circleId = circleUpdate.id;
      String circleName = circleNameController.text;
      String circlePlace = circlePlaceController.text;
      String circleSchedule = circleScheduleController.text;
      String gps = circleGpsController.text;

      setState(() {
          circleSubmitLoading = true;      
        });
      
      Result result = await circleApi.updateCircle(context, circleId, circleName, circlePlace, circleSchedule, gps);

      if (result.success == 1) {
        Alert(
          context: context,
          type: "success",
          title: message["success"],
          content: Text(result.message),
          cancel: false,
          defaultAction: () {
            Navigator.pop(context, true);
          }
        );
      } else if (result.success == 0) {
        Alert(
          type: "error",
          context: context,
          title: message["failed"],
          content: Text(result.message),
        );
      } else if (result.success == -1) {
        Alert(
          context: context,
          title: message["alert"],
          content: Text(message["loginFailed"]),
          cancel: false,
          defaultAction: () {
            Navigator.pushNamed(context, "login");
          }
        );
      }

      setState(() {
        circleSubmitLoading = false;
      });
    }
  }

  picker() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img != null){
      setState(() {
        image = img;
      });
    }
  }

  String getBase64Image(File img){
    List<int> imagesBytes = img.readAsBytesSync();
    String base64Image = base64Encode(imagesBytes);
    return base64Image;  
  }

  @override
  void dispose() {
    circleNameController.dispose();
    circlePlaceController.dispose();
    circleScheduleController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    var markers = <Marker>[
      Marker(
        width: 80,
        height: 80,
        point: myLocation,
        builder: (ctx) => Container(
          child: GestureDetector(
            onTap: (){
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: Text("Tapped on Blue FlutterLogo Marker"),
              ));
            },
            child: Icon(Icons.edit_location),
          )
        )
      )
    ];
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text(message["circleForm"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: CustomScrollView(
      slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate((context,index) => 
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.width/16*9,
                
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: image == null? 
                      NetworkImage(CompanyAsset("circle", widget.model.pic))
                      : FileImage(image),
                    fit: BoxFit.cover
                  )
                ),
                child: BackdropFilter(
                  filter: ui.ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    height: MediaQuery.of(context).size.width/16*9,
                    decoration: BoxDecoration(color: Colors.white.withOpacity(0.2)),
                    child: DrawerHeader(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              width: 100,
                              height: 100,
                              child:
                                image == null ? Image.network(CompanyAsset("circle", widget.model.pic)) : Image.file(image),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                border: Border.all(
                                  color: Colors.white,
                                  width: 4.0,
                                ),
                              ),
                            ),
                            Container(
                              child: RaisedButton(
                                child: Text(message["choosepicture"],style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),),
                                color: Colors.white,
                                shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                                onPressed: 
                                  () async {
                                    await picker();
                                    if(image != null){
                                      final imageTemp = getBase64Image(image);
                                      await circleApi.changeCircleImage(context, circleUpdate.id, imageTemp, "image.jpg");
                                    }
                                  }
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              childCount: 1,  
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) =>
              Container(
                padding: EdgeInsets.all(5.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                        child: Text(message["circleName"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0),
                        child: TextFields(
                          controller: circleNameController,
                          keyboardType: TextInputType.text,
                          hintText: message["circleName"],
                          validator: (String value){
                            if(value.isEmpty){
                              return "Please Enter Your Circle Name";
                            }
                          },
                        ),
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                        child: Text(message["place"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0),
                        child: TextFields(
                          controller: circlePlaceController,
                          keyboardType: TextInputType.text,
                          hintText: message["place"],
                          validator: (String value){
                            if(value.isEmpty){
                              return "Please Enter Your Place";
                            }
                          },
                        ),
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                        child: Text(message["schedule"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0),
                        child: TextFields(
                          controller: circleScheduleController,
                          keyboardType: TextInputType.text,
                          hintText: message["schedule"],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                        child: Text("GPS", style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0),
                        child: TextFields(
                          controller: circleGpsController,
                          keyboardType: TextInputType.multiline,
                          hintText: "GPS",
                          // suffixIcon: IconButton(
                          //   icon: Icon(Icons.map),
                          //   onPressed: (){
                          //     setState(() {
                          //       showMap ? showMap = false : showMap = true;
                          //     });
                          //   },
                          // ),
                          validator: (String value){
                            if(value.isEmpty){
                              return "Please Enter Your GPS";
                            }
                          },
                        ),
                      ),                     
                      !showMap ?
                      Container()
                      :
                      Container(
                        height: 300.0,
                        padding: EdgeInsets.only(bottom: 10, left: 1, right: 1),
                        color: Colors.white,
                        child: FlutterMap(
                          options: MapOptions(
                            center: map.LatLng(-7.3413573, 112.7417109),
                            zoom: 15.0,
                            maxZoom: 30.0,
                            minZoom: 3.0,
                            onTap: _handleTap
                          ),
                          layers: [
                            TileLayerOptions(
                              urlTemplate:
                                "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                                subdomains: ['a', 'b', 'c']),
                                MarkerLayerOptions(markers: markers)
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 7.0, right: 3.0, bottom: 10.0),
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: 50.0,
                            child: Button(
                              child: Text(message["cancel"], style: TextStyle(color: config.primaryColor),),
                              fill: false,
                              backgroundColor: config.primaryColor,
                              rounded: true,
                              onTap: (){
                                Navigator.pop(context);
                              }
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 3.0, right: 7.0, bottom: 10.0),
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: 50.0,
                            child: Button(
                              rounded: true,
                              loading: circleSubmitLoading,
                              child: Text(message["submit"], style: TextStyle(color: config.primaryTextColor)),
                              backgroundColor: config.primaryColor,                          
                              onTap: (){
                                submitCircle();
                              }
                            ),
                          )
                        ],
                      )
                    ],
                  )
                )
              ),
              childCount: 1,
            )
          ),
        ]
      )
    );
  }

  _handleTap(map.LatLng point){
    print("adsdasd");
    // setState(() {
    //   myLocation = point;
    //   lat = myLocation.latitude;
    //   lng = myLocation.longitude;
    //   circleGpsController.text = "$lat,$lng"; 
    // });
  }
}