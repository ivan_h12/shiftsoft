// package
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

// screens
import './lessonDetail.dart';
import './login.dart';

// model
import '../models/mlesson.dart';
import 'package:shiftsoft/settings/configuration.dart';

// resource
import '../resources/lessonApi.dart';

// widget
import '../widgets/placeholderList.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/listBox.dart';

class LessonList extends StatefulWidget {
  @override
  _LessonListState createState() => _LessonListState();
}

class _LessonListState extends State<LessonList> {
  List<Lesson> _lessonList = [];
  bool lessonListLoading = true;

  ScrollController _controller;
  bool isLoading = false;
  int lessonListMaxLength;
  int page = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["lessonList"];

  @override
  void initState(){
    _lessonList.clear();
    initLessonList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_lessonList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _lessonList.addAll(await lessonApi.getLessonList(context, page));
      if(_lessonList == null){
        _lessonList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initLessonList() async {
    setState(() {
      lessonListLoading = true;
    });
    
    _lessonList = await lessonApi.getLessonList(context, 1);
    if(_lessonList == null){
      _lessonList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;

    setState(() {
      _lessonList = _lessonList;
      lessonListLoading = false;
      page = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(message["lessonList"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: lessonListLoading ?
        ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, int index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: ListTile(
                contentPadding: EdgeInsets.all(25.0),
                leading: Container(width: 100, height: 100, color: Colors.grey[200]),
                //title: Container(width: 100, height: 50, color: Colors.grey[200])
              )
            );
          }
        )
        :
        ListView.builder(
          controller: _controller,
          itemCount: _lessonList.length != 0 ? _lessonList.length : 1,
          itemBuilder: (BuildContext context, int index){
            if(_lessonList.length != 0){
              return index < _lessonList.length ? 
              ListBox(
                childs: ListTile(
                  key: Key("matpel$index"),
                  contentPadding: EdgeInsets.only(left: 20.0, top: 13.0),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LessonDetail(mode: 1, id: _lessonList[index].id , model: _lessonList[index]))
                      );
                    },
                    title: Text(_lessonList[index].name)
                )
              )
              :
              Center(
                child: Opacity(
                  opacity: isLoading ? 1.0 : 0.0,
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return PlaceholderList(type: "lesson");
            }
          }
        ),
      )
    );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initLessonList();
    return null;
  }
}