// packages
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/profileBox.dart';

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';

class RentDetail extends StatefulWidget{
  String title;
  Object tagHero;
  createState(){
    return RentDetailState();
  }
  RentDetail({this.title,this.tagHero});
}

class RentDetailState extends State<RentDetail>{
  double rating = 3.5;
  int starCount = 5;

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","addToCart","readMore","all","reviews","listPrice","freeSample","aboutTheAuthor","rateThisBook","tellOtherWhatYouThink","writeAReview"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(fontSize: 14, color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.search),
            color: Colors.white,
          )
        ],
      ),
      body: Container(
        color: Colors.black12,
        padding: EdgeInsets.all(1),
        child: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            height: 200,
                            width: 125,
                            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                            child: Hero(
                              tag: widget.tagHero,
                              child: InkWell (
                                onTap: (){

                                },
                                child: ImageBox("https://ecs5.tokopedia.net/newimg/product-1/2014/2/12/3249666/3249666_5bd73756-93d0-11e3-b1cf-88bd2523fab8.jpg", 160),
                              )
                            ),
                          ),
                          Container(
                            width: 175,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 3,
                                      child: Text(widget.title, style: TextStyle(fontWeight: FontWeight.bold),),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: IconButton(
                                        onPressed: (){
                                          print("bookmark");
                                        },
                                        icon: Icon(Icons.turned_in_not),
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.only(top: 15, left: 5),
                                  child: Row(
                                    children: <Widget>[
                                      Container(
                                        width: 60,
                                        height: 60,
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          image: DecorationImage(
                                            fit: BoxFit.fill,
                                            image: NetworkImage("https://content.swncdn.com/zcast/oneplace/host-images/daily-hope/640x480.jpg")
                                          )
                                        ),
                                      ),
                                      Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text("Rick Warren",style: TextStyle(fontSize: 14),),
                                          Text("Zondervan", style: TextStyle(fontSize: 12.0,color: Colors.grey),),
                                          Text("23 October 2012", style: TextStyle(fontSize: 12.0,color: Colors.grey),),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 15, top: 5),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Text(message['listPrice']+": ",style: TextStyle(color: Colors.blue, fontSize: 12),),
                            Text("IDR 190,255.00", style: TextStyle(decoration: TextDecoration.lineThrough, color: Colors.blue, fontSize: 12),)
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 15, left: 15),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: OutlineButton(
                                borderSide: BorderSide(color: Colors.blue),
                                onPressed: (){},
                                color: Colors.white,
                                child: Text(message['freeSample'], style: TextStyle(color: Colors.blue, fontSize: 10.5),),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(),
                            ),
                            Expanded(
                              flex: 6,
                              child: RaisedButton(
                                color: Colors.blue,
                                onPressed: (){},
                                child: Text(message['addToCart'], style: TextStyle(fontSize: 12, color: Colors.white),),
                              ),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        child: Divider(),
                      ),
                      Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Center(
                                child: Container(
                                  child: Column(
                                    children: <Widget>[
                                      IconButton(
                                        icon: Icon(Icons.bookmark),
                                        onPressed: (){},
                                      ),
                                      Text("Wishlist")
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Center(
                                child: Container(
                                  child: Column(
                                    children: <Widget>[
                                      Stack(
                                        children: <Widget>[
                                          Icon(Icons.brightness_1, size: 75,),
                                          Positioned(
                                            top: 15,
                                            left: 25,
                                            child: Text("4.6", style: TextStyle(color: Colors.white,),)
                                          ),
                                          Positioned(
                                            top: 40,
                                            left: 12.5,
                                            child: Row(
                                              children: <Widget>[
                                                Icon(Icons.star, color: Colors.white, size: 10),
                                                Icon(Icons.star, color: Colors.white, size: 10),
                                                Icon(Icons.star, color: Colors.white, size: 10),
                                                Icon(Icons.star, color: Colors.white, size: 10),
                                                Icon(Icons.star, color: Colors.white, size: 10),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            Text("799", style: TextStyle(color: Colors.grey),),
                                            Icon(Icons.people, color: Colors.grey,)
                                          ],
                                        ),
                                      ),
                                    ],
                                  )
                                ),
                              ),
                            ),  
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        child: Divider(),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 5, bottom: 5, right: 15, left: 15),
                        child: Column(
                          children: <Widget>[
                            Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. "),
                            FlatButton(
                              onPressed: (){},
                              child: Text(message['readMore'], style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        child: Divider(),
                      ),
                      ListTile(
                        title: Text(message['aboutTheAuthor'], style: TextStyle(color: Colors.grey),),
                        subtitle: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. ", style: TextStyle(color: Colors.black),),
                      ),
                      FlatButton(
                        onPressed: (){},
                        child: Text(message['readMore'], style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5),
                        child: Divider(),
                      ),
                      ListTile(
                        title: Text(message['rateThisBook']),
                        subtitle: Text(message['tellOtherWhatYouThink']),
                      ),
                      StarRating(
                        size: 50.0,
                        rating: rating,
                        color: Colors.yellow,
                        borderColor: Colors.grey,
                        starCount: starCount,
                        onRatingChanged: (rating) => setState(
                          () {
                            this.rating = rating;
                          },
                        ),
                      ),
                      FlatButton(
                        onPressed: (){},
                        child: Text(message['writeAReview'], style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: 5, left: 5, bottom: 10),
                        child: Divider(),
                      ),
                      ListTile(
                        leading: Text("4.6", style: TextStyle(fontSize: 40),),
                        title: Row(
                          children: <Widget>[
                            Icon(Icons.star, color: Colors.grey, size: 20),
                            Icon(Icons.star, color: Colors.grey, size: 20),
                            Icon(Icons.star, color: Colors.grey, size: 20),
                            Icon(Icons.star, color: Colors.grey, size: 20),
                            Icon(Icons.star_half, color: Colors.grey, size: 20),
                          ],
                        ),
                        subtitle: Row(
                          children: <Widget>[
                            Text("799", style: TextStyle(color: Colors.grey)),
                            Icon(Icons.person_outline, color: Colors.grey,)
                          ],
                        ),
                        trailing: IconButton(
                          onPressed: (){},
                          icon: Icon(Icons.info,),
                        ),
                      )
                    ],
                  ),
                ),
                childCount: 1
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  padding: EdgeInsetsDirectional.only(top: 10),
                  color: Colors.white,
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            ProfileBox(
                              "https://content.swncdn.com/zcast/oneplace/host-images/daily-hope/640x480.jpg",
                              50.0,
                              "Carolyn Myres", 
                              Row(
                                children: <Widget>[
                                  Icon(Icons.star, color: Colors.grey, size: 15,),
                                  Icon(Icons.star, color: Colors.grey, size: 15,),
                                  Icon(Icons.star, color: Colors.grey, size: 15,),
                                  Icon(Icons.star, color: Colors.grey, size: 15,),
                                  Icon(Icons.star, color: Colors.grey, size: 15,),
                                  Text(" 31/10/18", style: TextStyle(color: Colors.grey),)
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                  onPressed: (){},
                                  icon: Icon(Icons.thumb_up),
                                  color: Colors.grey,
                                ),
                                IconButton(
                                  color: Colors.grey,
                                  onPressed: (){},
                                  icon: Icon(Icons.more_vert),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),Container(
                        padding: EdgeInsets.only(left: 50),
                        child: ListTile(
                          title: Text("The Purpose Driven Life"),
                          subtitle: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."),
                        ),
                      ),
                    ],
                  ),
                ),
                childCount: 3,
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  color: Colors.white,
                  child: FlatButton(
                    child: Text(message['all']+" "+message['reviews'], style: TextStyle(color: Colors.blue)),
                    onPressed: (){},
                  ),
                ),
                childCount: 1
              ),
            ),
          ],
        ),
      ),
    );
  }
}