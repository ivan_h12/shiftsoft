// packages
import 'package:flutter/material.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets

// resources
import 'package:shiftsoft/resources/productCommentApi.dart';
import 'package:shiftsoft/tools/functions.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';

class ProductCommentAdd extends StatefulWidget{
  int userId, productId;
  
  createState() {
    return ProductCommentAddState();
  }

  ProductCommentAdd({this.userId,this.productId});
}

class ProductCommentAddState extends State<ProductCommentAdd>{
  final myController = TextEditingController();

  Map<String, String> message=new Map();
  List<String> messageList = ["send","addNewComment","whatDoYouWantToAskAboutThisProduct","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['addNewComment'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
            padding: EdgeInsets.all(10),
            child: TextField(
              maxLines: 10,
              controller: myController,
              decoration: InputDecoration(
                hintText: message['whatDoYouWantToAskAboutThisProduct'],
              ),
            ),
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.all(20),
        child: RaisedButton(
          color: Colors.green,
          onPressed: () async {
            final messaged = await productCommentApi.addComment(context, widget.productId, widget.userId, myController.text);
            if(messaged == null){
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text("Alert"),
                    content: Text(message['loginFailed']),
                    actions: <Widget>[
                      FlatButton(
                        key: Key("ok"),
                        child: Text("OK"),
                        onPressed: (){
                          return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                        },
                      )
                    ],
                  );
                }
              );
            }else{
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text("Alert"),
                    content: Text(messaged),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("OK"),
                        onPressed: (){
                          Navigator.of(context).pop();
                          Navigator.of(context).pop(context);
                        },
                      )
                    ],
                  );
                }
              );
            }
          },
          child: Text(message['send'],style: TextStyle(color: Colors.white),),
        ),
      ),
    );
  }
}