// package
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';

// screen
import 'package:shiftsoft/screens/basicPage.dart';
import 'package:shiftsoft/screens/digitalCardList.dart';
import 'package:shiftsoft/screens/profilePage.dart';

// widgets
import '../widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/basicPageApi.dart';

// models
import 'package:shiftsoft/models/mbasicpage.dart';
import 'package:shiftsoft/models/muser.dart';

class Sidebar extends StatefulWidget{
  User model;
  List<String> menuList, menuNameList;
  Sidebar({
    Key key,
    this.model,
    this.menuList,
    this.menuNameList,
  }):super(key:key);

  createState() {
    return SidebarState();
  }
}

class SidebarState extends State<Sidebar> {
  List<MBasicPage> mbasicPage;
  List<Widget> widgetList = [];
  bool userProfileLoading = false;

  bool newsStatus = true;
  bool circleStatus = false;
  bool eventStatus = false;
  bool transactionStatus = false;
  bool downloadStatus = false;
  bool assetStatus = false;
  bool spiritualJourneyStatus = false;
  bool productCategoryStatus = false;
  bool productWishlistStatus = false;
  bool productCartlistStatus = false;
  bool lessonStatus = false;
  bool couponStatus = false;
  bool fosterChildStatus = false;
  bool promotionStatus = false;
  bool subscribeStatus = false;
  bool nearmeStatus = false;
  bool rentListStatus = false;
  bool rentWishlistStatus = false;
  bool rentCartlistStatus = false;
  bool budgetListStatus = false;
  bool reviewListStatus = false;
  bool commentListStatus = false;
  bool requestListStatus = false;

  Map<String, String> message = new Map();
  List<String> messageList = [ "digitalId","news","circle", "eventList", "transactionList", 
    "downloadList", "assetList", "spiritualJourney", "productCategory", "productWishlist",
    "productCartList", "lessonList", "couponList", "fosterChildList", "promotionList",
    "subscribeList", "nearMe", "rentList", "rentWishlist", "rentCartList",
    "reviewList", "commentList", "requestList" , "budgetList"];

  String tempDigitalId;
  @override
  void initState(){
    initBasicPage();
  }

  didChangeDependencies() async {
    messageList.map((item){
      if(item == "digitalId"){
        tempDigitalId = messages(context, item.split(","));
        List<String> temp = tempDigitalId.split(" ");
        tempDigitalId = temp[1] + " " + temp[2];
      }
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  initBasicPage() async {
    mbasicPage = await basicPageApi.getBasicPageList(context);
    
    mbasicPage.map((item) {
      widgetList.add(
        ListTile(
          key: Key("basicPage1"),
          title: Text(item.title),
          onTap: (){
            Navigator.pop(context);
            Navigator.push(context, 
              MaterialPageRoute(
                builder: (context) => BasicPage(mode: 3, id: item.id, model: item,)
              )
            );
          },
        ),
      );
    }).toList();

    setState(() {
      mbasicPage = mbasicPage;
    });
  }

  Widget build(context) {
    List<Widget> menuWidgetList = [];

    int menuListLength = widget.menuList.length;
    for (var i = 0; i < menuListLength; i++) {
      menuWidgetList.add(
        ListTile(
          key: Key(widget.menuNameList[i]),
          title: Text(message[widget.menuNameList[i]]),
          onTap: () {
            Navigator.pop(context);
            Navigator.pushNamed(
              context,
              "${widget.menuList[i]}"
            );
          },
        )
      );
    }

    return Drawer(
      child: ListView(
        key: Key("scroll_sidebar"),
        padding: EdgeInsets.zero,
        children: <Widget> [
          DrawerHeader(
            child: Padding(
              padding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        flex:3,
                        child: Hero(
                          tag: "profile",
                          child: InkWell (
                            onTap: (){
                              Navigator.push(context, 
                                MaterialPageRoute(
                                  builder: (context) => ProfilePage(url: CompanyAsset("avatar",  widget.model.pic), tagHero: "profile", model: widget.model,mode: 3,)
                                )
                              );
                            },
                            child: ImageBox(CompanyAsset("avatar",  widget.model.pic), 75.0)
                          )
                        )
                      ),
                      Padding(
                        padding: EdgeInsets.only(left:10.0, right:10.0)
                      ),
                      Expanded(
                        flex:6,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(widget.model.name,
                              style: TextStyle(
                                color: Colors.white
                              )                
                            ),
                            Text("hak akses",
                              style: TextStyle(
                                color: Colors.white
                              )                
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Container(
                    child: RaisedButton(
                      child: Text(message["digitalId"],style: TextStyle(fontWeight: FontWeight.bold),),
                      color: Colors.white,
                       shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>DigitalCardList(title: tempDigitalId, model: widget.model, ) ));
                      },
                    ),
                  ),
                ],
              ),
            ),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColorDark,
            ),
          ),
        ]..addAll(menuWidgetList)..addAll(widgetList),
      ),
    );
  }
}