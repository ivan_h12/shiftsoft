// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens

// widgets 

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';

class ReportForm extends StatefulWidget{
  createState(){
    return ReportFormState();
  }
}

class ReportFormState extends State<ReportForm> {
  final textController = TextEditingController();
  bool text1 = false, text2 = false, text3 = false;
  int _radioValue = 0;
  String finalStr;

  Map<String, String> message=new Map();
  List<String> messageList = ["spam","contentContainingSARAetc","other","report","helpUsToKnowWhatHappened","whyYouReportThis","fillTheReasonHere","send","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  void _handleRadioValueChange(int value){
    setState(() {
      _radioValue = value;
      switch(_radioValue){
        case 0:
          text1 = false;
          text2 = false;
          text3 = false;
          break;
        case 1:
          text1 = true;
          text2 = false;
          text3 = false;
          finalStr = message['spam'];
          break;
        case 2:
          text2 = true;
          text1 = false;
          text3 = false;
          finalStr = message['contentContainingSARAetc'];
          break;
        case 3:
          text1 = false;
          text2 = false;
          text3 = true;
          break;
      }
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    ThemeData theme = Theme.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message['report'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) => 
              Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(message['helpUsToKnowWhatHappened'], style: TextStyle(fontWeight: FontWeight.bold),),
                    Text(message['whyYouReportThis'], style: TextStyle(fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              childCount: 1,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) => 
              Container(
                padding: EdgeInsets.fromLTRB(20, 20, 20, 25),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ListTile(
                      leading: Radio(
                        value: 1,
                        groupValue: _radioValue,
                        onChanged: _handleRadioValueChange,
                      ),
                      title: text1 ? Text(message['spam']) : Text(message['spam'], style: TextStyle(color: Colors.grey),),
                    ),
                    ListTile(
                      leading: Radio(
                        value: 2,
                        groupValue: _radioValue,
                        onChanged: _handleRadioValueChange,
                      ),
                      title: text2 ? Text(message['contentContainingSARAetc']) : Text(message['contentContainingSARAetc'], style: TextStyle(color: Colors.grey)) ,
                    ),
                    ListTile(
                      leading: Radio(
                        value: 3,
                        groupValue: _radioValue,
                        onChanged: _handleRadioValueChange,
                      ),
                      title: text3 ? 
                      Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(message['other']),
                            TextFormField(
                              controller: textController,
                              decoration: InputDecoration(
                                hintText: message['fillTheReasonHere']
                              ),
                            )
                          ],
                        ),
                      )
                      :
                      Container(
                        padding: EdgeInsets.only(top: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(message['other'], style: TextStyle(color: Colors.grey),),
                            FocusScope(
                              node: FocusScopeNode(),
                              child: TextFormField(
                                style: theme.textTheme.subhead.copyWith(
                                  color: theme.disabledColor,
                                ),
                                decoration: InputDecoration(
                                  hintText: text3 ? 'type something' : message['fillTheReasonHere']
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              childCount: 1,
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        padding: EdgeInsets.only(bottom: 10, left: 20, right: 20),
        child: RaisedButton(
          elevation: 4,
          child: Text(message['send'], style: TextStyle(color: Colors.white),),
          color: Colors.green,
          onPressed: text1 || text2 || text3 ? 
          (){
            showDialog(
              context: context,
              barrierDismissible: false,
              builder: (context){
                return AlertDialog(
                  title: Text("Alert"),
                  content: text3 ? Text(textController.text): Text(finalStr),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("OK"),
                      onPressed: (){
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
              }
            );
          } : null,
        ),
      ),
    );
  }
}