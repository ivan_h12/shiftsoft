//packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_calendar/flutter_calendar.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart' show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart' as calCarousel;
import 'package:flutter_calendar_carousel/classes/event_list.dart';

//screens
import './eventDetail.dart';
import './login.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';

// resources
import 'package:shiftsoft/resources/eventApi.dart';

// models
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EventCalendar extends StatefulWidget {
  @override
  _EventCalendarState createState() => _EventCalendarState();
}

class _EventCalendarState extends State<EventCalendar>{ 
  DateTime dateNow;

  EventList<calCarousel.Event> _markedDateMap = EventList<calCarousel.Event>();
  DateTime _currentDate = DateTime.now();
  
  List<Event> _eventList = [];
  List<Event> _dateEventList = [];
  bool eventCalendarLoading = true;

  bool isLoading = false;
  ScrollController _controller;
  int page = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["eventcalendar"];

  Widget _eventIcon = Container(
    child: Container(
      padding: EdgeInsets.only(left: 22),
      child: Text(".", style: TextStyle(fontSize: 25, color: Colors.red),)
    )
    
    
  );

  @override
  void initState() {
    _eventList.clear();
    setState(() {
      initEventCalendar();
    });
    super.initState();
    dateNow = DateTime.now();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  void dateClicked(date){
    setState(() {
      dateNow = date;      
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_eventList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _eventList.addAll(await eventApi.getEventList(context, page));
      if(_eventList == null){
      _eventList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initEventCalendar() async {
    setState(() {
      eventCalendarLoading = true;
    });
    
    _eventList = await eventApi.getEventList(context, 1);
    if(_eventList == null){
      _eventList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    _markedDateMap.clear();
    for(int i = 0 ; i<_eventList.length ; i++){
      _markedDateMap.add(
        DateTime(_eventList[i].startAt.year, _eventList[i].startAt.month, _eventList[i].startAt.day),
        calCarousel.Event(
          date: DateTime(_eventList[i].startAt.year, _eventList[i].startAt.month, _eventList[i].startAt.day),
          title: _eventList[i].name,
          icon: _eventIcon,
        )
      );
    }
    page++;

    setState(() {
      _eventList = _eventList;
      _markedDateMap =_markedDateMap;
      page = page;
      eventCalendarLoading = false;
    });
  }

  Color hasEvent(List<Event> event, DateTime date){
    for(int i = 0 ; i<event.length; i++){
      print("");
      if(event[i].startAt.day == date.day && event[i].startAt.month == date.month && event[i].startAt.year == date.year){
        return Colors.red;
      }
    }
    return Colors.white;
  }

  int countEvent(List<Event> event){
    _dateEventList.clear();
    int count = 0;
    for(int i = 0 ; i<event.length ; i++){
      if(event[i].startAt.day == dateNow.day && event[i].startAt.month == dateNow.month && event[i].startAt.year == dateNow.year){
        count++;
        _dateEventList.add(event[i]);
      }  
    }
    return count;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }
  
  Widget showEvents(Event event){
    if(event.startAt.day == dateNow.day && event.startAt.month == dateNow.month && event.startAt.year == dateNow.year){
      return Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 10, top: 20),
            width: 500.0,
            child: Text(event.name, style: TextStyle(fontSize: 16.0)),
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0),
            width: 500.0,
            child: Text(DateFormat('kk:mm').format(event.startAt) + " - " + DateFormat('kk:mm').format(event.endAt),
              style: TextStyle(fontSize: 15.0)),
          ),
          Container(
            margin: EdgeInsets.only(left: 10.0, bottom: 20.0),
            width: 500.0,
            child: event.place == "" ? null : Text(event.place, style: TextStyle(fontSize: 15.0)),
          ),
        ],
      );
    }else{
      return Text("No Event");
    }
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["eventcalendar"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: eventCalendarLoading ? Center(child: CircularProgressIndicator())
        : 
        Container(
          margin: EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 10.0,
          ),
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              // Calendar(
              //   onDateSelected: (date){
              //     dateClicked(date);
              //   },
              //   onSelectedRangeChange: (range) =>
              //       print("Range is ${range.item1}, ${range.item2}"),
              //   dayBuilder: (BuildContext context, DateTime day) {
              //     return  InkWell(
              //       onTap: (){dateClicked(day);},
              //       child: Container(
              //         decoration: BoxDecoration(
              //             border: Border.all(color: Colors.black38)),
              //         child: Row(
              //           mainAxisAlignment: MainAxisAlignment.center,
              //           children: <Widget>[
              //             Container(
              //               alignment: Alignment.center,
              //               margin: EdgeInsets.only(top: 5.0, left: 7.0),
              //               padding: EdgeInsets.only(left: 5),
              //               child: Text(day.day.toString()),
              //             ),
              //             Container(
              //               margin: EdgeInsets.only(left: 3.0),
              //               child: Text(".", style: TextStyle(fontSize: 50.0, color: hasEvent(_eventList, day))),
              //             )         
              //           ],
              //         )
              //       ),
              //     );
              //   },
              // ),
              CalendarCarousel(
                height: 350,
                onDayPressed: (DateTime date, List<calCarousel.Event> events){
                  this.setState(() => _currentDate = date);
                  dateNow = date;
                  print(events);
                },
                iconColor: Colors.black,
                weekdayTextStyle: TextStyle(color: Colors.black),
                weekendTextStyle: TextStyle(
                  color: Colors.black26,
                ),
                weekFormat: false,
                markedDatesMap: _markedDateMap,
                selectedDateTime: _currentDate,
                markedDateShowIcon: true,
                markedDateIconMaxShown: 1,
                todayTextStyle: TextStyle(
                  color: config.primaryTextColor
                ),
                selectedDayTextStyle: TextStyle(color: config.primaryTextColor),
                todayButtonColor: Colors.black26,
                selectedDayButtonColor: config.primaryColor,
                headerTextStyle: TextStyle(color: Colors.black, fontSize: 24),
                markedDateIconBuilder: (event) {
                  return Container(
                    padding: EdgeInsets.only(left: 22),
                    child: Text(".", style: TextStyle(fontSize: 25, color: config.primaryColor,))
                  );
                },
                markedDateMoreShowTotal: false,
                customGridViewPhysics: NeverScrollableScrollPhysics(),
                
              ),
              Container(
                height: 370.0,
                child: ListView.builder(
                  itemCount: countEvent(_eventList),
                  itemBuilder: (BuildContext context, int index){
                  return Container(
                    padding: EdgeInsets.all(5),
                    child: GestureDetector(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => EventDetail(mode:1, id:_dateEventList[index].id, model: _dateEventList[index]))
                        );
                      },
                      child: Column(
                        children: <Widget>[
                          showEvents(_dateEventList[index]),
                          Divider(),
                        ],
                      ),
                      
                    )
                    );
                  },  
                )
              ),
            ],
          ),
        ),
      )
    );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initEventCalendar();
    return null;
  }
  
}

  
