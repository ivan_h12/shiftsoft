//packages
import 'package:flutter/material.dart';
import 'dart:async';

//screens
import './login.dart';

//widget
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/tools/functions.dart';

// resources
import 'package:shiftsoft/resources/eventServicesApi.dart';

// models
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/models/meventservicestype.dart';
import 'package:shiftsoft/models/meventservicesskill.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EventServiceAdd extends StatefulWidget {
  int id;
  Event model;
  EventServiceAdd({Key key, this.id, this.model}) : super(key:key);
  @override
  _EventServiceAddState createState() => _EventServiceAddState();
}

class _EventServiceAddState extends State<EventServiceAdd> {
  String typeID, skillID, userID;

  Event event;

  List<DropdownMenuItem<String>> _dropDownMenuItems = [
    DropdownMenuItem(
      key: Key("0"),
      value: "0",
      child: Text("----")
    )
  ];
  List<DropdownMenuItem<String>> _dropDownMenuItems2 = [
    DropdownMenuItem(
      key: Key("0"),
      value: "0",
      child: Text("----")
    )
  ];
  List<DropdownMenuItem<String>> _dropDownMenuItems3 = [
    DropdownMenuItem(
      key: Key("0"),
      value: "0",
      child: Text("----")
    )
  ];
  String _current;
  String _current2;
  String _current3;
  
  bool eventServiceTypeLoading = true;
  bool eventServiceSkillLoading = true;
  bool eventServiceSkillUserLoading = true;
  bool eventServiceSubmitLoading = false;

  List<EventServiceType> _eventServiceTypeList = [];
  List<EventServiceSkill> _eventServiceSkillList = [];
  List<User> _eventServiceSkillUserList = [];

  List data2 = ["-"];

  Map<String, String> message = new Map();
  List<String> messageList = [
    "serviceadd", "type", "skill", "person", "submit",
    "serviceTypeEmpty", "serviceTypeChoose", "serviceSkillEmpty", "serviceSkillChoose", "serviceSkillUserEmpty",
    "alert", "loginFailed"
  ];

  @override
  void initState() {
    super.initState();
    event = widget.model;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
    
    if (eventServiceTypeLoading)
      initEventServiceTypeList();
  }

  initEventServiceTypeList() async{
    setState(() {
      eventServiceTypeLoading = true;
    });
    
    _eventServiceTypeList = await eventServicesApi.getEventServiceTypeList(context);
    if(_eventServiceTypeList == null){
      _eventServiceTypeList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    
    _dropDownMenuItems = [
      DropdownMenuItem(
        key: Key("0"),
        value: "0",
        child: Text("----")
      )
    ];
    _eventServiceTypeList.map((item) {
      _dropDownMenuItems.add(DropdownMenuItem(
        key: Key("${item.id}"),
        value: item.id.toString(),
        child: Text(item.name)
      ));
    }).toList();
    
    setState(() {
      _dropDownMenuItems = _dropDownMenuItems;
      _eventServiceTypeList = _eventServiceTypeList;
      eventServiceTypeLoading = false;
    });
  }

  initEventServiceSkillList() async{
    setState(() {
      eventServiceSkillLoading = true;
    });

    _eventServiceSkillList = await eventServicesApi.getEventServiceSkillList(context, typeID);
    if(_eventServiceSkillList == null){
      _eventServiceSkillList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    _dropDownMenuItems2 = [
      DropdownMenuItem(
        key: Key("0"),
        value: "0",
        child: Text("----")
      )
    ];
    _eventServiceSkillList.map((item) {
      _dropDownMenuItems2.add(DropdownMenuItem(
        key: Key("${item.id}"),
        value: item.id.toString(),
        child:Text(item.name)
      ));
    }).toList();
    
    setState(() {
      _dropDownMenuItems2 = _dropDownMenuItems2;
      _eventServiceSkillList = _eventServiceSkillList;
      eventServiceSkillLoading = false;
    });
  }

  initEventServiceSkillUserList() async{
    setState(() {
      eventServiceSkillUserLoading = true;
    });

    _eventServiceSkillUserList = await eventServicesApi.getEventServiceSkillUserList(context, skillID);
    if(_eventServiceSkillUserList == null){
      _eventServiceSkillUserList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    _dropDownMenuItems3 = [
      DropdownMenuItem(
        key: Key("0"),
        value: "0",
        child: Text("----")
      )
    ];
    _eventServiceSkillUserList.map((item) {
      _dropDownMenuItems3.add(DropdownMenuItem(
        key: Key("${item.id}"),
        value: item.id.toString(),
        child:Text(item.name)
      ));
    }).toList();
    
    setState(() {
      _dropDownMenuItems3 = _dropDownMenuItems3;
      _eventServiceSkillUserList = _eventServiceSkillUserList;
      eventServiceSkillUserLoading = false;
    });
  }

  void submitButton() async {
    setState(() {
      eventServiceSubmitLoading = true;      
    });
    
    Result result = await eventServicesApi.createEventService(context, widget.id.toString(), userID, _current2);

    setState(() {
      eventServiceSubmitLoading = false;      
    });
    
    if(result.success == -1){
      Alert(
        context: context,
        title: message["alert"],
        content: Text(message["loginFailed"]),
        cancel: false,
        defaultAction: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
        }
      );
    } else if (result.success == 1) {
      Alert(
        context: context,
        type: "success",
        title: message["alert"],
        content: Text(result.message),
        cancel: false,
        defaultAction: () {
          Navigator.pop(context, true);
        }
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["serviceadd"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 10),
            child: Text(message["type"], style: TextStyle(fontSize: 18.0, color: Colors.grey))
          ),

          _dropDownMenuItems.length == 1 && !eventServiceTypeLoading ?
          Center(
            child: Text(message["serviceTypeEmpty"])
          ) : Container(),

          eventServiceTypeLoading ?
          Center(
            child:CircularProgressIndicator()
          ) : Container(),
          
          _dropDownMenuItems.length > 1 ?
          Container(
            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
            child: DropdownButton(
              isExpanded: true,
              value: _current,
              items: _dropDownMenuItems,
              onChanged: (selected) {
                setState(() {
                  typeID = selected;
                  _current = selected;
                });
                if (selected != "0")
                  initEventServiceSkillList();
              }
            )
          ) : Container(),

          Container(
            margin:  EdgeInsets.only(left: 7.0, right: 7.0),
            child: Text(message["skill"], style: TextStyle(fontSize: 18.0, color: Colors.grey))
          ),
          
          _dropDownMenuItems2.length == 1 && eventServiceSkillLoading ?
          Center(
            child: Text(message["serviceTypeChoose"])
          ) : Container(),

          _dropDownMenuItems2.length == 1 && !eventServiceSkillLoading ?
          Center(
            child: Text(message["serviceSkillEmpty"])
          ) : Container(),

          eventServiceSkillLoading ?
          Center(
            child:CircularProgressIndicator()
          ) : Container(),

          _dropDownMenuItems2.length > 1 ?
          Container(
            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
            child: DropdownButton(
              isExpanded: true,
              value: _current2,
              items: _dropDownMenuItems2,
              onChanged: (selected) {
                setState(() {
                  skillID = selected;
                  _current2 = selected;
                });
                if (selected != "0")
                  initEventServiceSkillUserList();
              }
            )
          ) : Container(),

          Container(
            margin:  EdgeInsets.only(left: 7.0, right: 7.0),
            child: Text(message["person"], style: TextStyle(fontSize: 18.0, color: Colors.grey))
          ),

          _dropDownMenuItems3.length == 1 && eventServiceSkillUserLoading ?
          Center(
            child: Text(message["serviceSkillChoose"])
          ) : Container(),

          _dropDownMenuItems3.length == 1 && !eventServiceSkillUserLoading ?
          Center(
            child: Text(message["serviceSkillUserEmpty"])
          ) : Container(),

          eventServiceSkillUserLoading ?
          Center(
            child:CircularProgressIndicator()
          ) : Container(),

          _dropDownMenuItems3.length > 1 ?
          Container(
            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
            child: DropdownButton(
              isExpanded: true,
              value: _current3,
              items: _dropDownMenuItems3,
              onChanged: (selected) {
                print("asdasd");
                print(selected);
                setState(() {
                  userID = selected;
                  _current3 = selected;
                });
              }
            )
          ) : Container(),
          Container(
            margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0, top: 15),
            width: MediaQuery.of(context).size.width,
            height: 50.0,
            child: Button(
              child: Text(message["submit"], style: TextStyle(fontWeight: FontWeight.bold, color: config.primaryTextColor),),
              backgroundColor: config.primaryColor,
              rounded: true,
              loading: eventServiceSubmitLoading,
              onTap: _current3 != "0" ?
              () {
                submitButton();
              }
              :
              null
            ),
          )
        ],
      ),
    );
  }
}