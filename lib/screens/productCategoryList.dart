// packages
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productCategoryDetail.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/productCategoryApi.dart';

// models
import 'package:shiftsoft/models/mproductcategory.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductCategoryList extends StatefulWidget{
  List<ImageBox> listImageBox= <ImageBox>[
    ImageBox("https://via.placeholder.com/480x640", 10),
    ImageBox("https://via.placeholder.com/480x640",10),
    ImageBox("https://via.placeholder.com/480x640",10),
    
  ];
  createState(){
    return ProductCategoryListState();
  }
}

class ProductCategoryListState extends State<ProductCategoryList>{
  List<ProductCategory> _productCategoryList = [];
  bool productCategoryLoading = true;
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;
  Map<String, String> message = Map();
  List<String> messageList = ["shoppingOnShiftsoft",];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }
  @override
  void initState(){
    initProductCategoryList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_productCategoryList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _productCategoryList.addAll(await productCategoryApi.getProductCategoryList(context, page));
      if(_productCategoryList == null){
        _productCategoryList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        _productCategoryList = _productCategoryList;
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initProductCategoryList() async {
    setState(() {
      productCategoryLoading = true;
    });
    
    _productCategoryList = await productCategoryApi.getProductCategoryList(context, page);
    if(_productCategoryList == null){
      _productCategoryList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;

    setState(() {
      _productCategoryList = _productCategoryList;
      productCategoryLoading = false;
      page = page;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message["shoppingOnShiftsoft"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productCategoryLoading ? 
        CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                Container(
                  height: MediaQuery.of(context).size.width/16*9,
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.grey[350],
                    period: Duration(milliseconds: 800),  
                    child: Swiper(
                      itemBuilder: (BuildContext context,int index){
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey[200],
                        );
                      },
                      itemCount: 3,
                      viewportFraction: 0.4,
                      scale: 0.4 ,
                    ),
                  ),
                ),
                childCount: 1,
              ),
            ),
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 5,
                childAspectRatio: 0.8,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index){
                  return Center(
                    child: Column(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.local_grocery_store),
                        ),
                        Shimmer.fromColors(
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.grey[350],
                          period: Duration(milliseconds: 800),
                          child: Container(width: 50, height: 20, color: Colors.grey[200]),
                        ),
                      ],
                    ),
                  );
                },
                childCount: 4
              ),
            ),
          ],
        )
        :
        CustomScrollView(
          //controller: _controller,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                Container(
                  height: MediaQuery.of(context).size.width/16*9,
                  child: Swiper(
                    itemBuilder: (BuildContext context,int index){
                      return widget.listImageBox[index];
                    },
                    itemCount: 3,
                    viewportFraction: 0.4,
                    scale: 0.4 ,
                  ),
                ),
                childCount: 1,
              ),
            ),
            _productCategoryList.length+1 != 1 ?
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 5,
                childAspectRatio: 0.8,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index){
                  if(index < _productCategoryList.length){
                    return Center(
                      child: Column(
                        children: <Widget>[
                          IconButton(
                            key: Key("productCategory$index"),
                            icon: Icon(Icons.local_grocery_store),
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ProductCategoryDetail(mode: 1,myTitle: _productCategoryList[index].name, id: _productCategoryList[index].id)),
                              );
                            },
                          ),
                          Text(
                            _productCategoryList[index].name,
                            style: TextStyle(fontSize: 12),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    );
                  }else{
                    return Center(
                      child: Opacity(
                        opacity: isLoading ? 1.0 : 0.0,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                },
                childCount: _productCategoryList.length,
              ),
            )
            :
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                PlaceholderList(type: "productcategory",),
                childCount: 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<Null> _refresh() async {
    page = 1;
    await initProductCategoryList();
    return null;
  }
}