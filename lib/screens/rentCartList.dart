// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/cartBox.dart';

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';

class RentCartList extends StatefulWidget{
  createState(){
    return RentCartListState();
  }
}

class RentCartListState extends State<RentCartList>{
  final key = GlobalKey<ScaffoldState>();
  List<int> rentCartlistList = [];
  List<bool> checked= [];

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","addToCart","removeFromWishlist","rentBooks"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      rentCartlistList.add(1);
      checked.add(false);
      // message[item] = item;
    }).toList();
  }


  @override
  void _select(Choice choice) {
    if(choice.title == "Add To Cart"){
      print("hi");
      key.currentState.showSnackBar(SnackBar(content: Text(message['addToCart']+" index-" + choice.index.toString() )));
    }else if(choice.title == "Remove From Wishlist"){
      print("ho");
      key.currentState.showSnackBar(SnackBar(content: Text(message['removeFromWishlist']+" index-" + choice.index.toString() )));
    }
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: Text("Rent Cart List", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            onPressed: (){},
            icon: Icon(Icons.search),
          ),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: ListView.builder(
                    itemCount: rentCartlistList.length != 0 ? rentCartlistList.length : 1,
                    itemBuilder: (context,index){
                      if(rentCartlistList.length != 0){
                        return CartBox(
                          price: 15000,
                          type: 1,
                          count: rentCartlistList[index],
                          checked: checked[index],
                          imageUrl: "https://upload.wikimedia.org/wikipedia/en/thumb/a/aa/One_Piece_DVD_18.png/200px-One_Piece_DVD_18.png",
                          title: "One Piece",
                          imageSize: 100,
                          onChecked: (){
                            setState(() {
                              if (checked[index]){
                                  checked[index] = false;
                                } else {
                                  checked[index] = true;
                                }
                            });
                          },
                          onAdd: (){
                            setState(() {
                            rentCartlistList[index]++; 
                            });
                          },
                          onRemove: (){
                            setState(() {
                              if(rentCartlistList[index]>1){
                                rentCartlistList[index]--; 
                              }
                            });
                          },
                        );
                      } else {
                        return Center(
                          child: PlaceholderList(type: "rentcartlist",),
                        );
                      }
                    },
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.all(5),
                    color: Colors.grey[200],
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Text("TOTAL", style: TextStyle(color: Colors.grey),textAlign: TextAlign.start,),
                            Text(numberFormat(12000, config.currency), textAlign: TextAlign.right,)
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Button(
                                child: Text("Cancel", style: TextStyle(color: config.primaryTextColor),),
                                rounded: true, 
                                backgroundColor: config.primaryColor,
                                onTap: (){
                                  print("hallo");
                                },
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Button(
                                child: Text("Check Out", style: TextStyle(color: config.primaryTextColor),),
                                rounded: true, 
                                backgroundColor: config.primaryColor,
                                onTap: (){
                                  print("hallo");
                                },
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ), 
                )
              ],
            )
          ),
        ],
      ), 
    );
  }
}
class Choice {
  Choice({this.title, this.index});
  String title;
  int index;
}

List<Choice> choices = <Choice>[
  Choice(title: 'Add To Cart'),
  Choice(title: 'Remove From Wishlist'),
];