import 'package:flutter/material.dart';

// widgets
import '../widgets/profileBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/button.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';

class FosterChildList extends StatefulWidget {
  @override
  _FosterChildListState createState() => _FosterChildListState();
}

class _FosterChildListState extends State<FosterChildList> {
  List<String> _data = new List<String>();

  @override
  void initState(){
    setState(() {
      for(int i=0; i<10; i++){
        int a=i+1;
        _data.add("Devina Yolanda " + a.toString());
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Foster Child List", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              child: Expanded(
                child: SizedBox(
                  height: 800.0,
                  child: ListView.builder(
                    itemCount: _data.length == 0 ? 1 : _data.length,
                    itemBuilder: (BuildContext context, int index){
                      if(_data.length != 0){
                        return Hero(
                          tag: "profile$index",
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                onTap: (){
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(builder: (context) => ProfilePage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2wrkRP88liEF31395h2-dlrJ4Czwx3Y9UY6MrGlGu33I75vmB", "profile$index"))
                                  // );
                                  print("aasdasda");
                                },
                                contentPadding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 10.0),
                                leading: ProfileBox("https://via.placeholder.com/150", 75.0, _data[index],
                                  Row(
                                    children: <Widget>[
                                      Text("id : 26416006", style: TextStyle(fontSize: 15.0)),
                                    ],
                                  ),
                                ),
                              ),
                              Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(top: 5.0, bottom: 5.0, right: 5.0, left: 35.0),
                                    child: Button(
                                      child: Text("Laporan Karakter", style: TextStyle(fontSize: 8.0, color: config.primaryTextColor)),
                                      backgroundColor: config.primaryColor,
                                      onTap: (){
                                        // Navigator.push(
                                        //   context,
                                        //   MaterialPageRoute(builder: (context) => CommentList())
                                        // );
                                        print("a");
                                      }
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(5.0),
                                    child: Button(
                                      child: Text("Laporan Nilai", style: TextStyle(fontSize: 8.0, color: config.primaryTextColor)),
                                      backgroundColor: config.primaryColor,
                                      onTap: (){
                                        // Navigator.push(
                                        //   context,
                                        //   MaterialPageRoute(builder: (context) => CommentList())
                                        // );
                                        print("b");
                                      }
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(5.0),
                                    child: Button(
                                      child: Text("Laporan Donasi", style: TextStyle(fontSize: 8.0, color: config.primaryTextColor)),
                                      backgroundColor: config.primaryColor,
                                      onTap: (){
                                        // Navigator.push(
                                        //   context,
                                        //   MaterialPageRoute(builder: (context) => CommentList())
                                        // );
                                        print("c");
                                      }
                                    ),
                                  )
                                ],
                              )
                            ],  
                          ),
                        );
                      }else{
                        return PlaceholderList(type: "fosterchildlist");
                      }   
                    }
                  ),
                ),
              ),
            )
          ],
        )
      ),
    );
  }
}