//package
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

//screens
import 'eventAttendanceList.dart';
import 'eventServicesList.dart';
import 'eventForm.dart';
import './profilePage.dart';
import './eventAttendanceAdd.dart';
import './login.dart';

//widgets
import '../widgets/itemDetail.dart';
import '../widgets/imageBox.dart';
import '../widgets/button.dart';
import '../widgets/carousel.dart';
import 'package:shiftsoft/widgets/textField.dart';


// resources
import 'package:shiftsoft/resources/eventApi.dart';
import 'package:shiftsoft/resources/attendanceApi.dart';
import '../resources/eventServicesApi.dart';

// models
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/models/meventservices.dart';
import 'package:shiftsoft/models/mattendance.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/mresult.dart';

class EventDetail extends StatefulWidget {
  Event model;
  int mode, id;

  EventDetail({Key key, this.mode, this.id, this.model}) : super(key:key);

  createState(){
    return _EventDetailState();
  }
}

class _EventDetailState extends State<EventDetail> {
  List<Attendance> _attendanceList = [];
  List<EventService> _eventServicesList = [];
  List<Choice> choices = [];
  User user;

  Event eventDetail;
  bool eventDetailLoading = true;
  bool attendanceListLoading = true;
  bool eventServicesLoading = true;
  bool eventSubmitLoading = false;

  bool attendanceAddStatus = false;
  bool attendanceListStatus = false;
  bool editStatus = false;
  bool serviceStatus = false;
  bool serviceCreateStatus = false;

  bool boolAttendance = false;
  bool boolServices = false;

  bool checkAttendance = false;
  bool yesFill = false;
  bool noFill = false;
  bool maybeFill = false;

  final _formKey = GlobalKey<FormState>();
  final attendanceCountController = TextEditingController();
  final offeringController = TextEditingController();
  final envelopeController = TextEditingController();



  Map<String, String> message = new Map();
  List<String> messageList = ["eventdetail", "joined", "services", "attendancelist", "eventEdit", "add", "yes", 
  "no", "maybe", "joinedEvent", "submitEvent", "place", "offering", "attendanceCount", "envelope"];

  @override
  void initState(){
    super.initState();
    user = User(id:1926);
    if(widget.mode == 1){
      initEventDetail();
    }else if(widget.mode == 3){
      eventDetail = widget.model;
      setState(() {
        eventDetailLoading = false;
        eventDetail = eventDetail;
      });
    }
    initAttendanceList();    
    initEventServices();
  }
  
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    initAttendanceStatus();
  }

  initAttendanceStatus() async{
    attendanceAddStatus = await checkPermission(context, "master.event", "BackendAttendance.Absent", true);
    attendanceListStatus = await checkPermission(context, "master.event", "BackendAttendance.Index", true);
    editStatus = await checkPermission(context, "master.event", "BackendEvent.Update", true);
    serviceStatus = await checkPermission(context, "master.service", "BackendService.Index", true);
    serviceCreateStatus = await checkPermission(context, "master.service", "BackendService.Create", true);

    print("1 $attendanceAddStatus");
    print("2 $attendanceListStatus");
    print("3 $editStatus");
    print("4 $serviceStatus");
    print("5 $serviceCreateStatus");

    setState(() {
     attendanceAddStatus = attendanceAddStatus; 
     attendanceListStatus = attendanceListStatus;
     editStatus = editStatus;
     serviceStatus = serviceStatus;
     serviceCreateStatus = serviceCreateStatus;
    });

    if(serviceStatus) choices.add(Choice(title: message["services"]));
    if(attendanceListStatus) choices.add(Choice(title: message["attendancelist"]));
    if(editStatus) choices.add(Choice(title: message["eventEdit"]));
    
  }

  initEventDetail() async{
    setState((){
      eventDetailLoading = true;
    });

    eventDetail = await eventApi.getEvents(context, widget.id);
    if(eventDetail == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      eventDetail = eventDetail;
      eventDetailLoading = false;    
    });
  }

  initAttendanceList() async {
    setState(() {
      attendanceListLoading = true;
    });
    
    _attendanceList = await attendanceApi.getAttendanceList(context, widget.id);

    setState(() {
      _attendanceList = _attendanceList;
      attendanceListLoading = false;
    });

    checkUserAttendance();
  }

  initEventServices() async {
    setState(() {
      eventServicesLoading = true;
    });
    
    _eventServicesList = await eventServicesApi.getEventServicesList(context, widget.id);

    setState(() {
      _eventServicesList = _eventServicesList;
      eventServicesLoading = false;
    });
  }

  checkBool(){
    setState(() {
      if(boolAttendance){
        boolServices = false;
      } else if(boolServices){
        boolAttendance = false;
      }
    });
  } 

  checkUserAttendance(){
    for(int i=0; i<_attendanceList.length; i++){
      if(_attendanceList[i].id == user.id && _attendanceList[i].status == 2){
        checkAttendance = true;
      } else if (_attendanceList[i].id == user.id && _attendanceList[i].status == -1){
        yesFill = false;
        noFill = true;
        maybeFill = false;
        checkAttendance = false;
      } else if (_attendanceList[i].id == user.id && _attendanceList[i].status == 1){
        yesFill = true;
        noFill = false;
        maybeFill = false;
        checkAttendance = false;
      } else if (_attendanceList[i].id == user.id && _attendanceList[i].status == 0){
        yesFill = false;
        noFill = false;
        maybeFill = true;
        checkAttendance = false;
      }
    }
  }

  void submitEvent() async{
    setState(() {
      eventSubmitLoading = true;      
    });

    // String name = widget.model.name;
    // String place = widget.model.place;
    // String content = widget.model.content;
    String name = eventDetail.name;
    String place = eventDetail.place;
    String content = eventDetail.content;
    String gps = "asd";
    int status = 3;
    int attendanceCount = int.parse(attendanceCountController.text);
    int offering = int.parse(offeringController.text);
    int envelope = int.parse(envelopeController.text);

    String dateStart = (DateFormat('yyyy-MM-dd').format(widget.model.startAt)).toString();
    String dateEnd = (DateFormat('yyyy-MM-dd').format(widget.model.endAt)).toString();
    String timeStart = (DateFormat('kk:mm').format(widget.model.startAt)).toString();
    String timeEnd = (DateFormat('kk:mm').format(widget.model.endAt)).toString();

    String startAt = dateStart + "T" + timeStart + ":00";
    String endAt = dateEnd + "T" + timeEnd + ":00";
    
    Result result;
    result = await eventApi.submitEvent(context, widget.model.idEncrypt, name, place, content, offering, envelope, gps, status, startAt, endAt);
    
    setState(() {
      eventSubmitLoading = false;
    });

    if(result.success == -1) {
      Alert(
        context: context,
        title: message["alert"],
        content: Text(message["loginFailed"]),
        cancel: false,
        defaultAction: () {
          Navigator.pushNamed(context, "login");
        }
      );
    } else {
      Alert(
        context: context,
        type: "success",
        title: message["alert"],
        content: Text(result.message),
        cancel: false,
        defaultAction: () {
          Navigator.pop(context, true);
        }
      );
    }
      
  }

  void submitAttendance(int status) async {
    Result result = await attendanceApi.addAttendance(context, eventDetail.id.toString(), "${user.id}", status);
    
    if (result.success == 1) {
      if (status == -1) {
        yesFill = false;
        noFill = true;
        maybeFill = false;
        checkAttendance = false;
      } else if (status == 1) {
        yesFill = true;
        noFill = false;
        maybeFill = false;
        checkAttendance = false;
      } else if (status == 0) {
        yesFill = false;
        noFill = false;
        maybeFill = true;
        checkAttendance = false;
      }

      setState(() {
        yesFill = yesFill;
        noFill = noFill;
        maybeFill = maybeFill;
        checkAttendance = checkAttendance;
      });

    } 
  }


  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["eventdetail"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        child: eventDetailLoading ?
        Shimmer.fromColors(
          baseColor: Colors.grey[200],
          highlightColor: Colors.grey[350],
          period: Duration(milliseconds: 800),
          child: Container(
            margin: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Container(height: 150, width: 800, color: Colors.grey[200]),
                Row(
                  children: <Widget>[
                    Container(height: 30, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(bottom: 5.0, top: 10.0),),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 30, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(bottom: 5.0, top: 10.0),),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 30, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(top: 15.0),),
                  ],
                ),
              ],
            ),
          ),
        ) 
        :
        ListView(
          shrinkWrap: true,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.width/16*9,
              child: Carousel("event", eventDetail.pic.split(","), [], [], []),
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(eventDetail.name, style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
              margin: EdgeInsets.only(top: 7.0, left: 13),
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(message["place"] + " : ${eventDetail.place}", style: TextStyle(fontSize: 20.0)),
              margin: EdgeInsets.only(top: 3.0, left: 5),
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(eventDetail.content, style: TextStyle(fontSize: 20.0)),
              margin: EdgeInsets.only(top: 3.0, left: 5),
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text(DateFormat('EEEE, dd MMMM yyyy').format(eventDetail.startAt), style: TextStyle(color: Colors.black45, fontSize: 15.0, fontWeight: FontWeight.bold)),
              margin: EdgeInsets.only(top: 10.0, left: 13),
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: Text("From "+ DateFormat('kk:mm').format(eventDetail.startAt) +" to " + DateFormat('kk:mm').format(eventDetail.endAt), style: TextStyle(color: Colors.black45, fontSize: 15.0, fontWeight: FontWeight.bold)),
              margin: EdgeInsets.only(top: 3.0, left: 13),
            ),
            editStatus?
            Container(
              alignment: Alignment.center,
              child: Container(
                width: MediaQuery.of(context).size.width - 10,
                child: Button(
                  backgroundColor: config.primaryColor,
                  child: Text(message["eventEdit"], style: TextStyle(color: config.primaryColor)),
                  splashColor: Colors.blueGrey,
                  fill: false,
                  onTap: () async {
                    bool refresh = await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => EventForm(mode:3, id: widget.id, model: eventDetail))
                    );
                    
                    if(refresh ?? false){
                      initEventDetail();
                    }
                  },
                  rounded: true,
                ),
              ),
            ) : Container(),
            editStatus ?
            Container(
              margin: EdgeInsets.only(bottom: 5),
              alignment: Alignment.center,
              child: Container(
                width: MediaQuery.of(context).size.width - 10,
                child: Button(
                  child: Text(message["submitEvent"], style: TextStyle(color: config.primaryTextColor)),
                  backgroundColor: config.primaryColor,
                  splashColor: Colors.blueGrey,
                  onTap: () {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text(message["submitEvent"]),
                          content: Container(
                            height: 300,
                            child: ListView(
                              children: <Widget>[
                                Form(
                                key: _formKey,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(top: 15),
                                      child: Text(message["attendanceCount"] + ": ", style: TextStyle(color: Colors.grey),),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                                      child: TextFields(
                                        key: Key("attendanceCount"),
                                        controller: attendanceCountController,
                                        keyboardType: TextInputType.number,
                                        hintText: message["attendanceCount"],
                                        validator: (String value){
                                          if(value.isEmpty){
                                            return "Please Enter Attendance Count";
                                          }
                                        },
                                      )
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 15),
                                      child: Text(message["offering"] + ": ", style: TextStyle(color: Colors.grey),),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                                      child: TextFields(
                                        key: Key("offering"),
                                        controller: offeringController,
                                        keyboardType: TextInputType.number,
                                        hintText: message["offering"],
                                        validator: (String value){
                                          if(value.isEmpty){
                                            return "Please Enter Offering";
                                          }
                                        },
                                      )
                                    ),Container(
                                      margin: EdgeInsets.only(top: 15),
                                      child: Text(message["envelope"] + ": ", style: TextStyle(color: Colors.grey),),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                                      child: TextFields(
                                        key: Key("envelope"),
                                        controller: envelopeController,
                                        keyboardType: TextInputType.number,
                                        hintText: message["envelope"],
                                        validator: (String value){
                                          if(value.isEmpty){
                                            return "Please Enter Envelope";
                                          }
                                        },
                                      )
                                    ),
                                  ],
                                ),
                              ), 
                              ],
                            ),
                          ),
                          actions: <Widget>[
                            Button(
                              child: Text("Submit"),
                              rounded: true,
                              backgroundColor: config.primaryColor,
                              splashColor: Colors.blueGrey,
                              onTap: ()async{
                                submitEvent();
                              },
                            ),
                          ],
                        );
                      }
                    ); 
                  },
                  rounded: true,
                ),
              ),
            ) : Container(),
            eventDetail.rsvp == 2 ?
              checkAttendance?
              Container(
                margin: EdgeInsets.only(left: 13, bottom: 10),
                child: Text(message["joined"], style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold))
              )
              :
              Container(
                margin: EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(message["joinedEvent"], style: TextStyle(fontSize: 15.0)),
                      margin: EdgeInsets.only(left: 13, top: 10),
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.fromLTRB(8, 0, 3, 0),
                          width: MediaQuery.of(context).size.width / 3 - 10,
                          child: Button(
                            child: Text(message["yes"], style: TextStyle(color: yesFill? config.primaryTextColor : config.primaryColor)),
                            backgroundColor: config.primaryColor,
                            splashColor: Colors.blueGrey,
                            fill: yesFill,
                            onTap: (){
                              submitAttendance(1);
                            },
                            rounded: true,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(3, 0, 3, 0),
                          width: MediaQuery.of(context).size.width / 3 - 10,
                          child: Button(
                            child: Text(message["no"], style: TextStyle(color: noFill? config.primaryTextColor : config.primaryColor)),
                            backgroundColor: config.primaryColor,
                            splashColor: Colors.blueGrey,
                            fill: noFill,
                            onTap: (){
                              submitAttendance(-1);
                            },
                            rounded: true,
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(3, 0, 8, 0),
                          width: MediaQuery.of(context).size.width / 3 - 10,
                          child: Button(
                            child: Text(message["maybe"], style: TextStyle(color: maybeFill? config.primaryTextColor : config.primaryColor)),
                            backgroundColor: config.primaryColor,
                            splashColor: Colors.blueGrey,
                            fill: maybeFill,
                            onTap: (){
                              submitAttendance(0);
                            },
                            rounded: true,
                          ),
                        ),
                      ],
                    )
                  ]
                )
              )
            :
            Container(),

            attendanceListStatus ?
            ExpansionTile(
              onExpansionChanged: checkBool(),
              initiallyExpanded: boolAttendance,
              title: Text(message["attendancelist"] + " (${_attendanceList.length})", style: TextStyle(fontWeight: FontWeight.bold)),
              children: <Widget>[
                _attendanceList.length != 0?
                Container(
                  margin: EdgeInsets.only(left: 13, right: 13),
                  child: GridView.count(
                    childAspectRatio: 0.8,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    crossAxisCount: 4,
                    children: List<Widget>.generate(_attendanceList.length, (index) {
                      return GridTile( 
                        child: Hero(
                          tag: "profile1$index",
                          child: InkWell(
                            onTap:  (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ProfilePage(url: CompanyAsset('avatar', _attendanceList[index].pic), tagHero: "profile$index", idUser: _attendanceList[index].id, mode: 1,))
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.all(5.0),
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      width: 70.0,
                                      height: 70.0,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: NetworkImage(CompanyAsset("avatar", _attendanceList[index].pic)),
                                          fit: BoxFit.fill,
                                        ),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 4.0,
                                        )
                                      ),
                                    ),
                                    Container(
                                      width: 80.0,
                                      child: Text(_attendanceList[index].name, style: TextStyle(fontSize: 15.0), overflow: TextOverflow.ellipsis),
                                      margin: EdgeInsets.only(left: 5.0, top: 3.0),
                                    ),
                                  ],
                                ) 
                              ),
                            )
                          )
                        )
                      );
                    }),
                  )
                ) : Container(),

                attendanceAddStatus ?
                Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(bottom: 5),
                  child: Container(
                    width: MediaQuery.of(context).size.width - 10,
                    child: Button(
                      backgroundColor: config.primaryColor,
                      child: Text(message["add"], style: TextStyle(color: config.primaryColor)),
                      splashColor: Colors.blueGrey,
                      fill: false,
                      onTap: () async {
                        bool refresh = await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => EventAttendanceList(mode: 1, id: widget.id, model: eventDetail))
                        );
                        
                        if(refresh ?? false){
                          initAttendanceList();
                        }
                      },
                      rounded: true,
                    ),
                  ),
                ) : Container(),
              ],
            ) : Container(),

            serviceStatus ?
            ExpansionTile(
              onExpansionChanged: checkBool(),
              initiallyExpanded: boolServices,
              title: Text(message["services"] + " (${_eventServicesList.length})", style: TextStyle(fontWeight: FontWeight.bold)),
              children: <Widget>[
                _eventServicesList.length != 0?
                Container(
                  margin: EdgeInsets.only(left: 13, right: 13),
                  child: GridView.count(
                    childAspectRatio: 0.8,
                    shrinkWrap: true,
                    crossAxisCount: 4,
                    physics: NeverScrollableScrollPhysics(),
                    children: List<Widget>.generate(_eventServicesList.length, (index) {
                      return GridTile( 
                        child: Hero(
                          tag: "profile2$index",
                          child: InkWell(
                            onTap:  (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ProfilePage(url: CompanyAsset('avatar', _eventServicesList[index].user.pic), tagHero: "profile$index", idUser: _attendanceList[index].id, mode: 1,))
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.all(5.0),
                              child: Container(
                                child: Column(
                                  children: <Widget>[
                                    Container(
                                      width: 70.0,
                                      height: 70.0,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: NetworkImage(CompanyAsset("avatar", _eventServicesList[index].user.pic)),
                                          fit: BoxFit.fill,
                                        ),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 4.0,
                                        )
                                      ),
                                    ),
                                    Container(
                                      width: 80.0,
                                      child: Text(_eventServicesList[index].user.name, style: TextStyle(fontSize: 15.0), overflow: TextOverflow.ellipsis),
                                      margin: EdgeInsets.only(left: 5.0, top: 3.0),
                                    ),
                                  ],
                                ) 
                              ),
                            )
                          )
                        )
                      );
                    }),
                  )
                ) : Container(),

                serviceCreateStatus ?
                Container(
                  alignment: Alignment.center,
                  child: Container(
                    width: MediaQuery.of(context).size.width - 10,
                    child: Button(
                      backgroundColor: config.primaryColor,
                      child: Text(message["services"], style: TextStyle(color: config.primaryColor)),
                      splashColor: Colors.blueGrey,
                      fill: false,
                      onTap: () async {
                        bool refresh = await Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => EventServicesList(mode: 1, id: widget.id, model: eventDetail))
                        );

                        if(refresh ?? false){
                          initEventServices();
                        }
                      },
                      rounded: true,
                    ),
                  ),
                ) : Container()
              ],
            ) : Container()
          ]
        )
      )
    );
  }
}

class Choice {
  const Choice({this.title});
  final String title;
}