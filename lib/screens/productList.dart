// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shimmer/shimmer.dart';
import 'package:material_search/material_search.dart';

// screens
import 'package:shiftsoft/screens/productDetail.dart';
import 'package:shiftsoft/screens/SortProduct.dart';
import 'package:shiftsoft/screens/productFilter.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/productApi.dart';

// models
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductList extends StatefulWidget{
  String title;
  int id;

  List <String> listTabBar = ["Produk", "Katalog"];
  createState(){
    return ProductListState();
  }
  ProductList({this.title,this.id});
}

class ProductListState extends State<ProductList>{
  int mCurrentIndex = 0;
  int counter=1;
  Widget _icon = Icon(Icons.grid_on); 
  Widget _title = Text("Grid 4");
  List<Product> _productList = [];
  bool productListLoading = true;
  List<String> _names =  [];
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void initState(){
    initProductList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_productList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }
  
  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _productList.addAll(await productApi.getProductList(context, widget.id, page));
      if(_productList == null){
        _productList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message['loginFailed']),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;
      _names.clear();
      for(int i=0; i<_productList.length; i++){
        _names.add(_productList[i].name);
      }

      setState(() {
        _productList = _productList;
        isLoading = false;
        page = page;
        _names = _names;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initProductList([String filter =""]) async {
    setState(() {
      productListLoading = true;
    });
    
    _productList = await productApi.getProductList(context, widget.id, page, filter);
    if(_productList == null){
      _productList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;
    _names.clear();
    for(int i=0; i<_productList.length; i++){
      _names.add(_productList[i].name);
    }

    setState(() {
      page = page;
      _productList = _productList;
      _names = _names;
      productListLoading = false;
    });
  }

  initSearchList([String filter =""]) async {
    _productList = await productApi.getProductSearchList(context, widget.id, filter);
    if(_productList == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    // page++;
    _names.clear();
    for(int i=0; i<_productList.length; i++){
      _names.add(_productList[i].name);
    }
    setState(() {
    //  page= page;
     _names = _names;
     _productList = _productList; 
    });
  }

  void onTabTapped(int index) async {
    setState(() {
      mCurrentIndex = index;
    });

    if(mCurrentIndex == 0){
      Navigator.push(context, MaterialPageRoute(builder: (context) => SortProduct() ));
    }
    else if(mCurrentIndex == 1){
      final filter = await Navigator.push(context, MaterialPageRoute(builder: (context) => ProductFilter() ));
      initProductList(filter);
    } else if(mCurrentIndex == 2){
      if ( counter == 1 ) {
        page = 1;
        _icon = Icon(Icons.grid_on);
        _title = Text("1 Column");
        counter = 2;
      } else if ( counter == 2 ) {
        page = 1;
        _icon = Icon(Icons.list);
        _title = Text("List");
        counter = 3;
      } else {
        page = 1;
        _icon = Icon(Icons.grid_on);
        _title = Text("2 Column");
        counter = 1;
      }
    }
  }

  String _name = 'No one';

  final _formKey = new GlobalKey<FormState>();

  _buildMaterialSearchPage(BuildContext context) {
    initSearchList();
    return new MaterialPageRoute<String>(
      settings: new RouteSettings(
        name: 'material_search',
        isInitialRoute: false,
      ),
      builder: (BuildContext context) {
        return new Material(
          child: new MaterialSearch<String>(
            placeholder: 'Search',
            results: _names.map((String v) => new MaterialSearchResult<String>(
              value: v,
              text: "$v",
            )).toList(),
            filter: (dynamic value, String criteria) {
              return value.toLowerCase().trim()
                .contains(new RegExp(r'' + criteria.toLowerCase().trim() + ''));
            },
            onSelect: (dynamic value){
              Navigator.pop(context, "&filters[]=name|like|%$value%|and");
              initSearchList("&filters[]=name|like|%$value%|and");
            },
            // onSubmit: (String value) => Navigator.of(context).pop(value),
            onSubmit: (String value){
              Navigator.pop(context, "&filters[]=name|like|%$value%|and");
              initSearchList("&filters[]=name|like|%$value%|and");
            },
          ),
        );
      }
    );
  }

  _showMaterialSearch(BuildContext context) {
    Navigator.of(context)
      .push(_buildMaterialSearchPage(context))
      .then((dynamic value) {
        setState(() => _name = value as String);
      });
  }
  
  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
          backgroundColor: config.topBarBackgroundColor,
          iconTheme: IconThemeData(
            color: config.topBarIconColor
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: (){
                Navigator.pushNamed(context, "productCartList");
              },
            ),
            IconButton(
              icon: Icon(Icons.search),
              onPressed: (){
                _showMaterialSearch(context);
              },
            )
          ],
          bottom: TabBar(
            indicatorColor: config.topBarIconColor,
            tabs: List<Widget>.generate(2, (int index){
                return Tab(child: Text(widget.listTabBar[index]));
              },
            ),
          ),
        ),
        body: TabBarView(
          children: List<Widget>.generate(2, (int index){
            if ( counter == 1 ) {
              return RefreshIndicator(
                onRefresh: _refresh,
                child: productListLoading ?
                CustomScrollView(
                  slivers: <Widget>[
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) => 
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey[200],
                        ),
                      ),
                      childCount: 1,
                      ),
                    ),
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 5.0,
                        childAspectRatio: 0.8,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                          return Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Center(
                                  child: Container(
                                    width: 80,
                                    height: 90,
                                    color: Colors.grey[200],            
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                ),
                                Container(
                                  width: 150,
                                  height: 40,
                                  color: Colors.grey[200],            
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                ),
                                Container(
                                  width: 80,
                                  height: 20,
                                  color: Colors.grey[200],            
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                ),
                                Container(
                                  width: 80,
                                  height: 20,
                                  color: Colors.grey[200],            
                                ),
                              ],
                            ),
                          );
                        },
                        childCount: 4
                      ),
                    )
                  ],
                )
                :
                CustomScrollView(
                  //controller: _controller,
                  slivers: <Widget>[
                    // SliverList(
                    //   delegate: SliverChildBuilderDelegate((context, index) => 
                    //   Container(
                    //     width: MediaQuery.of(context).size.width,
                    //     decoration: BoxDecoration(color: Colors.grey),
                    //     child: Text(_productList.length.toString()),
                    //   ),
                    //   childCount: 1,
                    //   ),
                    // ),
                    _productList.length+1 != 1 ?
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 5.0,
                        childAspectRatio: 0.65,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                          return index < _productList.length ?
                          InkWell(
                            key: Key("productDetail$index"),
                            child: Card(
                              elevation: 10,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Container(
                                child: Stack(
                                  children: [
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        ImageBox(CompanyAsset("product", _productList[index].pic), 175),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical:5, horizontal: 5),
                                          child: Text(
                                            _productList[index].name,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical:0.1, horizontal: 5),
                                          child: Text(
                                            numberFormat(_productList[index].price, config.currency),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(color: Colors.orange),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.symmetric(vertical:0.1, horizontal: 5),
                                          child: Text(
                                            "Jakarta",
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: TextStyle(fontSize: 12, color: Colors.grey)
                                          ),
                                        ),
                                      ],
                                    ),
                                    Positioned(
                                      bottom: 10,
                                      left: 5,
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.star, size: 16, color: Colors.orange,),
                                          Icon(Icons.star, size: 16, color: Colors.orange,),
                                          Icon(Icons.star, size: 16, color: Colors.orange,),
                                          Icon(Icons.star, size: 16, color: Colors.orange,),
                                          Icon(Icons.star, size: 16, color: Colors.orange,),
                                        ],
                                      )
                                    ),
                                  ]
                                )
                              ),
                            ),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetail(mode: 1, id:_productList[index].id, ) ));
                            }
                          )
                          :
                          Center(
                            child: Opacity(
                              opacity: isLoading ? 1.0 : 0.0,
                              child: CircularProgressIndicator(),
                            ),
                          );
                        },
                        childCount: _productList.length,
                      ),
                    )
                    :
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) => 
                      PlaceholderList(type: "productlist",),
                      childCount: 1,
                      ),
                    ),
                  ],
                )
              );
            } else if ( counter == 2 ) {
              return RefreshIndicator(
                onRefresh: _refresh,
                child: productListLoading ?
                  CustomScrollView(
                  slivers: <Widget>[
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) => 
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey[200],
                        ),
                      ),
                      childCount: 1,
                      ),    
                    ),
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 5.0,
                        childAspectRatio: 0.8,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                          return Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Center(
                                    child: Container(
                                      width: 300,
                                      height: 300,
                                      color: Colors.grey[200],            
                                    )
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                  ),
                                  ListTile(
                                    leading: Container(width: 200, height: 50, color: Colors.grey[200]),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(top: 10),
                                  ),
                                  ListTile(
                                    leading: Container(width: 50, height: 20, color: Colors.grey[200]),
                                    trailing: Container(width: 50, height: 20, color: Colors.grey[200]), 
                                  ),
                                ],
                              ),
                            )
                          );
                        },
                        childCount: 1
                      ),
                    )
                  ],
                )
                :
                CustomScrollView(
                  //controller: _controller,
                  slivers: <Widget>[
                    // SliverList(
                    //   delegate: SliverChildBuilderDelegate((context, index) => 
                    //   Container(
                    //     width: MediaQuery.of(context).size.width,
                    //     decoration: BoxDecoration(color: Colors.grey),
                    //     child: Text(_productList.length.toString()),
                    //   ),
                    //   childCount: 1,
                    //   ),
                    // ),
                    _productList.length+1 != 1 ?
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 5.0,
                        childAspectRatio: 0.8,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                          return index < _productList.length ? 
                          InkWell(
                            key: Key("productDetail$index"),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetail(mode: 1, id:_productList[index].id, ) ));
                            },
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Center(
                                    child: ImageBox(CompanyAsset("product", _productList[index].pic), 300),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 17, top: 7.5),
                                    child: Text(_productList[index].name,),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 17),
                                    child: Text(numberFormat(_productList[index].price, config.currency), style: TextStyle(color: Colors.orange),),
                                  ),
                                  ListTile(
                                    leading: Text("Jakarta", style: TextStyle(fontSize: 11, color: Colors.grey),),
                                    trailing: Row(        
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        Icon(Icons.star, size: 16, color: Colors.orange,),
                                        Icon(Icons.star, size: 16, color: Colors.orange,),
                                        Icon(Icons.star, size: 16, color: Colors.orange,),
                                        Icon(Icons.star, size: 16, color: Colors.orange,),
                                        Icon(Icons.star, size: 16, color: Colors.orange,),
                                      ],
                                    ), 
                                  ),      
                                ],
                              ),
                            ),
                          )
                          :
                          Center(
                            child: Opacity(
                              opacity: isLoading ? 1.0 : 0.0,
                              child: CircularProgressIndicator(),
                            ),
                          );
                        },
                        childCount: _productList.length
                      ),
                    )
                    :
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) => 
                      PlaceholderList(type: "productlist",),
                      childCount: 1,
                      ),
                    ),
                  ],
                )
              );
            } else {
              return RefreshIndicator(
                onRefresh: _refresh,
                child: productListLoading ?
                  CustomScrollView(
                  slivers: <Widget>[
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) => 
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey[200],
                        ),
                      ),
                      childCount: 1,
                      ),    
                    ),
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 0.2,
                        childAspectRatio: 3,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                          return Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                ImageBox(CompanyAsset("product", _productList[index].pic), 100),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(width: 100, height: 10, color: Colors.grey[200],),
                                    Padding(padding: EdgeInsets.only(top: 5),),
                                    Container(width: 100, height: 10, color: Colors.grey[200],),
                                    Padding(padding: EdgeInsets.only(top: 5),),
                                    Container(width: 100, height: 10, color: Colors.grey[200],),
                                    Padding(padding: EdgeInsets.only(top: 5),),
                                    Container(width: 100, height: 10, color: Colors.grey[200],), 
                                  ],
                                )
                              ],
                            )
                          );
                        },
                        childCount: 1
                      ),
                    )
                  ],
                )
                :
                CustomScrollView(
                  //controller: _controller,
                  slivers: <Widget>[
                    // SliverList(
                    //   delegate: SliverChildBuilderDelegate((context, index) => 
                    //   Container(
                    //     width: MediaQuery.of(context).size.width,
                    //     decoration: BoxDecoration(color: Colors.grey),
                    //     child: Text(_productList.length.toString()),
                    //   ),
                    //   childCount: 1,
                    //   ),
                    // ),
                    _productList.length+1 != 1 ?
                    SliverGrid(
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 1,
                        mainAxisSpacing: 0.2,
                        childAspectRatio: 3,
                      ),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index){
                          return index < _productList.length ? 
                          InkWell(
                            key: Key("productDetail$index"),
                            child: Container(
                              padding: EdgeInsets.only(left: 5, top: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: ImageBox(CompanyAsset("product", _productList[index].pic), 100, fit: BoxFit.cover, border: false),
                                    padding: EdgeInsets.all(5.0),
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(left: 10, top: 15),
                                        child: Text(_productList[index].name, overflow: TextOverflow.ellipsis, maxLines: 2),
                                        width: 200,
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 10, top: 5),
                                        child: Text(numberFormat(_productList[index].price, config.currency), style: TextStyle(color: Colors.orange))
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 10, top: 5),
                                        child: Text("Jakarta", style: TextStyle(fontSize: 12, color: Colors.grey))
                                      ),
                                      Container(
                                        padding: EdgeInsets.only(left: 10, top: 5),
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.star, size: 16, color: Colors.orange,),
                                            Icon(Icons.star, size: 16, color: Colors.orange,),
                                            Icon(Icons.star, size: 16, color: Colors.orange,),
                                            Icon(Icons.star, size: 16, color: Colors.orange,),
                                            Icon(Icons.star, size: 16, color: Colors.orange,),
                                          ],
                                        )
                                      )
                                    ],
                                  )
                                ]
                              )
                            ),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetail(mode: 1, id:_productList[index].id, ) ));
                            },
                          )
                          // InkWell(
                          //   key: Key("productDetail$index"),
                          //   onTap: (){
                          //     Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetail(mode: 1, id:_productList[index].id, ) ));
                          //   },
                          //   child: Row(
                          //       crossAxisAlignment: CrossAxisAlignment.start,
                          //       children: <Widget>[
                          //         ImageBox(CompanyAsset("product", _productList[index].pic), 100),
                          //         Column(
                          //           crossAxisAlignment: CrossAxisAlignment.start,
                          //           children: <Widget>[
                          //             Text(_productList[index].name),
                          //             Text(_productList[index].price.toString()),
                          //             Text("Jakarta"),
                          //             Row(              
                          //               children: <Widget>[
                          //                 Icon(Icons.star, size: 16, color: Colors.orange,),
                          //                 Icon(Icons.star, size: 16, color: Colors.orange,),
                          //                 Icon(Icons.star, size: 16, color: Colors.orange,),
                          //                 Icon(Icons.star, size: 16, color: Colors.orange,),
                          //                 Icon(Icons.star, size: 16, color: Colors.orange,),
                          //                 Text("(30)")
                          //               ],
                          //             ), 
                          //           ],
                          //         )
                          //       ],
                          //     ),
                          // )
                          :
                          Center(
                            child: Opacity(
                              opacity: isLoading ? 1.0 : 0.0,
                              child: CircularProgressIndicator(),
                            ),
                          );
                          ;
                        },
                        childCount: _productList.length
                      ),
                    )
                    :
                    SliverList(
                      delegate: SliverChildBuilderDelegate((context, index) => 
                      PlaceholderList(type: "productlist",),
                      childCount: 1,
                      ),
                    ),
                  ],
                )
              );
            }   
          })
        )
        ,
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: mCurrentIndex,
          type: BottomNavigationBarType.fixed,
          onTap: onTabTapped,
          items: [
            BottomNavigationBarItem(
              backgroundColor: Colors.blue,
              icon: Icon(Icons.sort),
              title: Text("Sort"),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.filter),
              title: Text("Filters")
            ),
            BottomNavigationBarItem(
              icon: _icon,
              title: _title
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.category),
              title: Text("Category")
            ),
          ],
        ),
      ),
    ); 
  }

  Future<Null> _refresh() async {
    page = 1;
    await initProductList();
    return null;
  }
}