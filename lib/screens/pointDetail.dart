// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/pointApi.dart';

// models
import 'package:shiftsoft/models/mpoint.dart';
import 'package:shiftsoft/settings/configuration.dart';

class PointDetail extends StatefulWidget{
  int  mode;
  PointHeader model;
  String title;
  createState(){
    return PointDetailState();
  }

  PointDetail({
    Key key,
    this.mode,
    this.model,
    this.title
  }) : super(key:key);
}

class PointDetailState extends State<PointDetail>{
  PointHeader _pointHeader, tempPointHeader;
  final _formKey = GlobalKey<FormState>();
  bool pointListLoading = false;
  String date1, date2;
  var tempString, tempSplit;
  DateTime _date = DateTime.now();
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;

  Map<String, String> message = Map();
  List<String> messageList = ["startDate","endDate","filter", "totalPoint", "currentPoint", "history"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        date1 = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }
  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        date2 = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }

  @override
  void initState(){
    super.initState();
    date1 = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();
    date2 = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();
    if(widget.mode == 1){
      initPointList();
    }
    else if(widget.mode == 3){
      _pointHeader = widget.model;
      setState(() {
        pointListLoading = false;
        _pointHeader = _pointHeader;
      });
    }
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_pointHeader.pointList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }
  
  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      tempPointHeader = await pointApi.getPointList(context, 1926, page);
      if(tempPointHeader == null){
        tempPointHeader = null;
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }else{
        _pointHeader.pointList.addAll(tempPointHeader.pointList);
        await makeAnimation();
        page++;
      }

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initPointList([String filter=""]) async {
    setState(() {
      pointListLoading = true;
    });
    
    _pointHeader = await pointApi.getPointList(context, 1926, page, filter);
    if(_pointHeader == null){
      _pointHeader = null;
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    } else{
      page++;
    }

    setState(() {
      _pointHeader = _pointHeader;
      pointListLoading = false;
      page = page;
    });
  }

  ScrollController _scrollController = new ScrollController();

  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.sort),
            color: Colors.white,
            onPressed: () {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text(message["filter"]),
                    content: Container(
                      height: 175,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text(message["startDate"] + ": ", style: TextStyle(color: Colors.grey),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                              child: InkWell(
                                onTap: (){ _selectDate(context); },
                                child: TextFields(
                                  type: 2,
                                  date: "${date1}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text(message["endDate"] + ": ", style: TextStyle(color: Colors.grey),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                              child: InkWell(
                                onTap: (){ _selectDate2(context); },
                                child: TextFields(
                                  type: 2,
                                  date: "${date2}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ), 
                    ),
                    actions: <Widget>[
                      Button(
                        child: Text("OK"),
                        rounded: true,
                        backgroundColor: config.primaryColor,
                        splashColor: Colors.blueGrey,
                        onTap: ()async{
                          Navigator.pop(context);
                          initPointList("filters[]=created_at|between|$date1,$date2 23:59:59|and");
                        },
                      ),
                    ],
                  );
                }
              );              
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: pointListLoading ?
        CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 20, bottom: 10),
                                  child: Text(message["totalPoint"], style: TextStyle(color: Colors.grey)),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(width: 100, height: 50, color: Colors.grey[200]),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 20, bottom: 10),
                                  child: Text(message["currentPoint"], style: TextStyle(color: Colors.grey)),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(width: 100, height: 50, color: Colors.grey[200]),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Center(
                          child: Text(message["history"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
                        ),
                      ),
                    ],
                  ),
                ),
                childCount: 1
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  padding: EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(width: 100, height: 25, color: Colors.grey[200]),
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(
                                width: 25, 
                                height: 25, 
                                color: Colors.grey[200],
                                margin: EdgeInsets.only(left: 15),
                                padding: EdgeInsets.only(left: 15),
                              ),
                            ),
                          )
                        ],
                      ),
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Container(
                          width: 75,
                          height: 25,
                          color: Colors.grey[200],
                          margin: EdgeInsets.only(top: 15),
                        ),
                      ),
                      Divider(),
                    ],
                  ),
                ),
                childCount: 5,
              ),
            ),
          ],
        )
        :
        CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 20, bottom: 10),
                                  child: Text(message["totalPoint"], style: TextStyle(color: Colors.grey)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: Text(_pointHeader.pointList.length == 0 ? "0" : _pointHeader.totalPoint.toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            flex: 1,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(top: 20, bottom: 10),
                                  child: Text(message["currentPoint"], style: TextStyle(color: Colors.grey)),
                                ),
                                Container(
                                  padding: EdgeInsets.only(bottom: 15),
                                  child: Text(_pointHeader.pointList.length == 0 ? "0" : _pointHeader.currentPoint.toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 40),),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                      Container(
                        padding: EdgeInsets.only(top: 10, bottom: 10),
                        child: Center(
                          child: Text(message["history"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),),
                        ),
                      ),
                    ],
                  ),
                ),
                childCount: 1
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                _pointHeader.pointList.length+1 !=1  ?
                  index < _pointHeader.pointList.length ? 
                  Container(
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Text(_pointHeader.pointList[index].roleName, style: TextStyle(fontWeight: FontWeight.bold),),
                            ),
                            Expanded(
                              flex: 1,
                              child: Row(
                                children: <Widget>[
                                  Icon(Icons.add, color: Colors.green,size: 20,),
                                  Text(_pointHeader.pointList[index].point.toString(), style: TextStyle(color: Colors.green, fontSize: 20),)
                                ],
                              ),
                            )
                          ],
                        ),
                        Text(DateFormat("dd MMM yyyy").format(_pointHeader.pointList[index].createdAt), style: TextStyle(color: Colors.grey,),),
                        Divider(),
                      ],
                    ),
                  )
                  :
                  Center(
                    child: Opacity(
                      opacity: isLoading ? 1.0 : 0.0,
                      child: CircularProgressIndicator(),
                    ),
                  )
                :
                PlaceholderList(type: "point",),
                childCount: _pointHeader.pointList.length +1 != 1 ? _pointHeader.pointList.length+1 : 1,
              ),
            ),
          ],
        ),
      ),
    );
  }
  Future<Null> _refresh() async {
    page = 1;
    await initPointList();
    return null;
  }
}