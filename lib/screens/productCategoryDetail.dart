// packages
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productList.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/carousel.dart';

// resources
import 'package:shiftsoft/resources/productCategoryApi.dart';

// models
import 'package:shiftsoft/models/mproductcategory.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductCategoryDetail extends StatefulWidget{
  List<ImageBox> listImageBox= <ImageBox>[
    ImageBox("https://via.placeholder.com/1280x720", 10),
    ImageBox("https://via.placeholder.com/1280x720",10),
    ImageBox("https://via.placeholder.com/1280x720",10),
  ];
  List<String> linkList = [
    "https://via.placeholder.com/1280x720",
    "https://via.placeholder.com/1280x720",
    "https://via.placeholder.com/1280x720",
  ];
  List<String> listNamaBarang = [
    "HANDPHONE",
    "POWER BANK",
    "SMARTWATCH",
    "TABLET",
    "AKSESORIS HANDPHONE", "AKSESORIS TABLET", "MEMORY", "KABEL",
    "ADAPTER"
  ];

  String myTitle;
  int mode, id;

  createState(){
    return ProductCategoryDetailState();
  }
  ProductCategoryDetail({Key key, this.myTitle, this.id, this.mode}) : super(key:key);
}

class ProductCategoryDetailState extends State<ProductCategoryDetail>{
  List<ProductCategory> _productCategoryDetailList = [];
  bool productCategoryDetailLoading = true;
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;

  void initState(){
    super.initState();
    if(widget.mode == 1){
      initProductCategoryListDetail();
    }
    else if(widget.mode == 2){

    }
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_productCategoryDetailList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      _productCategoryDetailList.addAll(await productCategoryApi.getProductCategoryDetailList(context, widget.id, page));
      if(_productCategoryDetailList == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        _productCategoryDetailList = _productCategoryDetailList;
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initProductCategoryListDetail() async {
    setState(() {
      productCategoryDetailLoading = true;
    });
    
    _productCategoryDetailList = await productCategoryApi.getProductCategoryDetailList(context, widget.id, page);
    if(_productCategoryDetailList == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;

    setState(() {
      page = page;
      _productCategoryDetailList = _productCategoryDetailList;
      productCategoryDetailLoading = false;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.myTitle, 
          style: TextStyle(color: config.topBarTextColor),
        ),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            color: Colors.white,
            icon: Icon(Icons.search),
            onPressed: (){print("Search");},
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productCategoryDetailLoading ?
        CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                height: MediaQuery.of(context).size.width/16*9,
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: Carousel("", widget.linkList, [], [], []),
                ),
              ),
              childCount: 1,
              ),
            ),
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 5,
                childAspectRatio: 0.8,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index){
                  return Center(
                    child: Column(
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.local_grocery_store),
                        ),
                        Shimmer.fromColors(
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.grey[350],
                          period: Duration(milliseconds: 800),
                          child: Container(width: 50, height: 20, color: Colors.grey[200]),
                        ),
                      ],
                    ),
                  );
                },
                childCount: 4
              ),
            ),
            // SliverGrid(
            //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //     crossAxisCount: 3,
            //     mainAxisSpacing: 5.0,
            //     childAspectRatio: 0.8,
            //   ),
            //   delegate: SliverChildBuilderDelegate(
            //     (BuildContext context, int index){
            //       return GridTile(
            //         child: GestureDetector(
            //           child: Card(
            //             color: Colors.white,
            //             child: Center(
            //               child: Column(
            //                 children: <Widget>[
            //                   IconButton(
            //                     icon: Icon(Icons.local_grocery_store),
            //                   ),
            //                   Shimmer.fromColors(
            //                     baseColor: Colors.grey[200],
            //                     highlightColor: Colors.grey[350],
            //                     period: Duration(milliseconds: 800),
            //                     child: Container(width: 50, height: 20, color: Colors.grey[200]),
            //                   ),
            //                 ],
            //               ),
            //             ),
            //           ),
            //         ),
            //       );
            //     },
            //     childCount: 3,
            //   ),
            // )
          ],
        )
        :
        CustomScrollView(
          //controller: _controller,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                height: MediaQuery.of(context).size.width/16*9,
                child: Carousel("", widget.linkList, [], [], []),
              ),
              childCount: 1,
              ),
            ),
            _productCategoryDetailList.length+1 != 1 ?
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisSpacing: 5.0,
                childAspectRatio: 1,
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index){
                  if(index < _productCategoryDetailList.length){
                    return InkWell(
                      key: Key("productList$index"),
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ProductList(title: _productCategoryDetailList[index].name,id:_productCategoryDetailList[index].id)),
                        );
                      },
                      child: Container(
                        padding: EdgeInsets.only(top: 25),
                        child: Center(
                          child: Column(
                            children: <Widget>[
                              Icon(Icons.local_grocery_store),
                              Text(
                                _productCategoryDetailList[index].name,
                                style: TextStyle(fontSize: 12, ),
                                textAlign: TextAlign.center,   
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                    // return GridTile(
                    //   child: GestureDetector(
                    //     key: Key("productList$index"),
                    //     onTap: (){
                    //       Navigator.push(
                    //         context,
                    //         MaterialPageRoute(builder: (context) => ProductList(title: _productCategoryDetailList[index].name,id:_productCategoryDetailList[index].id)),
                    //       );
                    //     },
                    //     child: Card(
                    //       color: Colors.white,
                    //       child: Center(
                    //         child: Column(
                    //           children: <Widget>[
                    //             IconButton(
                    //               icon: Icon(Icons.local_grocery_store),
                    //             ),
                    //             Text(
                    //               _productCategoryDetailList[index].name,
                    //               style: TextStyle(fontSize: 12, ),
                    //               textAlign: TextAlign.center,   
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // );
                  }else{
                    return Center(
                      child: Opacity(
                        opacity: isLoading ? 1.0 : 0.0,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                },
                childCount: _productCategoryDetailList.length,
              ),
            )
            :
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                PlaceholderList(type: "productcategorydetail",),
                childCount: 1,
              ),
            ),
          ],
        ),
      ), 
    );
  }
  Future<Null> _refresh() async {
    page = 1;
    await initProductCategoryListDetail();
    return null;
  }
}