// packages
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:timeago/timeago.dart' as timeago;

// widgets
import 'package:shiftsoft/widgets/profileBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/textField.dart';

// resources
import 'package:shiftsoft/resources/newsApi.dart';

// models
import '../models/mnews.dart';
import '../models/muser.dart';
import '../models/mcomment.dart';
import 'package:shiftsoft/settings/configuration.dart';

class CommentList extends StatefulWidget{
  int mode, id;
  News model;
  CommentList({Key key, this.mode, this.id, this.model}) : super(key:key);

  createState(){
    return CommentListState();
  }
}

class CommentListState extends State<CommentList>{
  final commentController = TextEditingController();

  News news;
  User user;
  List<Comment> commentList = [];
  bool commentListLoading = true;
  bool submitCommentLoading = true;
  bool refreshPage = false;

  Map<String, String> message = new Map();
  List<String> messageList = ["comment","postComment","writeYourComment", "areYouSure", "yourChangesWillbeDiscard"];

  @override
  void initState(){
    super.initState();
    user = User(id:1926);

    setState(() {
      if (widget.mode == 1) {
        initCommentList();
        news = widget.model;
      } else if (widget.mode == 3) {
        news = widget.model;
        setState(() {
          commentListLoading = false;
          news = news;
        });
      }
    });
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  initCommentList() async{
    setState((){
      commentListLoading = true;
    });

    commentList = await newsApi.getCommentList(context, widget.id);

    setState(() {
      commentList = commentList;
      commentListLoading = false;
    });
  }

  convertTime(int index){
    final dateNow = DateTime.now();
    final difference = dateNow.difference(commentList[index].createdAt).inMinutes;
    final time = new DateTime.now().subtract(new Duration(minutes: difference));
    return timeago.format(time).toString();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    commentController.dispose();
    super.dispose();
  }

  void submitComment() async {
    String content = commentController.text;
    
    setState(() {
      submitCommentLoading = true;      
    });

    final message =  await newsApi.createComment(context, news.id.toString(), user.id.toString(), content);

    setState(() {
      submitCommentLoading = false;
      refreshPage = true;
    });
    
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context){
        return AlertDialog(
          title: Text("Alert"),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
              child: Text("OK"),
              onPressed: (){
                initCommentList();
                commentController.text = "";
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }

  @override
  Widget build(BuildContext context){
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message["comment"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: WillPopScope(
        onWillPop: willPopScope,
          child: RefreshIndicator(
          onRefresh: _refresh,
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 20),
                  child: Text(message["comment"], style: TextStyle(fontSize: 16.0, color: Colors.grey)),
                  alignment: Alignment.topLeft,
                ),
                Container(
                  margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5.0),
                  child: Theme(
                    data: ThemeData(
                      primaryColor: Colors.blueAccent,
                      primaryColorDark: Colors.blue
                    ),
                    child: TextFields(
                      autoFocus: true,
                      controller: commentController,
                      maxLines: 3,
                      keyboardType: TextInputType.multiline,
                      hintText: "${message["writeYourComment"]}",
                    ),
                  ),
                ),
                Container(
                  alignment: Alignment.center,
                  child:Container(
                    width: MediaQuery.of(context).size.width - 10,
                    child: Button(
                      child: Text(message["postComment"], style: TextStyle(color: config.primaryTextColor),),
                      backgroundColor: config.primaryColor,
                      rounded: true,
                      onTap: () {
                        submitComment();
                      }
                    ),
                  )
                ),
                commentListLoading ?
                Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: Container(
                  padding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 15.0),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: 75,
                              height: 75,
                              //color: Colors.grey[200],
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                border: Border.all(
                                  color: Colors.white,
                                  width: 4.0,
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(margin: EdgeInsets.only(left: 20.0, top: 3.0),height: 20, width: 180.0, color: Colors.grey[200]),
                                Container(margin: EdgeInsets.only(left: 20.0, top: 3.0),height: 20, width: 100.0, color: Colors.grey[200])
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  )
                )
                :
                Container(
                  child: Expanded(
                    child: SizedBox(
                      height: 800.0,
                      child: ListView.builder(
                        itemCount: commentList.length,
                        itemBuilder: (BuildContext context, int index){
                          return Column(
                            children: <Widget>[
                              ListTile(
                                contentPadding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 15.0),
                                leading: ProfileBox(CompanyAsset("avatar", commentList[index].user.pic), 75.0, commentList[index].user.name,
                                  Row(
                                    children: <Widget>[
                                      Text(convertTime(index), style: TextStyle(fontSize: 15.0)),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 35.0, right: 20.0, bottom: 10.0),
                                alignment: Alignment(-1.0, -1.0),                            
                                child: Text(commentList[index].content),
                              ),
                            ],  
                          ); 
                          
                        }
                      ),
                    ),
                  ),
                )
                
              ],
            ) 
          ),
        )
      )
    );
  }
  Future<bool> willPopScope() async{
    if (commentController.text.length > 0) {
      Alert(
        context: context,
        title: message["areYouSure"],
        content: Text(message["yourChangesWillbeDiscard"]),
        defaultAction: () {
          Navigator.pop(context, refreshPage);
        }
      );
    } else {
      Navigator.pop(context, refreshPage);
    }
  }

  Future<Null> _refresh() async{
    await initCommentList();
    return null;
  }

}