// packages
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:path/path.dart' as p;

// screens


// widgets
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/textField.dart';

// resources
import 'package:shiftsoft/resources/requestApi.dart';
import 'package:shiftsoft/resources/requestTypeApi.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/mrequest.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mrequestType.dart';
import 'package:shiftsoft/models/mresult.dart';

class RequestForm extends StatefulWidget{
  int mode, id;
  Request model;

  RequestForm({Key key, this.mode, this.id, this.model}) : super(key:key);

  createState(){
    return RequestFormState();
  }
}

class RequestFormState extends State<RequestForm>{
  File image;
  List<File> tempFile = [];
  List<String> tempFileName = [];
  User user;
  List<RequestType> requestTypeList = [];

    List<DropdownMenuItem<String>> _dropDownMenuItems = [
      DropdownMenuItem(
        key: Key("0"),
        value: "0",
        child: Text("----")
      )
    ];
    String selectedValue = "0";
  
  bool requestTypeListLoading = true;
  int page = 1;

  final nameController = TextEditingController();
  final descriptionController = TextEditingController();
  
  bool requestLoading = false;

  Map<String, String> message = new Map();
  List<String> messageList = ["loginFailed","hiWhatCanWeDoForYou","send","problemDetail","writeYourProblem",
    "helpCenter","rent","required","giveReceiptOrOtherDocumentToStrengthenYourComplaint",
    "name", "description", "requestType",
    "save", "cancel", "success", "create", "failed", 
  ];

  @override
  void initState() {
    super.initState();
    user = User(id: 1926);

    if(widget.mode == 3) {
      
    }
  }
  
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

  	initRequestTypeList();
  }
    
  initRequestTypeList() async {
    setState(() {
      requestTypeListLoading = true;
    });

    requestTypeList = await requestTypeApi.getRequestTypeList(context, page);

    requestTypeList.map((item) {
      _dropDownMenuItems.add(
        DropdownMenuItem(
          key: Key("${item.id}"),
          value: item.id.toString(),
          child: Text(item.name)
        )
      );
    }).toList();

    page++;

    setState(() {
      _dropDownMenuItems = _dropDownMenuItems;
      requestTypeList = requestTypeList;
      requestTypeListLoading = false;
      page = page;
    });
  }
  
  int counter = 0;
  
  picker() async{
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    String baseName = p.basename(img.path);

    if(img != null){
      setState(() {
        image = img;
        counter++;
        tempFile.add(image);
        tempFileName.add(baseName);
      });
    } 
  }

  requestForm() async {
    setState(() {
      requestLoading = false;
    });
    
    if(widget.mode == 0) {
      List<String> pic = [];
      List<String> picName = [];

      for (var i = 0; i < tempFile.length; i++) {
        pic.add(getBase64Image(tempFile[i]));
        picName.add(tempFileName[i]);
      }

      Result result = await requestApi.create(context, user.id.toString(), selectedValue, nameController.text, descriptionController.text, pic.join("|"), picName.join("|"));
      if (result.success == 1) {
        Alert(
          context: context,
          type: "success",
          title: "${message["create"]} ${message["success"]}",
          cancel: false,
          defaultAction: () {
            Navigator.pop(context, true);
          }
        );
      } else {
        Alert(
          context: context,
          type: "error",
          title: "${message["create"]} ${message["failed"]}",
          content: Text(result.message),
          cancel: false,
          defaultAction: () {
            Navigator.of(context).pop();
          }
        );
      }
    }
    
    setState(() {
      requestLoading = false;
    });
  }


  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['helpCenter'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index){
          return Container(
            margin: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(message["hiWhatCanWeDoForYou"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                Padding(
                  padding: EdgeInsets.only(top: 15),
                ),
                Row(
                  children: <Widget>[
                    Text(message["problemDetail"], style: TextStyle(fontSize: 18),),
                    Card(
                      color: Colors.grey[200],
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: Text(message['required']),
                      ),
                    ),
                  ],
                ),
                Container(
                  margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                  child: Text(message["name"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                ),
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0),
                  child: TextFields(
                    key: Key("name-textfield"),
                    controller: nameController,
                    hintText: message["name"],
                    validator: (String value){
                      return null;
                    },
                  ),
                ),
                Container(
                  margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                  child: Text(message["requestType"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                ),
                requestTypeListLoading ?
                Center(
                  child: CircularProgressIndicator()
                )
                :
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0),
                  child: DropdownButton(
                    key: Key("requestType"),
                    isExpanded: true,
                    isDense: true,
                    value: selectedValue,
                    items: _dropDownMenuItems,
                    onChanged: (String selected) {
                      setState(() {
                        selectedValue = selected;
                      });
                    },
                  )
                ),
                Container(
                  margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                  child: Text(message["description"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                ),
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0),
                  child: TextFields(
                    maxLines: 3,
                    key: Key("description-textfield"),
                    controller: descriptionController,
                    keyboardType: TextInputType.multiline,
                    hintText: message["description"],
                    validator: (String value){
                      return null;
                    },
                  ),
                ),
                ListTile(
                  title: Text(message["giveReceiptOrOtherDocumentToStrengthenYourComplaint"], style: TextStyle(fontSize: 18),),
                ),
                Card(
                  color: Colors.grey[200],
                  child: Container(
                    margin: EdgeInsets.all(5),
                    child: Text(message['required']),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 7),
                  height: 125,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: counter+1,
                    itemBuilder: (BuildContext context, int i){
                      return Container(
                        child: Card(
                          child: Container(
                            width: 100,
                            child: i == counter ? 
                            IconButton(
                              icon: Icon(Icons.add_a_photo),
                              onPressed: () => picker(),
                            )
                            :
                            Image.file(tempFile[i]),
                          ),
                        ),
                      );
                    },
                  )
                ),
              ],
            ),
          );
        },
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(left: 7.0, right: 3.0, bottom: 10.0),
            width: MediaQuery.of(context).size.width / 2.5,
            height: 50.0,
            child: Button(
              child: Text(message["cancel"], style: TextStyle(color: config.primaryColor),),
              fill: false,
              backgroundColor: config.primaryColor,
              rounded: true,
              onTap: (){
                Navigator.pop(context);
              }
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 3.0, right: 7.0, bottom: 10.0),
            width: MediaQuery.of(context).size.width / 2.5,
            height: 50.0,
            child: Button(
              key: Key("save"),
              child: Text(message["save"], style: TextStyle(color: config.primaryTextColor),),
              backgroundColor: config.primaryColor,
              rounded: true,
              loading: requestLoading,
              onTap: () {
                requestForm();
              }
            ),
          )
        ],
      )
    );
  }
}