// packages
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/tools/functions.dart';

// resources
import 'package:shiftsoft/resources/promotionApi.dart';

// models
import 'package:shiftsoft/models/mpromotion.dart';
import 'package:shiftsoft/widgets/itemDetail.dart';
import 'package:shimmer/shimmer.dart';
import 'package:shiftsoft/settings/configuration.dart';

class PromotionDetail extends StatefulWidget{
  int type, id, mode;
  Promotion model;
  PromotionDetail({Key key, this.type, this.id, this.mode, this.model}) : super(key:key);

  createState(){
    return PromotionDetailState();
  }
}

class PromotionDetailState extends State<PromotionDetail>{
  Promotion promotionDetail;
  bool promotionDetailLoading = true;

  void initState(){
    super.initState();
    if(widget.mode == 1) {
      initPromotionDetail();
    } else if (widget.mode == 3) {
      promotionDetail = widget.model;
      setState(() {
        promotionDetailLoading = false;
        promotionDetail = promotionDetail;
      });
    }
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","buyNow","promotionDetail","withoutMinimumTransaction","termsAndCondition","promoPeriod","promoCode","withoutPromoCode","close","insertPromoCodeInCartList","copyCode","copySuccess"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  initPromotionDetail() async {
    setState(() {
      promotionDetailLoading = true;
    });

    promotionDetail = await promotionApi.getPromotion(context, widget.id);
    if(promotionDetail == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      promotionDetail = promotionDetail;
      promotionDetailLoading = false;
    });
  }

  final key= GlobalKey<ScaffoldState>();
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Stack(
      children: <Widget>[
        Scaffold(
          appBar: AppBar(
            title: Text(message["promotionDetail"], style: TextStyle(color: config.topBarTextColor),),
            backgroundColor: config.topBarBackgroundColor,
            iconTheme: IconThemeData(
              color: config.topBarIconColor
            ),
          ),
          key: key,
          body: RefreshIndicator(
            onRefresh: _refresh,
            child: promotionDetailLoading ?
            ListView.builder(
            itemCount: 1,
            itemBuilder: (context, index){
              return Column(
                  children: <Widget>[
                    Card(
                      child:Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Container(
                          margin: EdgeInsets.all(10.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(height:200, width: 400, color: Colors.grey[200]),
                              Padding(padding: EdgeInsets.only(top: 20),),
                              Container(height:50, width: 100, color: Colors.grey[200]),
                            ],
                          ),
                        ),
                      ), 
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    children: <Widget>[
                                      Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Container(height:50, width: 100, color: Colors.grey[200]),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    children: <Widget>[
                                      Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(
                                            margin: EdgeInsets.all(10.0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: <Widget>[
                                                Container(height:50, width: 100, color: Colors.grey[200]),
                                            ],
                                          ),
                                        )
                                      )
                                    ],
                                  ) 
                                )
                                ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(height:100, width: 400, color: Colors.grey[200]),
                                ],
                              ),
                            ),
                          ),
                          Divider(),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[200],
                                    highlightColor: Colors.grey[350],
                                    period: Duration(milliseconds: 800),
                                    child: Container(
                                      margin: EdgeInsets.all(10.0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(height:50, width: 100, color: Colors.grey[200]),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ) 
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(height:100, width: 400, color: Colors.grey[200]),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }
          )
          :
          ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context,int index){
              return Container(
                color: Colors.black12,
                child: Column(
                  children: <Widget>[
                    Card(
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.width/16*9,
                            width: MediaQuery.of(context).size.width,
                            child: ItemDetail(
                              imageList: promotionDetail.pic.split(","),
                              module: "promotion",
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 25, bottom: 25),
                            child: Text(promotionDetail.name, textAlign: TextAlign.center,),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    children: <Widget>[
                                      Icon(Icons.timer),
                                      Text(message['promoPeriod']),
                                      Text(DateFormat("dd-MMMM-yyyy").format(promotionDetail.startAt) + " ~ " + DateFormat("dd-MMMM-yyyy").format(promotionDetail.endAt))
                                    ],
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Column(
                                    children: <Widget>[
                                      Icon(Icons.monetization_on),
                                      promotionDetail.type != 1 ? 
                                      Column(
                                        children: <Widget>[
                                          Text(promotionDetail.description, textAlign: TextAlign.center,),
                                        ],
                                      )
                                      : 
                                      Text(message['withoutMinimumTransaction'], textAlign: TextAlign.center,)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Divider(),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                promotionDetail.type != 1 && promotionDetail.type != 2 ? 
                                Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Icon(Icons.monetization_on),
                                        Text(message['promoCode'], textAlign: TextAlign.center,),
                                        IconButton(
                                          icon: Icon(Icons.info),
                                          onPressed: (){
                                            showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (context){
                                                return AlertDialog(
                                                  title: Text(message['promoCode']),
                                                  content: ListTile(
                                                    leading: Text(message['insertPromoCodeInCartList']),
                                                    trailing: ImageBox("https://dharmamerchantservices.com/wp-content/uploads/2017/07/cropped-visa.png", 50),
                                                  ),
                                                  actions: <Widget>[
                                                    FlatButton(
                                                      color: Colors.green,
                                                      child: Text(message['close'], style: TextStyle(color: Colors.white),),
                                                      onPressed: (){
                                                        Navigator.of(context).pop();
                                                      },
                                                    )
                                                  ],
                                                );
                                              }
                                            );
                                          },
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                            border: Border.all(color: Colors.grey),
                                          ),
                                          child: Text(promotionDetail.code,style:TextStyle(color: Colors.red)),
                                        ),
                                        RaisedButton(
                                          child: Text(message['copyCode']),
                                          onPressed: (){
                                            Clipboard.setData(ClipboardData(text: promotionDetail.code));
                                            key.currentState.showSnackBar(SnackBar(content: Text(message['copySuccess']),));
                                          },
                                        )
                                      ],
                                    )
                                  ],
                                )
                                :
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.monetization_on),
                                    Text(message['withoutPromoCode'], textAlign: TextAlign.center,),
                                  ],
                                ) 
                              ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 5),
                        child: Card(
                          child: Column(
                            children: <Widget>[
                              Text(message['termsAndCondition'], textAlign: TextAlign.center,),
                              Divider(),
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 5),
                                child: Text("1) Promo berlaku selama periode 7 Januari 2019 (pukul 00.00 WIB) hingga 13 Januari 2019 (pukul 23.59 WIB)."),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 5),
                                child: Text("2) Promo hanya berlaku untuk pembelian produk di Jill Beauty Official Store."),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 5),
                                child: Text("3) Jika transaksi dibatalkan atau masuk ke Pusat Resolusi, dana yang kembali ke pembeli akan sesuai dengan nominal pembayaran yang dilakukan (tidak termasuk subsidi ongkir)."),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 5),
                                child: Text("4) Tokopedia berhak melakukan tindakan-tindakan yang diperlukan seperti pembatalan benefit jika ditemukan transaksi yang melanggar syarat & ketentuan Tokopedia."),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 5),
                                child: Text("5) Syarat dan Ketentuan promo ini merupakan bagian yang tidak terpisahkan dan satu kesatuan dengan Syarat dan Ketentuan Tokopedia, serta dapat berubah sewaktu-waktu untuk disesuaikan dengan kebijakan sehubungan dengan promo ini."),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 20, top: 5, bottom: 5, right: 5),
                                child: Text("6) Dengan mengikuti promo ini, pengguna dianggap mengerti dan menyetujui semua syarat & ketentuan berlaku."),
                              ),
                              Divider(),
                              Container(
                                padding: EdgeInsets.only(top: 15, bottom: 15, right: 10, left: 10),
                                child: Text("Tampil lebih mempesona dan memukau dengan produk kecantikan Jill di Tokopedia Official Store. Anda bisa dapatkan promo produk kecantikan ini dengan mudah, beli produk Jill Beauty Official Store Tokopedia dan dapatkan keuntungannya! Diskon 10% untuk produk compact powder Jill! Yuk, buruan serbu promo Tokopedia dan belanja jadi lebih hemat!"),
                              )                    
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            )
          ),
          bottomNavigationBar: !promotionDetailLoading ? 
          Container(
            color: Colors.green,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.all(5),
            child: FlatButton(
              onPressed: (){
                print("buy now");
              },
              child: Text(message['buyNow'], style: TextStyle(color: Colors.white)),
            )
          ) : null,
        ),
      ],
    );
  }

  Future<Null> _refresh() async {
    await initPromotionDetail()();
    return null;
  }


}