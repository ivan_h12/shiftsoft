// package
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

//screens
import './login.dart';

//widget
import 'package:shiftsoft/tools/functions.dart';
import '../widgets/profileBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/circleApi.dart';
import 'package:shiftsoft/resources/userApi.dart';
import 'package:shiftsoft/resources/attendanceApi.dart';

// models
import 'package:shiftsoft/models/mcircle.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/models/mattendance.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/mresult.dart';

class EventAttendanceAdd extends StatefulWidget {
  int mode, eventId;
  Event model;
  EventAttendanceAdd({Key key, this.mode, this.eventId, this.model}) : super(key:key);

  @override
  _EventAttendanceAdd createState() => _EventAttendanceAdd();
}

class _EventAttendanceAdd extends State<EventAttendanceAdd> {
  List<bool> _valueCircle = [];
  List<bool> _valueAllUser = [];
  List<Circle> _circleList = [];
  List<User> _userList = [];
  List<User> _userCircleList = [];
  List<Attendance> temp = [];

  Map<String, String> message = new Map();
  List<String> messageList = ["attendanceadd", "search"];

  bool circleListLoading = false;
  bool userListLoading = false;
  final TextEditingController _controller = TextEditingController();

  final globalKey = GlobalKey<ScaffoldState>();
  Widget appBarTitle;
  Icon icon = Icon(
    Icons.search,
    color: Colors.white,
  );
  bool _isSearching;
  String _searchText = "";

  _EventAttendanceAdd() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState(){
    initCircleList();
    _controller.addListener(checkController);
    _isSearching = false;
  }

  initCircleList() async {
    setState(() {
      circleListLoading = true;
    });
    
    _circleList = await circleApi.getCircleListByUser(context, 1926);
    if(_circleList == null){
      _circleList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    temp = await attendanceApi.getAttendanceList(context, widget.eventId);

    _circleList.map((item) {
      item.userList.map((item2) {
        _userCircleList.add(item2);
      }).toList();
    }).toList();    

    setState(() {
      _circleList = _circleList;
      circleListLoading = false;
      temp = temp;
    });

    for(int i=0; i<_userCircleList.length; i++){
      _valueCircle.add(false);
    }

    for(int i=0; i<_userCircleList.length; i++){
      for(int j=0; j<temp.length; j++){
        if(_userCircleList[i].id == temp[j].id){
          _valueCircle[i] = true;
        }
      }
    }

    temp.clear();
  }

  initUserList(String search) async {
    setState(() {
      userListLoading = true;
    });
    
    if(search.isNotEmpty){
      _userList = await userApi.getUserList(context, search);
      if(_userList == null){
      _userList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
      temp = await attendanceApi.getAttendanceList(context, widget.eventId);

      setState(() {
        _userList = _userList;
        userListLoading = false;
        temp = temp;
      });

      for(int i=0; i<_userList.length; i++){
        _valueAllUser.add(false);
      }

      for(int i=0; i<_userList.length; i++){
        for(int j=0; j<temp.length; j++){
          if(_userList[i].id == temp[j].id){
            _valueAllUser[i] = true;
          }
        }
      }

      temp.clear();
    }
  }

  void checkController(){
    if(_controller.text == "") _userList.clear();
  }

  void _onChange(bool value, String id, int index, int cek) async{
    setState(() {
      if(cek == 1){
        _valueAllUser[index] = value;
      } else {
        _valueCircle[index] = value;
      }
    });

    String message;
    if(value){
      Result result = await attendanceApi.addAttendance(context, widget.model.idEncrypt, id, 2);
    } else {
      Result result = await attendanceApi.addAttendance(context, widget.model.idEncrypt, id, 1);
    }
    if(message == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }else{
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: (){
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
      );
    } 
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    appBarTitle = Text(message["attendanceadd"], style: TextStyle(color: Colors.white));
  }

  Widget makeUserList(){
    return Container(
      padding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 5.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  width: 75,
                  height: 75,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100.0)),
                    border: Border.all(
                      color: Colors.white,
                      width: 4.0,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(margin: EdgeInsets.only(left: 20.0, top: 3.0),height: 20, width: 180.0, color: Colors.grey[200]),
                    Container(margin: EdgeInsets.only(left: 20.0, top: 3.0),height: 20, width: 100.0, color: Colors.grey[200])
                  ],
                )
              ],
            )
          ],
        ),
      );
  }
  
  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      key: globalKey,
      appBar: buildAppBar(context),
      body: circleListLoading || userListLoading?
      Shimmer.fromColors(
        baseColor: Colors.grey[200],
        highlightColor: Colors.grey[350],
        period: Duration(milliseconds: 800),
        child: Container(
          margin: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              makeUserList(),
              makeUserList(),
              makeUserList(),
              makeUserList(),
            ],
          ),
        )
      )
      :
      ListView.builder(
        itemCount: _isSearching ? _userList.length == 0 ? 1 : _userList.length : _userCircleList.length == 0 ? 1 : _userCircleList.length,
        itemBuilder: (BuildContext context, int index){
          // List<Circle> temp;
          // Circle tempCircle;
          // temp = _circleList.where((item) => item.userList[index].id == _userList[index].id).toList();
          // tempCircle = temp[0];

          
          if(_isSearching ? _userList.length != 0 : _userCircleList.length != 0 ){
            String circleName; 
            bool check = false;
            if(!circleListLoading){
              List<String> temp = [];
              _circleList.map((item){
                // item.userList.map((item) {
                //   if(item.id == _userCircleList[index].id) check = true;
                // }).toList();
                // if(check){
                //   temp.add(item.circleName);
                // } 
                temp.add(item.name);
              }).toList();
              circleName = temp.join(", ");
            }

            String allCircleName;
            if(_isSearching){
              List<String> temp = [];
              _userList[index].circleList.map((item) {
                temp.add(item.name);
              }).toList();
              allCircleName = temp.join(', ');
            }
            return ListTile(
              contentPadding: EdgeInsets.only(left: 18.0, top: 20.0, bottom: 10.0),
              leading: Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      child: ProfileBox(CompanyAsset("avatar", _isSearching ? _userList[index].pic : _userCircleList[index].pic), 75.0, _isSearching ? _userList[index].name : _userCircleList[index].name, 
                        Row(
                          children: <Widget>[
                            Container(
                              width: 170.0,
                              child: Text(_isSearching ? allCircleName : circleName, style: TextStyle(fontSize: 15.0), overflow: TextOverflow.ellipsis),
                            )
                          ],
                        ),
                      ),
                    ),
                    _isSearching ?
                    Switch(
                      value: _valueAllUser[index], onChanged: (bool value){
                        _onChange(value, _userList[index].idEncrypt, index, 1);
                      }
                    )
                    :
                    Switch(
                      activeColor: config.primaryColor,
                      value: _valueCircle[index], onChanged: (bool value){
                        _onChange(value, _userCircleList[index].idEncrypt, index, 2);
                      }
                    )
                  ],
                ),
              ),
            );
          }else{
            return PlaceholderList(type: "eventattendanceadd");
          }
        },
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    Configuration config = Configuration.of(context);

    return AppBar(
      centerTitle: true, 
      title: appBarTitle,
      backgroundColor: config.topBarBackgroundColor,
      iconTheme: IconThemeData(
        color: config.topBarIconColor
      ),
      actions: <Widget>[
      IconButton(
        icon: icon,
        onPressed: () {
          setState(() {
            if (this.icon.icon == Icons.search) {
              this.icon = Icon(
                Icons.close,
                color: Colors.white,
              );
              this.appBarTitle = TextField(
                autofocus: true,
                controller: _controller,
                style: TextStyle(color: Colors.white),
                decoration: InputDecoration(
                  hintText: message["search"]+"...",
                  hintStyle: TextStyle(color: Colors.white)
                ),
                onSubmitted: (newValue) {
                  initUserList(newValue);
                  _handleSearchStart();
                },
              );
            } else {
              _handleSearchEnd();
            }
          });
        },
      ),
    ]);
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = Text(message["attendanceadd"], style: TextStyle(color: Colors.white),);
      _isSearching = false;
      _controller.clear();
      _userList.clear();
    });
  }
}

class Choice {
  const Choice({this.icon});
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(icon: Icons.search)
];