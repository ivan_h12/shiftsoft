// packages
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/productApi.dart';

//models
import 'package:shiftsoft/models/mproductreview.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductReviewAdd extends StatefulWidget{
  int productId, userId;
  String productName, productPic;

  createState() {
    return ProductReviewAddState();
  }

  ProductReviewAdd({this.productId,this.productName,this.productPic,this.userId});
}

class ProductReviewAddState extends State<ProductReviewAdd>{
  double rating = 3.5;
  int starCount = 5;
  final myController = TextEditingController();
  bool _value=false;
  void _onChangedSwitch(bool value){
    setState(() {
          _value=value;
        });
  }

  File image;
  picker() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img != null){
      setState(() {
        image = img;
      });
    }
  }

  String getBase64Image(File img){
    List<int> imagesBytes = img.readAsBytesSync();
    String base64Image = base64Encode(imagesBytes);
    return base64Image;  
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["addNewReview","conformityWithDescription","productQuality","bad","productWritingTips","productFunctional","productRecommendation","productReview","inputImage","whatDoYouWantToAskAboutThisProduct","pickImage","send","noImageSelected","success","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['addNewReview'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context,int index){
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(20),
                child: ListTile(
                  leading: ImageBox(CompanyAsset("product", widget.productPic),50),
                  title: Text(widget.productName, style: TextStyle(fontWeight: FontWeight.bold),),
                )
              ),
              Divider(),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(message['productQuality'],style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),),
                    StarRating(
                      size: 50.0,
                      rating: rating,
                      color: Colors.yellow,
                      borderColor: Colors.grey,
                      starCount: starCount,
                      onRatingChanged: (rating) => setState(
                        () {
                          this.rating = rating;
                        },
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                    ),
                    Text(message['bad'],style: TextStyle(fontSize: 15),),
                    Padding(
                      padding: EdgeInsets.only(top: 10),
                    ),
                    ExpansionTile(
                      initiallyExpanded: false,
                      leading: Icon(Icons.lightbulb_outline,color: Colors.yellow,),
                      title: Text(message['productWritingTips']),
                      children: <Widget>[
                        ListTile(
                          leading: Icon(Icons.child_care),
                          title: Text(message["conformityWithDescription"]),
                          subtitle: Text("hhasdihsaihdshadhsajhdiusadsadsadsdsdsadsadsadsadasdasdadsadadasask"),
                        ),
                        ListTile(
                          leading: Icon(Icons.child_care),
                          title: Text(message['productFunctional']),
                          subtitle: Text("hhasdihsaihdshadhsajhdiusadsadsadsdsdsadsadsadsadasdasdadsadadasask"),
                        ),
                        ListTile(
                          leading: Icon(Icons.child_care),
                          title: Text(message["productRecommendation"]),
                          subtitle: Text("hhasdihsaihdshadhsajhdiusadsadsadsdsdsadsadsadsadasdasdadsadadasask",),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),

                    Text(message['productReview'],style: TextStyle(fontSize: 15),),
                    TextField(
                      controller: myController,
                      decoration: InputDecoration(
                        hintText: message['whatDoYouWantToAskAboutThisProduct'],
                        counterText: 'Minimal 30 Karakter',
                      ),
                    ),
                    ListTile(
                      title: Text(message['inputImage'],style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Container(
                        child: Container(
                          child: image == null ?
                          Text(message['noImageSelected'])
                          :
                          Image.file(image),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0),
                      width: MediaQuery.of(context).size.width,
                      height: 30.0,
                      child: RaisedButton(
                        child: Text(message['pickImage'], style: TextStyle(fontSize: 20.0),),
                        color: Colors.blue[200],
                        splashColor: Colors.blueGrey,
                        elevation: 4.0,
                        onPressed: (){
                          picker();
                        }
                      )
                    ),
                    ListTile(
                      title: Text("Anonim"),
                      subtitle: Text("Profil ditampilkan sebagai M***o"),
                      trailing: Switch(
                        value: _value,
                        onChanged: (bool value){_onChangedSwitch(value);},
                      ),
                    )
                  ],
                ),
              )
            ],
          );
        },
      ),
      bottomNavigationBar: Container(
        margin: EdgeInsets.all(20),
        child: RaisedButton(
          color: Colors.green,
          onPressed: () async {
            final stringImg = image == null ? "" : getBase64Image(image);
            final messaged = await productApi.addReview(context, widget.productId, widget.userId, rating, myController.text, stringImg, "tes.jpg");
            if(messaged == null){
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text("Alert"),
                    content: Text(message['loginFailed']),
                    actions: <Widget>[
                      FlatButton(
                        key: Key("ok"),
                        child: Text("OK"),
                        onPressed: (){
                          return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                        },
                      )
                    ],
                  );
                }
              );
            }else {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text("Alert"),
                    content: Text(messaged),
                    actions: <Widget>[
                      FlatButton(
                        child: Text("OK"),
                        onPressed: (){
                          Navigator.of(context).pop();
                          Navigator.of(context).pop(context);
                        },
                      )
                    ],
                  );
                }
              );
            }             
          },
          child: Text(message['send'],style: TextStyle(color: Colors.white),),
        ),
      ),
    );
  }
}