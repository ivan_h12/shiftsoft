// packages
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

// screens
import './profilePage.dart';
import './eventAttendanceAdd.dart';
import './login.dart';

// widgets
import '../widgets/profileBox.dart';
import '../widgets/button.dart';
import '../widgets/loadingUserList.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/attendanceApi.dart';

// models
import 'package:shiftsoft/models/mattendance.dart';
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EventAttendanceList extends StatefulWidget{
  int mode, id;
  Event model;
  EventAttendanceList({Key key, this.mode, this.id, this.model}) : super(key:key);

  createState(){
    return EventAttendanceListState();
  }
}

class EventAttendanceListState extends State<EventAttendanceList>{
  Choice _selectedChoice = choices[0];
  List<Attendance> _attendanceList = [];
  bool attendanceListLoading = true;
  bool attendanceAddStatus = false;

  Map<String, String> message = new Map();
  List<String> messageList = ["attendancelist", "attendanceadd"];

  @override
  void initState(){
    initAttendanceStatus();
  }

  initAttendanceStatus() async{
    attendanceAddStatus = await checkRolePrivileges("Attendance", "Index");

    setState(() {
     attendanceAddStatus = attendanceAddStatus;
    });
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
    
    initAttendanceList();
  }

  initAttendanceList() async {
    setState(() {
      attendanceListLoading = true;
    });
    
    _attendanceList = await attendanceApi.getAttendanceList(context, widget.id);
    if(_attendanceList == null){
      _attendanceList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _attendanceList = _attendanceList;
      attendanceListLoading = false;
    });
  }

  convertTime(int index){
    final dateNow = DateTime.now();
    final difference = dateNow.difference(_attendanceList[index].createdAt).inMinutes;
    final time = new DateTime.now().subtract(new Duration(minutes: difference));
    return timeago.format(time).toString();
  }

  @override
  Widget build(BuildContext context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["attendancelist"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        leading: IconButton(key: Key("Back"), icon: Icon(Icons.arrow_back), onPressed: () => Navigator.pop(context, true)), 
        // actions: <Widget>[
        //   attendanceAddStatus?
        //   IconButton(
        //     key: Key("attendanceform"),
        //     icon: Icon(choices[0].icon),
        //     onPressed: () async {
        //       var nav = await Navigator.push(context, MaterialPageRoute(builder: (context) => EventAttendanceAdd(mode:0, eventId: widget.id, model: widget.model)));
        //       if(nav==true||nav==null){
        //         initAttendanceList();
        //       }
        //     }
        //   )
        //   : Container(),
        // ]
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: attendanceListLoading ? LoadingUserList() :
        Container(
          child: Column(
            children: <Widget>[
              attendanceAddStatus ?
              Container(
                alignment: Alignment.center,
                child: Container(
                  width: MediaQuery.of(context).size.width - 10,
                  child: Button(
                    key: Key("attendanceform"),
                    child: Text(message["attendanceadd"], style: TextStyle(color: config.primaryTextColor)),
                    backgroundColor: config.primaryColor,
                    splashColor: Colors.blueGrey,
                    onTap: () async {
                      var nav = await Navigator.push(context, MaterialPageRoute(builder: (context) => EventAttendanceAdd(mode:0, eventId: widget.id, model: widget.model)));
                      if(nav==true||nav==null){
                        initAttendanceList();
                      }
                    },
                    rounded: true,
                  )
                ),
              ) : Container(),
              Container(
                child: Expanded(
                  child: SizedBox(
                    height: 800.0,
                    child: ListView.builder(
                      itemCount: _attendanceList.length == 0 ? 1 : _attendanceList.length,
                      itemBuilder: (BuildContext context, int index){
                        if(_attendanceList.length != 0){
                          return Hero(
                            tag: "profile$index",
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  onTap: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => ProfilePage(url: CompanyAsset('avatar', _attendanceList[index].pic), tagHero:"profile$index", idUser: _attendanceList[index].id, mode: 1,))
                                    );
                                  },
                                  contentPadding: EdgeInsets.only(top: 25.0, right: 8.0, left: 25.0, bottom: 15.0),
                                  leading: Row(
                                    children: <Widget>[
                                      ProfileBox(CompanyAsset('avatar', _attendanceList[index].pic), 75.0, _attendanceList[index].name,
                                        Row(
                                          children: <Widget>[
                                            Text(convertTime(index), style: TextStyle(fontSize: 15.0)),
                                          ],
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(Icons.cancel, color: Colors.redAccent),
                                        iconSize: 30.0,
                                        onPressed: (){
                                          attendanceApi.addAttendance(context, widget.model.idEncrypt, _attendanceList[index].idEncrypt, 1);
                                          setState(() {
                                            _attendanceList.removeAt(index);
                                            showDialog(
                                              context: context,
                                              barrierDismissible: false,
                                              builder: (context){
                                                return AlertDialog(
                                                  title: Text("Alert"),
                                                  content: Text("Delete berhasil !"),
                                                  actions: <Widget>[
                                                    FlatButton(
                                                      child: Text("OK"),
                                                      onPressed: (){ Navigator.pop(context); },
                                                    )
                                                  ],
                                                );
                                              }
                                            );
                                          });
                                        },
                                      )
                                    ],
                                  )
                                ),
                              ],  
                            ),
                          );
                        }else{
                          return PlaceholderList(type: "eventattendancelist");
                        }
                      }
                    ),
                  ),
                ),
              )
            ],
          ) 
        ) 
      )
    );
  }

  Future<Null> _refresh() async {
    await initAttendanceList();
    return null;
  }
}

class Choice {
  const Choice({this.icon});
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(icon: Icons.add)
];