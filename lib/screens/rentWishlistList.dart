// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';

class RentWishlistList extends StatefulWidget{
  createState(){
    return RentWishlistListState();
  }
}

class RentWishlistListState extends State<RentWishlistList>{
  final key = GlobalKey<ScaffoldState>();
  List<int> rentWishlistList = [];

   Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","addToCart","removeFromWishlist","rent"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void _select(Choice choice) {
    if(choice.title == "Add To Cart"){
      key.currentState.showSnackBar(SnackBar(content: Text(message['addToCart']+" index-" + choice.index.toString() )));
    }else if(choice.title == "Remove From Wishlist"){
      key.currentState.showSnackBar(SnackBar(content: Text(message['removeFromWishlist']+" index-" + choice.index.toString() )));
    }
  }

  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      key: key,
      appBar: AppBar(
        title: Text("Rent Wishlist", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
          rentWishlistList != 0 ?
          SliverList(
            key: Key("body"),
            delegate: SliverChildBuilderDelegate((context, index) => 
              InkWell(
                onTap: (){},
                child: Container(
                  padding: EdgeInsets.only(top: 15, left: 10, right: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          height: 150,
                          width: 100,
                          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                          child: ImageBox("https://prodimage.images-bn.com/pimages/9781484712375_p0_v3_s550x406.jpg", 100),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            ListTile(
                              title: Text("Percy Jackson"),
                              subtitle: Text("Tatsu Maki"),
                              trailing: PopupMenuButton<Choice>(
                                key: Key("popUp$index"),
                                onSelected: _select,
                                itemBuilder: (BuildContext context){
                                  return choices.map((Choice choice) {
                                    choice.index = index;
                                    return PopupMenuItem<Choice>(
                                      value: choice,
                                      child: Text(choice.title),
                                    );
                                  }).toList();
                                },
                              ),
                            ),
                            ListTile(
                              title: Row(
                                children: <Widget>[
                                  Text("4.6"),
                                  Icon(Icons.star),
                                ],
                              ),
                              trailing: Text(message['rent']+", 2 Month"),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              childCount: 10
            ),
          )
          :
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) => 
              PlaceholderList(type: "rentwishlist",),
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }
}
class Choice {
  Choice({this.title, this.index});
  String title;
  int index;
}

List<Choice> choices = <Choice>[
  Choice(title: 'Add To Cart'),
  Choice(title: 'Remove From Wishlist'),
];