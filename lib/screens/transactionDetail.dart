// packages
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

// screens
import 'package:shiftsoft/screens/productDetail.dart';
import './login.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/listBox.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/transactionApi.dart';

//models
import 'package:shiftsoft/models/mtransaction.dart';
import 'package:shiftsoft/settings/configuration.dart';


class TransactionDetail extends StatefulWidget {
  Transaction model;
  int id, mode;
  TransactionDetail({this.mode, this.id, this.model});

  createState() {
    return TransactionDetailState();
  }
  
}
class TransactionDetailState extends State<TransactionDetail> {

  Transaction _transaction;
  bool transactionListLoading = true;
  bool buttonConfirmationLoading = false;

  File image;
  picker() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img != null){
      setState(() {
        image = img;
      });
    }
  }

  String getBase64Image(File img){
    List<int> imagesBytes = img.readAsBytesSync();
    String base64Image = base64Encode(imagesBytes);
    return base64Image;  
  }

  @override
  void initState(){
    super.initState();
    if(widget.mode == 1){
      initTransactionList();
    }else if(widget.mode == 3){
      _transaction = widget.model;
      setState(() {
        _transaction = widget.model;
        transactionListLoading = false;
      });
    }
  }

  initTransactionList() async {
    setState(() {
      transactionListLoading = true;
    });

    _transaction = await transactionApi.getTransaction(context, widget.id);
    _transaction.detailTransaction = await transactionApi.getTransactionDetailList(context, widget.id);

    if(_transaction.detailTransaction == null){
      _transaction.detailTransaction = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message["loginFailed"]),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _transaction = _transaction;
      transactionListLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initTransactionList();
    image = null;
    return null;
  }
  
  Map<String, String> message=new Map();
  List<String> messageList = ["transactionDetail","notesForSeller","insuranceFee","shippingCharge","totalPayment","shippingDestination","recipientName","address","recipientPhoneNumber","courierAgent","acceptSome","confirmImage","noImageSelected","pickImage","chooseImageFirst","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(message['transactionDetail'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: transactionListLoading ?
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context,int index) {
            return InkWell(
              child: Container(
                color: Color.fromRGBO(229, 229, 229, 1),
                child: Container(
                  padding: EdgeInsets.all(1),
                  child: ListBox(
                    childs: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,  
                      children: <Widget>[
                        Shimmer.fromColors(
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.grey[350],
                          period: Duration(milliseconds: 800),
                          child: Container(margin: EdgeInsets.only(left: 10,top: 5),width: 120, height: 20, color: Colors.grey[200]),
                        ),
                        Divider(),
                        ListTile(
                          leading: ImageBox("",50),
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                          subtitle: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(margin: EdgeInsets.only(top: 5),width: 120, height: 20, color: Colors.grey[200]),
                          ),
                          trailing: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        Divider(),
                        ListView.builder(
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemCount: 1,
                          itemBuilder: (BuildContext context,int index) {
                            return Column(
                                children: <Widget>[
                                  ListTile(
                                    isThreeLine: true,
                                    leading: ImageBox("", 50),
                                    title: Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(width: 120, height: 20, color: Colors.grey[200]),
                                    ),
                                    subtitle: Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(margin: EdgeInsets.only(top:5),width: 120, height: 40, color: Colors.grey[200]),
                                    ),
                                  ),
                                  ListTile(
                                    title: Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(width: 120, height: 20, color: Colors.grey[200]),
                                    ),
                                    subtitle: Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(margin: EdgeInsets.only(top: 5),width: 120, height: 20, color: Colors.grey[200]),
                                    ),
                                  ),
                              ],
                            );
                          },
                        ),
                        Divider(),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        Divider(),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                        ListTile(
                          title: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(width: 120, height: 20, color: Colors.grey[200]),
                          ),
                        ),
                      ],
                    ),
                  )
                ),
              ),
            );
          },
        )  
        :
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context,int index) {
            return InkWell(
              child: Container(
                color: Color.fromRGBO(229, 229, 229, 1),
                child: Container(
                  padding: EdgeInsets.all(1),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListBox(
                        childs: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text("INV/00" + _transaction.id.toString(),style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18.0),textAlign: TextAlign.left,),
                              subtitle: Text(DateFormat("dd/MMMM/yyyy").format(_transaction.dateCreated).toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15.0), textAlign: TextAlign.left,),
                            ),
                            Divider(),
                            _transaction.detailTransaction.length != 0 ?
                            ListView.builder(
                              shrinkWrap: true,
                              physics: ScrollPhysics(),
                              itemCount: _transaction.detailTransaction.length,
                              itemBuilder: (BuildContext context,int index) {
                                return Column(
                                  children: <Widget>[
                                    ListTile(
                                      key: Key("productDetail0"),
                                      onTap: (){
                                        Navigator.push(
                                          context, 
                                          MaterialPageRoute(builder: (context)=> ProductDetail(mode: 1, id: _transaction.detailTransaction[index].product.id))
                                          );
                                      },
                                      isThreeLine: true,
                                      leading: ImageBox(CompanyAsset("product", _transaction.detailTransaction[index].product.pic), 50),
                                      title: Text(_transaction.detailTransaction[index].product.name.toString(),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18.0),textAlign: TextAlign.left,),
                                      subtitle: Column(
                                        children: <Widget>[
                                          Row(
                                            children: <Widget>[
                                              Text(numberFormat(_transaction.detailTransaction[index].price, config.currency),style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold,fontSize: 15.0),textAlign: TextAlign.left,),
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Text(_transaction.detailTransaction[index].quantity.toString() + " Barang (1 ton)",style: TextStyle(color: Colors.grey,fontWeight: FontWeight.bold,fontSize: 15.0),textAlign: TextAlign.left,),
                                            ],
                                          ),
                                        ],
                                      )
                                    ),
                                    ListTile(
                                      title: Text(message["notesForSeller"]),
                                      subtitle: Text(_transaction.detailTransaction[index].description),
                                    ),
                                  ],
                                );
                              },
                            )
                            :
                            PlaceholderList(type: "transactiondetail",),
                          ],
                        ),
                      ),
                      ListBox(
                        childs: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text("Subtotal"),
                              trailing: Text(numberFormat(_transaction.totalPrice, config.currency)),
                            ),
                            ListTile(
                              title: Text(message['insuranceFee']),
                              trailing: Text(numberFormat(_transaction.totalPrice, config.currency)),
                            ),
                            ListTile(
                              title: Text(message['shippingCharge']),
                              trailing: Text(numberFormat(_transaction.totalPrice, config.currency)),
                            ),
                            Divider(),
                            ListTile(
                              title: Text(message['totalPayment'],style: TextStyle(fontWeight: FontWeight.bold),),
                              trailing: Text(numberFormat(_transaction.totalPrice, config.currency),style: TextStyle(fontWeight: FontWeight.bold),),
                            ),
                          ],
                        )
                      ),
                      ListBox(
                        childs: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(message['shippingDestination'],style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                            ),
                            ListTile(
                              title: Text(message['recipientName'],style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text("Hadi"),
                            ),
                            ListTile(
                              title: Text(message['address'],style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text("Siwalankerto"),
                            ),
                            ListTile(
                              title: Text(message['recipientPhoneNumber'],style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text("14045"),
                            ),
                            ListTile(
                              title: Text(message['courierAgent'],style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text("Bambang"),
                            ),
                            ListTile(
                              title: Text(message['acceptSome'],style: TextStyle(fontWeight: FontWeight.bold),),
                              subtitle: Text("Tidak"),
                            ),
                          ],
                        ),
                      ),
                      ListBox(
                        childs: Column(
                          children: <Widget>[
                            _transaction.detailTransaction.length != 0?
                              _transaction.detailTransaction[index].status == 2 ?
                              Column(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.all(10),
                                    child: Text(message['confirmImage'],style: TextStyle(fontWeight: FontWeight.bold),),
                                  ),
                                  Container(
                                    margin: EdgeInsets.all(10),
                                    child: image == null ?
                                    Text(message['noImageSelected'])
                                    :
                                    Image.file(image),
                                  ),
                                ],
                              )
                              :
                              Container()
                            :
                            Container(),
                            _transaction.detailTransaction.length != 0?
                              _transaction.detailTransaction[index].status == 2 ?
                              Container(
                                margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0),
                                width: MediaQuery.of(context).size.width,
                                height: 50.0,
                                child: Button(
                                  child: Text(message['pickImage'], style: TextStyle(fontSize: 20.0, color: config.primaryTextColor),),
                                  backgroundColor: config.primaryColor,
                                  onTap: (){
                                    picker();
                                  }
                                )
                              )
                              :
                              Container()
                            :
                            Container(),
                            _transaction.detailTransaction.length != 0?
                              _transaction.detailTransaction[index].status == 2 ?
                              Container(
                                margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0),
                                width: MediaQuery.of(context).size.width,
                                height: 50.0,
                                child:  
                                buttonConfirmationLoading ?
                                Padding(
                                  padding: EdgeInsets.only(top:15, bottom:15),
                                  child:Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      CircularProgressIndicator(),
                                    ]
                                  )
                                )
                                :
                                Button(
                                  child: Text(image == null ? message['chooseImageFirst']:"Upload Image", style: TextStyle(fontSize: 20.0, color: config.secondaryTextColor)),
                                  backgroundColor: config.secondaryColor,
                                  onTap: image == null ? 
                                  null
                                  :
                                  () async {
                                    final stringImg = getBase64Image(image);
                                    setState(() {
                                      buttonConfirmationLoading = true;
                                    });
                                    final messaged = await transactionApi.uploadCofirmationImage(context, _transaction.id, stringImg, "coba.jpg");
                                    setState(() {
                                      buttonConfirmationLoading = false;
                                    });
                                    if(messaged == null){
                                      showDialog(
                                        context: context,
                                        barrierDismissible: false,
                                        builder: (context){
                                          return AlertDialog(
                                            title: Text("Alert"),
                                            content: Text(message['failedLogin']),
                                            actions: <Widget>[
                                              FlatButton(
                                                key: Key("ok"),
                                                child: Text("OK"),
                                                onPressed: (){
                                                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                                },
                                              )
                                            ],
                                          );
                                        }
                                      );
                                    }else {
                                      showDialog(
                                        context: context,
                                        barrierDismissible: false,
                                        builder: (context){
                                          return AlertDialog(
                                            title: Text("Alert"),
                                            content: Text(messaged),
                                            actions: <Widget>[
                                              FlatButton(
                                                child: Text("OK"),
                                                onPressed: (){
                                                  Navigator.of(context).pop();
                                                  Navigator.of(context).pop();
                                                },
                                              )
                                            ],
                                          );
                                        }
                                      ); 
                                    }
                                  }
                                )
                              )
                              :
                              Container()
                            :
                            Container(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      )
    );
  }
}