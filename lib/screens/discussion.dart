// packages
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/profileBox.dart';


// models
import 'package:shiftsoft/settings/configuration.dart';

class Discussion extends StatefulWidget{
  List<String> imageList= [
    "https://vignette.wikia.nocookie.net/vsbattles/images/3/36/Sabo.png/revision/latest?cb=20160222000110",
    "https://media.licdn.com/dms/image/C5603AQGNFq8ymLcAOw/profile-displayphoto-shrink_200_200/0?e=1548892800&v=beta&t=zJJTpTtybtWfsHDJWvuuwXKjpnyxNOxyOlzMyg2cUMo",
    "https://www.wowkeren.com/images/photo/hotman_paris_hutapea.jpg",
  ];

  createState(){
    return DiscussionState();
  }
}

class DiscussionState extends State<Discussion>{
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Diskusi", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          BadgeIconButton(
            icon: Icon(Icons.add_circle_outline,color: Colors.white,),
            itemCount: 0,
            onPressed: (){
              
            },
            )
        ],
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context,int index) {
          return InkWell(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                              title: Text("Powerbank superpower tahan lama ",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0)),
                              subtitle: Text("399.000",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0,color: Colors.red)),
                            ),
                        Divider(),
                        ListView.builder(
                          itemCount: 5,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemBuilder: (BuildContext context,int index) {
                            return Card(
                                  color: Colors.white70,
                                  margin: EdgeInsets.all(10),
                                  child: Container(
                                    padding: EdgeInsets.all(10),
                                    child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          ListTile(
                                            leading: ImageBox("https://static.thenounproject.com/png/194977-200.png",50),
                                            title: Row(
                                              children: <Widget>[
                                                Card(
                                                  color: Colors.green,
                                                  child: Container(
                                                    padding: EdgeInsets.all(5),
                                                    child: Text("Pengguna"),
                                                  ),
                                                ),
                                                Text("Madeee"),
                                              ],
                                            ),
                                            subtitle: Text("23 Dec 2018, 20:36 WIB"),
                                            trailing: IconButton(
                                              icon: Icon(Icons.keyboard_arrow_down),
                                              onPressed: (){

                                              },
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.all(10),
                                            child: Text("Ready kapan gan?", style: TextStyle(fontSize: 15.0,),),
                                          ),
                                          Divider(),
                                          Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 1,
                                                child: FlatButton(
                                                    onPressed: (){

                                                    },
                                                    child: Row(
                                                      children: <Widget>[
                                                        Icon(Icons.chat_bubble),
                                                        Text("0 Komentar"),
                                                      ],
                                                    ),
                                                )
                                              ),
                                              
                                              Expanded(
                                                flex: 1,
                                                child: FlatButton(
                                                    onPressed: (){

                                                    },
                                                    child: Row(
                                                      children: <Widget>[
                                                        Icon(Icons.verified_user),
                                                        Text("Ikuti"),
                                                      ],
                                                    ),
                                                )
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                  ),
                                );
                          },
                        )
                      ],
                    ),
                  ),
                );
        },
      ),
    );
  }
}