// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/transactionListCard.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';


// resources
import 'package:shiftsoft/resources/transactionApi.dart';

//models
import 'package:shiftsoft/models/mtransaction.dart';
import 'package:shiftsoft/settings/configuration.dart';

class TransactionList extends StatefulWidget {  
  createState() {
    return TransactionListState();
  }
  
}
class TransactionListState extends State<TransactionList> {
  List<Transaction> _transactionList = [];
  bool transactionListLoading = true;
  String _status;
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;

  @override
  void initState(){
    _transactionList.clear();
    initTransactionList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_transactionList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _transactionList.addAll(await transactionApi.getTransactionList(context, 1926, page));
      if(_transactionList == null){
        _transactionList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;
      
      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initTransactionList() async {
    setState(() {
      transactionListLoading = true;
    });
    
    _transactionList = await transactionApi.getTransactionList(context, 1926, page);
    if(_transactionList == null){
      _transactionList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message["loginFailed"]),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;
    setState(() {
      _transactionList = _transactionList;
      transactionListLoading = false;
      page = page;
    });
  }

  Future<Null> _refresh() async {
    page = 1;
    await initTransactionList();
    return null;
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["transactionList","uploadTransferReceipt","dataForwardToSeller","productSend","productArrived","latestStatus","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  String setStatus(int status) {
    if ( status == 1 ) {
      _status = "";
    } else if ( status == 2 ) {
      _status = message['uploadTransferReceipt'];
    } else if ( status == 3 ) {
      _status = message['dataForwardToSeller'];
    } else if ( status == 4 ) {
      _status = message['productSend'];
    } else if ( status == 5 ) {
      _status = message['productArrived'];
    }
    return _status;
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      // backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(message['transactionList'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: transactionListLoading ?
        ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: 1,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              margin: EdgeInsets.all(10),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: Container(margin: EdgeInsets.only(left: 10,top: 5),width: 120, height: 20, color: Colors.grey[200]),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: Container(margin: EdgeInsets.only(left: 10,top: 5),width: 120, height: 20, color: Colors.grey[200]),
                    ),
                    Divider(),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: Container(margin: EdgeInsets.only(left: 10,top: 5),width: 120, height: 20, color: Colors.grey[200]),
                    ),
                    Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: Container(margin: EdgeInsets.only(left: 10,top: 5,bottom: 5),width: 200, height: 50, color: Colors.grey[200]),
                    ),
                  ],
                ),
              ),
            );
          }
        )
        :
        ListView.builder(
          key: Key("scroll-list"),
          controller: _controller,
          itemCount: _transactionList.length +1 != 1 ? _transactionList.length+1 : 1,
          itemBuilder: (BuildContext context,int index) {
            if ( _transactionList.length+1 !=1 ) {
              return index < _transactionList.length ? InkWell(
                child: Container(
                  child: TransactionListCard(invoice: "INV/00" + _transactionList[index].id.toString() ,date: DateFormat("dd/MMMM/yyyy HH:mm:ss").format(_transactionList[index].dateCreated).toString(), status: setStatus(_transactionList[index].status,), userId: 1926, transactionId: _transactionList[index].id, model: _transactionList[index], index: index,message: message['latestStatus'],),
                ),
              )
              :
              Center(
                child: Opacity(
                  opacity: isLoading ? 1.0 : 0.0,
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return PlaceholderList(type: "transaction");
            }
          },
        ),
      )
    );
  }
}