// packages
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';

// screens
import 'package:shiftsoft/screens/requestForm.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/listBox.dart';
import 'package:shiftsoft/widgets/carousel.dart';

// resources
import 'package:shiftsoft/resources/requestApi.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/mrequest.dart';


class RequestList extends StatefulWidget{
  String title;
  int id,price;  

  createState(){
    return RequestListState();
  }

  RequestList({Key key}) : super(key:key);
}

class RequestListState extends State<RequestList>{
  int page = 0;

  List<Request> requestList = [];
  bool requestListLoading = true;
  
  Map<String, String> message=new Map();
  List<String> messageList = ["requestList", "loginFailed","read","unread"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    initRequestList();
  }

  initRequestList() async {
    setState(() {
      requestListLoading = true;
    });

    requestList = await requestApi.getRequestList(context, page, parameter: "with[0]=Visitation");

    page++;

    setState(() {
      requestList = requestList;
      requestListLoading = false;
      page = page;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(message["requestList"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          BadgeIconButton(
            icon: Icon(Icons.add,color: Colors.white,),
            itemCount: 0,
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RequestForm(mode: 0)));
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: requestListLoading ?
        ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, int index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: ListTile(
                title: Container(width: 100, height: 50, color: Colors.grey[200]),
                trailing: Container(width: 50, height: 50, color: Colors.grey[200])
              )
            );
          }
        )
        :
        ListView.builder(
          itemCount: requestList.length != 0 ? requestList.length : 1,
          itemBuilder: (BuildContext context, int index) {
            if (requestList.length != 0) {
              Request item = requestList[index];

              return Column(
                children: [
                  ListBox(
                    childs: ListTile(
                      title: Text("[${item.requestType.name}] - ${item.name}", style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Text(item.content),
                      trailing: Text(DateFormat('dd MMM yyyy').format(item.createdAt)),
                    ),
                  ),
                  item.pic != "" ?
                  Container(
                    height: MediaQuery.of(context).size.width/16*9,
                    child: Carousel( "VisitationRequest", item.pic.split("|"), [], [], [] )
                  ) : Container()
                ]
              );
            } else {
              return PlaceholderList(type: "requestList");
            }
          },
        ),
      )
    );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initRequestList();
    return null;
  }
}