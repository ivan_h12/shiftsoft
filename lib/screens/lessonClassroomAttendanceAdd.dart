// package
import 'package:flutter/material.dart';

//screens
import './login.dart';

// resource
import 'package:shiftsoft/resources/attendanceApi.dart';

// widget
import '../widgets/placeholderList.dart';
import '../widgets/loadingUserList.dart';

// models
import 'package:shiftsoft/models/mattendance.dart';
import 'package:shiftsoft/settings/configuration.dart';

class LessonClassroomAttendanceAdd extends StatefulWidget {
  @override
  _LessonClassroomAttendanceAddState createState() => _LessonClassroomAttendanceAddState();
}

class _LessonClassroomAttendanceAddState extends State<LessonClassroomAttendanceAdd> {
  bool _value = false;
  Choice _selectedChoice = choices[0];

  List<Attendance> _attendanceList = [];
  bool attendanceListLoading = true;

  void _onChange(bool value){
    setState(() {
      initAttendanceList();
    });
  }

  @override
  void initState(){
    setState(() {
      initAttendanceList();
    });
  }

  initAttendanceList() async{
    setState(() {
      attendanceListLoading = true;
    });
    
    _attendanceList = await attendanceApi.getAttendanceList(context, 326);
    if(_attendanceList == null){
      _attendanceList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _attendanceList = _attendanceList;
      attendanceListLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Classroom Attendance Add", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(choices[0].icon),
            onPressed: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(builder: (context) => EventAttendanceAdd())
              // );
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: attendanceListLoading ? LoadingUserList() :
        ListView.builder(
          itemCount: _attendanceList.length != 0 ? _attendanceList.length : 1,
          itemBuilder: (BuildContext context, int index){
            if(_attendanceList.length != 0){
              return ListTile(
                contentPadding: EdgeInsets.only(left: 20.0, top: 20.0, bottom:10.0),
                onTap: (){
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => LessonDetail())
                  // );
                },
                leading: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        child: Image.network(_attendanceList[index].pic, scale: 1.2),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10.0, right: 50.0),
                        child: Text(_attendanceList[index].name, style: TextStyle(fontSize: 18.0))
                      ),
                      Switch(value: _value, onChanged: (bool value){_onChange(value);})
                    ],
                  ),
                ),
              );
            } else {
              return PlaceholderList(type: "attendance");
            }
          }
        ),
      )
    );
  }

  Future<Null> _refresh() async {
    await initAttendanceList();
    return null;
  }
}

class Choice {
  const Choice({this.icon});
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(icon: Icons.search)
];