// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/transactionDetail.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/transactionListCard.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/steppers.dart';
import 'package:shiftsoft/widgets/listBox.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/transactionApi.dart';

//models
import 'package:shiftsoft/models/mtransactionhistory.dart';
import 'package:shiftsoft/models/mtransaction.dart';
import 'package:shiftsoft/settings/configuration.dart';

class TransactionDetailHistoryStatus extends StatefulWidget{
  Transaction model;
  int transactionId, userId;
  TransactionDetailHistoryStatus({this.transactionId,this.userId, this.model});
  createState(){
    return TransactionDetailHistoryStatusState();
  }
}

class TransactionDetailHistoryStatusState extends State<TransactionDetailHistoryStatus>{
  int currentStep = 0;

  List<Step> mySteps(){
    List<Step> steps = [
      Step(
        title: Text("27 Desember 2018 12:47", style: TextStyle(color: Colors.grey, fontSize: 12.0)),
        content: Text("Transaksi Selesai. \nDana akan diteruskan ke penjual."),
        // isActive: currentStep == 0,
      ),
      Step(
        title: Text("27 Desember 2018 12:47", style: TextStyle(color: Colors.grey, fontSize: 12.0)),
        content: Text("Transaksi Dikonfirmasi. \nTransaksi telah dikonfirmasi dan menunggu review Tokopedia."),
        //isActive: currentStep == 1,
      ),
      Step(
        title: Text("27 Desember 2018 12:47", style: TextStyle(color: Colors.grey, fontSize: 12.0)),
        content: Text("Transaksi Selesai. \nDana akan diteruskan ke penjual."),
        //isActive: currentStep == 2,
      ),
    ];
    return steps;
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["statusDetail","noTransaction","transactionCreated","productSend","uploadImageSucceed","transactionSucceed","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  String statusType;

  List<TransactionHistory> _transactionHistory;
  bool transactionListLoading = true;
  @override
  void initState(){
    super.initState();
    // if(widget.mode == 1){
      // _transaction = widget.model;
      initTransactionHistory();
    // }else if(widget.mode == 3){
    //   _transaction = widget.model;
    //   setState(() {
    //     _transaction = widget.model;
    //     transactionListLoading = false;
    //   });
    // }
    
  }

  initTransactionHistory() async {
    setState(() {
      transactionListLoading = true;
    });

    _transactionHistory = await transactionApi.getTransactionHistory(context, widget.transactionId);
    if(_transactionHistory == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _transactionHistory = _transactionHistory;
      transactionListLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initTransactionHistory();
    return null;
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['statusDetail'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: transactionListLoading ?
        Column(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              child: Container(
                child: DecoratedBox(
                  child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: Container(width: 100, height: 50, color: Colors.grey[200]),
                    )
                  ), 
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    border: Border(bottom: BorderSide(color: Colors.grey, width: 1.0))
                  ),
                ),
              ) 
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index){
                  return Stack(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 50.0),
                        child: Card(
                          margin: EdgeInsets.only(left: 50.0, top: 25.0, right: 25.0, bottom: 25.0),
                          child: Container(
                            width: double.infinity,
                            height: 100.0,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(width: 100, height: 50, color: Colors.grey[200]),
                            )
                          ),
                        ),
                      ),
                      Positioned(
                        top: 0.0,
                        bottom: 0.0,
                        left: 75.0,
                        child: Container(
                          height: double.infinity,
                          width: 1.0,
                          color: Colors.blue,
                        ),
                      ),
                      Positioned(
                        top: 50.0,
                        left: 55.0,
                        child: Container(
                          height: 40.0,
                          width: 40.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white
                          ),
                          child: Container(
                            margin: EdgeInsets.all(5.0),
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.red
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
                itemCount: 1,
              )
            ),
          ],
        )
        :
        Column(
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 50.0,
              child: Container(
                child: DecoratedBox(
                  child: Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Text("INV/00"+widget.transactionId.toString(), style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                  ), 
                  decoration: BoxDecoration(
                    color: Colors.white70,
                    border: Border(bottom: BorderSide(color: Colors.grey, width: 1.0))
                  ),
                ),
              ) 
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index){
                  return _transactionHistory.length != 0 ? InkWell(
                    child: Steppers(
                      type: 2, 
                      nama: _transactionHistory[index].description,
                      tanggal: DateFormat("dd-MMMM-yyyy").format(_transactionHistory[index].createdDate), 
                    )
                  )
                  :
                  Container(
                    child: PlaceholderList(type:"transaction"),
                  );
                },
                itemCount: _transactionHistory.length != 0 ? _transactionHistory.length : 1,
              )
            ),
            Container(
              child: Button(
                key: Key("transactionDetail"),
                child: Text("Transaction Detail", style: TextStyle(color: config.primaryTextColor),),
                // rounded: true, 
                backgroundColor: config.primaryColor,
                onTap: (){
                  Navigator.push(context, 
                  MaterialPageRoute(builder: (context) => TransactionDetail(mode: 1, id: widget.transactionId) ));
                },
              ),
            ),
          ],
        ), 
      )
    );
  }
}