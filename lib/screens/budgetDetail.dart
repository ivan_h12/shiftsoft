// package
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:shimmer/shimmer.dart';

// widget
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/listBox.dart';

// screen
import './budgetForm.dart';
import './login.dart';

//resources
import 'package:shiftsoft/resources/budgetApi.dart';

// models
import 'package:shiftsoft/models/mbudget.dart';
import 'package:shiftsoft/models/mbudgetdetail.dart';
import 'package:shiftsoft/settings/configuration.dart';

class BudgetDetail extends StatefulWidget {
  Budget model;
  int mode, id;

  BudgetDetail({Key key, this.mode, this.id, this.model}) : super(key:key);

  @override
  _BudgetDetailState createState() => _BudgetDetailState();
}

class _BudgetDetailState extends State<BudgetDetail> with SingleTickerProviderStateMixin  {
  Budget budget;
  TabController _tabController;
  List<File> temp = [];
  List<List<String>> imageList = [];
  List<List<String>> imageList2 = [];
  int counter = 0;

  bool budgetDetailLoading = true;
  bool disapproveBudgetLoading = false;
  bool approveBudgetLoading = false;

  List<BudgetDetails> budgetDetailSubmission = [];
  List<BudgetDetails> budgetDetailRealization = [];

  bool isLoadingSubmission = false, isLoadingRealization = false;
  ScrollController _controllerSubmission, _controllerRealization;
  int pageSubmission = 1, pageRealization = 1;

  bool formStatus = false;

  Map<String, String> message = new Map();
  List<String> messageList = ["budgetDetail"];
  
  @override
  void initState() {
    _tabController = TabController(vsync: this, length: 2);
    budgetDetailSubmission.clear();
    budgetDetailRealization.clear();
    if(widget.mode == 1){
      initBudgetDetail();
      budget = widget.model;
    }else if(widget.mode == 3){
      
    }
    _controllerSubmission = ScrollController();
    _controllerRealization = ScrollController();
    _controllerSubmission.addListener(_scrollListenerSubmission);
    _controllerRealization.addListener(_scrollListenerRealization);
    super.initState();
    initStatus();
  }

  initStatus() async{
    formStatus = await checkRolePrivileges("Product", "Create");

    setState(() {
      formStatus = formStatus;
    });
  }

  _scrollListenerSubmission() {
    if (_controllerSubmission.offset >= _controllerSubmission.position.maxScrollExtent && !_controllerSubmission.position.outOfRange) {
      if(budgetDetailSubmission.length % c.apiLimit == 0){
        loadMoreSubmission();
      } else {
        this.setState(() {
          isLoadingSubmission = false;
        });
      }
    }
  }
  loadMoreSubmission() async {
    if (!isLoadingSubmission) {
      setState(() {
        isLoadingSubmission = true;
      });
      
      budgetDetailSubmission.addAll(await budgetApi.getBudget(context, widget.id, 2, pageSubmission));
      if(budgetDetailSubmission == null){
        budgetDetailSubmission = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimationSubmission();
      pageSubmission++;

      setState(() {
        isLoadingSubmission = false;
        pageSubmission = pageSubmission;
      });
    }
  }

  makeAnimationSubmission() async {
    final offsetFromBottom = _controllerSubmission.position.maxScrollExtent - _controllerSubmission.offset;
    if (offsetFromBottom < 50) {
      await _controllerSubmission.animateTo(
        _controllerSubmission.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  _scrollListenerRealization() {
    if (_controllerRealization.offset >= _controllerRealization.position.maxScrollExtent && !_controllerRealization.position.outOfRange) {
      if(budgetDetailRealization.length % c.apiLimit == 0){
        loadMoreRealization();
      } else {
        this.setState(() {
          isLoadingRealization = false;
        });
      }
    }
  }
  loadMoreRealization() async {
    if (!isLoadingRealization) {
      setState(() {
        isLoadingRealization = true;
      });
      
      budgetDetailRealization.addAll(await budgetApi.getBudget(context, widget.id, 1, pageRealization));
      if(budgetDetailRealization == null){
        budgetDetailRealization = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimationRealization();
      pageRealization++;

      setState(() {
        isLoadingRealization = false;
        pageRealization = pageRealization;
      });
    }
  }

  makeAnimationRealization() async {
    final offsetFromBottom = _controllerRealization.position.maxScrollExtent - _controllerRealization.offset;
    if (offsetFromBottom < 50) {
      await _controllerRealization.animateTo(
        _controllerRealization.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }
  

  initBudgetDetail() async{
    setState(() {
      budgetDetailLoading = true;
    });
    
    budgetDetailRealization = await budgetApi.getBudget(context, widget.id, 1, pageRealization);
    if(budgetDetailRealization == null){
      budgetDetailRealization = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    budgetDetailSubmission = await budgetApi.getBudget(context, widget.id, 2, pageSubmission);
    
    // if(budgetDetailSubmission != null){
    //   for(int i = 0 ; i<budgetDetailSubmission.length ; i++){
    //     var temp = budgetDetailSubmission[i].pic.split(",");
    //     for(int j = 0 ; j<temp.length ; j++){
    //       imageList[i].add(temp[j]);
    //     }
    //   }
    // }
    // if(budgetDetailRealization != null){
    //   for(int i = 0 ; i<budgetDetailRealization.length ; i++){
    //     var temp = budgetDetailRealization[i].pic.split(",");
    //     for(int j = 0 ; j<temp.length ; j++){
    //       imageList2[i].add(temp[j]);
    //     }
    //   }
    // }
    
    setState(() {
      budgetDetailSubmission = budgetDetailSubmission;
      budgetDetailRealization = budgetDetailRealization;
      budgetDetailLoading = false;
    });
  }

  @override
  void dispose(){
    _tabController.dispose();
    super.dispose();
  }

  Future<Null> _refresh() async{
    await initBudgetDetail();
    return null;
  }

  void disapproveBudgetDetail(int id) async{
    setState(() {
      disapproveBudgetLoading = true;   
    });

    String message = await budgetApi.disapproveBudgetDetail(context, id);

    setState(() {
      disapproveBudgetLoading = false;   
    });

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: (){
                  Navigator.of(context).pop();
                  initBudgetDetail();
                },
              )
            ],
          );
        }
      );

  }

  void approveBudgetDetail(int id) async{
    setState(() {
      approveBudgetLoading = true;   
    });

    String message = await budgetApi.approveBudgetDetail(context, id);

    setState(() {
      approveBudgetLoading = false;   
    });

    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: (){
                  Navigator.of(context).pop();
                  initBudgetDetail();
                },
              )
            ],
          );
        }
      );
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: config.topBarBackgroundColor,
          iconTheme: IconThemeData(
            color: config.topBarIconColor
          ),
          bottom: TabBar(
            tabs: [
              Tab(text: "Submission"),
              Tab(text: "Realization", key: Key("budgetrealization"),),
            ],
          ),
          actions: <Widget>[
            formStatus?
            IconButton(
              icon: Icon(Icons.add),
              key: Key("budgetform"),
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BudgetForm(id: widget.id, check: 0))
                );
              },
            )
            : Container(),
          ],
          title: Text(message["budgetDetail"]),
        ),
        body: RefreshIndicator(
          onRefresh: _refresh,
          child: budgetDetailLoading ?
          TabBarView(
            children: [
              ListView.builder(
                itemCount: 2,
                itemBuilder: (BuildContext context, int index){
                  return ListTile(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                    title: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              width: 150, 
                              height: 15, 
                              color: Colors.grey[200],                    
                              margin: EdgeInsets.only(right: 30.0),
                            ),
                          ),
                          padding: EdgeInsets.only(top: 20.0, left: 5.0, bottom: 5.0),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0),
                                ),
                              ),
                              padding: EdgeInsets.only(left: 5.0),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0, top: 5),
                                ),
                              ),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0),
                                ),
                              ),
                              padding: EdgeInsets.only(left: 5.0),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0, top: 5),
                                ),
                              ),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                          ],
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(left: 5.0, top: 5.0),
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              height: 15, 
                              color: Colors.grey[200],                    
                              margin: EdgeInsets.only(right: 30.0),
                            ),
                          ),
                        ),
                        Container(
                          height: 200.0,       
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 4,
                            itemBuilder: (BuildContext context, int index){
                              return Container(
                                child: Card(
                                  child: Container(
                                    width: 200,
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(
                                        height: 200, 
                                        color: Colors.grey[200],                    
                                        
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                        ),
                      ],
                    ),
                  );
                }
              ),
              ListView.builder(
                itemCount: 2,
                itemBuilder: (BuildContext context, int index){
                  return ListTile(
                    contentPadding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 10.0),
                    title: Column(
                      children: <Widget>[
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              width: 150, 
                              height: 15, 
                              color: Colors.grey[200],                    
                              margin: EdgeInsets.only(right: 30.0),
                            ),
                          ),
                          padding: EdgeInsets.only(top: 20.0, left: 5.0, bottom: 5.0),
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0),
                                ),
                              ),
                              padding: EdgeInsets.only(left: 5.0),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0, top: 5),
                                ),
                              ),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0),
                                ),
                              ),
                              padding: EdgeInsets.only(left: 5.0),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                            Container(
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  height: 15, 
                                  color: Colors.grey[200],                    
                                  margin: EdgeInsets.only(right: 30.0, top: 5),
                                ),
                              ),
                              width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                            ),
                          ],
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              height: 15, 
                              width: 150,
                              color: Colors.grey[200],                    
                              margin: EdgeInsets.only(right: 30.0, top: 5),
                            ),
                          ),
                          padding: EdgeInsets.only(left: 5.0),
                          
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.only(left: 5.0, top: 5.0),
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              height: 15, 
                              color: Colors.grey[200],                    
                              margin: EdgeInsets.only(right: 30.0),
                            ),
                          ),
                        ),
                        Container(
                          height: 200.0,       
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: 4,
                            itemBuilder: (BuildContext context, int index){
                              return Container(
                                child: Card(
                                  child: Container(
                                    width: 200,
                                    child: Shimmer.fromColors(
                                      baseColor: Colors.grey[200],
                                      highlightColor: Colors.grey[350],
                                      period: Duration(milliseconds: 800),
                                      child: Container(
                                        height: 200, 
                                        color: Colors.grey[200],                                               
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                        ),
                      ],
                    ),
                  );
                }
              )
            ]
          )
          :
          TabBarView(
            children: [
              ListView.builder(
                padding: EdgeInsets.all(5),
                controller: _controllerSubmission,
                itemCount: budgetDetailSubmission.length == 0 ? 1 : budgetDetailSubmission.length,
                itemBuilder: (BuildContext context, int index){
                  if(budgetDetailSubmission.length != 0){
                    return InkWell(
                      child: ListBox(
                        childs: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(budgetDetailSubmission[index].circleName, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  padding: EdgeInsets.only(top: 20.0, bottom: 5.0),
                                ),
                                Container(
                                  child: iconSubmission(context, index),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text(budgetDetailSubmission[index].name, style: TextStyle(fontSize: 15.0)),
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                                ),
                                Container(
                                  child: Text("Price : Rp ${budgetDetailSubmission[index].price}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 20.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text("Qty : ${budgetDetailSubmission[index].qty}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                                Container(
                                  child: Text("Total : Rp ${budgetDetailSubmission[index].total}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 20.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                              ],
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text("Deskripsi : ${budgetDetailSubmission[index].content}", ),
                              margin: EdgeInsets.only(left: 5, right: 5),
                            ),
                            Container(
                              height: 200.0,
                              margin: EdgeInsets.only(bottom: 20, left: 5, right: 5),       
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: 1,
                                // itemCount: imageList.length,
                                itemBuilder: (BuildContext context, int i){
                                  return Container(
                                    child: Card(
                                      child: Container(
                                        child: Image.network(CompanyAsset("budgetdetail/list", budgetDetailSubmission[index].pic)),
                                        width: 200,
                                      ),
                                    ),
                                  );
                                },
                              )
                            )
                          ]
                        )
                      )
                    );
                  }else{
                    return PlaceholderList(type: "budgetsubmission");
                  }
                }
              ),
              ListView.builder(
                padding: EdgeInsets.all(5),
                controller: _controllerRealization,
                itemCount: budgetDetailRealization.length == 0 ? 1 : budgetDetailRealization.length,
                itemBuilder: (BuildContext context, int index){
                  if(budgetDetailRealization.length != 0){
                    return InkWell(
                      child: ListBox(
                        childs: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(budgetDetailRealization[index].circleName, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                                  padding: EdgeInsets.only(top: 20.0, bottom: 5.0),
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                                Container(
                                  child: iconRealization(context, index),    
                                ),
                                
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text("${budgetDetailRealization[index].name}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                                Container(
                                  child: Text("Price : Rp ${budgetDetailRealization[index].price}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 20.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text("Qty : ${budgetDetailRealization[index].qty}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                                Container(
                                  child: Text("Total : Rp ${budgetDetailRealization[index].total}", style: TextStyle(fontSize: 15.0)),
                                  width:  MediaQuery.of(context).size.width / 2.0 - 20.0,
                                  margin: EdgeInsets.only(left: 5, right: 5),
                                ),
                              ],
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text("Tax : ${budgetDetailRealization[index].tax}%"),
                              margin: EdgeInsets.only(left: 5, right: 5),
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text("Deskripsi : ${budgetDetailRealization[index].content}"),
                              margin: EdgeInsets.only(left: 5, right: 5),
                            ),
                            Container(
                              height: 200.0,       
                              margin: EdgeInsets.only(bottom: 20, left:5, right: 5),  
                              child: ListView.builder(
                                itemCount: 1,
                                // itemCount: imageList2.length,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (BuildContext context, int i){
                                  return Container(
                                    child: Card(
                                      child: Container(
                                        width: 200,
                                        child: Image.network(CompanyAsset("budgetdetail/list", budgetDetailRealization[index].pic))
                                      ),
                                    ),
                                  );
                                },
                              )
                            )
                          ]
                        )
                      ),
                    );
                    // return ListTile(
                    //   contentPadding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 30.0),
                    //   title: Column(
                    //     children: <Widget>[
                    //       Row(
                    //         children: <Widget>[
                    //           Container(
                    //             alignment: Alignment.centerLeft,
                    //             child: Text(budgetDetailRealization[index].circleName, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                    //             padding: EdgeInsets.only(top: 20.0, left: 20.0, bottom: 5.0),
                    //           ),
                    //           iconRealization(context, index),    
                    //         ],
                    //       ),
                    //       Row(
                    //         children: <Widget>[
                    //           Container(
                    //             child: Text("${budgetDetailRealization[index].name}", style: TextStyle(fontSize: 15.0)),
                    //             padding: EdgeInsets.only(left: 20.0),
                    //             width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                    //           ),
                    //           Container(
                    //             child: Text("Price : Rp ${budgetDetailRealization[index].price}", style: TextStyle(fontSize: 15.0)),
                    //             width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                    //           ),
                    //         ],
                    //       ),
                    //       Row(
                    //         children: <Widget>[
                    //           Container(
                    //             child: Text("Qty : ${budgetDetailRealization[index].qty}", style: TextStyle(fontSize: 15.0)),
                    //             padding: EdgeInsets.only(left: 20.0),
                    //             width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                    //           ),
                    //           Container(
                    //             child: Text("Total : Rp ${budgetDetailRealization[index].total}", style: TextStyle(fontSize: 15.0)),
                    //             width:  MediaQuery.of(context).size.width / 2.0 - 10.0,
                    //           ),
                    //         ],
                    //       ),
                    //       Container(
                    //         alignment: Alignment.centerLeft,
                    //         padding: EdgeInsets.only(left: 20.0, top: 5.0),
                    //         child: Text("Tax : ${budgetDetailRealization[index].tax}%")
                    //       ),
                    //       Container(
                    //         alignment: Alignment.centerLeft,
                    //         padding: EdgeInsets.only(left: 20.0, top: 5.0),
                    //         child: Text("Deskripsi : ${budgetDetailRealization[index].content}")
                    //       ),
                    //       Container(
                    //         height: 200.0,       
                    //         child: ListView.builder(
                    //           itemCount: 1,
                    //           // itemCount: imageList2.length,
                    //           scrollDirection: Axis.horizontal,
                    //           itemBuilder: (BuildContext context, int i){
                    //             return Container(
                    //               child: Card(
                    //                 child: Container(
                    //                   width: 200,
                    //                   child: Image.network(CompanyAsset("budgetdetail/list", budgetDetailRealization[index].pic))
                    //                 ),
                    //               ),
                    //             );
                    //           },
                    //         )
                    //       ),
                    //     ],
                    //   ),
                    // );
                  }else{
                    return PlaceholderList(type: "budgetrealization");
                  }   
                }
              )
            ],
          ),
        )
      ),
    );
  }

  Widget iconRealization(BuildContext context, int index){
    if(budgetDetailRealization[index].status == 0){
      if(budgetDetailRealization[index].userId == 1926){
        return Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: InkWell(
                borderRadius: BorderRadius.circular(10),
                child: Icon(Icons.edit, color: Colors.green),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BudgetForm(model: budgetDetailRealization[index], check: 1, type: 1))
                  );
                }
              ),
            ),
            Container(
              child: InkWell(
                borderRadius: BorderRadius.circular(10),
                child: Icon(Icons.close, color: Colors.red),
                onTap: (){
                 disapproveBudgetDetail(budgetDetailRealization[index].id);
                }
              ),
            ),   
          ],
        );
      } else if (budget.userId == 1927 && budget.userId!=budgetDetailRealization[index].userId){
        return Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: InkWell(
                child: Icon(Icons.check, color: Colors.green),
                borderRadius: BorderRadius.circular(10),
                onTap: (){
                  approveBudgetDetail(budgetDetailRealization[index].id);
                }
              ),
            ),
            Container(
              child: InkWell(
                child: Icon(Icons.close, color: Colors.red),
                borderRadius: BorderRadius.circular(10),
                onTap: (){
                  disapproveBudgetDetail(budgetDetailRealization[index].id);
                }
              ),
            ),
          ],
        );
      }
    } else if(budgetDetailRealization[index].status == 1){
      return Container();
    } else if(budgetDetailRealization[index].status == 2){
      return Container();
    }
  }

  Widget iconSubmission(BuildContext context, int index){
    if(budgetDetailSubmission[index].status == 0){
      if(budgetDetailSubmission[index].userId == 1926){
        return Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: InkWell(
                child: Icon(Icons.edit, color: Colors.green),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BudgetForm(model: budgetDetailSubmission[index], check: 1, type: 2))
                  );
                }
              ),
            ),
            Container(
              child: InkWell(
                borderRadius: BorderRadius.circular(10),
                child: Icon(Icons.close, color: Colors.red),
                onTap: (){
                 disapproveBudgetDetail(budgetDetailSubmission[index].id);
                }
              ),
            ),   
          ],
        );
      } else if (budget.userId == 1927 && budget.userId!=budgetDetailSubmission[index].userId){
        return Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: InkWell(
                child: Icon(Icons.check, color: Colors.green),
                borderRadius: BorderRadius.circular(10),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BudgetForm(model: budgetDetailSubmission[index], check: 1, type: 2))
                  );
                }
              ),
            ),
            Container(
              child: InkWell(
                child: Icon(Icons.close, color: Colors.red),
                borderRadius: BorderRadius.circular(10),
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BudgetForm(model: budgetDetailSubmission[index], check: 1, type: 2))
                  );
                }
              ),
            ),
          ],
        );
      }
    } else if(budgetDetailSubmission[index].status == 1){
      return Container();
    } else if(budgetDetailSubmission[index].status == 2){
      return Container();
    }
  }
}