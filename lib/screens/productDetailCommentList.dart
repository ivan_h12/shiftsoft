// packages
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productCommentAdd.dart';
import 'package:shiftsoft/screens/reportForm.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/productCommentApi.dart';

// models
import 'package:shiftsoft/models/mproductcomment.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductDetailCommentList extends StatefulWidget{
  String title;
  int id,price;

  createState(){
    return ProductDetailCommentListState();
  }

  ProductDetailCommentList({this.title,this.id,this.price});
}

class ProductDetailCommentListState extends State<ProductDetailCommentList>{
  List<ProductComment> _productCommentList = [];
  bool productCommentListLoading = true;

  @override
  void initState(){
    initProductCommentList();
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["user","comments","follow","joinDiscussion","reportDiscussion","cancel","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  initProductCommentList() async {
    setState(() {
      productCommentListLoading = true;
    });
    
    _productCommentList = await productCommentApi.getProductCommentList(context, widget.id);
    if(_productCommentList == null){
      _productCommentList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _productCommentList = _productCommentList;
      productCommentListLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initProductCommentList();
    return null;
  }
  
  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['comments'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          BadgeIconButton(
            icon: Icon(Icons.add,color: Colors.white,),
            itemCount: 0,
            onPressed: () async {
              await Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProductCommentAdd(userId: 1926,productId: widget.id,)));
              initProductCommentList();
            },
            )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productCommentListLoading ?
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context,int index) {
            return InkWell(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: ListTile(
                        title: Container(width: 200, height: 20, color: Colors.grey[200],),
                        subtitle: Container(width: 10, height: 15, color: Colors.grey[200],),
                      ),
                    ),
                    Divider(),
                    ListView.builder(
                      itemCount: 1,
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemBuilder: (BuildContext context,int index) {
                        return Card(
                          color: Colors.white70,
                          margin: EdgeInsets.all(10),
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: ListTile(
                                    leading: Container(width: 50, height: 50, color: Colors.grey[200],),
                                    title: Row(
                                      children: <Widget>[
                                        Card(
                                          color: Colors.green,
                                          child: Container(
                                            padding: EdgeInsets.all(5),
                                            child: Text("Pengguna"),
                                          ),
                                        ),
                                        Container(width: 70, height: 20, color: Colors.grey[200],),
                                      ],
                                    ),
                                    subtitle: Container(width: 7, height: 10, color: Colors.grey[200],),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(10),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(width: 100, height: 20, color: Colors.grey[200],),
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Shimmer.fromColors(
                                        baseColor: Colors.grey[200],
                                        highlightColor: Colors.grey[350],
                                        period: Duration(milliseconds: 800),
                                        child: Container(width: 100, height: 20, color: Colors.grey[200],),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Shimmer.fromColors(
                                        baseColor: Colors.grey[200],
                                        highlightColor: Colors.grey[350],
                                        period: Duration(milliseconds: 800),
                                        child: Container(width: 100, height: 20, color: Colors.grey[200],),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            );
          },
        )
        :
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context,int index) {
            return InkWell(
              child: Container(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(widget.title,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20.0)),
                      subtitle: Text(numberFormat(widget.price, config.currency),style: TextStyle(color:Colors.grey,fontWeight: FontWeight.bold,fontSize: 18.0,)),
                    ),
                    Divider(),
                    ListView.builder(
                      itemCount: _productCommentList.length != 0 ? _productCommentList.length : 1 ,
                      shrinkWrap: true,
                      physics: ScrollPhysics(),
                      itemBuilder: (BuildContext context,int index) {
                        if(_productCommentList.length != 0)
                        {
                          return Card(
                            color: Colors.white70,
                            margin: EdgeInsets.all(7),
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: Container(
                                                child: ImageBox(CompanyAsset("user", _productCommentList[index].userPic),50),
                                              )
                                            ),
                                            Expanded(
                                              flex: 4,
                                              child: Container(
                                                margin: EdgeInsets.all(5),
                                                child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Row(
                                                      children: <Widget>[
                                                        
                                                        Text(_productCommentList[index].userName,style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),overflow: TextOverflow.ellipsis,maxLines: 2,),
                                                      ],
                                                    ),
                                                    Card(
                                                      color: Colors.green,
                                                      child: Container(
                                                        padding: EdgeInsets.all(5),
                                                        child: Text(message['user'],style: TextStyle(color: Colors.black),),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: EdgeInsets.all(5),
                                                      child: Text(DateFormat("dd/MMMM/yyyy HH:mm:ss").format(_productCommentList[index].createDate),style: TextStyle(color: Colors.grey),),
                                                    ),
                                                    
                                                  ],
                                                ),
                                              )
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: IconButton(
                                                icon: Icon(Icons.more_vert),
                                                onPressed: (){
                                                  showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                                                    return Container(
                                                      padding: EdgeInsets.only(left: 15),
                                                      child: Column(
                                                        mainAxisSize: MainAxisSize.min,
                                                        children: <Widget>[
                                                          ListTile(
                                                            leading: Text(message['joinDiscussion']),
                                                          ),
                                                          Divider(),
                                                          InkWell(
                                                            onTap: (){
                                                              Navigator.push(
                                                                context,
                                                                MaterialPageRoute(builder: (context) => ReportForm()),
                                                              );
                                                            },
                                                            child:ListTile(
                                                              leading: Text(message['reportDiscussion']),
                                                            ),
                                                          ),
                                                          Divider(),
                                                          FlatButton(
                                                            child: Text(message['cancel'], style: TextStyle(color: Colors.green),),
                                                            onPressed: (){
                                                              Navigator.pop(context);
                                                            },
                                                          )
                                                        ],
                                                      ),
                                                    );
                                                  });
                                                },
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  // ListTile(
                                  //   leading: ImageBox(CompanyAsset("user", _productCommentList[index].userPic),50),
                                  //   title: Row(
                                  //     children: <Widget>[
                                  //       Card(
                                  //         color: Colors.green,
                                  //         child: Container(
                                  //           padding: EdgeInsets.all(5),
                                  //           child: Text(message['user'],style: TextStyle(color: Colors.black),),
                                  //         ),
                                  //       ),
                                  //       Text(_productCommentList[index].userName,overflow: TextOverflow.ellipsis,maxLines: 2,),
                                  //     ],
                                  //   ),
                                  //   subtitle: Text(DateFormat("dd/MMMM/yyyy HH:mm:ss").format(_productCommentList[index].createDate)),
                                  //   trailing: IconButton(
                                  //     icon: Icon(Icons.more_vert),
                                  //     onPressed: (){
                                  //       showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                                  //         return Container(
                                  //           padding: EdgeInsets.only(left: 15),
                                  //           child: Column(
                                  //             mainAxisSize: MainAxisSize.min,
                                  //             children: <Widget>[
                                  //               ListTile(
                                  //                 leading: Text(message['joinDiscussion']),
                                  //               ),
                                  //               Divider(),
                                  //               InkWell(
                                  //                 onTap: (){
                                  //                   Navigator.push(
                                  //                     context,
                                  //                     MaterialPageRoute(builder: (context) => ReportForm()),
                                  //                   );
                                  //                 },
                                  //                 child:ListTile(
                                  //                   leading: Text(message['reportDiscussion']),
                                  //                 ),
                                  //               ),
                                  //               Divider(),
                                  //               FlatButton(
                                  //                 child: Text(message['cancel'], style: TextStyle(color: Colors.green),),
                                  //                 onPressed: (){
                                  //                   Navigator.pop(context);
                                  //                 },
                                  //               )
                                  //             ],
                                  //           ),
                                  //         );
                                  //       });
                                  //     },
                                  //   ),
                                  // ),
                                  Padding(
                                    padding: EdgeInsets.all(10),
                                    child: Text(_productCommentList[index].description, style: TextStyle(fontSize: 15.0,),),
                                  ),
                                  // Divider(),
                                  // Row(
                                  //   children: <Widget>[
                                  //     Expanded(
                                  //       flex: 1,
                                  //       child: FlatButton(
                                  //           onPressed: (){

                                  //           },
                                  //           child: Row(
                                  //             children: <Widget>[
                                  //               Icon(Icons.chat_bubble),
                                  //               Text("0 "+message['comments']),
                                  //             ],
                                  //           ),
                                  //       )
                                  //     ),
                                  //     Expanded(
                                  //       flex: 1,
                                  //       child: FlatButton(
                                  //           onPressed: (){

                                  //           },
                                  //           child: Row(
                                  //             children: <Widget>[
                                  //               Icon(Icons.verified_user),
                                  //               Text(message['follow']),
                                  //             ],
                                  //           ),
                                  //       )
                                  //     ),
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                          );
                        } else {
                          return PlaceholderList(type: "productcomment");
                        }
                      },
                    )
                  ],
                ),
              ),
            );
          },
        ),
      )
    );
  }
}