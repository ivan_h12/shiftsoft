// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

// screens
import './subscribeForm.dart';
import './login.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/listBox.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/subscribeApi.dart';

//models
import 'package:shiftsoft/models/msubscribe.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/tools/functions.dart';

class SubscribeList extends StatefulWidget {
  @override
  _SubscribeListState createState() => _SubscribeListState();
}

class _SubscribeListState extends State<SubscribeList> {
  List<String> _data = new List<String>();
  List<int> tanggal = new List<int>();
  var tempString, tempSplit;
  DateTime _date = DateTime.now();
  final _formKey = GlobalKey<FormState>();
  String date1, date2;
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;

  bool formStatus = false;
  
  @override
  void initState(){
    super.initState();
    date1 = "1999-09-23";
    date2 = "1999-09-23";
    initSubscribeList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    initStatus();
  }

  initStatus() async{
    formStatus = await checkRolePrivileges("Product", "Create");

    setState(() {
      formStatus = formStatus;
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_subscribeList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _subscribeList.addAll(await subscribeApi.getSubscribeList(context, page));
      if(_subscribeList == null){
        _subscribeList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        date1 = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }
  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        date2 = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }
  
  List<Subscribe> _subscribeList = [];
  bool subscribeListLoading = true;

  initSubscribeList([String filter=""]) async {
    setState(() {
      subscribeListLoading = true;
    });
    
    _subscribeList = await subscribeApi.getSubscribeList(context, page, filter);
    if(_subscribeList == null){
        _subscribeList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
    page++;

    setState(() {
      _subscribeList = _subscribeList;
      subscribeListLoading = false;
      page = page;
    });
  }

  Future<Null> _refresh() async {
    page = 1;
    await initSubscribeList();
    date1 = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();
    date2 = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();
    return null;
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text("Subscribe List", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.filter_list),
            onPressed: (){
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text("Filter"),
                    content: Container(
                      height: 175,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text("Start: ", style: TextStyle(color: Colors.grey),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                              child: InkWell(
                                onTap: (){ _selectDate(context); },
                                child: TextFields(
                                  type: 2,
                                  date: "${date1}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text("End: ", style: TextStyle(color: Colors.grey),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                              child: InkWell(
                                onTap: (){ _selectDate2(context); },
                                child: TextFields(
                                  type: 2,
                                  date: "${date2}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ), 
                    ),
                    actions: <Widget>[
                      Button(
                        child: Text("OK"),
                        rounded: true,
                        backgroundColor: config.primaryColor,
                        splashColor: Colors.blueGrey,
                        onTap: ()async{
                          Navigator.pop(context);
                          initSubscribeList("filters[]=created_at|between|$date1,$date2 23:59:59|and");
                        },
                      ),
                    ],
                  );
                }
              );
            },
          ),
          formStatus?
          IconButton(
            key: Key("subscribeForm"),
            icon: Icon(choices[0].icon),
            onPressed: () async{
              var nav = await Navigator.push(context,MaterialPageRoute(builder: (context) => SubscribeForm()) );
              if(nav == true || nav == null){
                initSubscribeList();
              }
            },
          )
          : Container(),
        ]
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: subscribeListLoading ?
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context, int index) {
            return Column(
              children: <Widget>[
                Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: ListTile(
                    contentPadding: EdgeInsets.only(top: 10,left: 10, right: 10),
                    title: Container(width: 10,height: MediaQuery.of(context).size.width/16, color: Colors.grey[200]),
                  ),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: ListTile(
                    contentPadding: EdgeInsets.only(left: 10, right: 10),
                    title: Container(width: 10,height: MediaQuery.of(context).size.width/16, color: Colors.grey[200]),
                  )
                ),
                Divider()
              ],
            );
          }
        )
        :
        ListView.builder(
          controller: _controller,
          itemCount: _subscribeList.length+1 != 1 ? _subscribeList.length+1 : 1,
          itemBuilder: (BuildContext context, int index){
            if (_subscribeList.length+1 != 1){
              return index < _subscribeList.length ?
              ListBox(
                childs: Column(
                  children: <Widget>[
                    ListTile(
                      contentPadding: EdgeInsets.only(top: 10,left: 10, right: 10),
                      title: Text("Periode : " + DateFormat("MMMM yyyy").format(_subscribeList[index].period).toString(), style: TextStyle(fontWeight: FontWeight.bold),),
                      trailing: Text(_subscribeList[index].status == 1 ? "Canceled" : "Waiting for review"),
                    ),
                    ListTile(
                      contentPadding: EdgeInsets.only(left: 10, right: 10),
                      title: Text("Paid at : " + DateFormat("dd/MMMM/yyyy hh:mm").format(_subscribeList[index].paidAt).toString()),
                      subtitle: Text(_subscribeList[index].description),
                      trailing: Text(numberFormat(_subscribeList[index].value, config.currency)),
                    ),
                  ],
                ),
              )
              :
              Center(
                child: Opacity(
                  opacity: isLoading ? 1.0 : 0.0,
                  child: CircularProgressIndicator(),
                ),
              );
            }else{
              return PlaceholderList(type: "subscribe");
            }
          }
        )
      )
    );
  }
}

class Choice {
  const Choice({this.icon});
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(icon: Icons.add)
];

