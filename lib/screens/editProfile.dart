// packages
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/userApi.dart';

// models
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EditProfile extends StatefulWidget{
  int id, mode;
  String title;
  User model;
  createState(){
    return EditProfileState();
  }
  EditProfile({
    Key key,
    this.model, 
    this.id, 
    this.mode,
    this.title
  }) : super(key:key);
}

class EditProfileState extends State<EditProfile>{
  DateTime _date = DateTime.now();
  User _user;
  String birthdayDate;
  var tempString, tempSplit;
  bool editProfileLoading = false;
  bool buttonEditLoading = false;
  bool buttonImageLoading = false;

  var _formKey = GlobalKey<FormState>();
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var addressController = TextEditingController();
  var cityController = TextEditingController();

  Map<String, String> message=new Map();
  List<String> messageList = ["editProfilePicture","username","name","birthday","phone","address","city","email","update","cancel"];
  
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  File image;
  picker() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img != null){
      setState(() {
        image = img;
      });
    }
  }
  String getBase64Image(File img){
    List<int> imagesBytes = img.readAsBytesSync();
    String base64Image = base64Encode(imagesBytes);
    return base64Image;  
  }

  void initState(){
    super.initState();
    birthdayDate = "1999-09-23";
    if(widget.mode == 1) {
      initEditProfile();
    } else if (widget.mode == 3) {
      _user = widget.model;
      setState(() {
        editProfileLoading = false;
        _user = _user;
        nameController = TextEditingController(text: _user.name);
        phoneController = TextEditingController(text: _user.phone);
        addressController = TextEditingController(text: _user.address);
        cityController = TextEditingController(text: _user.city);
        emailController = TextEditingController(text: _user.email);
        birthdayDate = DateFormat("dd-MMMM-yyyy").format(_user.birthday);
      });
    }
  }

  initEditProfile() async {
    setState(() {
      editProfileLoading = true;
    });

    _user = await userApi.getUser(context, widget.id);

    setState(() {
      _user = _user;
      editProfileLoading = false;
      nameController = TextEditingController(text: _user.name);
      phoneController = TextEditingController(text: _user.phone);
      addressController = TextEditingController(text: _user.address);
      cityController = TextEditingController(text: _user.city);
      emailController = TextEditingController(text: _user.email);
      birthdayDate = DateFormat("dd-MMMM-yyyy").format(_user.birthday);
    });
  }

  Future<Null> _refresh() async {
    await initEditProfile();
    image = null;
    return null;
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        birthdayDate = tempSplit[0];
      });
    }
  }
  
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: editProfileLoading ? 
        Center(
          child: CircularProgressIndicator(),
        )
        : 
        CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context,index) => 
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width/16*9,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: image == null? 
                      NetworkImage(CompanyAsset("avatar", _user.pic))
                      : 
                      FileImage(image),
                      fit: BoxFit.cover
                    )
                  ),
                  child: BackdropFilter(
                    filter: ui.ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                    child: Container(
                      decoration: BoxDecoration(color: Colors.white.withOpacity(0.2)),
                      child: DrawerHeader(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width: 100,
                                height: 100,
                                child: image == null ?
                                Image.network(CompanyAsset("avatar",  _user.pic))
                                : 
                                Image.file(image),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  border: Border.all(
                                    color: Colors.white,
                                    width: 4.0,
                                  ),
                                ),
                              ),
                              buttonImageLoading ?
                              Padding(
                                padding: EdgeInsets.only(top:15, bottom:5),
                                child:Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    CircularProgressIndicator(),
                                  ]
                                )
                              )
                              :
                              Container(
                                child: RaisedButton(
                                  child: Text(message["editProfilePicture"],style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),),
                                  color: Colors.white,
                                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                                    onPressed: ()async{
                                      await picker();
                                      if(image != null){
                                        final imageTemp = getBase64Image(image);
                                        setState(() {
                                          buttonImageLoading = true;
                                        });
                                        final message = await userApi.changeProfileImage(context, widget.id, imageTemp, "test.jpg");
                                        setState(() {
                                          buttonImageLoading = false;
                                        });
                                        if(message == null){
                                          showDialog(
                                            context: context,
                                            barrierDismissible: false,
                                            builder: (context){
                                              return AlertDialog(
                                                title: Text("Alert"),
                                                content: Text("Login Gagal"),
                                                actions: <Widget>[
                                                  FlatButton(
                                                    key: Key("ok"),
                                                    child: Text("OK"),
                                                    onPressed: (){
                                                      return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                                    },
                                                  )
                                                ],
                                              );
                                            }
                                          );
                                        }
                                      }
                                    },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                childCount: 1,  
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context,index) =>
                Container(
                  padding: EdgeInsets.all(5),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                          child: Text(message["username"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0),
                          child: Text(_user.nickname, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0),),
                        ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                          child: Text("Name", style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 7.0, left: 7),
                          child: TextFields(
                            controller: nameController,
                            keyboardType: TextInputType.multiline,
                            hintText: "Write Your Name Here",
                            validator: (value){
                              if(value.isEmpty){
                                return "Please enter your name";
                              }
                            },
                          ),
                        ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                          child: Text(message["birthday"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0),
                              child: InkWell(
                                onTap: (){ _selectDate(context); },
                                child: TextFields(
                                  type: 2,
                                  isFullDate: true,
                                  date: "${birthdayDate}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                          child: Text("Phone Number", style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 7.0, left: 7.0),
                          child: TextFields(
                            controller: phoneController,
                            keyboardType: TextInputType.number,
                            hintText: "Phone Number",
                            validator: (value){
                                if(value.isEmpty){
                                  return "Please Enter Your Phone Number";
                                }
                              },
                          ),
                        ),
                        // Container(
                        //   child: Theme(
                        //     data: ThemeData(
                        //       primaryColor: Colors.blueAccent,
                        //       primaryColorDark: Colors.blue
                        //     ),
                        //     child:TextFormField(
                        //       validator: (value){
                        //         if(value.isEmpty){
                        //           return "Please Enter Your Phone Number";
                        //         }
                        //       },
                        //       controller: phoneController,
                        //       inputFormatters: [
                        //         LengthLimitingTextInputFormatter(12),
                        //       ],
                        //       keyboardType: TextInputType.number,
                        //       decoration: InputDecoration(
                        //         enabledBorder: OutlineInputBorder(
                        //           borderSide: BorderSide(color: Colors.black26, width: 1.0)
                        //         ),
                        //         border: OutlineInputBorder(),
                        //         hintStyle: TextStyle(color: Colors.black26),
                        //         hintText: "Write Your Phone Number Here",
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                          child: Text("Address", style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 7.0, left: 7.0),
                          child: TextFields(
                            controller: addressController,
                            keyboardType: TextInputType.multiline,
                            hintText: "Address",
                            validator: (value){
                              if(value.isEmpty){
                                return "Please enter your address";
                              }
                            },
                          ),
                        ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                          child: Text("City", style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 7.0, left: 7.0),
                          child: TextFields(
                            controller: cityController,
                            keyboardType: TextInputType.multiline,
                            hintText: "City",
                            validator: (value){
                              if(value.isEmpty){
                                return "Please enter your city";
                              }
                            },
                          ),
                        ),
                        Container(
                          margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                          child: Text("Email", style: TextStyle(fontSize: 16.0, color: Colors.grey))
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 7.0, left: 7.0),
                          child: TextFields(
                            controller: emailController,
                            keyboardType: TextInputType.multiline,
                            hintText: "Email",
                            validator: (value){
                              if(!value.contains("@")){
                                return "Input must be Email";
                              }
                            },
                          ),
                        ),
                        buttonEditLoading ?
                        Padding(
                          padding: EdgeInsets.only(top:10),
                          child:Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                            ]
                          )
                        )
                        :
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Button(
                            rounded: true,
                            child: Text(message["update"],style: TextStyle( color: config.primaryTextColor),),
                            backgroundColor: config.primaryColor,
                            onTap: () async {
                              if(_formKey.currentState.validate()){
                                setState(() {
                                  buttonEditLoading = true;
                                });
                                final message = await userApi.editProfile(context, widget.id, emailController.text, nameController.text, birthdayDate, addressController.text, cityController.text, phoneController.text);
                                setState(() {
                                  buttonEditLoading = false;
                                });
                                if(message == null){
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (context){
                                      return AlertDialog(
                                        title: Text("Alert"),
                                        content: Text("Login Gagal"),
                                        actions: <Widget>[
                                          FlatButton(
                                            key: Key("ok"),
                                            child: Text("OK"),
                                            onPressed: (){
                                              return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                            },
                                          )
                                        ],
                                      );
                                    }
                                  );
                                } else {
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (context){
                                      return AlertDialog(
                                        title: Text("Alert"),
                                        content: Text(message),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text("OK"),
                                            onPressed: (){
                                              Navigator.of(context).pop();            
                                              Navigator.of(context).pop();
                                            },
                                          )
                                        ],
                                      );
                                    }
                                  );
                                }
                              }
                            },
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Button(
                            child: Text(message["cancel"],style: TextStyle( color: config.primaryColor),),
                            fill: false,
                            rounded: true,
                            backgroundColor: config.primaryColor,
                            onTap: (){
                              Navigator.pop(context);
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                childCount: 1
              ),
            ),
          ],
        ),
      ),
    );
  }
}