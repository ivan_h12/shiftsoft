//packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

//screens
import 'package:shiftsoft/screens/assetCalendar.dart';
import 'package:shiftsoft/screens/assetRequestForm.dart';
import 'package:shiftsoft/screens/login.dart';

//widgets
import '../widgets/itemDetail.dart';
import '../widgets/button.dart';
import '../widgets/placeholderList.dart';

//resources
import 'package:shiftsoft/resources/assetApi.dart';

//models
import 'package:shiftsoft/models/massetrequest.dart';
import 'package:shiftsoft/models/masset.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/tools/functions.dart';

class AssetRequestList extends StatefulWidget {
  int mode, id;
  Asset model;
  AssetRequestList({Key key, this.mode, this.id, this.model}) : super(key:key);

  List<String> imageLists=<String>[
    "http://lorempixel.com/output/abstract-q-c-1920-1080-8.jpg",
    "http://lorempixel.com/output/abstract-q-c-1920-1080-8.jpg",
    "http://lorempixel.com/output/abstract-q-c-1920-1080-8.jpg",
  ];

  @override
  _AssetRequestListState createState() => _AssetRequestListState();
}

class _AssetRequestListState extends State<AssetRequestList> {
  String titleName = "";
  Asset asset, assetDetail;
  List<AssetRequest> assetRequestDetail;
  List<String> tempImages = [];
  bool assetRequestDetailLoading = true;

  ScrollController _controller;
  bool isLoading = false;
  int lessonListMaxLength;
  int page = 1;

  bool requestForm = false;

  User user = User(id: 1926);

  Map<String, String> message = new Map();
  List<String> messageList = ["request","assetCalendar","requestSchedule", "areYouSure", "loginFailed", "ok", "approve", "success", "delete"];

  @override
  void initState() {
    super.initState();
    user = User(id: 1926);

    asset = widget.model;
    setState(() {
      asset = asset;
    });

    if (widget.mode == 1) {
      //assetRequestDetail.clear();
      _controller = ScrollController();
      _controller.addListener(_scrollListener);
    } else if (widget.mode == 3) {

    }
  }
  
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(','));
    }).toList();

    initRequestStatus();

    if (assetRequestDetailLoading)
      initAssetRequestList();
  }

  initRequestStatus() async{
    requestForm = await checkPermission(context, "master.asset", "BackendAsset.Create", true);

    setState(() {
      requestForm = requestForm;
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(assetRequestDetail.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      assetRequestDetail.addAll(await assetApi.getAssetRequestDetail(context, widget.id, page));
      if(assetRequestDetail == null){
        assetRequestDetail = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();

      setState(() {
        isLoading = false;
        page = page++;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initAssetRequestList() async{
    setState((){
      assetRequestDetailLoading = true;
    });

    assetRequestDetail = await assetApi.getAssetRequestDetail(context, widget.id, 1);
    if(assetRequestDetail == null){
      assetRequestDetail = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    assetDetail = await assetApi.getAssets(context, widget.id);
    titleName = asset.name;
    tempImages.add(asset.pic);
    page++;

    setState(() {
      assetDetail = assetDetail;
      assetRequestDetail = assetRequestDetail;
      assetRequestDetailLoading = false;    
      page = page;
    });
  }

  status(int index){
    if(assetRequestDetail[index].status == 0){
      return "Waiting";
    } else if(assetRequestDetail[index].status == 1){
      return "Ditolak";
    } else {
      return "Diterima";
    }
  }

  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Text(titleName, style: TextStyle(color: config.topBarTextColor),),
        ),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            key: Key("eventcalendar"),
            icon: Icon(Icons.calendar_today),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AssetCalendar(mode: 3, id: widget.id))
              );
            },
          ),
        ]
      ),
      body: Container(
        child: assetRequestDetailLoading ?
        Shimmer.fromColors(
          baseColor: Colors.grey[200],
          highlightColor: Colors.grey[350],
          period: Duration(milliseconds: 800),
          child: Container(
            margin: EdgeInsets.all(10.0),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(height: 180, width: 800, color: Colors.grey[200]),
                Row(
                  children: <Widget>[
                    Container(height: 30, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(bottom: 10.0, top: 10.0),),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(height: 50, width: MediaQuery.of(context).size.width / 2.0 - 30.0, color: Colors.grey[200], alignment: Alignment.centerLeft),
                    Container(height: 50, width: MediaQuery.of(context).size.width / 2.0 - 30.0, color: Colors.grey[200], alignment: Alignment.centerLeft),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 40, width: 200, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(bottom: 20.0, top: 10.0,),),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 125, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 10),),
                    Container(height: 15, width: 175, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 10),),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 125, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 3),),
                    Container(height: 15, width: 175, color: Colors.grey[200], alignment: Alignment.centerLeft),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 125, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 10),),
                    Container(height: 15, width: 175, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 10)),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 125, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 10),),
                    Container(height: 15, width: 175, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10, bottom: 10)),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Container(height: 15, width: 125, color: Colors.grey[200], alignment: Alignment.centerLeft, margin: EdgeInsets.only(right: 10),),
                    Container(height: 15, width: 175, color: Colors.grey[200], alignment: Alignment.centerLeft),
                  ],
                ),
              ],
            ),
          ),
        ) 
        :
        Column(
          children: <Widget>[
            Expanded(
              child: SizedBox(
                height: 1000.0,
                child: ItemDetail(
                  pinned: false,
                  content: Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 10.0, right: 10.0),
                          child: Column(
                            children: <Widget>[
                              asset.content != "" ? 
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(asset.content, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.black38)),
                                margin: EdgeInsets.only(top: 10.0),
                              ):Container(),
                              !requestForm ?
                              Container(
                                width: MediaQuery.of(context).size.width,
                                child: Button(
                                  backgroundColor: config.primaryColor,
                                  rounded: true,
                                  key: Key("request"),
                                  child: Text(message["request"],
                                    style: TextStyle(
                                      color: config.primaryTextColor
                                    ),
                                  ),
                                  onTap: () async {
                                    bool refresh = await Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => AssetRequestForm(mode: 0, id: widget.id, asset: widget.model))
                                    );

                                    if (refresh ?? false) {
                                      initAssetRequestList();
                                    }
                                  },
                                )
                              )
                              : Container(),
                              Container(
                                alignment: Alignment.centerLeft,
                                child: Text(message["requestSchedule"], style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
                                margin: EdgeInsets.only(top: 10.0),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  list: assetRequestDetail.length > 0 ?
                  SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) {
                      AssetRequest item = assetRequestDetail[index];

                      return index < assetRequestDetail.length ?
                      Container(
                        margin: EdgeInsets.all(10.0),
                        height: 200.0,
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width / 2.0 - 50.0,
                                  alignment: Alignment.centerLeft,
                                  child: Text("Dipinjam Oleh", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black38)),
                                  margin: EdgeInsets.only(top: 20.0, bottom: 5.0),
                                ),
                                Container(
                                  width: MediaQuery.of(context).size.width / 3.0,
                                  alignment: Alignment.centerLeft,
                                  child: Text(item.user.name, style: TextStyle(color: Colors.black)),
                                  margin: EdgeInsets.only(top: 20.0, bottom: 5.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width / 2.0 - 50.0,
                                  alignment: Alignment.centerLeft,
                                  child: Text("Tanggal", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black38)),
                                ),
                                Container(
                                  child: Text(DateFormat('EEEE, dd MMMM yyyy').format(item.startAt), style: TextStyle(color: Colors.black)),
                                  alignment: Alignment.centerLeft,
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width / 2.0 - 50.0,
                                  alignment: Alignment.centerLeft,
                                  child: Text("Peminjaman", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black38)),
                                  margin: EdgeInsets.only(top: 0.0),
                                ),
                                Container(
                                  child: Text(DateFormat('kk:mm').format(item.startAt) + " to " + DateFormat('kk:mm').format(item.endAt), style: TextStyle(color: Colors.black)),
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 0.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width / 2.0 - 50.0,
                                  alignment: Alignment.centerLeft,
                                  child: Text("Keperluan", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black38)),
                                  margin: EdgeInsets.only(top: 10.0),
                                ),
                                Container(
                                  child: Text(item.content, style: TextStyle(color: Colors.black)),
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  width: MediaQuery.of(context).size.width / 2.0 - 50.0,
                                  alignment: Alignment.centerLeft,
                                  child: Text("Status", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black38)),
                                  margin: EdgeInsets.only(top: 10.0),
                                ),
                                Container(
                                  child: Text(status(index), style: TextStyle(color: Colors.black)),
                                  alignment: Alignment.centerLeft,
                                  margin: EdgeInsets.only(top: 10.0),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                item.status == 0 && item.asset.userId == user.id ?
                                IconButton(
                                  icon: Icon(Icons.thumb_up, color: Colors.green),
                                  iconSize: 25.0,
                                  onPressed: () async {
                                    approveRequest(context, item);
                                  }
                                ):Container(),
                                item.status == 0 && item.asset.userId == user.id ?
                                IconButton(
                                  icon: Icon(Icons.thumb_down, color: Colors.red),
                                  iconSize: 25.0,
                                  onPressed: () async {
                                    disapproveRequest(context, item);
                                  }
                                ):Container(),
                                item.status != 1 && item.userId == user.id ?
                                IconButton(
                                  icon: Icon(Icons.close, color: Colors.red),
                                  iconSize: 25.0,
                                  onPressed: () async {
                                    deleteRequest(context, item);
                                  }
                                ):Container(),
                                item.status != 1 && item.userId == user.id ?
                                IconButton(
                                  icon: Icon(Icons.edit),
                                  iconSize: 25.0,
                                  onPressed: () async {
                                    bool refresh = await Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => AssetRequestForm(mode: 3, id: item.id, model: item, asset: asset)),
                                    );
                                    
                                    if (refresh ?? false) {
                                      initAssetRequestList();
                                    }
                                  }
                                ):Container(),
                              ],
                            ),
                          ],
                        ),
                      )
                      :
                      Center(
                        child: Opacity(
                          opacity: isLoading ? 1.0 : 0.0,
                          child: CircularProgressIndicator(),
                        ),
                      );
                    },
                    childCount: assetRequestDetail.length != null ? assetRequestDetail.length : 0
                  ),
                  )
                  :
                  SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) =>
                      Container(
                        child: PlaceholderList(type: "assetRequest", paddingTop: 75),
                      ),
                      childCount: 1,
                    )
                  ),
                  imageList: tempImages[0] == "" ? widget.imageLists
                  :
                  tempImages[0],
                  module: "",
                ),
              ),
            )
          ],
        ),
      )  
    );
  }

  approveRequest(BuildContext context, AssetRequest item) async {
    Alert(
      context: context,
      title: message["areYouSure"],
      defaultAction: () async {
        if (await assetApi.approveAssetRequest(context, item.id.toString())) {
          Alert(
            context: context,
            type: "success",
            title: "${message["approve"]} ${message["success"]}",
            cancel: false,
            defaultAction: () {
              initAssetRequestList();
            }
          );
        } else {
          Alert(
            context: context,
            type: "error",
            title: "${message["approve"]} ${message["failed"]}",
          );
        }
      },
    );
  }

  disapproveRequest(BuildContext context, AssetRequest item) async {
    Alert(
      context: context,
      title: message["areYouSure"],
      cancel: true,
      defaultAction: () async {
        if (await assetApi.disapproveAssetRequest(context, item.id.toString())) {
          Alert(
            type: "success",
            context: context,
            title: "${message["approve"]} ${message["success"]}",
            cancel: false,
            defaultAction: () {
              initAssetRequestList();
            }
          );
        } else {
          Alert(
            type: "error",
            context: context,
            title: "${message["approve"]} ${message["failed"]}",
          );
        }
      }
    );
  }

  deleteRequest(BuildContext context, AssetRequest item) async {
    Alert(
      context: context,
      title: message["areYouSure"],
      cancel: true,
      defaultAction: () async {
        if (await assetApi.deleteAssetRequest(context, item.id.toString())) {
          Alert(
            context: context,
            type: "success",
            title: "${message["delete"]} ${message["success"]}",
            cancel: false,
            defaultAction: () {
              initAssetRequestList();
            }
          );
        } else {
          Alert(
            type: "error",
            context: context,
            title: "${message["delete"]} ${message["failed"]}",
          );
        }
      }
    );
  }

  Widget icon(BuildContext context, int index){
    final assetRequestStatus = assetRequestDetail[index].status;
    
    if(assetRequestDetail[index].status == 1){
      if(assetRequestDetail[index].userId == user.id){
        return Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.edit, color: Colors.green),
              iconSize: 25.0,
              onPressed: (){
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => Maps(gps: circleList[index].gps))
                // );
              },
            ),
            IconButton(
              icon: Icon(Icons.close, color: Colors.red),
              iconSize: 25.0,
              onPressed: (){
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => Maps(gps: circleList[index].gps))
                // );
              },
            ),
          ],
        );
      } else if (assetDetail.userId == user.id && assetDetail.userId!=assetRequestDetail[index].userId){
        return Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.check, color: Colors.green),
              iconSize: 25.0,
              onPressed: (){
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => Maps(gps: circleList[index].gps))
                // );
              },
            ),
            IconButton(
              icon: Icon(Icons.close, color: Colors.red),
              iconSize: 25.0,
              onPressed: (){
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => Maps(gps: circleList[index].gps))
                // );
              },
            ),
          ],
        );
      }
    } else if(assetRequestDetail[index].status == 1){
      return Container();
    } else if(assetRequestDetail[index].status == 2){
      return Container();
    }

    return Container();
  }
}