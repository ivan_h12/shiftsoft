// packages
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';

// screens
import './newsDetail.dart';
import './newsSearch.dart';
import './login.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/oneColumnView.dart';
import 'package:shiftsoft/widgets/twoColumnView.dart';

// resources
import 'package:shiftsoft/resources/newsApi.dart';

// models
import 'package:shiftsoft/models/mnews.dart';
import 'package:shiftsoft/settings/configuration.dart';

class NewsList extends StatefulWidget {
  @override
  _NewsListState createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  List<News> _newsList = [];
  bool newsListLoading = true;

  ScrollController _controller, _controllerNews;

  final globalKey = GlobalKey<ScaffoldState>();
  Widget appBarTitle = Text("News",style: TextStyle(color: Colors.white));
  Icon icon = Icon(Icons.search, color: Colors.white);
  
  bool isLoading = false;
  int newsListMaxLength;
  int page = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["news"];

  int counter = 1;
  Widget _icon = Icon(Icons.grid_on);

  @override
  void initState(){
    _newsList.clear();
    initNewsList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  void onTabTapped(){
    if (counter == 1) {
      _icon = Icon(Icons.list);
      counter = 2;
    } else {
      _icon = Icon(Icons.grid_on);
      counter = 1;
    }
    
    setState(() {
      _icon = _icon;
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_newsList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _newsList.addAll(await newsApi.getNewsList(context, page));
      if(_newsList == null){
        _newsList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initNewsList() async {
    setState(() {
      newsListLoading = true;
    });
    
    _newsList = await newsApi.getNewsList(context, 1);
    page++;

    if(_newsList == null){
      _newsList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _newsList = _newsList;
      newsListLoading = false;
      page = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message["news"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: _icon,
            onPressed: () { 
              setState(() {
                if(!newsListLoading){
                  onTabTapped();
                }
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NewsSearch())
              );
            },
          ),
        ]
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: newsListLoading ?
        ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, int index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: ListTile(
                contentPadding: EdgeInsets.all(25.0),
                leading: Container(width: 100, height: 100, color: Colors.grey[200]),
                title: Container(width: 100, height: 50, color: Colors.grey[200])
              )
            );
          }
        )
        :
        counter == 1?
          CustomScrollView(
            controller: _controllerNews,
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                    final item = _newsList[index];
                    
                    return _newsList.length != 0 ?
                    index < _newsList.length ?
                      OneColumnView(
                        key: Key("newsdetail$index"),
                        imageUrl: item.pic,
                        title: item.subject,
                        date: DateFormat('dd MMM yyyy').format(item.createdAt),
                        widgets: [
                          Icon(
                            Icons.thumb_up,
                            color: Colors.grey,
                            size: 15
                          ),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                          Text(item.likes.toString()),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
                          Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey,
                            size: 15
                          ),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
                          Text(item.recipients.toString()),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                          Icon(
                            Icons.comment,
                            color: Colors.grey,
                            size: 15
                          ),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                          Text(item.comments.toString()),
                        ],
                        onTap: () async {
                          final refresh = await Navigator.pushNamed(context, "newsDetail/${item.id}/1", arguments: item);
                          if (refresh ?? false) {
                            initNewsList();
                          }
                        },
                      )
                      :
                      Center(
                        child: Opacity(
                          opacity: newsListLoading ? 1.0 : 0.0,
                          child: CircularProgressIndicator(),
                        ),
                      )
                    :
                    PlaceholderList(type: "news");
                  },
                  childCount: _newsList.length == 0 ? 1 : _newsList.length,
                ),
              ),
            ],
          )
          :
          CustomScrollView(
            controller: _controllerNews,
            slivers: <Widget>[
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 5.0,
                  childAspectRatio: 0.6,
                ),
                delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                    final item = _newsList[index];
                    
                    return _newsList.length != 0 ?
                    index < _newsList.length ?
                      TwoColumnView(
                        key: Key("newsdetail$index"),
                        imageUrl: item.pic,
                        title: item.subject,
                        date: DateFormat('dd MMM yyyy').format(item.createdAt),
                        widgets: [
                          Icon(
                            Icons.thumb_up,
                            color: Colors.grey,
                            size: 15
                          ),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                          Text(item.likes.toString()),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
                          Icon(
                            Icons.remove_red_eye,
                            color: Colors.grey,
                            size: 15
                          ),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
                          Text(item.recipients.toString()),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                          Icon(
                            Icons.comment,
                            color: Colors.grey,
                            size: 15
                          ),
                          Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                          Text(item.comments.toString()),
                        ],
                        onTap: () {
                          Navigator.pushNamed(context, "newsDetail/${item.id}/1", arguments: item);
                        },
                      )
                      :
                      Center(
                        child: Opacity(
                          opacity: newsListLoading ? 1.0 : 0.0,
                          child: CircularProgressIndicator(),
                        ),
                      )
                    :
                    PlaceholderList(type: "news",);
                  },
                  childCount: _newsList.length == 0 ? 1 : _newsList.length,
                ),
              ),
            ],
          ),
        )
      );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initNewsList();
    return null;
  }
}