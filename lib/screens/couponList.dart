// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';
import 'package:timeago/timeago.dart' as timeago;

// screens
import 'package:shiftsoft/screens/couponDetail.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/couponApi.dart';

// models
import 'package:shiftsoft/models/mcoupon.dart';
import 'package:shiftsoft/settings/configuration.dart';

class CouponList extends StatefulWidget{
  createState(){
    return CouponListState();
  }
}

class CouponListState extends State<CouponList>{
  List<Coupon> _couponList = [];
  bool couponListLoading = false;
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;
  Map<String, String> message = Map();
  List<String> messageList = ["loadAllCoupon"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  @override
  void initState(){
    initCouponList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_couponList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _couponList.addAll(await couponApi.getCouponList(context, page));
      if(_couponList == null){
        _couponList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }else {
        await makeAnimation();
        page++;
      }

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initCouponList() async {
    setState(() {
      couponListLoading = true;
    });
    
    _couponList = await couponApi.getCouponList(context, page);
    if(_couponList == null){
      _couponList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }else {
      page++;
    }

    setState(() {
      _couponList = _couponList;
      couponListLoading = false;
      page = page;
    });
  }
  
  convertTime(int index){
    final dateNow = DateTime.now();
    final difference = _couponList[index].endAt.difference(dateNow).inMinutes;
    return timeago.format(dateNow.add(Duration(minutes:difference)), allowFromNow: true).toString();
  }

  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Today's Hot Details", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: couponListLoading ?  
        ListView.builder(
          itemCount: 3,
          itemBuilder: (context, index){
            return Container(
              margin: EdgeInsets.all(7),
              child: GestureDetector(
                onTap: (){},
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: Card(
                    color: Colors.grey[200],
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 100,
                      padding: EdgeInsets.all(5),
                    ),
                  ),
                ),
              ),
            );
          },
        )
        :
        ListView.builder(
          controller: _controller,
          itemCount: _couponList.length+1 != 1 ? _couponList.length+1 : 1,
          itemBuilder: (context, int index){
            if(_couponList.length+1 != 1){
              return index < _couponList.length ?
                _couponList[index].type == 2 ?
                  Container(
                    margin: EdgeInsets.all(7),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CouponDetail(mode:3, id: _couponList[index].id, model: _couponList[index],)),
                        );
                      },
                      child: Card(
                        key: Key("couponDetail$index"),
                        color: Colors.blue,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: Text(_couponList[index].name, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17)),
                              ),
                              Text("Remaining time: "+convertTime(index), style: TextStyle(color: Colors.white)),
                              Text(DateFormat("dd-MMMM-yyyy").format(_couponList[index].startAt) + " ~ " + DateFormat("dd-MMMM-yyyy").format(_couponList[index].endAt), style: TextStyle(color: Colors.white70)),
                              Text("Continous Coupon", style: TextStyle(color: Colors.white70)),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                  : _couponList[index].type == 3 ?
                  Container(
                    margin: EdgeInsets.all(7),
                    child: InkWell(
                      onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => CouponDetail(mode:3, id: _couponList[index].id, model: _couponList[index],)),
                        );
                      },
                      child: Card(
                        color: Colors.blue,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.all(5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: Text(_couponList[index].name, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 17)),
                              ),
                              Text("Remaining time: "+convertTime(index), style: TextStyle(color: Colors.white)),
                              Text(DateFormat("dd-MMMM-yyyy").format(_couponList[index].startAt) + " ~ " + DateFormat("dd-MMMM-yyyy").format(_couponList[index].endAt), style: TextStyle(color: Colors.white70)),
                              Text("One Time Coupon", style: TextStyle(color: Colors.white70)),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ) 
                  : 
                  Container(
                    margin: EdgeInsets.all(1),
                  )
                :
                Center(
                  child: Opacity(
                    opacity: isLoading ? 1.0 : 0.0,
                    child: CircularProgressIndicator(),
                  ),
                );
            }else {
              return PlaceholderList(type: "coupon",);
            }
          },
        ),
      ),
    );
  }
  Future<Null> _refresh() async {
    await initCouponList();
    return null;
  }
}