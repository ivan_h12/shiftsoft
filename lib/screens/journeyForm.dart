//packages
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';

// screens
import './login.dart';

//widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/journeyApi.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';

class JourneyForm extends StatefulWidget { 
  @override
  _JourneyFormState createState() => _JourneyFormState();
}

class _JourneyFormState extends State<JourneyForm> {
  final _formKey = GlobalKey<FormState>();
  DateTime _date = DateTime.now();

  final journeyNameController = TextEditingController();
  final journeyContentController = TextEditingController();

  bool journeySubmitLoading = false;

  String dateStart;
  var tempJourney, tempJourney1;

  Map<String, String> message = new Map();
  List<String> messageList = ["journeyForm","journeyName", "start", "description", "submit"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2016),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempJourney1 = picked.toString();
        tempJourney = tempJourney1.split(" ");
        dateStart = tempJourney[0];
      });
    }
  }

  void submitJourney() async {
    if(_formKey.currentState.validate()){
      setState(() {
        journeySubmitLoading = true;      
      });

      String journeyName = journeyNameController.text;
      String journeyContent = journeyContentController.text;
      String journeyDate;
      journeyDate = dateStart + "T" + "00:01";

      String idUser = "1926";

      String message = await journeyApi.addJourney(context, idUser, journeyName, journeyDate, journeyContent);

      if(message == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      }

      setState(() {
        journeySubmitLoading = false;      
      });
    }
    
  }

  @override
  void initState() {
    super.initState();
    DateTime tempDate = DateTime.now();
    dateStart = DateFormat('yyyy-MM-dd').format(tempDate).toString();
  }

  
  @override
  void dispose() {
    journeyNameController.dispose();
    journeyContentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["journeyForm"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                child: Text(message["journeyName"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("journeyname"),
                  controller: journeyNameController,
                  keyboardType: TextInputType.text,
                  hintText: message["journeyName"],
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter Your Journey Name";
                    }
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text(message["start"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(left: 7.0, right: 7.0),
                    child: InkWell(
                      onTap: (){ _selectDate(context); },
                      key: Key("datestart"),
                      child: TextFields(
                        type: 2,
                        date: "${dateStart}",
                        icon: new Icon(Icons.calendar_today),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15),
                child: Text(message["description"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("content"),
                  maxLines: 3,
                  controller: journeyContentController,
                  keyboardType: TextInputType.multiline,
                  hintText: message["description"],
                  validator: (String value){
                    return null;
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0),
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: journeySubmitLoading ? 
                CircularProgressIndicator()
                :
                Button(
                  key: Key("submit"),
                  child: Text(message["submit"], style: TextStyle(fontSize: 20.0, color: config.primaryTextColor),),
                  backgroundColor: config.primaryColor,
                  rounded: true,
                  onTap: () {
                    submitJourney();
                  }
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}