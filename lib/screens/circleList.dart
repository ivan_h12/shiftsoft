//packages
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

//screens
import './profilePage.dart';
import './maps.dart';
import './circleForm.dart';
import './login.dart';

//widgets
import '../widgets/profileBox.dart';
import '../widgets/itemDetail.dart';

//resources
import 'package:shiftsoft/resources/circleApi.dart';

//models
import 'package:shiftsoft/models/mcircle.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart';

class CircleList extends StatefulWidget {
  int mode, id;
  Circle model;
  CircleList({Key key, this.mode, this.id, this.model}) : super(key:key);

  @override
  CircleListState createState() => CircleListState();
}

class CircleListState extends State<CircleList> {
  int id;
  List<Circle> circleList = [];
  List<User> userList = [];
  bool circleListLoading = true;

  List<String> imageLists = [];

  TabController _tabController;
  User user;

  final globalKey = GlobalKey<ScaffoldState>();
  final TextEditingController _controller = TextEditingController();
  bool _isSearching = false;
  String _searchText = "";
  List searchresult = List();

  Map<String, String> message = new Map();
  List<String> messageList = ["circle", "circleList", "circleChildren"];

  bool circleUpdateStatus = false;

  // _CircleListState() {
  //   _controller.addListener(() {
  //     if (_controller.text.isEmpty) {
  //       setState(() {
  //         _isSearching = false;
  //         _searchText = "";
  //       });
  //     } else {
  //       setState(() {
  //         _isSearching = true;
  //         _searchText = _controller.text;
  //       });
  //     }
  //   });
  // }

  @override
  void initState() {
    super.initState();
    user = User(id: 1926);

    _isSearching = false;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    initStatus();

    if (circleListLoading) {
      if (widget.mode == 0) {
        initCircleList();
      } else if (widget.mode == 1) {
        initCircleList(true);
      }
    }
  }

  @override
  void dispose(){
    _tabController.dispose();
    super.dispose();
  }

  initStatus() async {
    circleUpdateStatus = await checkPermission(context, "master.circle", "BackendCircle.Update", true);

    setState(() {
      circleUpdateStatus = circleUpdateStatus;
    });
  }

  initCircleList([bool parentId = false]) async{
    setState(() {
      circleListLoading = true;    
    });

    if (parentId) {
      circleList = await circleApi.getCircleListByParentId(context, widget.id);
    } else {
      circleList = await circleApi.getCircleListByUser(context, user.id);
    }

    if(circleList == null){
      circleList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      circleList = circleList;
      circleListLoading = false;    
    });
  }

  Widget makeUserList(){
    return Container(
      padding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 5.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  width: 75,
                  height: 75,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100.0)),
                    border: Border.all(
                      color: Colors.white,
                      width: 4.0,
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(margin: EdgeInsets.only(left: 20.0, top: 3.0),height: 20, width: 180.0, color: Colors.grey[200]),
                    Container(margin: EdgeInsets.only(left: 20.0, top: 3.0),height: 20, width: 100.0, color: Colors.grey[200])
                  ],
                )
              ],
            )
          ],
        ),
      );
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return DefaultTabController(
      length: 15,
      child: Scaffold(
        key: globalKey,
        appBar: AppBar(
          centerTitle: true, 
          title: !_isSearching ? 
          Text(
            message["circleList"],
            style: TextStyle(color: Colors.white),
          )
          :
          TextField(
            controller: _controller,
            style: TextStyle(
              color: Colors.white,
            ),
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.search, color: Colors.white),
              hintText: "Search...",
              hintStyle: TextStyle(color: Colors.white)),
              onChanged: searchOperation,
          ),
          backgroundColor: config.topBarBackgroundColor,
          iconTheme: IconThemeData(
            color: config.topBarIconColor
          ),
          bottom: TabBar(
            indicatorColor: config.topBarIconColor,
            tabs: List<Widget>.generate(circleList.length, (int index){
                return Tab(child: Text("${circleList[index].name}"));
              },
            )
          ),
          // actions: <Widget>[
          //   IconButton(
          //     icon: Icon(
          //       Icons.search,
          //       color: Colors.white,
          //     ),
          //     onPressed: () {
          //       if (_isSearching) {
          //         _handleSearchStart();
          //       } else {
          //         _handleSearchEnd();
          //       }
          //     }
          //   ),
          // ]
        ),
        body: RefreshIndicator(
          onRefresh: _refresh,
          child: circleListLoading ? 
          Shimmer.fromColors(
            baseColor: Colors.grey[200],
            highlightColor: Colors.grey[350],
            period: Duration(milliseconds: 800),
            child: Container(
              margin: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Container(height:200, width: 400, color: Colors.grey[200]),
                  Row(
                    children: <Widget>[
                      Container(height: 30, width: 200, color: Colors.grey[200], margin: EdgeInsets.only(top: 10.0, right: 10.0),),
                      Container(height: 30, width: 30, color: Colors.grey[200], margin: EdgeInsets.only(top: 10.0),),
                    ],
                  ),
                  makeUserList(),
                  makeUserList(),
                ],
              ),
            )
          )
          : 
          TabBarView(
            children: List<Widget>.generate(circleList.length, (int index){
              List<Widget> widgetList = [];

              if (circleList[index].childs != null) {
                circleList[index].childs.map((item) {
                  widgetList.add(
                    ListTile(
                      title: Text(item.name),
                      trailing: IconButton(
                        icon: Icon(
                          Icons.arrow_right
                        ),
                        onPressed: () {
                          Navigator.pushNamed(context, "circleList/${circleList[index].id}/1");
                        },
                      ),
                    )
                  );
                }).toList();
              }

              return RefreshIndicator(
                onRefresh: _refresh,
                child: ItemDetail(
                  content: Container(
                    child: Column(
                      children: <Widget>[
                        Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                child: Text(circleList[index].name, style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold)),
                                margin: EdgeInsets.only(top: 7.0),
                              ),
                              circleList[index].gps != "" ? 
                              IconButton(
                                key: Key("circlemaps$index"),
                                icon: Icon(Icons.gps_fixed, color: Colors.redAccent),
                                iconSize: 25.0,
                                onPressed: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => Maps(gps: circleList[index].gps))
                                  );
                                },
                              ) : Container(),
                              circleUpdateStatus ?
                              IconButton(
                                icon: Icon(Icons.edit),
                                iconSize: 25.0,
                                onPressed: () async {
                                  var nav = await Navigator.push(context, MaterialPageRoute(builder: (context) => CircleForm(model: circleList[index])));
                                  if(nav==true||nav==null){
                                    initCircleList();
                                  }
                                },
                              )
                              :
                              Container(),
                            ],
                          ),
                          margin: EdgeInsets.only(top: 8.0, left: 18.0),
                        ),
                        widgetList.length > 0 ?
                        ExpansionTile(
                          initiallyExpanded: false,
                          title: Text(message["circleChildren"]),
                          children: widgetList
                        ): Container()
                      ],
                    ),
                  ),
                  list: SliverList(
                    delegate: SliverChildBuilderDelegate((context, i) =>
                      Hero(
                        tag: "profile$i",
                        child: Column(
                          children: <Widget>[
                            InkWell(
                              key: Key("userdetail$i"),
                              child: Container(
                                padding: EdgeInsets.fromLTRB(10, 5, 0, 5),
                                child: ProfileBox(CompanyAsset("avatar", circleList[index].userList[i].pic), 75.0, searchresult.length != 0 || _controller.text.isNotEmpty ? searchresult[i] : circleList[index].userList[i].name, 
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.account_box),
                                      Text(circleList[index].userList[i].id.toString(), style: TextStyle(fontSize: 15.0))],
                                  ),
                                )
                              ),
                              onTap: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => ProfilePage(url: CompanyAsset("avatar", circleList[index].userList[i].pic), tagHero: "profile$index", model: circleList[index].userList[i], mode: 3,))
                                );
                              },
                            ),
                            Divider()
                          ],
                        )
                      ),
                      childCount: searchresult.length != 0 || _controller.text.isNotEmpty ? searchresult.length : circleList[index].userList.length,
                    )
                  ),
                  imageList: circleList[index].pic.split(","),
                  module: "circle",
                  pinned: false,
                ),
              );
            }),
          )
        )
      ) 
    );
  }

  Future<Null> _refresh() async {
    await initCircleList();
    return null;
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      _isSearching = false;
      _controller.clear();
      searchresult.clear();
    });
  }

  void searchOperation(String searchText) {
    int index = _tabController.index;
    String data;
    searchresult.clear();
    if (_isSearching != null) {
      for (int i = 0; i < circleList[index].userList.length; i++) {
        data = circleList[index].userList[i].name;
        if (data.toLowerCase().contains(searchText.toLowerCase())) {
          searchresult.add(data);
        }
      }
    }
  }
}