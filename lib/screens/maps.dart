import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shiftsoft/settings/configuration.dart';

class Maps extends StatefulWidget {
  String gps;
  Maps({Key key, this.gps}) : super(key:key);
  @override
  _MapsState createState() => _MapsState();
}

class _MapsState extends State<Maps> {
  String gps;
  double lat, lng;
  
  @override
  void initState() {
    gps = widget.gps;
    var tempGps = gps.split(',');
    lat = double.parse(tempGps[0]);
    lng = double.parse(tempGps[1]);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Maps", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body:Container(
        child: Column(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height-80,
              width: MediaQuery.of(context).size.width,
              child: GoogleMap(
                onMapCreated: (GoogleMapController controller) {
                  controller.addMarker(MarkerOptions(
                    position: LatLng(lat, lng),
                  ));
                },
                options: GoogleMapOptions(
                cameraPosition: CameraPosition(
                  target: LatLng(lat, lng), zoom: 15.0
                  ),
                ),
              ),
            )
          ],
        ),
      )
    );
  }
}