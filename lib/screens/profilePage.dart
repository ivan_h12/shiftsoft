// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/resources/userApi.dart';
import 'package:shiftsoft/screens/digitalCardList.dart';
import 'package:shiftsoft/screens/login.dart';
import 'package:shiftsoft/tools/functions.dart';

// widgets
import '../widgets/imageBox.dart';
import 'package:shiftsoft/widgets/listBox.dart';
import 'package:shiftsoft/widgets/button.dart';

// screens
import 'package:shiftsoft/screens/productCommentList.dart';
import 'package:shiftsoft/screens/productReviewList.dart';
import 'package:shiftsoft/screens/requestList.dart';
import 'package:shiftsoft/screens/journeyList.dart';
import 'package:shiftsoft/screens/transactionList.dart';
import 'package:shiftsoft/screens/changePassword.dart';
import 'package:shiftsoft/screens/editProfile.dart';
import 'package:shiftsoft/screens/setting.dart';
import 'package:shiftsoft/screens/productWishlistList.dart';

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/muser.dart';

class ProfilePage extends StatefulWidget{
  String url;
  Object tagHero;
  User model;
  int mode, idUser;

  ProfilePage({Key key, this.url, this.tagHero, this.model, this.mode, this.idUser}):super(key:key);

  createState(){
    return ProfilePageState();
  }
}

class ProfilePageState extends State<ProfilePage>{
  Map<String, String> message = Map();
  List<String> messageList = ["digitalId","reviewList","requestList","commentList","productWishlistList","profile","name","address","changePassword","editProfile","transactionList","spiritualJourney"];
  bool userLoading = true;
  User mUser;

  void initState(){
    super.initState();
    if(widget.mode == 1){
      initUserPoint();
    }else if(widget.mode == 3){
      mUser = widget.model;
      setState(() {
        userLoading = false;
        mUser = mUser; 
      });
    }
  }
  initUserPoint() async{
    setState(() {
      userLoading = true;
    });

    mUser = await userApi.getUser(context, widget.idUser);
    if(mUser == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    
    setState(() {
      mUser = mUser;
      userLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initUserPoint();
    return null;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      // backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(message["profile"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.settings
            ),
            color: Colors.white,
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => Setting()));
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: userLoading ? 
          Center(
            child: Padding(
              padding: EdgeInsets.only(top:15, bottom:15),
              child:Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircularProgressIndicator(),
                ]
              )
            ),
          )
          :
          ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context,int index){
              return Container(
                // height: MediaQuery.of(context).size.height/7*6,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10),
                      alignment: FractionalOffset.topCenter,
                      child: Hero(
                        tag: widget.tagHero,
                        child: GestureDetector(
                          child: ImageBox(widget.url, 200.0),
                        )
                      ),
                      margin: EdgeInsets.only(bottom: 10.0),  
                    ),
                    Text(message["name"] + ": " + mUser.name),
                    Text(message["address"] + ": "+ mUser.address),
                    Container(
                      width: MediaQuery.of(context).size.width-20,
                      margin: EdgeInsets.only(top: 10),
                      child: Button(
                        backgroundColor: config.primaryColor,
                        rounded: true,
                        child: Text(message["digitalId"],style: TextStyle(fontWeight: FontWeight.bold, color: config.primaryTextColor),),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>DigitalCardList(title: message["digitalId"], model: widget.model, ) ));
                        },
                      ),
                  ),
                    Center(
                      child: Container(
                        margin: EdgeInsets.all(10),
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.only(right: 10),
                                child: Button(
                                  child: Text(message["changePassword"], style: TextStyle(color: config.primaryTextColor),),
                                  rounded: true,
                                  backgroundColor: config.primaryColor,
                                  onTap: () {
                                    Navigator.push(context,
                                      MaterialPageRoute(
                                          builder: (context) => ChangePassword(title: message["changePassword"] ) 
                                        )
                                    );
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Button(
                                  rounded: true,
                                  child: Text(message["editProfile"], style: TextStyle(color: config.primaryTextColor),),
                                  backgroundColor: config.primaryColor,
                                  onTap: () {
                                    Navigator.push(context,
                                      MaterialPageRoute(
                                        builder: (context) => EditProfile(mode: 3,id: widget.model.id,title: message["editProfile"], model: widget.model,) 
                                      )
                                    );
                                  },
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(),
                    ListBox(
                      childs: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                            child: Text(message["transactionList"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,)),
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionList()));
                            },
                          )
                          
                        ],
                      ),
                    ),
                    ListBox(
                      childs: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                            child: Text(message["spiritualJourney"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,)),
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => JourneyList()));
                            },
                          )
                          
                        ],
                      ),
                    ),
                    ListBox(
                      childs: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                            child: Text(message["productWishlistList"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,)),
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductWishlistList()));
                            },
                          )
                          
                        ],
                      ),
                    ),
                    ListBox(
                      childs: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                            child: Text(message["commentList"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,)),
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductCommentList()));
                            },
                          )
                          
                        ],
                      ),
                    ),  
                    ListBox(
                      childs: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                            child: Text(message["reviewList"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,)),                          
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductReviewList()));
                            },
                          )
                          
                        ],
                      ),
                    ),
                    ListBox(
                      childs: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.only(left: 15, top: 15, bottom: 15),
                            child: Text(message["requestList"], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18,)),
                          ),
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_right),
                            onPressed: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => RequestList()));
                            },
                          )
                          
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          )
      )
    );
  }
}
