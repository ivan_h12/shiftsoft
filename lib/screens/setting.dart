import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shiftsoft/DemoLocalizations.dart';
import 'package:shiftsoft/settings/configuration.dart';

class Setting extends StatefulWidget {
  createState() {
    return SettingState();
  }
}
class SettingState extends State<Setting> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String currentLanguage;

  void initState() {
    super.initState();
    initLanguage();
  }

  initLanguage() async {
    String nowLanguage = await _prefs.then((SharedPreferences prefs) {
      return prefs.getString('language');
    });

    setState(() {
      currentLanguage = nowLanguage;
    });
  }

  List<DropdownMenuItem<String>> languages = [
    DropdownMenuItem(
      value: "en",
      child: Text("Inggris"),
    ),
    DropdownMenuItem(
      value: "id",
      child: Text("Indonesia"),
    )
  ];

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        child: Container(
          margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
          child: DropdownButton(
            isExpanded: true,
            value: currentLanguage,
            items: languages,
            onChanged: (String selected) async {
              final SharedPreferences prefs = await _prefs;
              prefs.setString("language", selected).then((bool success) {
                return selected;
              });

              DemoLocalizations.of(context).load(selected);
              
              setState(() {
                currentLanguage = selected;
              });
            },
          )
        ),
      )
    );
  }
}