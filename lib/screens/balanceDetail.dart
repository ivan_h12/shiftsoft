// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'dart:async';
import 'package:shiftsoft/tools/functions.dart';

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/pointApi.dart';

// models
import 'package:shiftsoft/models/mbalance.dart';
import 'package:shiftsoft/settings/configuration.dart';

class BalanceDetail extends StatefulWidget{
  String title;
  createState() {
    return BalanceDetailState();
  }
  BalanceDetail({this.title});
}

class BalanceDetailState extends State<BalanceDetail>{
  bool balanceDetailLoading = false;
  List<Balance> _balanceList = [];
  List<Widget> listMyWidget = List();
  final _formKey = GlobalKey<FormState>();
  String date1, date2;
  var tempString, tempSplit;
  DateTime _date = DateTime.now();
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;
  String tempDate;

  Map<String, String> message = Map();
  List<String> messageList = ["startDate","endDate","filter"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  @override
  void initState(){
    super.initState();
    date1 = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();
    date2 = DateFormat('yyyy-MM-dd').format(DateTime.now()).toString();
    tempDate = "";
    initBalanceList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_balanceList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }
  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _balanceList.addAll(await pointApi.getBalanceList(context, 1926, page));
      if(_balanceList == null){
        _balanceList = [];
        listMyWidget = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }else{
        await makeAnimation();
        listMyWidget.clear();
        _balanceList.map((item){
          if(tempDate != DateFormat("dd-MMMM-yyyy").format(item.createdAt).toString()){
            tempDate = DateFormat("dd-MMMM-yyyy").format(item.createdAt).toString();
            listMyWidget.add(
              Container(
                padding: EdgeInsets.all(7.5),
                width: MediaQuery.of(context).size.width,
                color: Colors.black12,
                child: Text(DateFormat("dd MMM yyyy").format(item.createdAt), style: TextStyle(color: Colors.black),),
              )
            );
          }
          listMyWidget.add(
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 15, right: 5, top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Text(item.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                          ),
                          Expanded(
                            flex: 1,
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.add, color: Colors.purple,size: 20,),
                                item.point.toString() != "" ?Text(item.point.toString(), style: TextStyle(color: Colors.purple, fontSize: 18),) : null,
                              ],
                            ),
                          )
                        ],
                      ),
                      Text(item.value.toString(), style: TextStyle(color: Colors.grey),),
                      Divider(),
                    ],
                  ),
                ),
              ],
            )
          );
        }).toList();
        page++;
      }

      setState(() {
        listMyWidget = listMyWidget;
        _balanceList = _balanceList;
        isLoading = false;
        page = page;
      });
    }
  }
  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }
  
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        date1 = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }
  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        date2 = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }
  
  initBalanceList([String filter=""]) async {
    setState(() {
      balanceDetailLoading = true;
      listMyWidget.clear();
    });
    
    _balanceList = await pointApi.getBalanceList(context, 1926, page, filter);
    if(_balanceList == null){
      _balanceList = [];
      listMyWidget = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }else {
      page++;
      if(_balanceList.length+1 !=1){
        _balanceList.map((item){
          if(tempDate != DateFormat("dd-MMMM-yyyy").format(item.createdAt).toString()){
            tempDate = DateFormat("dd-MMMM-yyyy").format(item.createdAt).toString();
            listMyWidget.add(
              Container(
                padding: EdgeInsets.all(7.5),
                width: MediaQuery.of(context).size.width,
                color: Colors.black12,
                child: Text(DateFormat("dd MMM yyyy").format(item.createdAt), style: TextStyle(color: Colors.black),),
              )
            );
          }
          listMyWidget.add(
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            flex: 3,
                            child: Text(item.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),),
                          ),
                          Expanded(
                            flex: 1,
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.add, color: Colors.purple,size: 20,),
                                item.point.toString() != "" ?Text(item.point.toString(), style: TextStyle(color: Colors.purple, fontSize: 18),) : null,
                              ],
                            ),
                          )
                        ],
                      ),
                      Text(item.value.toString(), style: TextStyle(color: Colors.grey),),
                      Divider(),
                    ],
                  ),
                ),
              ],
            )
          );
        }).toList();
        listMyWidget.add(
          Center(
            child: Opacity(
              opacity: isLoading ? 1.0 : 0.0,
              child: CircularProgressIndicator(),
            ),
          )
        );
      }else{
        listMyWidget.add(
          PlaceholderList(type: "balance",),
        );
      }
    }

    setState(() {
      listMyWidget = listMyWidget;
      _balanceList = _balanceList;
      balanceDetailLoading = false;
      page = page;
    });
  }

  Widget build(context){
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.sort),
            color: Colors.white,
            onPressed: () {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (context){
                  return AlertDialog(
                    title: Text(message["filter"]),
                    content: Container(
                      height: 175,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text(message["startDate"] + ": ", style: TextStyle(color: Colors.grey),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                              child: InkWell(
                                onTap: (){ _selectDate(context); },
                                child: TextFields(
                                  type: 2,
                                  date: "${date1}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              child: Text(message["endDate"] + ": ", style: TextStyle(color: Colors.grey),),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                              child: InkWell(
                                onTap: (){ _selectDate2(context); },
                                child: TextFields(
                                  type: 2,
                                  date: "${date2}",
                                  icon: new Icon(Icons.calendar_today),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ), 
                    ),
                    actions: <Widget>[
                      Button(
                        child: Text("OK"),
                        rounded: true,
                        backgroundColor: config.primaryColor,
                        splashColor: Colors.blueGrey,
                        onTap: ()async{
                          Navigator.pop(context);
                          initBalanceList("filters[]=created_at|between|$date1,$date2 23:59:59|and");
                        },
                      ),
                    ],
                  );
                }
              );              
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: balanceDetailLoading ? 
        CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
               Column(
                  children: <Widget>[
                    Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: Container(
                        padding: EdgeInsets.only(left: 5),
                        width: MediaQuery.of(context).size.width,
                        color: Colors.grey[200],
                        height: 20,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.only(left: 5, top: 15),
                      color: Colors.white,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              padding: EdgeInsets.only(left: 5),
                              width: 100,
                              color: Colors.grey[200],
                              height: 20,
                            ),
                          ),
                          ListTile(
                            leading: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(
                                width: 100,
                                color: Colors.grey[200],
                                height: 20,
                              ),
                            ),
                            trailing: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(
                                width: 50,
                                color: Colors.grey[200],
                                height: 20,
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                childCount: 5
              ),
            )
          ],
        )
        :
        CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                Column(
                  children: listMyWidget,
                ),
                childCount: 1,
              ),
            )
          ],
        ),
      ),
    );
  }
  Future<Null> _refresh() async {
    page = 1;
    await initBalanceList();
    _date = DateTime.now();
    date1 = "1999-09-23";
    date2 = "1999-09-23";
    tempDate = "";
    return null;
  }
}