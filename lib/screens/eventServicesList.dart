// packages
import 'package:flutter/material.dart';

// widget
import '../widgets/profileBox.dart';
import '../widgets/button.dart';
import '../widgets/loadingUserList.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// screens
import './profilePage.dart';
import './eventServiceAdd.dart';
import './login.dart';

// resources
import '../resources/eventServicesApi.dart';

// models
import 'package:shiftsoft/models/meventservices.dart';
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EventServicesList extends StatefulWidget{
  int id, mode;
  Event model;
  EventServicesList({Key key, this.mode, this.id, this.model}) : super(key:key);

  createState(){
    return EventServicesListState();
  }
}

class EventServicesListState extends State<EventServicesList>{
  List<EventService> _eventServicesList = [];
  bool eventServicesLoading = true;
  Event event;

  bool serviceAddStatus = false;

  Map<String, String> message = new Map();
  List<String> messageList = ["serviceslist", "serviceadd"];

  @override
  void initState(){
    super.initState();
    event = widget.model;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    if (eventServicesLoading)
      initEventServices();
    initServiceStatus();
  }

  initServiceStatus() async{
    serviceAddStatus = await checkPermission(context, "master.service", "BackendService.Create", true);

    setState(() {
      serviceAddStatus = serviceAddStatus;
    });
  }

  initEventServices() async {
    setState(() {
      eventServicesLoading = true;
    });
    
    _eventServicesList = await eventServicesApi.getEventServicesList(context, widget.id);
    if(_eventServicesList == null){
      _eventServicesList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _eventServicesList = _eventServicesList;
      eventServicesLoading = false;
    });
  }

  removeEventService(int id) async {
    String message = await eventServicesApi.removeEventService(context, id);
    if(message == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }else{
      setState(() {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: (){ Navigator.pop(context); },
                )
              ],
            );
          }
        );
        initEventServices();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["serviceslist"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: eventServicesLoading ? LoadingUserList() :
        Container(
          child: Column(
            children: <Widget>[
              serviceAddStatus?
              Container(
                alignment: Alignment.center,
                child: Container(
                  width: MediaQuery.of(context).size.width-10,
                  child: Button(
                    key: Key("eventservicesform"),
                    child: Text(message["serviceadd"], style: TextStyle(color: config.primaryTextColor)),
                    backgroundColor: config.primaryColor,
                    splashColor: Colors.blueGrey,
                    onTap: () async {
                      bool refresh = await Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => EventServiceAdd(id: widget.id, model: event))
                      );

                      if (refresh ?? false) {
                        initEventServices();
                      }
                    },
                    rounded: true
                  )
                )
              ): Container(),
              Container(
                child: Expanded(
                  child: SizedBox(
                    height: 800.0,
                    child: ListView.builder(
                      itemCount: _eventServicesList.length == 0 ? 1 : _eventServicesList.length,
                      itemBuilder: (BuildContext context, int index){
                        if(_eventServicesList.length != 0){
                          return Hero(
                            tag: "profile$index",
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  onTap: () async {
                                    var nav = await Navigator.push(context, MaterialPageRoute(builder: (context) => ProfilePage(url:CompanyAsset('avatar', _eventServicesList[index].user.pic) , tagHero: "profile$index", model: _eventServicesList[index].user, mode: 3,)));
                                    if(nav==true||nav==null){
                                      initEventServices();
                                    }
                                  },
                                  contentPadding: EdgeInsets.only(top: 25.0, right: 5.0, left: 25.0, bottom: 15.0),
                                  leading: Row(
                                    children: <Widget>[
                                      ProfileBox(CompanyAsset("avatar", _eventServicesList[index].user.pic), 75.0, _eventServicesList[index].user.name,
                                        Row(
                                          children: <Widget>[
                                            Text(_eventServicesList[index].name, style: TextStyle(fontSize: 15.0)),
                                          ],
                                        ),
                                      ),
                                      IconButton(
                                        icon: Icon(Icons.cancel, color: Colors.redAccent),
                                        iconSize: 30.0,
                                        onPressed: (){
                                          removeEventService(_eventServicesList[index].id);
                                        },
                                      )
                                    ],
                                  )
                                ),
                              ],  
                            ),
                          );
                        } else {
                          return PlaceholderList(type: "eventservices");
                        }
                      }
                    ),
                  ),
                ),
              )
            ],
          ) 
        ) 
      )
    );
  }

  Future<Null> _refresh() async {
    await initEventServices();
    return null;
  }
}