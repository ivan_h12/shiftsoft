// packages
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens


// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/productCartApi.dart';

// models
import 'package:shiftsoft/models/mproductcart.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductPromotionInsert extends StatefulWidget{
  String title;
  createState() {
    return ProductPromotionInsertState();
  }
  ProductPromotionInsert({this.title});
}

class ProductPromotionInsertState extends State<ProductPromotionInsert>{
  final codeController = TextEditingController();
  String result = "";
  
  Future scanQR() async{
    try{
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult;
      });
    } on PlatformException catch(ex){
      if (ex.code == BarcodeScanner.CameraAccessDenied){
        setState(() {
          result = "Permission Denied";          
        });
      }
      else{
        setState(() {
          result = "Unknown Error";          
        });
      }
    } on FormatException {
      setState(() {
        result = "You Pressed the back button before scan anything";        
      });
    } catch (ex){
      setState(() {
        result = "Unknown Error";        
      });
    }
    codeController.text = result;
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["insertCodeOrPromo","success","loginFailed","enterCode","insertCode","scanQr"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['insertCodeOrPromo'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: new EdgeInsets.all(20.0),
        child: new Form(
          child: new ListView(
            children: <Widget>[
              TextFormField( 
                key: Key("promoCode"),
                controller: codeController,
                decoration: new InputDecoration(
                  hintText: message["insertCode"],
                  labelText: message["enterCode"]
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5),
              ),
              RaisedButton(
                key: Key("scan"),
                child: Text(message["scanQr"]),
                onPressed: scanQR,
              ),
              RaisedButton(
                key: Key("use"),
                child: Text(widget.title),
                onPressed: () {
                  showDialog(
                    context: context,
                    barrierDismissible: false,
                    builder: (context){
                      return AlertDialog(
                        title: Text("Alert"),
                        content: Text("Code ${codeController.text}" + message['success']),
                        actions: <Widget>[
                          FlatButton(
                            key: Key("OK"),
                            child: Text("OK"),
                            onPressed: (){
                              Navigator.of(context).pop();            
                              Navigator.of(context).pop();
                            },
                          )
                        ],
                      );
                    }
                  );
                },
              )
            ],
          ),
        ),
      ),
    );

  }
}