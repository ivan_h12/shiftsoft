// package
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';

// screens
import './newsDetail.dart';
import './login.dart';

// widget
import '../widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/oneColumnView.dart';

// resources
import 'package:shiftsoft/resources/newsApi.dart';

// models
import 'package:shiftsoft/models/mnews.dart';
import 'package:shiftsoft/settings/configuration.dart';

class NewsSearch extends StatefulWidget {
  @override
  _NewsSearchState createState() => _NewsSearchState();
}

class _NewsSearchState extends State<NewsSearch> with SingleTickerProviderStateMixin {
  List<News> _newsList = [];
  List<News> _tagsList = [];
  bool newsListLoading = true;
  bool tagsListLoading = true;
  
  TabController _tabController;

  final TextEditingController _controller = TextEditingController();
  final globalKey = GlobalKey<ScaffoldState>();
  Widget appBarTitle = Text("",style: TextStyle(color: Colors.white));
  bool _isSearching = false;
  Icon icon = Icon(Icons.search, color: Colors.white);

  Map<String, String> message=new Map();
  List<String> messageList = ["search", "category", "news"];

  @override
  void initState() {
    super.initState();
    initNewsListAwal();
    initTagsList();
    _tabController = TabController(vsync: this, length: 2);
  }

  didChangeDependencies() {
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  initNewsListAwal() async {
    setState(() {
      newsListLoading = true;
    });
    
    _newsList = await newsApi.getNewsList(context, 1);

    if(_newsList == null){
      _newsList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _newsList = _newsList;
      newsListLoading = false;
    });
  }

  initNewsList() async {
    setState(() {
      newsListLoading = true;
    });
    
    _newsList = await newsApi.getNewsSearchList(context, _controller.text);
    if(_newsList == null){
      _newsList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _newsList = _newsList;
      newsListLoading = false;
    });
  }

  initTagsList() async {
    setState(() {
      tagsListLoading = true;
    });
    
    _tagsList = await newsApi.getTagsList(context);

    setState(() {
      _tagsList = _tagsList;
      tagsListLoading = false;
    });
  }

  initTagsDetailList(String search) async {
    setState(() {
      newsListLoading = true;
    });
    
    _newsList = await newsApi.getTagsDetailList(context, search);

    setState(() {
      _newsList = _newsList;
      newsListLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        key: globalKey,
        appBar: AppBar(
          backgroundColor: config.topBarBackgroundColor,
          iconTheme: IconThemeData(
            color: config.topBarIconColor
          ),
          title: TextField(
            controller: _controller,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              hintText: "${message["search"]}...",
              hintStyle: TextStyle(color: Colors.white)
            ),
            onSubmitted: (newValue) {
              setState(() {
                if(_controller.text.isNotEmpty) {
                  _newsList.clear();
                  initNewsList();
                  initTagsList();
                  _isSearching = true;
                }
              });
            },
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.close),
              onPressed: () {
                setState(() {
                  _isSearching = false;
                  _controller.clear();
                  _newsList.clear();
                  initNewsListAwal();
                });
              },
            )
          ],
          bottom: TabBar(
            indicatorColor: config.topBarIconColor,
            controller: _tabController,
            tabs: [
              Tab(text: message["news"]),
              Tab(text: message["category"]),
            ],
          ),
        ),
        body: RefreshIndicator(
          onRefresh: _refresh,
          child: newsListLoading ?
          ListView.builder(
            itemCount: 5,
            itemBuilder: (BuildContext context, int index) {
              return Shimmer.fromColors(
                baseColor: Colors.grey[200],
                highlightColor: Colors.grey[350],
                period: Duration(milliseconds: 800),
                child: ListTile(
                  contentPadding: EdgeInsets.all(25.0),
                  leading: Container(width: 100, height: 100, color: Colors.grey[200]),
                  title: Container(width: 100, height: 50, color: Colors.grey[200])
                )
              );
            }
          )
          :
          TabBarView(
            controller: _tabController,
            children: <Widget>[
              _newsList.length > 0 ?
              ListView.builder(
                itemCount: _newsList.length,
                itemBuilder: (BuildContext context, int index){
                  News item = _newsList[index];
                  
                  return OneColumnView(
                    key: Key("newsdetail$index"),
                    imageUrl: item.pic,
                    title: item.subject,
                    date: DateFormat('dd MMM yyyy').format(item.createdAt),
                    widgets: [
                      Icon(
                        Icons.thumb_up,
                        color: Colors.grey,
                        size: 15
                      ),
                      Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                      Text(item.likes.toString()),
                      Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
                      Icon(
                        Icons.remove_red_eye,
                        color: Colors.grey,
                        size: 15
                      ),
                      Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
                      Text(item.recipients.toString()),
                      Padding(padding: EdgeInsets.symmetric(horizontal: 4)),
                      Icon(
                        Icons.comment,
                        color: Colors.grey,
                        size: 15
                      ),
                      Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                      Text(item.comments.toString()),
                    ],
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => NewsDetail(mode:1, id:item.id, model: item))
                      );
                    },
                  );
                },
              )
              :
              ListView.builder(
                itemCount: 1,
                itemBuilder: (BuildContext context, int index){
                  return PlaceholderList(type: "news");
                },
              ),
              _tagsList.length > 0 ?
              ListView.builder(
                itemCount: _tagsList.length,
                itemBuilder: (BuildContext context, int index){
                  return ListTile(
                    contentPadding: EdgeInsets.only(left: 25.0, top: 10.0),
                    leading: Text(_tagsList[index].content),
                    onTap: (){
                      _newsList.clear();
                      initTagsDetailList(_tagsList[index].content);
                      _tabController.animateTo((_tabController.index + 1) % 2);
                    },
                  );
                },
              )
              :
              ListView.builder(
                itemCount: 1,
                itemBuilder: (BuildContext context, int index){
                  return PlaceholderList(type: "tags");
                },
              ),
            ],
          ),
        )
      )
    );
  }

  Future<Null> _refresh() async {
    if(_isSearching){
      await initNewsList();
      await initTagsList();
    } else {
      await initNewsListAwal();
      await initTagsList();      
    }
    return null;
  }
}