// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/screens/digitalCardDetail.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:barcode_flutter/barcode_flutter.dart';

// widgets
import 'package:shiftsoft/widgets/listBox.dart';

// screens

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/models/muser.dart';

class DigitalCardList extends StatefulWidget{
  List<String> imageLists=<String>[
    "https://cdn.shopify.com/s/files/1/2008/1475/products/10218_0_0x2_70bb1e8c-5fb9-4fee-9f6d-dc841103edd7_grande.jpg?v=1526348925",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/Cascade_Badge_Fix_grande.png?v=1526350712",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/10218_0_0x2_be8f032c-dce8-40a0-b9bf-8e51f71753e4_grande.jpg?v=1526349218",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/10218_0_0x2_fa6a4c30-208d-4df1-b32c-779a8e941134_grande.jpg?v=1526349320",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/Soul_Badge_Fix_grande.png?v=1526350678",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/10218_0_0x2_15696bd2-1d52-45a0-9c73-5f9e9ae8b163_grande.jpg?v=1526349726",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/Volcano_Badge_Fix_grande.png?v=1526350743",
    "https://cdn.shopify.com/s/files/1/2008/1475/products/Earth_Badge_Fix_grande.png?v=1526350795"
  ];
  List<String> badgeName=<String>[
    "Boulder Badge", "Cascade Badge", "Thunder Badge", "Rainbow Badge", "Soul Badge","Marsh Badge", "Volcano Badge", "Earth Badge"
  ];
  String title;
  User model;

  @override
  createState() {
    return DigitalCardListState();
  }

  DigitalCardList({this.title,this.model});
}

class DigitalCardListState extends State<DigitalCardList>{
  Map<String, String> message = Map();
  List<String> messageList = ["tapCard", "idCard"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      // backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
           SliverList(
             delegate: SliverChildBuilderDelegate((context, index) =>
              Column(
                children: <Widget>[
                  Center(
                    child:FlatButton(
                      onPressed: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context)=>DigitalCardDetail(title: message["idCard"],)));
                      },
                      child: ImageBox("https://img1.iwascoding.com/4/paid/2018/02/16/EC/6E5DDB00F5580135DFC2543D7EF8F2C1.jpg",300),
                    ),
                    
                  ),
                  ListBox(
                    childs: Center(
                      child: Text(message["tapCard"]),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(5),
                    child: ListBox(
                      childs: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left:20,),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Barcode ID",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                                ButtonBar(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(Icons.info, color: Colors.blue,),
                                      onPressed: (){

                                      },
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 10, right: 5),
                            child: BarCodeImage(
                              data: widget.model.name,              // Code string. (required)
                              codeType: BarCodeType.Code128,  // Code type (required)
                              lineWidth: 1.4,                // width for a single black/white bar (default: 2.0)
                              barHeight: 90.0,               // height for the entire widget (default: 100.0)
                              hasText: false,                 // Render with text label or not (default: false)
                              onError: (error) {             // Error handler
                                print('error = $error');
                              },
                            ),
                          )
                        ],
                      )
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(5),
                    child: ListBox(
                      childs: Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left:20,),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("QR Code",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
                                ButtonBar(
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(Icons.info, color: Colors.blue,),
                                      onPressed: (){

                                      },
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(bottom: 10),
                            child: QrImage(
                              data: widget.model.name,
                              size: 150,
                            ),
                          )
                        ],
                      )
                    ),
                  ),
                ],
              ),
              childCount: 1,
             )
           ),
           SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 5,
                childAspectRatio: 0.8
      
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.only(top: 20,right: 5),
                    alignment: Alignment.center,
                    child: Column(
                      children: <Widget>[
                        ImageBox(widget.imageLists[index], 50),
                        Text(widget.badgeName[index],textAlign: TextAlign.center,),
                      ],
                    ),
                  );
                },
                childCount: 8,
              ),
            )   
        ],
      )
    );
  }
}