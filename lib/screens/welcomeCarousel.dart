import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/screens/companyList.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/screens/login.dart';

class WelcomeCarousel extends StatefulWidget {
  createState() {
    return WelcomeCarouselState();
  }
}
class WelcomeCarouselState extends State<WelcomeCarousel> {
  bool next = false;
  bool skip = true;

  final swiperController = SwiperController();

  List<String> imageList = [
    "https://data.whicdn.com/images/102035823/original.png",
    "https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX3055293.jpg",
    "http://vimazing.com/wp-content/uploads/2018/11/beautiful-vertical-shower-tiles-accent-bathroom-gombrel-home-designs-768x1024.jpeg",
  ];

  void initState() {
    super.initState();
    
  }

  Widget build(context) {
    final config = Configuration.of(context);

    return Scaffold(
      body: Stack(
        children: [
          Swiper(
            onIndexChanged: (index) {
              if (index == imageList.length -1) {
                next = true;
                skip = false;
              } else {
                next = false;
                skip = true;
              }

              setState(() {
                next = next;
                skip = skip;
              });
            },
            itemBuilder: (BuildContext context,int index){
              return InkWell(
                key: Key("welcome carousel$index"),
                child:ImageBox(imageList[index], 0, border: false),
                onTap: () {
                  if (index != imageList.length-1) {
                    swiperController.next();
                  }
                },
              );
            },
            itemCount: imageList.length,
            controller: swiperController,
            pagination: SwiperPagination(
              builder: SwiperCustomPagination(builder:
                (BuildContext context, SwiperPluginConfig config) {
                  return DotSwiperPaginationBuilder(
                    color: Colors.white,
                    activeColor: Colors.grey[300],
                    size: 10.0,
                    activeSize: 12.0
                  ).build(context, config);
                }
              )
            ),
            loop: false,
            autoplay: false,
          ),
          skip ?
          Positioned(
            right: 15,
            top: 25,
            child: Button(
              fill: false,
              border: false,
              child: Text(
                "Skip",
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                swiperController.move(imageList.length-1);
              },
            )
          )
          :
          Container(),
          
          next ?
          Positioned(
            right: 15,
            bottom: 15,
            child: Button(
              fill: false,
              border: false,
              child: Text(
                "Next",
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                if (config.multipleCompany && config.triggerCompanyList) {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => CompanyList())
                  );
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Login())
                  );
                }
              },
            )
          )
          :
          Container()
        ],  
      )
    );
  }
}