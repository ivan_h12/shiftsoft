// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/rentDetail.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources
import 'package:shiftsoft/resources/productApi.dart';

//models
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/settings/configuration.dart';

class RentSearch extends StatefulWidget{
  String searchText;

  RentSearch({this.searchText});

  @override
    createState() {
      return RentSearchState();
    }
}

class RentSearchState extends State<RentSearch>{
  TextEditingController _controller = TextEditingController();
  final TextEditingController _title = TextEditingController();
  List<DropdownMenuItem<String>> _categoryItems, _subCategoryItems;
  String _currentCategory, _currentSubCategory;
  bool productListLoading = true;

  @override
  void initState(){
    initRentSearchList();
    _categoryItems = getCategoryItems();
    _subCategoryItems = getSubCategoryItems();
    _currentCategory = _categoryItems[0].value;
    _currentSubCategory = _subCategoryItems[0].value;
  }

  List category = ["Comics","News","Paper","Magazines"];
  List subCategory = ["Anime","Marvel","DC","KPOP"];

  List<DropdownMenuItem<String>> getCategoryItems() {
    List<DropdownMenuItem<String>> items = List();
    for (String da in category) {
      items.add(DropdownMenuItem(
          value: da,
          child:Text(da)
      ));
    }
    return items;
  }
  
  List<DropdownMenuItem<String>> getSubCategoryItems() {
    List<DropdownMenuItem<String>> items = List();
    for (String da in subCategory) {
      items.add(DropdownMenuItem(
          value: da,
          child:Text(da)
      ));
    }
    return items;
  }

  void changedCategory(String selected) {
    setState(() {
      _currentCategory = selected;
    });
  }

  void changedSubCategory(String selected) {
    setState(() {
      print(selected);
      _currentSubCategory = selected;
    });
  }

  initRentSearchList([String filter =""]) async {
    setState(() {
      productListLoading = true;
    });
    _controller.text= widget.searchText;  
    _productList = await productApi.getProductList(context, 3, 1, filter);

    setState(() {
      _productList = _productList;
      _controller.text= widget.searchText;  
      productListLoading = false;
    });

  }

  Future<Null> _refresh() async {
    await initRentSearchList();
    return null;
  }

  List<Product> _productList = [];

   final key = GlobalKey<ScaffoldState>();

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","addToCart","addToWishlist","rentBooks","title","author","available","rent"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void _select(Choice choice) {
    if(choice.title == "Add To Cart"){
      key.currentState.showSnackBar(SnackBar(content: Text(message['addToCart']+" index-" + choice.index.toString() )));
    }else if(choice.title == "Add To Wishlist"){
      key.currentState.showSnackBar(SnackBar(content: Text(message['addToWishlist']+" index-" + choice.index.toString() )));
    }
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      key: key,
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: config.topBarBackgroundColor,
            iconTheme: IconThemeData(
              color: config.topBarIconColor
            ),
            automaticallyImplyLeading: false,
            floating: false,
            // pinned: true,
            title: Container(    
              // height: MediaQuery.of(context).size.width/16*9,
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.only(left: 10),
              color: config.topBarTextColor,
              child: Form(
                child: TextField(
                  controller: _controller,
                  keyboardType: TextInputType.text,
                  // onChanged: searchOperation,
                  decoration: InputDecoration(
                    prefixIcon: IconButton(
                      onPressed: (){
                        Navigator.of(context).pop();
                      },
                      icon: Icon(Icons.arrow_back),
                    ),
                    hintText: "Search Book",
                    suffixIcon: IconButton(
                      onPressed: (){
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context){
                            return AlertDialog(
                              title: Text("Filter"),
                              content: Form(
                                child: ListView(
                                  children: <Widget>[
                                    TextFormField(
                                      controller: _title,
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                        hintText: message['title'],
                                        labelText: message['title']
                                      ),
                                    ),
                                    TextFormField(
                                      keyboardType: TextInputType.text,
                                      decoration: InputDecoration(
                                        hintText: message['author'],
                                        labelText: message['author']
                                      ),
                                    ),
                                    Container(
                                      margin:  EdgeInsets.only(top: 7.0),
                                      child: Text("Category", style: TextStyle(fontSize: 18.0, color: Colors.grey))
                                    ),
                                    Container(
                                      height: 30,
                                      margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
                                      child: DropdownButton(
                                        isExpanded: true,
                                        value: _currentCategory,
                                        items: _categoryItems,
                                        onChanged: changedCategory,
                                      )
                                    ),
                                    Container(
                                      margin:  EdgeInsets.only(top: 7.0),
                                      child: Text("Sub Category", style: TextStyle(fontSize: 18.0, color: Colors.grey))
                                    ),
                                    Container(
                                      height: 30,
                                      margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
                                      child: DropdownButton(
                                        isExpanded: true,
                                        value: _currentSubCategory,
                                        items: _subCategoryItems,
                                        onChanged: changedSubCategory,
                                      )
                                    ),
                                  ],
                                ),
                              ),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text("OK"),
                                  onPressed: (){
                                    Navigator.pop(context, "&filters[]=name|like|%"+_title.text+"%|and");
                                    print(_title.text);
                                    initRentSearchList("&filters[]=name|like|%"+_title.text+"%|and");
                                    // Navigator.of(context).pop(context);
                                  },
                                )
                              ],
                            );
                          }
                        );
                      },
                      icon: Icon(Icons.filter_list),
                    ),
                  ),
                )
              ),
            ),
          ),
          RefreshIndicator(
            onRefresh: _refresh,
            child: productListLoading ?
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                Container(
                  padding: EdgeInsets.only(top: 10),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  child: Card(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            child: ImageBox("", 100),
                            height: 150,
                            width: 100,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  margin: EdgeInsets.all(10),
                                  height: 40,
                                  width: MediaQuery.of(context).size.width,
                                  color: Colors.grey[200],
                                ),
                              ),
                              ListTile(
                                title: Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(
                                    margin: EdgeInsets.all(10),
                                    height: 40,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.grey[200],
                                  ),
                                ),
                                trailing: Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(
                                    margin: EdgeInsets.all(10),
                                    height: 40,
                                    width: MediaQuery.of(context).size.width,
                                    color: Colors.grey[200],
                                  ),
                                ),
                              ),
                            ],
                          ),  
                        ),
                      ],
                    ),
                  ),  
                ),
              childCount: 1,
              ),
            )
            :
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
              Container(
                padding: EdgeInsets.only(top: 10),
                width: MediaQuery.of(context).size.width,
                height: 200,
                child: Card(
                  child: InkWell(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RentDetail(title: _productList[index].name, tagHero: "image$index",)),
                      );
                    },
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Container(
                            child: Hero(
                              tag: "image$index",
                              child: InkWell (
                                onTap: (){
                                  Navigator.push(context, 
                                    MaterialPageRoute(
                                      builder: (context) => RentDetail(title: _productList[index].name,tagHero: "image$index",)
                                    )
                                  );
                                },
                                child: ImageBox("https://ecs5.tokopedia.net/newimg/product-1/2014/2/12/3249666/3249666_5bd73756-93d0-11e3-b1cf-88bd2523fab8.jpg", 100),
                              )
                            ),
                            height: 150,
                            width: 100,
                          ),
                        ),
                        Expanded(
                          flex: 2,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              ListTile(
                                title: Text(widget.searchText + _productList[index].name ),
                                subtitle: Text("Rick Warren"),
                                trailing:  PopupMenuButton<Choice>(
                                onSelected: _select,
                                  itemBuilder: (BuildContext context){
                                    return choices.map((Choice choice) {
                                      choice.index = index;
                                      return PopupMenuItem<Choice>(
                                        value: choice,
                                        child: Text(choice.title),
                                      );
                                    }).toList();
                                  },
                                ),
                              ),
                              ListTile(
                                title:Row(
                                  children: <Widget>[
                                    Text("4." + "$index" ),
                                    Icon(Icons.star),
                                  ],
                                ),
                                trailing: index%2 == 0 ? Text(message['available']) : Text(message['rent']+", 2 month"),
                              ),
                            ],
                          ),  
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              childCount: _productList.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Choice {
  Choice({this.title, this.index});
  String title;
  int index;
}

List<Choice> choices = <Choice>[
  Choice(title: 'Add To Cart'),
  Choice(title: 'Add To Wishlist'),
];