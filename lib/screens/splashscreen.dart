// packages
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:onesignal/onesignal.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shared_preferences/shared_preferences.dart';

// screens
import 'package:shiftsoft/screens/forgetPassword.dart';
import 'package:shiftsoft/screens/welcomeCarousel.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/DemoLocalizations.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/companyApi.dart';

// models
import 'package:shiftsoft/models/mcompany.dart';

class SplashScreen extends StatefulWidget {
  int companyId;
  
  SplashScreen({Key key, this.companyId}): super(key:key);

  createState() {
    return SplashScreenState();
  }
}
class SplashScreenState extends State<SplashScreen> {
  bool languageLoading = true;
  bool oneSignalLoading = true;
  bool companyLoading = true;

  void initState() {
    super.initState();
    initLanguage();
    initOneSignal();
  }
  var settings = {
    OSiOSSettings.autoPrompt: false,
    OSiOSSettings.promptBeforeOpeningPushUrl: true
  };

  didChangeDependencies() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    List<String> loginList = await prefs.getStringList("loginList") ?? [];
    print(loginList);
  }

  initOneSignal() async {
    if (oneSignalLoading) {
      await OneSignal.shared.init(c.oneSignalAppId, iOSSettings: settings);
      final status = await OneSignal.shared.getPermissionSubscriptionState();
      String playerId = status.subscriptionStatus.userId;
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setString('oneSignalPlayerId', playerId);

      OneSignal.shared.setInFocusDisplayType(OSNotificationDisplayType.notification);
      OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
        if (result.notification.payload.additionalData["payload"] != "") {
          final linkArr = result.notification.payload.additionalData["payload"].split("/");
          String link = "";
          
          if (linkArr.length > 1) {
            link = "${linkArr[0]}/${linkArr[1]}/1";
          } else {
            link = "${linkArr[0]}";
          }
          // setState(() {
          //   companyLoading = false;
          // });

          Navigator.pushNamed(context, link);
        }
      });

      initCompanySetting();
    }
  }


  initLanguage() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    String nowLanguage = prefs.getString("language") ?? "en";
    
    await DemoLocalizations.of(context).load(nowLanguage);

    prefs.setString("language", nowLanguage).then((bool success) {
      return nowLanguage;
    });

    setState(() {
      languageLoading = false;
    });
  }

  initCompanySetting() async {
    Configuration config = Configuration.of(context);
    int companyId;

    if (widget.companyId == null)
      companyId = config.companyId;
    else companyId = widget.companyId;

    if (companyLoading) {
      Company company = await companyApi.getCompany(context, companyId);

      config.serverProdHash = company.hash;
      config.serverProdCompany = company.slug;
      config.company = company.name;
      config.companyId = company.id;
      config.oneSignalAppId = company.oneSignalAppId;
      // config.topBarBackgroundColor = Colors.black;
      // config.topBarTextColor = Colors.white;
      // config.topBarIconColor = Colors.white;
      // config.primaryColor = Colors.black;
      // config.primaryTextColor = Colors.white;
      // config.secondaryColor = Colors.black;
      // config.secondaryTextColor = Colors.white;
      setState(() {
        companyLoading = false;
      });

      Navigator.pushNamed(context, config.initialRoute == "" ? "welcomeCarousel":config.initialRoute);
    }
  }

  Widget build(context) {
    final config = Configuration.of(context);

    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              languageLoading ?
              Text("Loading wonderful language...")
              :
              Text(""),

              languageLoading == false && companyLoading ?
              Text("Loading setting from company...")
              :
              Text(""),

              languageLoading == false && companyLoading == false ?
              Text("Loading completed...")
              :
              Text(""),

              Padding(padding: EdgeInsets.symmetric(vertical: 15),),

              CircularProgressIndicator(),

              Button(
                child: Text("Next"),
                onTap: () {
                  Navigator.pushNamed(context, config.initialRoute == "" ? "welcomeCarousel":config.initialRoute);
                },
              )
            ],
          ),
        )
      )
    );
  } 
}