// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shimmer/shimmer.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens
import 'package:shiftsoft/screens/productPromotionInsert.dart';
import 'package:shiftsoft/screens/productWishlistList.dart';
import 'package:shiftsoft/screens/login.dart';
import 'package:shiftsoft/screens/transactionDetail.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/textField.dart';

// resources
import 'package:shiftsoft/resources/productCartApi.dart';

// models
import 'package:shiftsoft/models/mproductcart.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/settings/configuration.dart';


class ProductCartList extends StatefulWidget{
  createState(){
    return ProductCartListState();
  }

}

class ProductCartListState extends State<ProductCartList>{
  int mCounter = 1;
  int currentIndex = 0;
  TextEditingController counterController = TextEditingController(text: "1");
  TextEditingController quantityController;
  Map<String, String> message = Map();
  List<String> messageList = [
    "writeNote","addFromWishlist","useCode","totalPrice","inbox","cart","account",
    "delete", "alert", "areYouSure", "success", "checkOut"
  ];

  User user;

  @override
  void initState(){
    super.initState();
    user = User(id: 1926);
    initProductCartList();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  void onTapIndex(int index){
    setState(() {
      currentIndex = index;
    });
  }

  void changeQuantity(int index, int quantity) {
    quantityController = TextEditingController(text: quantity.toString());
    Alert(
      context: context,
      title: "",
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  flex: 7,
                  child: Text(_productCartList[index].name),
                ),
                Expanded(
                  flex: 4,
                  child: Button(
                    fill: false,
                    border: false,
                    child: Icon(Icons.remove_circle),
                    onTap: () {
                      int tempQuantity = int.tryParse(quantityController.text) - 1;
                      if (tempQuantity > 0) {
                        quantityController.text = tempQuantity.toString();
                      }
                    },
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: TextFields(
                    controller: quantityController,
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    enabled: false,
                  ),
                ),
                Expanded(
                  flex: 4,
                  child: Button(
                    fill: false,
                    border: false,
                    child: Icon(Icons.add_circle_outline),
                    onTap: () { 
                      int tempQuantity = int.tryParse(quantityController.text) + 1;
                      quantityController.text = tempQuantity.toString();
                    },
                  ),
                )
              ],
            ),
          ],
        )
      ),
      defaultAction: () async {
        int tempQuantity = int.tryParse(quantityController.text);
        if (tempQuantity != quantity) {
          Result result = await productCartApi.checkProductCartQuantity(context, _productCartList[index].id.toString(), tempQuantity);
          
          if (result.success == 1) {
            setState(() {
              _productCartList[index].quantity = tempQuantity;
            });
            Alert(
              context: context,
              title: message["success"],
              type: "success",
              content: Text(result.message),
            );
          } else if (result.success == 0) {
            Alert(
              context: context,
              title: message["alert"],
              content: Text(result.message),
            );
          }
        }
      }
    );
  }

  List<ProductCart> _productCartList = [];
  bool productCartListLoading = true;

  initProductCartList() async {
    setState(() {
      productCartListLoading = true;
    });
    
    _productCartList = await productCartApi.getProductCartList(context, 2);
    if(_productCartList == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _productCartList = _productCartList;
      productCartListLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initProductCartList();
    return null;
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    int totalPrice = 0;
    _productCartList.map((item) {
      totalPrice += item.quantity * item.price;
    }).toList();

    return Scaffold(
      appBar: AppBar(
        title: Text(message["cart"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productCartListLoading ?
          ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context, int index){
            return Column(
              children: <Widget>[
                ListView.builder(
                  itemCount: 2,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (BuildContext context, int index){
                    return Card(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: ListTile(
                              leading: ImageBox("", 50),
                              title: Container(width: 10,height: 10,color: Colors.grey[200],),
                              subtitle: Container(width: 1,height: 10,color: Colors.grey[200],),
                              trailing: IconButton(
                                onPressed: (){
                                },
                                icon: Icon(Icons.delete),
                              ),
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(Icons.remove_circle),
                                  onPressed: (){
                                  },
                                ),
                                Container(
                                  width: 30,
                                  color: Colors.grey[200],
                                ),
                                IconButton(
                                  icon: Icon(Icons.add_circle),
                                  onPressed: (){
                                  },
                                ),
                              ],
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: FlatButton(
                              onPressed: (){
                              },
                              child: Text(message["writeNote"], style: TextStyle(color: Colors.green),),
                            ),
                          ),      
                        ],
                      ),
                    );
                  },
                ),
                Card(
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.grey[350],
                    period: Duration(milliseconds: 800),
                    child: Center(
                      child: FlatButton(
                        child: Text(message["addFromWishlist"], style: TextStyle(color: Colors.green)),
                      ),
                    ),
                  ),
                ),
                Divider(),
                Container(
                  child: Card(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Center(
                              child: Column(
                                children: <Widget>[
                                  Text(message["totalPrice"], style: TextStyle(color: Colors.grey),),
                                  Container(width: 100,height: 10,color: Colors.grey[200],),
                                ],
                              ),
                            ),
                          ),
                          
                        ),
                        Expanded(
                          flex: 1,
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: RaisedButton(
                              color: Colors.orange,
                              child: Text("Checkout(1)"),
                              onPressed: (){

                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        )
        :
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context, int index){
            return Column(
              children: <Widget>[
                ListView.builder(
                  itemCount: _productCartList.length != 0 ? _productCartList.length : 1,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (BuildContext context, int index){
                    if(_productCartList.length != 0) {
                      ProductCart item = _productCartList[index];
                      return Card(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                              leading: ImageBox(CompanyAsset("product", _productCartList[index].pic), 50),
                              title: Text(_productCartList[index].name, style: TextStyle(fontWeight: FontWeight.bold)),
                              subtitle: Text(numberFormat(_productCartList[index].price, config.currency), style: TextStyle(color: Colors.orange),),
                              trailing: IconButton(
                                onPressed: () async {
                                  Alert(
                                    context: context,
                                    title: message["areYouSure"],
                                    defaultAction: () async {
                                      Result result = await productCartApi.deleteCart(context, item.id);
                                      if (result.success == 1) {
                                        Alert(
                                          context: context,
                                          type: "success",
                                          title: message["alert"],
                                          content: Text(result.message),
                                        );
                                        _productCartList.removeAt(index);

                                        setState(() {
                                          _productCartList = _productCartList; 
                                        });
                                      } else if (result.success == 0) {
                                        Alert(
                                          context: context,
                                          type: "error",
                                          title: message["alert"],
                                          content: Text(result.message),
                                        );
                                      }
                                    },
                                  );
                                },
                                icon: Icon(Icons.delete),
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(Icons.remove_circle),
                                  onPressed: (){
                                    changeQuantity(index, item.quantity);
                                  },
                                ),
                                Button(
                                  fill: false,
                                  border: false,
                                  child: Text("${item.quantity}"),
                                  onTap: () {
                                    changeQuantity(index, item.quantity);
                                  }
                                ),
                                IconButton(
                                  icon: Icon(Icons.add_circle),
                                  onPressed: (){
                                    changeQuantity(index, item.quantity);
                                  },
                                ),
                              ],
                            ),
                            Button(
                              fill: false,
                              border: false,
                              onTap: () {
                                print("catatan penjual");
                              },
                              child: Text(message["writeNote"], style: TextStyle(color: config.primaryColor),),
                            )
                          ],
                        ),
                      );  
                    } else {
                      return PlaceholderList(type: "cart", paddingBottom: 50,);
                    }
                  },
                ),
                Divider(),
                Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only( right: 5),
                          child: Button(
                            rounded: true,
                            backgroundColor: Colors.white,
                            key: Key("fromWishlist"),
                            child: Text(
                              message["addFromWishlist"],
                              style: TextStyle(color: config.primaryColor),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            onTap: ()async{
                              bool refresh = await Navigator.push(context,MaterialPageRoute(builder: (context) => ProductWishlistList()));

                              if(refresh ?? false) {
                                initProductCartList();
                              }
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Button(
                          rounded: true,
                          key: Key("useCode"),
                          backgroundColor: config.primaryColor,
                          child: Text(message["useCode"], style: TextStyle(color: config.primaryTextColor)),
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ProductPromotionInsert(title: message["useCode"],)));
                          },
                        ),
                      )
                    ],
                  ),
                ),
                Divider(),
                Container(
                  child: Card(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: Column(
                            children: <Widget>[
                              Text(message["totalPrice"], style: TextStyle(color: Colors.grey),),
                              Text(numberFormat(totalPrice, config.currency), style: TextStyle(color: Colors.orange))
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: Padding(
                            padding: EdgeInsets.only(right: 5),
                            child: Button(
                              rounded: true,
                              backgroundColor: config.primaryColor,
                              child: Text( message["checkOut"] + " ("+ _productCartList.length.toString() +")", style: TextStyle(color: config.primaryTextColor),),
                              onTap: _productCartList.length == 0 ?
                              null
                              :
                              () async {
                                Result result = await productCartApi.checkoutCart(context, user.id);
                                var temp = result.message.split("|");
                                if(message == null){
                                  showDialog(
                                    context: context,
                                    barrierDismissible: false,
                                    builder: (context){
                                      return AlertDialog(
                                        title: Text("Alert"),
                                        content: Text("Login Gagal"),
                                        actions: <Widget>[
                                          FlatButton(
                                            key: Key("ok"),
                                            child: Text("OK"),
                                            onPressed: (){
                                              return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                            },
                                          )
                                        ],
                                      );
                                    }
                                  );
                                }
                                Alert(
                                  type: "success",
                                  content: Text(temp[0]),
                                  cancel: false,
                                  defaultAction: () {
                                    Navigator.pushNamed(context, "transactionList");
                                    Navigator.pushNamed(context, "transactionDetail/${temp[1]}/1");
                                  }
                                );
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            );
          },
        ),
      ),
      // bottomNavigationBar: !productCartListLoading ? 
      // BottomNavigationBar(
      //   items: <BottomNavigationBarItem>[
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.home),
      //       title: Text("Home"),
      //       backgroundColor: Colors.grey,
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.feedback),
      //       title: Text("Feed"),
      //       backgroundColor: Colors.grey 
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.message),
      //       title: Text(message["inbox"]),
      //       backgroundColor: Colors.grey 
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.local_grocery_store),
      //       title: Text(message["cart"]),
      //       backgroundColor: Colors.grey
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.account_box),
      //       title: Text(message["account"]) ,
      //       backgroundColor: Colors.grey,
      //     ),
      //   ],
      //   fixedColor: Colors.black,
      //   currentIndex: currentIndex,
      //   onTap: onTapIndex,
      // ):null,
    );
  }
}