// package
import 'package:flutter/material.dart';

// screens
import './assetRequestList.dart';
import './login.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';

// resources
import 'package:shiftsoft/resources/categoryApi.dart';

// models
import 'package:shiftsoft/models/mcategory.dart';
import 'package:shiftsoft/settings/configuration.dart';

class AssetList extends StatefulWidget {
    @override
    AssetListState createState() => new AssetListState();
}

class AssetListState extends State<AssetList> {
  List<Category> _categoryList = [];
  bool categoryListLoading = true;

  Map<String, String> message = new Map();
  List<String> messageList = ["assetList"];

  @override
  void initState() {
    initCategoryList();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  initCategoryList() async {
    setState(() {
      categoryListLoading = true;
    });

    _categoryList = await categoryApi.getCategoryListWithAsset(context);

    if(_categoryList == null){
      _categoryList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _categoryList = _categoryList;
      categoryListLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["assetList"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: categoryListLoading ?
      Center(
        child: CircularProgressIndicator()
      )
      :
      ListView.builder(
        itemCount: _categoryList.length,
        itemBuilder: (BuildContext context, int index) {
          List<Widget> widgetList= [];
          _categoryList[index].assetList.map((item) {
            widgetList.add(ListTile(
              key: Key("assetdetail$index${widgetList.length}"),  
              title: Text(item.name),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AssetRequestList(mode: 1, id: item.id, model: item,))
                );
              },
            ));
          }).toList();

          return ExpansionTile(
            key: Key("expansiontile"),
            initiallyExpanded: false,
            title: Text(_categoryList[index].categoryName),
            children: widgetList
          );
        }
      )
    );
  }
}