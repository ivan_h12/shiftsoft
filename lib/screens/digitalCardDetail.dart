// packages
import 'package:flutter/material.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';

class DigitalCardDetail extends StatefulWidget{
  String title;
  @override
  createState() {  
    return DigitalCardDetailState();
  }
  DigitalCardDetail({this.title});
}

class DigitalCardDetailState extends State<DigitalCardDetail>{
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: CustomScrollView(
        slivers: <Widget>[
           SliverList(
             delegate: SliverChildBuilderDelegate((context, index) =>
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child:FlatButton(
                      onPressed: (){
                        
                      },
                      child: ImageBox("https://img1.iwascoding.com/4/paid/2018/02/16/EC/6E5DDB00F5580135DFC2543D7EF8F2C1.jpg",300),
                    ),
                    
                  ),
                  Center(
                    child: Text("Stark Premium Member Card",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 25),),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20,left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("5 Keuntungan Stark Card",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)),
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                          child: Text("- Poin Berlipat",style: TextStyle(fontSize: 20)),
                        ),
                        Text("Kumpulkan Poin saat Berbelanja",style: TextStyle(fontSize: 20)),
                        Padding(
                          padding: EdgeInsets.only(top: 25),
                          child: Text("- Promo Memikat",style: TextStyle(fontSize: 20)),
                        ),
                        Text("Kumpulkan Poin saat Berbelanja",style: TextStyle(fontSize: 20)),
                      ],
                    ),
                  )
                ],
              ),
              childCount: 1,
             )
           ),
           
        ],
      ),
    );
  }
}