// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';
import 'package:timeago/timeago.dart' as timeago;

// screens
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/notification.dart';

// resources
import 'package:shiftsoft/resources/shoutApi.dart';

//models
import 'package:shiftsoft/models/mshout.dart';
import 'package:shiftsoft/settings/configuration.dart';

class NotificationList extends StatefulWidget {
  createState() {
    return NotificationListState();
  }
  int userId;
  NotificationList({this.userId});
}
class NotificationListState extends State<NotificationList> {
  List<Shout> _shoutList = [];
  bool shoutListLoading = true;

  @override
  void initState(){
   initShoutList();
  }

  initShoutList() async {
    setState(() {
      shoutListLoading = true;
    });
    
    _shoutList = await shoutApi.getShoutList(context, widget.userId);
    if(_shoutList == null){
      _shoutList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message["loginFailed"]),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    _shoutList.map((item) {
      if(item.status == 2){
        shoutApi.updateStatus(context, item.id);
      }
    }).toList();

    setState(() {
      _shoutList = _shoutList;
      shoutListLoading = false;
    });
  }

  Future<Null> _refresh() async {
    await initShoutList();
    return null;
  }

  convertTime(DateTime date){
    final dateNow = DateTime.now();
    final difference = date.difference(dateNow).inMinutes;
    return timeago.format(dateNow.add(Duration(minutes:difference)), allowFromNow: true).toString();
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["notification","read","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }
  
  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["notification"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        child: RefreshIndicator(
          onRefresh: _refresh,
          child: shoutListLoading ?
          ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context,int index) {
              return InkWell(
                child: Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: ListTile(
                          leading: Container(width: 40, height: 40, color: Colors.grey[200]),
                          title: Container(width: 100, height: 40, color: Colors.grey[200])
                        )
                      ),
                      Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Container(width: MediaQuery.of(context).size.width, height: 40, color: Colors.grey[200])
                      ),
                    ],  
                  ) 
                ),
              ); 
            },
          )
          :
          ListView.builder(
            itemCount: _shoutList.length != 0 ? _shoutList.length : 1,
            itemBuilder: (BuildContext context,int index) {
              final item = _shoutList[index];

              if(_shoutList.length!=0){
                return NotificationCard(
                  status: item.status,
                  title: item.title,
                  message: message["read"],
                  content: item.content,
                  createDate: convertTime(item.createdAt),
                  updateDate: convertTime(item.updatedAt),
                  onTap: () {
                    if (item.link != "") {
                      final linkArr = item.link.split("/");
                      String link = "";
                      
                      if (linkArr.length > 1) {
                        link = "${linkArr[0]}/${linkArr[1]}/1";
                      } else {
                        link = "${linkArr[0]}";
                      }

                      return Navigator.pushNamed(context, link);
                    }
                  },
                );
              } else {
                return PlaceholderList(type: "notification");
              }                  
            },
          ),
        ),
      )
    );
  }
}