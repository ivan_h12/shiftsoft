//packages
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

//screens
import './login.dart';

// resources
import 'package:shiftsoft/resources/eventApi.dart';
import 'package:shiftsoft/resources/circleApi.dart';

// widget
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// models
import '../models/mevent.dart';
import '../models/muser.dart';
import '../models/mresult.dart';
import 'package:shiftsoft/models/mcircle.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EventForm extends StatefulWidget {
  int mode, id;
  Event model;
  EventForm({Key key, this.mode, this.id, this.model}) : super(key:key);

  @override
  _EventFormState createState() => _EventFormState();
}

class _EventFormState extends State<EventForm> {
  final _formKey = GlobalKey<FormState>();
  final eventNameController = TextEditingController();
  final eventPlaceController = TextEditingController();
  final eventContentController = TextEditingController();

  Event eventUpdate;
  bool eventUpdateLoading = true;
  bool eventSubmitLoading = false;
  User user;

  List data = ["-"];
  bool circleListLoading = true;
  List<Circle> circleList = [];

  List<DropdownMenuItem<String>> _dropDownMenuItems = [
    DropdownMenuItem(
      key: Key("0"),
      value: "0",
      child: Text("----")
    )
  ];
  String selectedValue = "0";
  File image;

  DateTime _dateStart = DateTime.now();
  TimeOfDay _timeStart = TimeOfDay.now();
  DateTime _dateEnd = DateTime.now();
  TimeOfDay _timeEnd = TimeOfDay.now();

  String dateStart, dateEnd, timeStart, timeEnd;
  var temp, temp1;

  Map<String, String> message = new Map();
  List<String> messageList = ["eventform", "choosepicture", "eventname", "circle", "place", "start", "end", "description", "submit", "cancel", "alert", "loginFailed"];

  @override
  void initState() {
    super.initState();
    user = User(id: 1926);
    
    if(widget.mode == 3) {
      // if (widget.mode == 1) {
      //   initEventUpdate();
      // } else if (widget.mode == 3) {
      //   newsDetail = widget.model;
      //   setState(() {
      //     newsDetailLoading = false;
      //     newsDetail = newsDetail;
      //   });
      // }

      eventUpdate = widget.model;
      setState(() {
        eventUpdateLoading = false;
        eventUpdate = eventUpdate;
      });
      
      dateStart = (DateFormat('yyyy-MM-dd').format(widget.model.startAt)).toString();
      dateEnd = (DateFormat('yyyy-MM-dd').format(widget.model.endAt)).toString();
      timeStart = (DateFormat('kk:mm').format(widget.model.startAt)).toString();
      timeEnd = (DateFormat('kk:mm').format(widget.model.endAt)).toString();

      eventNameController.text = widget.model.name;
      eventPlaceController.text = widget.model.place;
      eventContentController.text = widget.model.content;

      _dateStart = widget.model.startAt;
      _timeStart = TimeOfDay.fromDateTime(widget.model.startAt);
      _dateEnd = widget.model.endAt;
      _timeEnd = TimeOfDay.fromDateTime(widget.model.endAt);

    } else {
      DateTime tempDate = DateTime.now();
      dateStart = dateEnd = DateFormat('yyyy-MM-dd').format(tempDate).toString();
      timeStart = timeEnd = DateFormat('kk:mm').format(tempDate).toString();
    }
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    if (circleListLoading) {
      initCircleList();
    }
  }

  Future<Null> _selectDateStart(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _dateStart,
      firstDate: DateTime.now().subtract(Duration(days: 1)),
      lastDate: DateTime.now().add(Duration(days: 365)),
    );

    if(picked != null){
      setState(() {
        _dateStart = picked;
        temp = picked.toString();
        temp1 = temp.split(" ");
        dateStart = temp1[0];
      });
    }
  }

  Future<Null> _selectDateEnd(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _dateEnd,
      firstDate: DateTime.now().subtract(Duration(days: 1)),
      lastDate: DateTime.now().add(Duration(days: 365)),
    );

    if(picked != null){
      setState(() {
        _dateEnd = picked;
        temp = picked.toString();
        temp1 = temp.split(" ");
        dateEnd = temp1[0];
      });
    }
  }

  Future<Null> _selectTimeStart(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _timeStart
    );

    if(picked != null && picked != _timeStart){
      setState(() {
        _timeStart = picked;
        temp = picked.toString();
        temp1 = temp.split("TimeOfDay(");
        timeStart = temp1[1];
        temp1 = timeStart.split(")");
        timeStart = temp1[0];
      });
    }
  }

  Future<Null> _selectTimeEnd(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _timeEnd
    );

    if(picked != null && picked != _timeEnd){
      setState(() {
        _timeEnd = picked;
        temp = picked.toString();
        temp1 = temp.split("TimeOfDay(");
        timeEnd = temp1[1];
        temp1 = timeEnd.split(")");
        timeEnd = temp1[0];
      });
    }
  }

  initCircleList() async{
    setState(() {
      circleListLoading = true;    
    });   
    
    circleList = await circleApi.getCircleListByUser(context, user.id);
    if(circleList == null){
      circleList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    
    circleList.map((item) {
      _dropDownMenuItems.add(DropdownMenuItem(
        key: Key("${item.id}"),
        value: item.id.toString(),
        child:Text(item.name)
      ));
    }).toList();

    setState(() {
      _dropDownMenuItems = _dropDownMenuItems;
      circleList = circleList;
      circleListLoading = false;    
    });
  }

  void submitEvent() async {
    if(_formKey.currentState.validate()){
      setState(() {
        eventSubmitLoading = true;      
      });

      String eventName = eventNameController.text;
      String eventPlace = eventPlaceController.text;
      String eventContent = eventContentController.text;

      String startAt = dateStart + "T" + timeStart + ":00";
      String endAt = dateEnd + "T" + timeEnd + ":00";

      DateTime tempDateStart = DateTime.parse(startAt);
      DateTime tempDateEnd = DateTime.parse(endAt);

      if(tempDateEnd.difference(tempDateStart).isNegative) {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Date Invalid"),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
        setState(() {
          eventSubmitLoading = false;      
        });

      } else {
        Result result;
        if(widget.mode == 0) {
          result = await eventApi.addEvent(context, eventName, eventPlace, eventContent, selectedValue, startAt, endAt, getBase64Image(image));
        } else {
          result = await eventApi.updateEvent(context, widget.model.idEncrypt, eventName, eventPlace, eventContent, selectedValue, startAt, endAt);
        }
        setState(() {
          eventSubmitLoading = false;
        });

        if(result.success == -1) {
          Alert(
            context: context,
            title: message["alert"],
            content: Text(message["loginFailed"]),
            cancel: false,
            defaultAction: () {
              Navigator.pushNamed(context, "login");
            }
          );
        } else {
          Alert(
            context: context,
            type: "success",
            title: message["alert"],
            content: Text(result.message),
            cancel: false,
            defaultAction: () {
              Navigator.pop(context, true);
            }
          );
        }
      }
    }  
  }

  picker() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img != null){
      setState(() {
        image = img;
      });
    }
  }

  String getBase64Image(File img){
    String base64Image = "";

    if (img != null) {
      List<int> imagesBytes = img.readAsBytesSync();
      base64Image = base64Encode(imagesBytes);
    }

    return base64Image;  
  }

  @override
  void dispose() {
    eventNameController.dispose();
    eventPlaceController.dispose();
    eventContentController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message["eventform"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: CustomScrollView(
      slivers: <Widget>[
          SliverList(
            delegate: SliverChildBuilderDelegate((context,index) { 
                return Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width/16*9,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: widget.mode == 0 ?
                        image == null ? AssetImage('lib/settings/placeholder.png') : FileImage(image)
                        :
                        image == null ? NetworkImage(CompanyAsset("event", widget.model.pic)) : FileImage(image)
                      ,
                      fit: BoxFit.cover
                    )
                  ),
                  child: BackdropFilter(
                    filter: ui.ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                    child: Container(
                      height: MediaQuery.of(context).size.width/16*9,
                      decoration: BoxDecoration(color: Colors.white.withOpacity(0.2)),
                      child: DrawerHeader(
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                width: 100,
                                height: 100,
                                child: widget.mode == 0 ?
                                  image == null ? Image.network(CompanyAsset("event", "")) : Image.file(image)
                                  :
                                  image == null ? Image.network(CompanyAsset("event", widget.model.pic)) : Image.file(image)
                                ,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                  border: Border.all(
                                    color: Colors.white,
                                    width: 4.0,
                                  ),
                                ),
                              ),
                              Container(
                                child: Button(
                                  child: Text(message["choosepicture"], style: TextStyle(fontWeight: FontWeight.bold, color: config.primaryTextColor),),
                                  backgroundColor: config.primaryColor,
                                  rounded: true,
                                  onTap: widget.mode == 0  ?
                                    () { picker(); }
                                    :
                                    () async {
                                      await picker();
                                      if (image != null) {
                                        final imageTemp = getBase64Image(image);
                                        await eventApi.changeEventImage(context, widget.id, imageTemp, "image.jpg");
                                      }
                                    }
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              },
              childCount: 1,
            ),
          ),
          SliverList(
            key: Key("scroll"),
            delegate: SliverChildBuilderDelegate((context, index) =>
              Container(
                padding: EdgeInsets.all(5.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0),
                        child: Text(message["eventname"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0),
                        child: TextFields(
                          key: Key("eventname"),
                          controller: eventNameController,
                          keyboardType: TextInputType.text,
                          hintText: message["eventname"],
                          validator: (String value){
                            if(value.isEmpty){
                              return "Please Enter Your Event Name";
                            }
                          },
                        ),
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                        child: Text(message["circle"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0),
                        child: DropdownButton(
                          key: Key("circle"),
                          isExpanded: true,
                          value: selectedValue,
                          items: _dropDownMenuItems,
                          onChanged: (String selected) {
                            print(selected);
                            setState(() {
                              selectedValue = selected;
                            });
                          },
                        )
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                        child: Text(message["place"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0),
                        child: TextFields(
                          key: Key("place"),
                          controller: eventPlaceController,
                          keyboardType: TextInputType.text,
                          hintText: message["place"],
                          validator: (String value){
                            if(value.isEmpty){
                              return "Please Enter Your Place";
                            }
                          },
                        ),
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                        child: Text(message["start"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                            child: InkWell(
                              onTap: (){ _selectDateStart(context); },
                              key: Key("datestart"),
                              child: TextFields(
                                type: 2,
                                date: "${dateStart}",
                                icon: new Icon(Icons.calendar_today),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                            child: InkWell(
                              onTap: (){ _selectTimeStart(context); },
                              key: Key("timestart"),
                              child: TextFields(
                                type: 2,
                                date: "${timeStart}",
                                icon: new Icon(Icons.access_time),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                        child: Text(message["end"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                            child: InkWell(
                              onTap: (){ _selectDateEnd(context); },
                              key: Key("dateend"),
                              child: TextFields(
                                type: 2,
                                date: "${dateEnd}",
                                icon: new Icon(Icons.calendar_today),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                            child: InkWell(
                              onTap: (){ _selectTimeEnd(context); },
                              key: Key("timeend"),
                              child: TextFields(
                                type: 2,
                                date: "${timeEnd}",
                                icon: new Icon(Icons.access_time),
                              ),
                            ),
                          ),
                        ],
                      ),   
                      Container(
                        margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                        child: Text(message["description"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
                      ), 
                      Container(
                        margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0),
                        child: TextFields(
                          key: Key("deskripsi"),
                          controller: eventContentController,
                          maxLines: 3,
                          keyboardType: TextInputType.multiline,
                          hintText: message["description"],
                          validator: (String value){
                            if(value.isEmpty){
                              return null;
                            }
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 7.0, right: 3.0, bottom: 10.0),
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: 50.0,
                            child: Button(
                              backgroundColor: config.primaryColor,
                              child: Text(message["cancel"], style: TextStyle(color: config.primaryColor)),
                              rounded:true,
                              fill: false,
                              onTap: (){
                                Navigator.of(context).pop();
                              }
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 3.0, right: 7.0, bottom: 10.0),
                            width: MediaQuery.of(context).size.width / 2.5,
                            height: 50.0,
                            child: Button(
                              key: Key("submit"),
                              backgroundColor: config.primaryColor,
                              child: Text(message["submit"], style: TextStyle(color: config.primaryTextColor),),
                              rounded:true,
                              loading: eventSubmitLoading,
                              splashColor: Colors.blueGrey,
                              onTap: (){
                                submitEvent();
                              }
                            ),
                          ),
                        ],
                      ),
                    ],
                  )
                )
              ),
              childCount: 1,
            )
          ),
        ]
      )
    );
  }
}