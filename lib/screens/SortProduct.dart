import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart';

class SortProduct extends StatefulWidget{
  createState(){
    return SortProdutState();
  }
}

class SortProdutState extends State<SortProduct>{
  List<bool> listActiveButton = [true, false, false, false, false, false];

  void handlePressed(int index){
    setState(() {
      listActiveButton[index] = !listActiveButton[index];
      for(int i = 0;i<6;i++){
        if(i != index){
          listActiveButton[i] = false;
        }
      }      
    });
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","sort","reviews","latest","highestPrice","mostSuitable","lowestPrice"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }
  
  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['sort'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            ListTile(
              leading: FlatButton(
                child: Text(message['mostSuitable'],
                  style: listActiveButton[0] ? TextStyle(color: Colors.green ) : TextStyle(color: Colors.grey ),
                ),
                onPressed: (){
                  handlePressed(0);
                },
              ),
              trailing: IconTheme(
                child: Icon(Icons.check),
                data: IconThemeData(
                  color: listActiveButton[0] ? Colors.green : Colors.transparent
                ),
              ),
            ),
            Divider(),
            ListTile(
              leading: FlatButton(
                child: Text("Promo", 
                  style: listActiveButton[1] ? TextStyle(color: Colors.green ) : TextStyle(color: Colors.grey ),
                ),
                onPressed: (){
                  handlePressed(1);
                },
              ),
              trailing: IconTheme(
                child: Icon(Icons.check),
                data: IconThemeData(
                  color: listActiveButton[1] ? Colors.green : Colors.transparent
                ),
              ),
            ),
            Divider(),
            ListTile(
              leading: FlatButton(
                child: Text(message['reviews'],
                  style: listActiveButton[2] ? TextStyle(color: Colors.green ) : TextStyle(color: Colors.grey ),
                ),
                onPressed: (){
                  handlePressed(2);
                },
              ),
              trailing: IconTheme(
                child: Icon(Icons.check),
                data: IconThemeData(
                  color: listActiveButton[2] ? Colors.green : Colors.transparent
                ),
              ),
            ),
            Divider(),
            ListTile(
              leading: FlatButton(
                child: Text(message['latest'],
                  style: listActiveButton[3] ? TextStyle(color: Colors.green ) : TextStyle(color: Colors.grey ),
                ),
                onPressed: (){
                  handlePressed(3);
                },
              ),
              trailing: IconTheme(
                child: Icon(Icons.check),
                data: IconThemeData(
                  color: listActiveButton[3] ? Colors.green : Colors.transparent
                ),
              ),
            ),
            Divider(),
            ListTile(
              leading: FlatButton(
                child: Text(message['highestPrice'],
                  style: listActiveButton[4] ? TextStyle(color: Colors.green ) : TextStyle(color: Colors.grey ),
                ),
                onPressed: (){
                  handlePressed(4);
                },
              ),
              trailing: IconTheme(
                child: Icon(Icons.check),
                data: IconThemeData(
                  color: listActiveButton[4] ? Colors.green : Colors.transparent
                ),
              ),
            ),
            Divider(),
            ListTile(
              leading: FlatButton(
                child: Text(message['lowestPrice'],
                  style: listActiveButton[5] ? TextStyle(color: Colors.green ) : TextStyle(color: Colors.grey ),
                ),
                onPressed: (){
                  handlePressed(5);
                },
              ),
              trailing: IconTheme(
                child: Icon(Icons.check),
                data: IconThemeData(
                  color: listActiveButton[5] ? Colors.green : Colors.transparent
                ),
              ),
            ),
            Divider()
          ],
        ),
      ),
    );
  }
}