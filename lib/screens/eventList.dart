// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

// screens
import './eventForm.dart';
import './eventDetail.dart';
import './eventCalendar.dart';
import './login.dart';

// widgets
import '../widgets/button.dart';
import '../widgets/eventBox.dart';
import '../widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/grid.dart';

// resources
import 'package:shiftsoft/resources/eventApi.dart';

// models
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/settings/configuration.dart';

class EventList extends StatefulWidget {
  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  Choice _selectedChoice = choices[0];
  Event event;
  List<Event> _eventList = [];
  List<Event> _eventHistoryList = [];
  bool eventListLoading = true;
  bool loadHistory = true;

  bool isLoading = false, isLoadingHistory = false;
  ScrollController _controller, _controllerHistory;
  int page = 1, pageHistory = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["eventlist", "ongoing", "history", "loadhistory", "eventCreate"];
  List<String> listTabBar = [];

  bool formStatus = false;

  int counter = 1;
  Widget _icon = Icon(Icons.grid_on);

  @override
  void initState(){
    _eventList.clear();
    _eventHistoryList.clear();
    initEventList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
    _controllerHistory = ScrollController();
    _controller.addListener(_scrollListenerHistory);
    initEventHistoryList();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    initStatus();

    if (eventListLoading) {
      initEventList();
    }

    if (loadHistory) {
      initEventHistoryList();
    }

    setState(() {
      listTabBar.add(message["ongoing"]);
      listTabBar.add(message["history"]);
    });
  }

  initStatus() async{
    formStatus = await checkPermission(context, "master.event", "BackendEvent.Create", true);

    setState(() {
     formStatus = formStatus; 
    });
  }

   _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_eventList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }
  _scrollListenerHistory() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_eventHistoryList.length % c.apiLimit == 0){
        loadMoreHistory();
      } else {
        this.setState(() {
          isLoadingHistory = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _eventList.addAll(await eventApi.getEventList(context, page));
      if(_eventList == null){
      _eventList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  loadMoreHistory() async {
    if (!isLoadingHistory) {
      setState(() {
        isLoadingHistory = true;
      });
      
      _eventHistoryList.addAll(await eventApi.getEventHistoryList(context, 1926, pageHistory));
      if(_eventHistoryList == null){
      _eventHistoryList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
      await makeAnimation();
      pageHistory++;

      setState(() {
        isLoadingHistory = false;
        pageHistory = pageHistory;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  void onTabTapped(){
    if (counter == 1) {
      _icon = Icon(Icons.list);
      counter = 2;
    } else {
      _icon = Icon(Icons.grid_on);
      counter = 1;
    }

    setState(() {
      _icon = _icon;
    });
  }

  initEventList() async {
    setState(() {
      eventListLoading = true;
    });
    
    _eventList = await eventApi.getEventList(context, page);
    if(_eventList == null){
      _eventList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;

    setState(() {
      _eventList = _eventList;
      page = page;
      eventListLoading = false;
    });
  }

  initEventHistoryList() async {
    setState(() {
      loadHistory = true;
    });

    _eventHistoryList = await eventApi.getEventHistoryList(context, 1926, page);
    if(_eventList == null){
      _eventList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;
    setState(() {
      loadHistory = false;
    });
    // _eventHistoryList = await eventApi.getEventHistoryList(1926);
    setState(() {
      _eventHistoryList = _eventHistoryList;
      page = page;
      // loadHistory = true;
    });
  }

  void _select(Choice choice) {
    setState(() {
      _selectedChoice = choice;   
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(message["eventlist"], style: TextStyle(color: config.topBarTextColor),),
          backgroundColor: config.topBarBackgroundColor,
          iconTheme: IconThemeData(
            color: config.topBarIconColor
          ),
          actions: <Widget>[
            IconButton(
              icon: _icon,
              onPressed: () { 
                setState(() {
                  if(!eventListLoading){
                    onTabTapped();
                  }
                });
              },
            ),
            IconButton(
              key: Key("eventcalendar"),
              icon: Icon(choices[0].icon),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EventCalendar())
                );
              },
            ),
            // formStatus ?
            // IconButton(
            //   key: Key("eventform"),
            //   icon: Icon(choices[1].icon),
            //   onPressed: () {
            //     Navigator.push(
            //       context,
            //       MaterialPageRoute(builder: (context) => EventForm(mode:3, id: 0, model: event, check: 1))
            //     );
            //   },
            // )
            // :
            // Container()
          ],
          bottom: TabBar(
            indicatorColor: config.topBarIconColor,
            tabs: List<Widget>.generate(2, (int index){
                return Tab(child: Text(listTabBar[index]));
              },
            ),
          )
        ),
        body: TabBarView(
          children: List<Widget>.generate(2, (int index){
            if (index == 0) {
              return RefreshIndicator(
                onRefresh: _refresh,
                child: eventListLoading ?
                ListView.builder(
                  itemCount: 6,
                  itemBuilder: (BuildContext context, int index) {
                    return Shimmer.fromColors(
                      baseColor: Colors.grey[200],
                      highlightColor: Colors.grey[350],
                      period: Duration(milliseconds: 800),
                      child: ListTile(
                        contentPadding: EdgeInsets.fromLTRB(13, 25, 13, 30),
                        leading: Container(width: 100, height: 100, color: Colors.grey[200]),
                        title: Container(width: 100, height: 50, color: Colors.grey[200])
                      )
                    );
                  }
                )
                :
                Column(
                  children: <Widget>[
                    formStatus ?
                    Container(
                      alignment: Alignment.topCenter,
                      child: Container(
                        width: MediaQuery.of(context).size.width - 10,
                        child: Button(
                          key: Key("eventform"),
                          child: Text(message["eventCreate"], style: TextStyle(color: config.primaryTextColor)),
                          backgroundColor: config.primaryColor,
                          splashColor: Colors.blueGrey,
                          onTap: () async {
                            bool refresh = await Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => EventForm(mode:0, id: 0, model: event))
                            );
                            
                            if (refresh ?? false) {
                              _refresh();
                            }
                          },
                          rounded: true,
                        )
                      ),
                    )
                    : Container(),
                    counter == 1 ?
                    Expanded(
                      child: ListView.builder(
                        controller: _controller,
                        itemCount: _eventList.length == 0 ? 1 : _eventList.length,
                        itemBuilder: (BuildContext context, int index){
                          if(_eventList.length != 0){
                            return Column(
                              children: <Widget>[
                                ListTile(
                                  contentPadding: EdgeInsets.fromLTRB(13, 20, 13, 20),
                                  key: Key("eventdetail$index"),
                                  onTap: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => EventDetail(mode:1, id:_eventList[index].id, model: _eventList[index]))
                                    );
                                  },
                                  leading: EventBox(
                                    _eventList[index].pic != "" ?
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            child: ImageBox(CompanyAsset("event", _eventList[index].pic), 100, border: false),
                                          ) 
                                        ],
                                      ) 
                                      : 
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            width: 100.0,
                                            height: 100.0,
                                            color: Colors.black12,
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(DateFormat('MMMM').format(_eventList[index].startAt), style: TextStyle(fontSize: 15.0, color: Colors.white)),
                                                Text(DateFormat('dd').format(_eventList[index].startAt), style: TextStyle(fontSize: 40.0, color: Colors.white))
                                              ],
                                            )
                                          )
                                        ],
                                      ),
                                      _eventList[index].name, DateFormat('dd MMMM yyyy').format(_eventList[index].startAt), DateFormat('kk:mm').format(_eventList[index].startAt) + " - " + DateFormat('kk:mm').format(_eventList[index].endAt), _eventList[index].place)
                                ),
                                Divider()
                              ]
                            );
                          }else{
                            return PlaceholderList(type: "eventlist");
                          }
                        }
                      )
                    )
                    :
                    Expanded(
                      child: CustomScrollView(
                        //controller: _controllerNews,
                        slivers: <Widget>[
                          SliverGrid(
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 5.0,
                              childAspectRatio: 0.6,
                            ),
                            delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                                final item = _eventList[index];
                                return _eventList.length != 0 ?
                                index < _eventList.length ?
                                  Grid(
                                    imageUrl: _eventList[index].pic,
                                    title: _eventList[index].name,
                                    date: DateFormat('dd MMMM yyyy').format(_eventList[index].startAt),
                                    time: DateFormat('kk:mm').format(_eventList[index].startAt) + " - " + DateFormat('kk:mm').format(_eventList[index].endAt),
                                    place: _eventList[index].place,
                                    onTap: (){
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => EventDetail(mode:1, id:_eventList[index].id, model: _eventList[index]))
                                      );
                                    },
                                  )
                                  :
                                  Center(
                                    child: Opacity(
                                      opacity: eventListLoading ? 1.0 : 0.0,
                                      child: CircularProgressIndicator(),
                                    ),
                                  )
                                :
                                PlaceholderList(type: "event");
                              },
                              childCount: _eventList.length == 0 ? 1 : _eventList.length,
                            ),
                          ),
                        ],
                      ),
                    )
                  ]
                )
              );
            } else {
              return loadHistory ? 
              Center(
                child: Text(message["loadhistory"], key: Key("loadhistory"))
              )
              :
              counter == 1? 
              Container(
                margin: EdgeInsets.only(top: 8.0),
                child: ListView.builder(
                  itemCount: _eventHistoryList.length,
                  itemBuilder: (BuildContext context, int index){
                    return Column(
                      children: <Widget>[
                        ListTile(
                          contentPadding: EdgeInsets.fromLTRB(13, 20, 13, 20),
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => EventDetail(mode:1, id:_eventHistoryList[index].id, model: _eventHistoryList[index]))
                            );
                          },
                          leading: EventBox(
                            _eventHistoryList[index].pic != "" ?
                              Row(
                                children: <Widget>[
                                  Container(
                                    child: ImageBox(CompanyAsset("event", _eventHistoryList[index].pic), 100, border: false),
                                  ) 
                                ],
                              ) 
                              : 
                              Row(
                                children: <Widget>[
                                  Container(
                                    width: 100.0,
                                    height: 100.0,
                                    color: Colors.black12,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(DateFormat('MMMM').format(_eventHistoryList[index].startAt), style: TextStyle(fontSize: 15.0, color: Colors.white)),
                                        Text(DateFormat('dd').format(_eventHistoryList[index].startAt), style: TextStyle(fontSize: 40.0, color: Colors.white))
                                      ],
                                    )
                                  )
                                ],
                              ),
                              _eventHistoryList[index].name, DateFormat('dd MMMM yyyy').format(_eventHistoryList[index].startAt), DateFormat('kk:mm').format(_eventHistoryList[index].startAt) + " - " + DateFormat('kk:mm').format(_eventHistoryList[index].endAt), _eventHistoryList[index].place)                      
                        ),
                        Divider()
                      ],
                    );
                  }
                )
              )
              :
              CustomScrollView(
                slivers: <Widget>[
                  SliverGrid(
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 5.0,
                      childAspectRatio: 0.6,
                    ),
                    delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                        final item = _eventHistoryList[index];
                        return _eventHistoryList.length != 0 ?
                        index < _eventHistoryList.length ?
                          Grid(
                            imageUrl: _eventHistoryList[index].pic,
                            title: _eventHistoryList[index].name,
                            date: DateFormat('dd MMMM yyyy').format(_eventHistoryList[index].startAt),
                            time: DateFormat('kk:mm').format(_eventHistoryList[index].startAt) + " - " + DateFormat('kk:mm').format(_eventHistoryList[index].endAt),
                            place: _eventHistoryList[index].place,
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => EventDetail(mode:1, id:_eventHistoryList[index].id, model: _eventHistoryList[index]))
                              );
                            },
                          )
                          :
                          Center(
                            child: Opacity(
                              opacity: eventListLoading ? 1.0 : 0.0,
                              child: CircularProgressIndicator(),
                            ),
                          )
                        :
                        PlaceholderList(type: "event");
                      },
                      childCount: _eventHistoryList.length == 0 ? 1 : _eventHistoryList.length,
                    ),
                  ),
                ],
              );
            }
          })
        ),
      ),
    );
  }

  Future<Null> _refresh() async {
    page = pageHistory = 1;
    await initEventList();
    return null;
  }
}

class Choice {
  const Choice({this.icon});
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(icon: Icons.calendar_today),
  const Choice(icon: Icons.add)
];