import 'package:flutter/material.dart';
import 'eventAttendanceList.dart';
import 'eventServicesList.dart';
import 'eventForm.dart';
import './profilePage.dart';

// widgets
import '../widgets/profileBox.dart';
import '../widgets/itemDetail.dart';
import 'package:shiftsoft/tools/functions.dart';

import './lessonClassroomAttendanceAdd.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';
class LessonClassroomDetail extends StatefulWidget {
  List<String> imageLists=<String>[
    "http://lorempixel.com/output/abstract-q-c-1920-1080-8.jpg",
  ];
  @override
  _LessonClassroomDetailState createState() => _LessonClassroomDetailState();
}



class _LessonClassroomDetailState extends State<LessonClassroomDetail> {
  List<String> _judulBab = new List<String>();
  List<String> data = new List<String>();
  String pertemuanKe, tanggal, namaJudul, lokasi;

  Map<String, String> message = new Map();
  List<String> messageList = ["lessonClassroomDetail"];

  @override
  void initState(){
    setState(() {
      pertemuanKe = "Pertemuan ke-1";
      tanggal = "09-10-2019";
      namaJudul = "Matematika Dasar";
      lokasi = "PT. DINAMIKA INTEGRA";
      _judulBab.add("Bangun Segitiga.docx");
      _judulBab.add("Bangun Ruang.pdf");
      _judulBab.add("Bangun Trapesium - 01.ppt");
      _judulBab.add("Bangun Trapesium - 02.ppt");
      _judulBab.add("Pythagoras Theorem.docx");
      
    });
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["lessonClassroomDetail"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: SizedBox(
                height: 1000.0,
                child: ItemDetail(
                  content: Column(
                    children: <Widget>[
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(namaJudul, style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
                        margin: EdgeInsets.only(top: 7.0, left: 10.0),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(pertemuanKe, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                        margin: EdgeInsets.only(top: 3.0, left: 10.0),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(tanggal, style: TextStyle(color: Colors.black45, fontSize: 15.0, fontWeight: FontWeight.bold)),
                        margin: EdgeInsets.only(top: 10.0, left: 10.0),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(lokasi, style: TextStyle(color: Colors.black45, fontSize: 15.0, fontWeight: FontWeight.bold)),
                        margin: EdgeInsets.only(top: 3.0, left: 10.0),
                      ),
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text("Download List", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                        margin: EdgeInsets.only(top: 15.0, left: 10.0),
                      ),
                      SizedBox(
                        height: 50.0,
                          child: CustomScrollView(
                            scrollDirection: Axis.horizontal,
                            slivers: <Widget>[
                              SliverList(
                                delegate: SliverChildBuilderDelegate((context, index) =>
                                  Container(
                                    margin: EdgeInsets.only(right: 20.0),
                                    alignment: Alignment.center,
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          child: IconButton(
                                            icon: Icon(Icons.file_download, color: Colors.redAccent,),
                                            iconSize: 25.0,
                                            onPressed: (){
                                              
                                            },
                                          ),
                                        ),
                                        Container(
                                          child: Text(_judulBab[index], style: TextStyle(fontSize: 15.0),),
                                        )
                                      ],
                                    ),
                                  ),
                                  childCount: _judulBab.length,
                                )
                              )
                            ]
                          )
                        
                        )
                    ],
                  ),
                  list: SliverList(
                    delegate: SliverChildBuilderDelegate((context, index) =>
                      Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0, top: 20.0),
                            alignment: Alignment.centerLeft,
                            child: Text("Daftar Hadir", style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                          ),
                          SizedBox(
                            height: 100.0,
                              child: InkWell(
                                key: Key("classroomattendance"),
                                onTap: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => LessonClassroomAttendanceAdd())
                                  );
                                },
                                child: CustomScrollView(
                                  scrollDirection: Axis.horizontal,
                                  slivers: <Widget>[
                                    SliverList(
                                      delegate: SliverChildBuilderDelegate((context, index) =>
                                        Container(
                                          alignment: Alignment.center,
                                          child: Column(
                                            children: <Widget>[
                                              Container(
                                                width: 80.0,
                                                height: 80.0,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                    image: NetworkImage("https://via.placeholder.com/150"),
                                                    fit: BoxFit.fill,
                                                  ),
                                                  borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                                  border: Border.all(
                                                    color: Colors.white,
                                                    width: 4.0,
                                                  ),
                                                ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: <Widget>[
                                                    Text("Made", style: TextStyle(fontSize: 15.0))
                                                  ],  
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        childCount: 10,
                                      )
                                    )
                                  ]
                                )
                              )
                            )
                        ],
                      ),
                      childCount: 1,
                    )
                  ),
                  imageList: widget.imageLists,
                ),
              ),
            )
          ],
        ),
      )  
    );
  }
}
