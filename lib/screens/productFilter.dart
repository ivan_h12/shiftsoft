import 'package:flutter/material.dart';
import 'package:flutter_range_slider/flutter_range_slider.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/widgets/button.dart';

class ProductFilter extends StatefulWidget{
  createState(){
    return ProductFilterState();
  }
}

class ProductFilterState extends State<ProductFilter>{
  double mLowerValue = 100;
  double mUpperValue = 100000000;

  var textControllerLower = TextEditingController(text: "0");
  var textControllerUpper = TextEditingController(text: "100000000");

  void dispose(){
    textControllerLower.dispose();
    textControllerUpper.dispose();
    super.dispose();
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["colour","location","deliverySupport","condition","shop","success","filterSuccess","apply","by","helped","seeReply","reviews","all","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(Context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Filters", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          FlatButton(
            child: Text("Reset", style: TextStyle(color: Colors.white)),
            onPressed: (){print("Reset");},
          )
        ],
        
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index){
          return Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: TextField(
                      controller: textControllerLower,
                      keyboardType: TextInputType.numberWithOptions(decimal: false),
                      decoration: InputDecoration(
                        labelText: "Min Price",
                        hintText: mLowerValue == null ? "0" : mLowerValue.toInt().toString(),
                      ),
                      onChanged: (text){
                        setState(() {
                          mLowerValue = double.tryParse(textControllerLower.text);
                        });
                      },
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Text("-", textAlign: TextAlign.center,),
                  ),
                  Expanded(
                    flex: 2,
                    child: TextField(
                      controller: textControllerUpper,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "Max Price",
                        hintText: mUpperValue == null ? "100000000": mUpperValue.toInt().toString(),
                      ),
                      onChanged: (text){
                        setState(() {
                          mUpperValue = double.tryParse(textControllerUpper.text);
                        });
                      },
                    ),
                  )
                ],
              ),
              RangeSlider(
                showValueIndicator: true,
                min: 0,
                max: 100000000,
                divisions: 10000,
                lowerValue: mLowerValue == null ? mLowerValue = 0 : mLowerValue > 100000000 ? 100000000 : mLowerValue,
                upperValue: mUpperValue == null ? 100000000 : mUpperValue < mLowerValue ? mLowerValue : mUpperValue ,
                valueIndicatorMaxDecimals: 0,
                onChanged: (double newLower, newUpper){
                  mLowerValue = newLower;
                  mUpperValue = newUpper;
                  setState(() {
                    textControllerLower.text = newLower.toInt().toString();
                    textControllerUpper.text = newUpper.toInt().toString();
                  });
                },
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
                color: Colors.black12,
                child: Divider(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(message['colour'],textAlign: TextAlign.left,),
                  FlatButton(
                    onPressed: (){print("Lihat Semua");},
                    child: Text(message['all'], style: TextStyle(color: Colors.green),textAlign: TextAlign.right,),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 5, bottom: 5),
                color: Colors.black12,
                child: Divider(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(message['location'],textAlign: TextAlign.left,),
                  FlatButton(
                    onPressed: (){print("Lihat Semua");},
                    child: Text(message['all'], style: TextStyle(color: Colors.green),textAlign: TextAlign.right,),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(message['deliverySupport'],textAlign: TextAlign.left,),
                  FlatButton(
                    onPressed: (){print("Lihat Semua");},
                    child: Text(message['all'], style: TextStyle(color: Colors.green),textAlign: TextAlign.right,),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(message['condition'],textAlign: TextAlign.left,),
                  FlatButton(
                    onPressed: (){print("Lihat Semua");},
                    child: Text(message['all'], style: TextStyle(color: Colors.green),textAlign: TextAlign.right,),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(message['shop'],textAlign: TextAlign.left,),
                  FlatButton(
                    onPressed: (){print("Lihat Semua");},
                    child: Text(message['all'], style: TextStyle(color: Colors.green),textAlign: TextAlign.right,),
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("Rating",textAlign: TextAlign.left,),
                  FlatButton(
                    onPressed: (){print("Lihat Semua");},
                    child: Text(message['all'], style: TextStyle(color: Colors.green),textAlign: TextAlign.right,),
                  ),
                ],
              ),
              Divider()  
            ],
          );
        },
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        color: config.primaryColor,
        child: Button(
          fill: true,
          backgroundColor: config.primaryColor,
          child: Text(message['apply'], style: TextStyle(color: config.primaryTextColor)),
          onTap: () {
            Alert(
              context: context,
              title: message['success'],
              content: Text(message['filterSuccess']),
              cancel: false,
              defaultAction: () {
                Navigator.pop(context, "filters[]=price|between|${textControllerLower.text},${textControllerUpper.text}|and");
              }
            );
          },
        ),
      ),
    );
  }
}