// package
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

//screens
import './login.dart';

//widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/assetApi.dart';

// model
import 'package:shiftsoft/models/massetrequest.dart';
import 'package:shiftsoft/models/masset.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/settings/configuration.dart';


class AssetRequestForm extends StatefulWidget {
  int mode, id;
  AssetRequest model;
  Asset asset;
  AssetRequestForm({Key key, this.mode, this.id, this.model, this.asset}) : super(key:key);

  @override
  _AssetRequestFormState createState() => _AssetRequestFormState();
}

class _AssetRequestFormState extends State<AssetRequestForm> {
  final _formKey = GlobalKey<FormState>();
  final eventContentController = TextEditingController();
  User user;
  Asset asset;

  DateTime _date = DateTime.now();
  TimeOfDay _time = TimeOfDay.now();
  DateTime _date1 = DateTime.now();
  TimeOfDay _time1 = TimeOfDay.now();

  String dateStart, dateEnd, timeStart, timeEnd;
  var tempAsset, tempAsset1;

  bool assetRequestSubmitLoading = false;

  Map<String, String> message = new Map();
  List<String> messageList = ["assetName","start","end","description","cancel","save","assetRequestCreate"];

  @override
  void initState() {
    super.initState();
    user = User(id: 1926);
    
    DateTime tempDate = DateTime.now();
    if (widget.mode == 3) {
      dateStart = DateFormat('yyyy-MM-dd').format(widget.model.startAt).toString();
      dateEnd = DateFormat('yyyy-MM-dd').format(widget.model.endAt).toString();
      timeStart = DateFormat('kk:mm').format(widget.model.startAt).toString();
      timeEnd = DateFormat('kk:mm').format(widget.model.endAt).toString();

      _date = widget.model.startAt;
      _time = TimeOfDay.fromDateTime(widget.model.startAt);
      _date1 = widget.model.endAt;
      _time1 = TimeOfDay.fromDateTime(widget.model.endAt);

      eventContentController.text = widget.model.content;
    } else {
      dateStart = dateEnd = DateFormat('yyyy-MM-dd').format(tempDate).toString();
      timeStart = timeEnd = DateFormat('kk:mm').format(tempDate).toString();
    }

    asset = widget.asset;
  }
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(2016),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempAsset1 = picked.toString();
        tempAsset = tempAsset1.split(" ");
        dateStart = tempAsset[0];
      });
    }
  }

  Future<Null> _selectDate1(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date1,
      firstDate: DateTime(2016),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date1){
      setState(() {
        _date1 = picked;
        tempAsset1 = picked.toString();
        tempAsset = tempAsset1.split(" ");
        dateEnd = tempAsset[0];
      });
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _time
    );

    if(picked != null && picked != _time){
      setState(() {
        _time = picked;
        tempAsset1 = picked.toString();
        tempAsset = tempAsset1.split("TimeOfDay(");
        timeStart = tempAsset[1];
        tempAsset = timeStart.split(")");
        timeStart = tempAsset[0];
      });
    }
  }

  Future<Null> _selectTime1(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: _time1
    );

    if(picked != null && picked != _time1){
      setState(() {
        _time1 = picked;
        tempAsset1 = picked.toString();
        tempAsset = tempAsset1.split("TimeOfDay(");
        timeEnd = tempAsset[1];
        tempAsset = timeEnd.split(")");
        timeEnd = tempAsset[0];
      });
    }
  }

  void submitAsset() async {
    setState(() {
      assetRequestSubmitLoading = true;      
    });
    
    String startAt = dateStart + " " + timeStart + ":00";
    String endAt = dateEnd + " " + timeEnd + ":00";
    
    DateTime tempDateStart = DateTime.parse(startAt);
    DateTime tempDateEnd = DateTime.parse(endAt);

    if(tempDateEnd.difference(tempDateStart).isNegative){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Date Invalid"),
            actions: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: (){
                  Navigator.of(context).pop(context);
                },
              )
            ],
          );
        }
      );
      setState(() {
        assetRequestSubmitLoading = false;      
      });
    } else {
      String mode = "create";
      String idEncrypt = "";

      if (widget.mode == 3) {
        mode = "edit";
        idEncrypt = widget.model.idEncrypt;
      }

      Result result = await assetApi.createAssetRequest(context, asset.name, eventContentController.text, asset.id, user.id, startAt, endAt, mode, idEncrypt);
      setState(() {
        assetRequestSubmitLoading = false;      
      });
      if(message == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(result.message),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  key: Key("ok"),
                  onPressed: (){
                    Navigator.of(context).pop();
                    if (result.success == 1) {
                      Navigator.pop(context, true);
                    }
                  },
                )
              ],
            );
          }
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["assetRequestCreate"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5),
        child: ListView(
          children: <Widget>[
            Container(
              margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
              child: Text(message["assetName"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
            ),
            Container(
              margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
              child: Text(asset.name, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold))
            ), 
            Container(
              margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
              child: Text(message["start"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                  child: InkWell(
                    onTap: () { _selectDate(context); },
                    key: Key("datestart"),
                    child: TextFields(
                      type: 2,
                      date: "$dateStart",
                      icon: new Icon(Icons.calendar_today),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                  child: InkWell(
                    onTap: (){ _selectTime(context); },
                    key: Key("timestart"),
                    child: TextFields(
                      type: 2,
                      date: "$timeStart",
                      icon: new Icon(Icons.access_time),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
              child: Text(message["end"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                  child: InkWell(
                    onTap: (){ _selectDate1(context); },
                    key: Key("dateend"),
                    child: TextFields(
                      type: 2,
                      date: "$dateEnd",
                      icon: Icon(Icons.calendar_today),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                  child: InkWell(
                    onTap: (){ _selectTime1(context); },
                    key: Key("timeend"),
                    child: TextFields(
                      type: 2,
                      date: "$timeEnd",
                      icon: Icon(Icons.access_time),
                    ),
                  ),
                ),
              ],
            ),
            Container(
              margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
              child: Text(message["description"], style: TextStyle(fontSize: 16.0, color: Colors.grey))
            ),
            Container(
              margin: EdgeInsets.only(left: 7.0, right: 7.0),
              child: TextFields(
                maxLines: 3,
                key: Key("content-textfield"),
                controller: eventContentController,
                keyboardType: TextInputType.multiline,
                hintText: message["description"],
                validator: (String value){
                  return null;
                },
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 7.0, right: 3.0, bottom: 10.0),
                  width: MediaQuery.of(context).size.width / 2.5,
                  height: 50.0,
                  child: Button(
                    child: Text(message["cancel"], style: TextStyle(color: config.primaryColor),),
                    fill: false,
                    backgroundColor: config.primaryColor,
                    rounded: true,
                    onTap: (){
                      Navigator.pop(context);
                    }
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 3.0, right: 7.0, bottom: 10.0),
                  width: MediaQuery.of(context).size.width / 2.5,
                  height: 50.0,
                  child: Button(
                    key: Key("save"),
                    child: Text(message["save"], style: TextStyle(color: config.primaryTextColor),),
                    backgroundColor: config.primaryColor,
                    rounded: true,
                    loading: assetRequestSubmitLoading,
                    onTap: () {
                      submitAsset();
                    }
                  ),
                )
              ],
            )
          ],
        ),
      )
    );
  }
}