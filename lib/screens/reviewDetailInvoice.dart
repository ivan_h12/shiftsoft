// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productReviewAdd.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';

// resources

// models
import 'package:shiftsoft/settings/configuration.dart';

class ReviewDetailInvoice extends StatefulWidget{
  String invoice, date;
  
  createState(){
    return ReviewDetailInvoiceState();
  }
  
  ReviewDetailInvoice({Key key, this.invoice, this.date}) : super(key:key);
}

class ReviewDetailInvoiceState extends State<ReviewDetailInvoice>{

  Map<String, String> message=new Map();
  List<String> messageList = ["reviewTheProduct","notReviewed","loginFailed","waitForReview","myRate","sellerNotRated","myReview"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: ListTile(
          title: Text(widget.invoice, style: TextStyle(fontWeight: FontWeight.bold, color: config.topBarTextColor)),
          subtitle: Text(widget.date+ "2019, 18:23") ,
        ),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index){
          return Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(message['myRate'], style: TextStyle(fontSize: 20),),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Icon(Icons.lock_outline, size: 50,),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Divider(),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(message["sellerNotRated"], style: TextStyle(fontSize: 18, color: Colors.grey),),
                        Icon(Icons.face),
                      ],
                    )
                  ],
                )
              ),
              ListView.builder(
                itemCount: 1,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int i){
                  return Container(
                    margin: EdgeInsets.all(10),
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            leading: ImageBox("", 50),
                            title: Text("Powerbank Super Quick Charge Limited Edition", style: TextStyle(fontWeight: FontWeight.bold),),
                            subtitle: Text(message["notReviewed"], style: TextStyle(),),
                          ),
                          Divider(),
                          InkWell(
                            key: Key("productReviewAdd"),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProductReviewAdd(productId: 5, productName: "PowerBank", userId: 1926, productPic: "",)));
                            },
                            child: ButtonBar(
                              children: <Widget>[
                                FlatButton(
                                  disabledTextColor: Colors.green,
                                  child: Row(
                                    children: <Widget>[
                                      Text(message['reviewTheProduct'],style: TextStyle(),),
                                      Icon(Icons.arrow_forward_ios),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              )
            ],
          );
        },
      ),
    );
  }  
}