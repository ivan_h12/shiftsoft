//packages
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';

//screens
import './journeyForm.dart';
import './login.dart';

//widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/steppers.dart';

// resources
import 'package:shiftsoft/resources/journeyApi.dart';

// models
import 'package:shiftsoft/models/mjourney.dart';
import 'package:shiftsoft/settings/configuration.dart';

class JourneyList extends StatefulWidget{
  @override
  createState(){
    return _JourneyListState();
  }
}

class _JourneyListState extends State<JourneyList>{
  List<String> _title = new List<String>();

  List<Journey> _journeyList = [];
  bool journeyListLoading = true;
  
  bool isLoading = false;
  ScrollController _controller;
  int page = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["spiritualJourney"];

  @override
  void initState(){
    _journeyList.clear();
    initJourneyList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_journeyList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });

      int idUser = 1926;
      _journeyList.addAll(await journeyApi.getJourneyList(context, idUser, page));

      if(_journeyList == null){
        _journeyList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }

      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initJourneyList() async {
    setState(() {
      journeyListLoading = true;
    });
    
    int idUser = 1926;
    _journeyList = await journeyApi.getJourneyList(context, idUser, page);

    if(_journeyList == null){
      _journeyList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _journeyList = _journeyList;
      journeyListLoading = false;
    });
  }

  Future<Null> _refresh() async {
    page = 1;
    await initJourneyList();
    return null;
  }

  Widget build(context){
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text(message["spiritualJourney"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            key: Key("journeyform"),
            icon: Icon(choices[0].icon),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => JourneyForm())
              );
            },
          ),
        ]
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: journeyListLoading ?
        Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index){
                  return Stack(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left: 50.0),
                        child: Card(
                          margin: EdgeInsets.only(left: 50.0, top: 25.0, right: 25.0, bottom: 25.0),
                          child: Container(
                            width: double.infinity,
                            height: 100.0,
                            child: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(width: 100, height: 50, color: Colors.grey[200]),
                            )
                          ),
                        ),
                      ),
                      Positioned(
                        top: 0.0,
                        bottom: 0.0,
                        left: 75.0,
                        child: Container(
                          height: double.infinity,
                          width: 1.0,
                          color: Colors.black45,
                        ),
                      ),
                      Positioned(
                        top: 50.0,
                        left: 55.0,
                        child: Container(
                          height: 40.0,
                          width: 40.0,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white
                          ),
                          child: Container(
                            margin: EdgeInsets.all(5.0),
                            height: 30.0,
                            width: 30.0,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.black26
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
                itemCount: 4,
              )
            ),
          ],
        )
        :
        Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                controller: _controller,
                itemBuilder: (BuildContext context, int index){
                  if(_journeyList.length != 0){
                    return InkWell(
                      child: Steppers(
                        type: 1,
                        nama: _journeyList[index].name, 
                        tanggal: DateFormat('dd MMMM yyyy').format(_journeyList[index].createdAt), 
                        gambarLink: _journeyList[index].badgePic, 
                        gambarNama: "levelhistories",
                        isFirst: index == 0 ? true : false,
                      )
                    );
                  }else{
                    return PlaceholderList(type: "spiritualjourney");
                  }
                },
                itemCount: _journeyList.length != 0 ? _journeyList.length : 1,
              )
            ),
          ],
        ) 
      )
    );
  }
}
class Choice {
  const Choice({this.icon});
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(icon: Icons.add)
];