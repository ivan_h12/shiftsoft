// package
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

// screen
import './budgetDetail.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/listBox.dart';

//resources
import 'package:shiftsoft/resources/budgetApi.dart';

// models
import 'package:shiftsoft/models/mbudget.dart';
import 'package:shiftsoft/settings/configuration.dart';

class BudgetList extends StatefulWidget {
  @override
  _BudgetListState createState() => _BudgetListState();
}

class _BudgetListState extends State<BudgetList> {
  List<Budget> _budgetList = [];
  bool budgetListLoading = true;
  
  bool isLoading = false;
  ScrollController _controller;
  int page = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["budgetList"];

  @override
  void initState(){
    _budgetList.clear();
    initBudgetList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_budgetList.length % c.apiLimit == 0){
        loadMore();
      } else {
        this.setState(() {
          isLoading = false;
        });
      }
    }
  }
  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _budgetList.addAll(await budgetApi.getBudgetList(context, page));
      if(_budgetList == null){
        _budgetList = [];
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text("Login Gagal"),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        isLoading = false;
        page = page;
      });
    }
  }
  makeAnimation() async {
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initBudgetList() async {
    setState(() {
      budgetListLoading = true;     
    });

    _budgetList = await budgetApi.getBudgetList(context, page);
    page++;
    
    if(_budgetList == null){
      _budgetList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _budgetList = _budgetList;
      page = page;
      budgetListLoading = false;     
    });

  }

  Future<Null> _refresh() async {
    page = 1;
    await initBudgetList();
    return null;
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(message["budgetList"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: budgetListLoading ?
        ListView.builder(
          itemCount: 10,
          itemBuilder: (BuildContext context, int index){
            return ListTile(
              contentPadding: EdgeInsets.all(10.0),
              leading: Container(
                padding: EdgeInsets.only(bottom: 20.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black12,
                      width: 1.0,
                    )
                  )
                ),
                child:Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          child: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(
                                width: 150, 
                                height: 15, 
                                color: Colors.grey[200],                    
                                margin: EdgeInsets.only(right: 30.0, bottom: 10),
                            ),
                          )
                        ),
                        Container(
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              width: 150, 
                              height: 15, 
                              color: Colors.grey[200],
                              margin: EdgeInsets.only(bottom: 10),
                            ),
                          )
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          child: Shimmer.fromColors(
                              baseColor: Colors.grey[200],
                              highlightColor: Colors.grey[350],
                              period: Duration(milliseconds: 800),
                              child: Container(
                                width: 150, 
                                height: 15, 
                                color: Colors.grey[200],                    
                                margin: EdgeInsets.only(right: 30.0),
                            ),
                          )
                        ),
                        Container(
                          child: Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              width: 150, 
                              height: 15, 
                              color: Colors.grey[200],
                            ),
                          )
                        ),
                      ],
                    ),
                  ],
                )
              ),
            );
          }
        )
        :
        ListView.builder(
          controller: _controller,
          itemCount: _budgetList.length == 0 ? 1 : _budgetList.length,
          itemBuilder: (BuildContext context, int index){
            if(_budgetList.length != 0){
              return InkWell(
                key: Key("budgetdetail$index"),
                child: ListBox(
                  childs: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(_budgetList[index].name, style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
                            padding: EdgeInsets.only(left: 10.0, top: 10.0),
                            // width:  MediaQuery.of(context).size.width / 2.0,
                          ),
                          Container(
                            child: Text("Total : Rp ${_budgetList[index].total}", style: TextStyle(fontSize: 15.0)),
                            // width:  MediaQuery.of(context).size.width / 2.0,
                            padding: EdgeInsets.only(top: 10.0,right: 10.0),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text("Left : Rp ${_budgetList[index].left}", style: TextStyle(fontSize: 15.0)),
                            padding: EdgeInsets.only(left: 10.0,top: 10,bottom: 10),
                            // width:  MediaQuery.of(context).size.width / 2.0,
                          ),
                          Container(
                            child: Text("Realization : Rp ${_budgetList[index].realization}", style: TextStyle(fontSize: 15.0)),
                            padding: EdgeInsets.only(right: 10.0,top: 10,bottom: 10),
                            // width:  MediaQuery.of(context).size.width / 2.0,
                          ),
                        ],
                      ),
                    ],
                  )
                ),  
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BudgetDetail(mode: 1, id: _budgetList[index].id, model: _budgetList[index]))
                  );
                },
              );
            }else{
              return PlaceholderList(type: "budgetlist");
            }
          }
        )
      )
    );
  }
}