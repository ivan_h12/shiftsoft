// packages
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';
import 'package:intl/intl.dart';
import 'package:flutter/rendering.dart';

// screens
import 'package:shiftsoft/screens/transactionList.dart';
import 'package:shiftsoft/screens/productCategoryList.dart';
import 'package:shiftsoft/screens/profilePage.dart';
import './newsDetail.dart';
import 'package:shiftsoft/screens/lessonList.dart';
import 'package:shiftsoft/screens/login.dart';
import 'package:shiftsoft/screens/balanceDetail.dart';
import 'package:shiftsoft/screens/pointDetail.dart';
import 'package:shiftsoft/screens/sidebar.dart';
import 'package:shiftsoft/screens/couponList.dart';
import 'package:shiftsoft/screens/newsList.dart';
import 'package:shiftsoft/screens/promotionList.dart';
import 'package:shiftsoft/screens/notificationList.dart';
import 'package:shiftsoft/screens/setting.dart';

// widgets
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';
import 'package:shiftsoft/widgets/carousel.dart';
import 'package:shiftsoft/widgets/twoColumnView.dart';
import 'package:shiftsoft/widgets/SSText.dart';

// resources
import 'package:shiftsoft/resources/dashboardApi.dart';
import 'package:shiftsoft/resources/shoutApi.dart';
import 'package:shiftsoft/resources/userApi.dart';
import 'package:shiftsoft/resources/eventApi.dart';
import 'package:shiftsoft/resources/newsApi.dart';

// models
import 'package:shiftsoft/models/mdashboard.dart';
import 'package:shiftsoft/models/mshout.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mevent.dart';
import 'package:shiftsoft/models/mnews.dart';

class Dashboard extends StatefulWidget {
  
  createState() {
    return DashboardState();
  }
}

class DashboardState extends State<Dashboard> {
  int page = 1, pageNews = 1;
  int mCurrentIndex = 0;
  ScrollController _controllerEvent, _controller;
  Event event;
  List<Event> _eventList = [];
  List<MDashboard> _mDashboardList = [];
  List<Shout> _shoutList = [];
  List<News> _newsList = [];
  User mUser;
  bool isVisible = true;
  bool dashboardListLoading = true;
  bool userBalancePointLoading = false;
  bool isLoadingEvent = false, eventListLoading = true;
  bool isLoadingNews = false, newsListLoading = true;

  void onTabTapped(int index) async {
    setState(() {
      mCurrentIndex = index;
    });

    if(mCurrentIndex == 0){
      
    }
    else if(mCurrentIndex == 1){
      
    } else if(mCurrentIndex == 2){
      
    }
  }

  Map<String, String> message = Map();
  List<String> messageList = ["balance", "point", "refresh", "success", "newsList", "circleList", "eventList", "transactionList",
    "downloadList", "assetList", "spiritualJourney",
    "productCategoryList", "productWishlistList",
    "productCartList", "lessonList", "couponList", "fosterChildList", "promotionList",
    "subscribeList", "nearMe", "rentList", "rentWishlist", "rentCartList",
    "productReviewList", "productCommentList", "requestList", "budgetList"];

  // list semua fitur yang bisa ditampilkan muncul di halaman tengah
  List<String> menuCheckList = ["newsList", "circleList", "eventList", 
    // "transactionList",
    "downloadList", "assetList", 
    // "journeyList", 
    "productCategoryList", "productWishlistList",
    "productCartList", "lessonList", "couponList", "fosterChildList", "promotionList",
    "subscribeList", "nearMe", 
    // "rentList", "rentWishlistList", "rentCartList",
    "productReviewList", "productCommentList", "requestList", "budgetList"
  ];

  bool bannerStatus = false;
  bool pointStatus = false;
  bool eventStatus = false;
  bool newsStatus = false;
  bool balanceStatus = false;
  bool circleStatus = false;
  // bool transactionStatus = false;
  bool downloadStatus = false;
  bool assetStatus = false;
  // bool spiritualJourneyStatus = false;
  bool productCategoryListStatus = false;
  bool productWishlistListStatus = false;
  bool productCartlistStatus = false;
  bool lessonStatus = false;
  bool couponStatus = false;
  bool fosterChildStatus = false;
  bool promotionStatus = false;
  bool subscribeStatus = false;
  bool nearmeStatus = false;
  bool rentListStatus = false;
  bool rentWishlistStatus = false;
  bool rentCartlistStatus = false;
  bool budgetListStatus = false;
  bool productReviewListStatus = false;
  bool productCommentListStatus = false;
  bool requestListStatus = false;

  List<Widget> listWidget = [];

  @override
  void initState() {
   _eventList.clear();
   initEventList();
   _newsList.clear();
   initNewsList(); 

   _controller = new ScrollController();
    _controller.addListener(() {
      if (_controller.position.userScrollDirection ==
          ScrollDirection.reverse) {
        setState(() {
          isVisible = false;
          print("**** $isVisible up");
        });
      }
      if (_controller.position.userScrollDirection ==
          ScrollDirection.forward) {
        setState(() {
          isVisible = true;
          print("**** $isVisible down");
        });
      }
    });
  }

  didChangeDependencies() async {
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    checkUser();

    if (dashboardListLoading) {
      initDashboardList();
    }

    if (userBalancePointLoading) {
      initUserBalancePoint();
    }
  }

  int getUnreadNotification(int length){
    int count=0;
    for(int i=0;i<length;i++){
      if(_shoutList[i].status==2){
        count++;
      }
    }
    return count;
  }

  checkUser() async {
    Configuration config = Configuration.of(context);
    mUser = await userApi.getUser(context, config.user.id, parameter: "with[0]=Role.Privileges");
    bannerStatus = await checkPermission(context, "master.notification.banner", "BackendBanner.Index", false);
    pointStatus = await checkPermission(context, "master.member", "BackendMember.Point", true);
    balanceStatus = await checkPermission(context, "master.member", "BackendMember.Point", true);
    eventStatus = await checkPermission(context, "master.event", "BackendEvent.Index", false);
    newsStatus = await checkPermission(context, "master.notification", "BackendNotification.Index", false);
    circleStatus = await checkPermission(context, "master.circle", "BackendCircle.Index", true);
    downloadStatus = await checkPermission(context, "master.download", "BackendDownload.Index", true);
    assetStatus = await checkPermission(context, "master.asset", "BackendAsset.Index", true);
    // spiritualJourneyStatus = await checkPermission(context, "master.member", "BackendMember.Levelhistory", true);
    // transactionStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    productCategoryListStatus = await checkPermission(context, "master.product.category", "BackendProductCategory.Index", true);
    productWishlistListStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    productCartlistStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    lessonStatus = await checkPermission(context, "master.lesson", "BackendProduct.Index", true);
    couponStatus = await checkPermission(context, "master.promotion", "BackendPromotion.Index", true);
    fosterChildStatus = await checkPermission(context, "master.school", "BackendSchoolreport.Foster", true);
    promotionStatus = await checkPermission(context, "master.promotion", "BackendPromotion.Index", true);
    subscribeStatus = await checkPermission(context, "master.promotion", "BackendPromotion.Index", true);
    nearmeStatus = await checkPermission(context, "master.circle", "BackendCircle.Index", false);
    rentListStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    rentWishlistStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    rentCartlistStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    productReviewListStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    productCommentListStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    requestListStatus = await checkPermission(context, "master.product", "BackendProduct.Index", true);
    budgetListStatus = await checkPermission(context, "master.asset", "BackendAsset.Index", true);

    setState(() {
      bannerStatus = bannerStatus;
      pointStatus = pointStatus;
      balanceStatus = balanceStatus;
      eventStatus = eventStatus;
      newsStatus = newsStatus;
      circleStatus = circleStatus;
      eventStatus = eventStatus;
      // transactionStatus = transactionStatus;
      downloadStatus = downloadStatus;
      assetStatus = assetStatus;
      // spiritualJourneyStatus = spiritualJourneyStatus;
      productCategoryListStatus = productCategoryListStatus;
      productWishlistListStatus = productWishlistListStatus;
      productCartlistStatus = productCartlistStatus;
      lessonStatus = lessonStatus;
      couponStatus = couponStatus;
      fosterChildStatus = fosterChildStatus;
      promotionStatus = promotionStatus;
      subscribeStatus = subscribeStatus;
      nearmeStatus = nearmeStatus;
      rentListStatus = rentListStatus;
      rentWishlistStatus = rentWishlistStatus;
      rentCartlistStatus = rentCartlistStatus;
      productReviewListStatus = productReviewListStatus;
      productCommentListStatus = productCommentListStatus;
      requestListStatus = requestListStatus;
      budgetListStatus = budgetListStatus;
    });
  }

  initEventList() async {
    setState(() {
      eventListLoading = true;
    });
    
    _eventList = await eventApi.getEventList(context, page);
    if(_eventList == null){
      _eventList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;

    setState(() {
      _eventList = _eventList;
      page = page;
      eventListLoading = false;
    });
  }

  initNewsList() async {
    setState(() {
      newsListLoading = true;
    });
    
    _newsList = await newsApi.getNewsList(context, pageNews);

    if(_newsList == null){
      _newsList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _newsList = _newsList;
      newsListLoading = false;
      pageNews = pageNews;
    });
  }

  initDashboardList() async {
    Configuration config = Configuration.of(context);

    setState(() {
      dashboardListLoading = true;
    });
    
    _mDashboardList = await dashboardApi.getDashboardList(context);
    if(_mDashboardList == null){
      _mDashboardList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    _shoutList = await shoutApi.getShoutList(context, config.user.id);
    if(_shoutList == null){
      _shoutList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _shoutList = _shoutList;
      _mDashboardList = _mDashboardList;
      dashboardListLoading = false;
      bannerStatus = bannerStatus;
    });
  }
  initUserBalancePoint() async{
    Configuration config = Configuration.of(context);
    
    setState(() {
      userBalancePointLoading = true;
    });

    mUser = await userApi.getUser(context, config.user.id);
    if(mUser == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    
    setState(() {
      mUser = mUser;
      userBalancePointLoading = false;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    double width = MediaQuery.of(context).size.width;
    double widthWidget = (width-60)/2.3;

    // check permission setiap module
    List<String> tempMenuList = checkMenuList();

    // check fitur apa yang diaktifkan
    List<String> tempMenuNameList = checkMenuNameList(tempMenuList);
    List<Widget> menuWidgetList = [];

    List<Widget> wrapItemList = [];
    for (int i = 0; i < _newsList.length; i++) {
        if(i<6) {
        var item = _newsList[i];
        wrapItemList.add(
        Container(
          width: (width-45)/2,
          height: ((width-45)/2)+120,
          child: TwoColumnView(
            key: Key("newsdetail"+item.id.toString()),
            imageUrl: item.pic,
            title: item.subject,
            date: DateFormat('dd MMM yyyy').format(item.createdAt),
            widgets: [
              Row(children: <Widget>[
                Icon(
                  Icons.thumb_up,
                  size:20,
                  color: config.grayColor,
                ),
                Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                SSText(text:item.likes.toString(),type: 4),
              ],),
              Row(children: <Widget>[
                Icon(
                  Icons.remove_red_eye,
                  size:20,
                  color: config.grayColor
                ),
                Padding(padding: EdgeInsets.symmetric(horizontal: 2)),
                SSText(text:item.recipients.toString(),type: 4),
              ],),
              Row(children: <Widget>[
                Icon(
                  Icons.comment,
                  size:20,
                  color: config.grayColor,
                ),
                Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                SSText(text:item.comments.toString(),type: 4),
              ],),
            ],
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => NewsDetail(mode:1, id:item.id, model: item))
              );
            },
          )
        )
      );
      }
    }

    final List<Widget> _childrenMenu = [
      Scaffold(
      // backgroundColor: Color.fromRGBO(229, 229, 229, 1),
        // appBar: AppBar(
          
        // ),bu
        // drawer: Sidebar(model: mUser, menuList:menuCheckList, menuNameList:menuNameList),
        floatingActionButton: FloatingActionButton(
          child: Text("Testing"),
          onPressed: () async {
            User user = await userApi.loadFromLocalStorage();
            print(user);
          },
        ),
        body: RefreshIndicator(
        backgroundColor: config.primaryColor,
        color: config.primaryTextColor,
        onRefresh: _refresh,
        child: dashboardListLoading ?
        ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context, int index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: Container(width: MediaQuery.of(context).size.width, height:MediaQuery.of(context).size.width/16*9, color: Colors.grey[200]),
            );
          }
        )
        :
        CustomScrollView(
          controller: _controller,
          slivers: <Widget>[
            SliverAppBar(
              floating: true,
              automaticallyImplyLeading: false,
              title: SSText(text: c.company, type: 1, caps:true),
              backgroundColor: c.topBarBackgroundColor,
              actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.crop_free, size:30, color: config.topBarTextColor),
                  onPressed: scanQR,
                ),
                BadgeIconButton(
                  icon: Icon(Icons.notifications, size:30, color: config.topBarTextColor, key: Key("shout"),),
                  itemCount: getUnreadNotification(_shoutList.length),
                  onPressed: (){
                    _shoutList.clear();
                    Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationList(userId:mUser.id)));
                  },
                ),
                IconButton(
                  icon: Icon(Icons.list, size:30, color: config.topBarTextColor),
                  // itemCount: getUnreadNotification(_shoutList.length),
                  onPressed: (){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => TransactionList()));
                  },
                ),
                // IconButton(
                //   icon: Icon(
                //     Icons.settings
                //   ),
                //   color: Colors.white,
                //   onPressed: () {
                    // Navigator.push(context, MaterialPageRoute(builder: (context) => Setting()));
                //   },
                // )
              ],
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                Container(
                  margin: EdgeInsets.only(left: 10),
                  color: Colors.white,
                  child: Row(
                    children: <Widget>[
                      balanceStatus ?
                      Expanded(
                        flex: 1,
                        child: userBalancePointLoading ?
                        Padding(
                          padding: EdgeInsets.only(top:15, bottom:15),
                          child:Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircularProgressIndicator(),
                            ]
                          )
                        )
                        :
                        InkWell(
                          key: Key("balance"),
                          onTap: (){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => BalanceDetail(title: message["balance"])),
                            );
                          },
                           child: Row(
                            children: <Widget>[
                              Icon(Icons.account_balance_wallet, size: 25, color: Color(0xFF777777)),
                              Container(
                                margin: EdgeInsets.only(left: 10),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SSText(text: mUser.balance.toString(), type:2),
                                    SSText(text: message["balance"], type:6)
                                  ],
                                ),
                              ),
                              // IconButton(
                              //   icon:Icon(Icons.refresh),
                              //   onPressed: () {
                              //     initUserBalancePoint();
                              //   },
                              // )
                            ],
                          ),
                        ),
                      )
                      :
                      Container(),
                      
                      pointStatus || balanceStatus ?
                      Container(
                        height: 50.0,
                        width: 1,
                        color: Colors.black12,
                        margin: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 5, top: 5),
                      )
                      :
                      Container(),

                      pointStatus ?
                        Expanded(
                          child: userBalancePointLoading ?
                          Padding(
                            padding: EdgeInsets.only(top:15, bottom:15),
                            child:Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CircularProgressIndicator(),
                              ]
                            )
                          )
                          : 
                          InkWell(
                            key: Key("point"),
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => PointDetail(mode: 1,title: message["point"],)),
                              );
                            },
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.star, size: 30, color: Color(0xFF777777)),
                                Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      SSText(text: mUser.point.toString(), type:2),
                                      SSText(text: message["point"], type:6)
                                    ],
                                  ),
                                ),
                                // IconButton(
                                //   icon:Icon(Icons.refresh),
                                //   onPressed: () {
                                //     initUserBalancePoint();
                                //   },
                                // )
                              ],
                            ),
                          ),
                          flex: 1,
                        )
                      :
                      Container()
                    ],
                  ),
                ),
                childCount: 1,
              ),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
                bannerStatus ?
                Container(
                  height: MediaQuery.of(context).size.width/16*9,
                  child: Carousel("banner", _mDashboardList.map((item) => item.pic).toList(), _mDashboardList.map((item) => item.link).toList(), _mDashboardList.map((item) => item.name).toList(), _mDashboardList.map((item) => item.content).toList()),
                )
                :
                Container(),
                childCount: 1,
              ),
            ),
            SliverPadding(
              padding: EdgeInsets.all(5),
            ),
            Container(
              child: SliverGrid.count(
                crossAxisCount: 4,
                children: menuWidgetList
              ),
            ),
            eventStatus ?
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        color: config.lighterGrayColor,
                        width: 1.0,
                      ),
                    )
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top:15),
                          child:SSText(text: "Upcoming Events", type:2, caps:false, size:20),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              "eventList"
                            );
                          },
                          child: Row(
                            children:[
                              SSText(text: "See More", type:5),
                              Icon(
                                Icons.chevron_right,
                                size: 20,
                                color: config.lightGrayColor,
                              )
                            ]
                          )
                        ),
                      ],
                    ),
                  ),
                ),
                childCount: 1
              ),
            )
            :
            SliverPadding(
              padding: EdgeInsets.all(0),
            ),
            eventStatus ?
            SliverToBoxAdapter(
              child: Container(
                padding: EdgeInsets.only(top:10),
                height: 250,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  controller: _controllerEvent,
                  itemCount: _eventList.length == 0 ? 1 : _eventList.length,
                  itemBuilder: (BuildContext context, int index){
                    if(_eventList.length != 0){
                      return InkWell(
                        key: Key("eventdetail$index"),
                        onTap: (){
                          Navigator.pushNamed(
                            context,
                            "eventDetail/${_eventList[index].id}/1",
                            arguments: _eventList[index]
                          );
                        },
                        child: Padding(
                          padding: index==0 ? EdgeInsets.only(right:20, left: 20):EdgeInsets.only(right:20),
                          child : Column(
                            children: <Widget>[
                              Container(
                                child:  _eventList[index].pic != "" ?
                                Container(
                                  width: widthWidget,
                                  height: widthWidget,
                                  child: PhysicalModel(
                                    elevation: 3,
                                    color: config.lighterGrayColor,
                                    child :
                                    ClipRRect(
                                      borderRadius: new BorderRadius.circular(5),
                                      child: ImageBox(
                                        CompanyAsset("event", _eventList[index].pic), 
                                        widthWidget,
                                        border: false,
                                        fit: BoxFit.cover
                                      ),
                                    )
                                  )
                                )
                                :
                                Column(
                                  children: <Widget>[
                                    Container(
                                      width: widthWidget,
                                      height: widthWidget,
                                      child: PhysicalModel(
                                        elevation: 3,
                                        borderRadius: BorderRadius.circular(5),
                                        color: config.lighterGrayColor,
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[
                                            SSText(text: DateFormat('MMMM').format(_eventList[index].startAt), type:4),
                                            SSText(text: DateFormat('dd').format(_eventList[index].startAt), type:7, size: widthWidget/2.5 ),
                                          ],
                                        )
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(top:10),
                                width: widthWidget,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SSText(text:_eventList[index].name, type:4, maxLines: 2),
                                    SSText(text:DateFormat('dd MMMM yyyy').format(_eventList[index].startAt), type:6, maxLines: 1),
                                    SSText(text:"@"+_eventList[index].place, type:6, maxLines: 1),
                                  ]
                                ),
                              )
                            ],
                          )
                        )
                      );
                    } else {
                      return Center(
                        child: PlaceholderList(type: "eventlist")
                      );
                    }
                  }
                ),
              ),
            )
            :
            SliverPadding(
              padding: EdgeInsets.all(0),
            ),
            newsStatus ?
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) => 
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        color: config.lighterGrayColor,
                        width: 1.0,
                      ),
                    )
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top:15),
                          child:SSText(text: "Progress Weekly", type:2, caps:false, size:20),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              "newsList"
                            );
                          },
                          child: Row(
                            children:[
                              SSText(text: "See More", type:5),
                              Icon(
                                Icons.chevron_right,
                                size: 20,
                                color: config.lightGrayColor,
                              )
                            ]
                          )
                        ),
                      ],
                    ),
                  ),
                ),
                childCount: 1
              ),
            )
            :
            SliverPadding(
              padding: EdgeInsets.all(0),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return Container(
                  child: Padding(
                    padding: EdgeInsets.only(left: 15, right:5, top: 5, bottom:25),
                    child:Wrap(
                      direction: Axis.horizontal,
                      spacing: 15,
                      runSpacing: 15,
                      children: wrapItemList,
                    )
                  )
                );
              },
              childCount: 1,
              )
            ),
            // newsStatus ?
            // SliverGrid(
            //   gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            //     crossAxisCount: 2,
            //     mainAxisSpacing: 15,
            //     crossAxisSpacing: 15,
            //     childAspectRatio: 1.5,
            //   ),
            //   delegate: SliverChildBuilderDelegate(
            //     (BuildContext context, int index) {
            //       final item = _newsList[index];
                  
            //       return _newsList.length != 0 ?
            //       index < _newsList.length ?
            //         TwoColumnView(
            //           key: Key("newsdetail$index"),
            //           imageUrl: item.pic,
            //           title: item.subject,
            //           date: DateFormat('dd MMM yyyy').format(item.createdAt),
            //           widgets: [
            //             Icon(
            //               Icons.thumb_up,
            //               color: Colors.grey,
            //             ),
            //             Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
            //             Text(item.likes.toString()),
            //             Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
            //             Icon(
            //               Icons.remove_red_eye,
            //               color: Colors.grey,
            //             ),
            //             Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
            //             Text(item.recipients.toString()),
            //             Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
            //             Icon(
            //               Icons.comment,
            //               color: Colors.grey,
            //             ),
            //             Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
            //             Text(item.comments.toString()),
            //           ],
            //           onTap: () {
            //             Navigator.push(
            //               context,
            //               MaterialPageRoute(builder: (context) => NewsDetail(mode:1, id:item.id, model: item))
            //             );
            //           },
            //         )
            //         :
            //         Center(
            //           child: Opacity(
            //             opacity: isLoadingNews ? 1.0 : 0.0,
            //             child: CircularProgressIndicator(),
            //           ),
            //         )
            //       :
            //       PlaceholderList(type: "news",);
            //     },
            //     childCount: _newsList.length == 0 ? 1 : _newsList.length,
            //   ),
            // )
            // :
            // SliverPadding(
            //   padding: EdgeInsets.all(0),
            // ),
          ],
        ),
      )
      ), 
      ProductCategoryList(),
      LessonList(),
      ProfilePage(url: CompanyAsset("avatar",  mUser.pic), tagHero: "profile", model: mUser, mode: 3),
    ];
    
    for (var i = 0; i < tempMenuList.length; i++) {
      menuWidgetList.add(
        Container(
          width: 100.0,
          height: 100.0,
          child: Center(
            child: Column(
              children: <Widget>[
                PhysicalModel(
                  color: Color(0xFFF7F7F7),
                  borderRadius: BorderRadius.circular(25.0),
                  elevation: 3,
                  child: InkWell(
                    child: Container(
                        padding: EdgeInsets.all(12.5),
                        height: 50.0,
                        decoration:  BoxDecoration(
                          borderRadius: BorderRadius.circular(16.0),
                          border: Border.all(
                            width: 1.5,
                            color: Colors.transparent,
                          ),
                        ),
                        child: Icon(Icons.local_grocery_store),
                      ),
                    onTap: (){
                      Navigator.pushNamed(
                        context,
                        "${tempMenuList[i]}"
                      );
                    },
                  ),
                ),
                Container (
                  margin: EdgeInsets.only(top: 5),
                  child: SSText(
                    text: message[tempMenuNameList[i]],
                    type: 7,
                    align: TextAlign.center,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
          ),
        )
      );
    }

    return Scaffold(
      // backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      // appBar: AppBar(
      //   title: Text(c.company, style: TextStyle(color: c.topBarTextColor),),
      //   backgroundColor: c.topBarBackgroundColor,
      //   actions: <Widget>[
      //     IconButton(
      //       icon: Icon(Icons.add_a_photo, color: Colors.white,),
      //       onPressed: scanQR,
      //     ),
      //     BadgeIconButton(
      //       icon: Icon(Icons.notifications,color: Colors.white, key: Key("shout"),),
      //       itemCount: getUnreadNotification(_shoutList.length),
      //       onPressed: (){
      //         _shoutList.clear();
      //         Navigator.push(context, MaterialPageRoute(builder: (context) => NotificationList(userId:1127)));
      //       },
      //     ),
      //     IconButton(
      //       icon: Icon(
      //         Icons.settings
      //       ),
      //       color: Colors.white,
      //       onPressed: () {
      //         Navigator.push(context, MaterialPageRoute(builder: (context) => Setting()));
      //       },
      //     )
      //   ],
      // ),
      // drawer: Sidebar(model: mUser, menuList:menuCheckList, menuNameList:menuNameList),
      body: _childrenMenu[mCurrentIndex],
      bottomNavigationBar: AnimatedContainer(
        height: isVisible ? 60:0,
        duration: Duration(milliseconds: 400),
        child: BottomNavigationBar(
          currentIndex: mCurrentIndex,
          type: BottomNavigationBarType.shifting,
          onTap: onTabTapped,
          items: [
            BottomNavigationBarItem(
              backgroundColor: config.bottomBarBackgroundColor,
              icon: Icon(
                Icons.home,
                color: config.bottomBarIconColor,
              ),
              title: Text(
                "Home",
                style: TextStyle(
                  color: config.bottomBarTextColor
                ),
              ),
            ),
            BottomNavigationBarItem(
              backgroundColor: config.bottomBarBackgroundColor,
              icon: Icon(
                Icons.category,
                color: config.bottomBarIconColor,
              ),
              title: Text(
                "Product",
                style: TextStyle(
                  color: config.bottomBarTextColor
                ),
              )
            ),
            BottomNavigationBarItem(
              backgroundColor: config.bottomBarBackgroundColor,
              icon: Icon(
                Icons.list,
                color: config.bottomBarIconColor,
              ),
              title: Text(
                "Lesson",
                style: TextStyle(
                  color: config.bottomBarTextColor
                ),
              )
            ),
            BottomNavigationBarItem(
              backgroundColor: config.bottomBarBackgroundColor,
              icon: Icon(
                Icons.account_circle,
                color: config.bottomBarIconColor,
              ),
              title: Text(
                "Profile",
                style: TextStyle(
                  color: config.bottomBarTextColor
                ),  
              )
            ),
          ],
        ),
      )
    );
  }

  List<String> checkMenuList() {
    List<String> menuList = [];
    newsStatus ? menuList.add("newsList"):false;
    circleStatus ? menuList.add("circleList"):false;
    eventStatus ? menuList.add("eventList"):false;
    // transactionStatus ? menuList.add("transactionList"):false;
    downloadStatus ? menuList.add("downloadList"):false;
    assetStatus ? menuList.add("assetList"):false;
    // spiritualJourneyStatus ? menuList.add("journeyList"):false;
    productCategoryListStatus ? menuList.add("productCategoryList"):false;
    productWishlistListStatus ? menuList.add("productWishlistList"):false;
    productCartlistStatus ? menuList.add("productCartList"):false;
    lessonStatus ? menuList.add("lessonList"):false;
    couponStatus ? menuList.add("couponList"):false;
    fosterChildStatus ? menuList.add("fosterChildList"):false;
    promotionStatus ? menuList.add("promotionList"):false;
    subscribeStatus ? menuList.add("subscribeList"):false;
    nearmeStatus ? menuList.add("nearMe"):false;
    // rentListStatus ? menuList.add("rentList"):false;
    // rentWishlistStatus ? menuList.add("rentWishlistList"):false;
    // rentCartlistStatus ? menuList.add("rentCartList"):false;
    productReviewListStatus ? menuList.add("productReviewList"):false;
    productCommentListStatus ? menuList.add("productCommentList"):false;
    requestListStatus ? menuList.add("requestList"):false;
    budgetListStatus ? menuList.add("budgetList"):false;
    return menuList;
  }

  List<String> checkMenuNameList(List<String> menuList) {
    List<String> tempMenuNameList = [];
    
    int menuListLength = menuList.length;
    for (var i = 0; i < menuListLength; i++) {
      if (menuCheckList.contains(menuList[i])) {
        tempMenuNameList.add(menuList[i]);
      }
    }

    return tempMenuNameList;
  }

  String result = "Hello word";
  
  Future scanQR() async{
    try{
      String qrResult = await BarcodeScanner.scan();
      setState(() {
        result = qrResult; 
        Navigator.pushNamed(context, "$result");
      });
    } on PlatformException catch(ex){
      if (ex.code == BarcodeScanner.CameraAccessDenied){
        setState(() {
          result = "Permission Denied";          
        });
      }
      else{
        setState(() {
          result = "Unknown Error";          
        });
      }
    } on FormatException {
      setState(() {
        result = "You Pressed the back button before scan anything";        
      });
    } catch (ex){
      setState(() {
        result = "Unknown Error";        
      });
    }
  }
  
  Future<Null> _refresh() async {
    pageNews = 1;    
    page = 1;
    
    await initDashboardList();
    await initUserBalancePoint();
    await checkUser();
    return null;
  }
}