// packages
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// widgets
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// screens

// resources
import 'package:shiftsoft/resources/userApi.dart';
import 'package:shiftsoft/tools/functions.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';

class SignUp extends StatefulWidget{
  createState(){
    return SignUpState();
  }
}

class SignUpState extends State<SignUp>{
  final _formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final phoneController = TextEditingController();
  final addressController = TextEditingController();
  final cityController = TextEditingController();

  final FocusNode nameFocus = FocusNode();
  final FocusNode emailFocus = FocusNode();
  final FocusNode newPasswordFocus = FocusNode();
  final FocusNode phoneFocus = FocusNode();
  final FocusNode addressFocus = FocusNode();
  final FocusNode cityFocus = FocusNode();

  bool signUpLoading = false;

  void dispose(){
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    phoneController.dispose();
    super.dispose();
  }
  void initState(){
    super.initState();
  }

  Map<String, String> message=new Map();
  List<String> messageList = ["signUp","enterName","enterPassword","enterEmail","enterPhone","enterAddress","enterCity","createAccount"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["signUp"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: nameController,
                  focusNode: nameFocus,
                  keyboardType: TextInputType.text,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.person),
                    onPressed: (){},
                  ),
                  hintText: message["enterName"],
                  labelText: message["enterName"],
                  textInputAction: TextInputAction.next,
                  onSubmitted: (value) {
                    emailFocus.unfocus();
                    FocusScope.of(context).requestFocus(emailFocus);
                  },
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter Your Name";
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: emailController,
                  focusNode: emailFocus,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.emailAddress,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.mail),
                    onPressed: (){},
                  ),
                  hintText: message["enterEmail"],
                  labelText: message["enterEmail"],
                  onSubmitted: (value) {
                    newPasswordFocus.unfocus();
                    FocusScope.of(context).requestFocus(newPasswordFocus);
                  },
                  validator: (value){
                    if(!value.contains("@")){
                      return "Input must be Email";
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: newPasswordController,
                  focusNode: newPasswordFocus,
                  textInputAction: TextInputAction.next,
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.lock),
                    onPressed: (){},
                  ),
                  hintText: message["enterPassword"],
                  labelText: message["enterPassword"],
                  onSubmitted: (value) {
                    phoneFocus.unfocus();
                    FocusScope.of(context).requestFocus(phoneFocus);
                  },
                  validator: (value){
                    if(value.isEmpty){
                      return "Please Enter Your Password";
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: phoneController,
                  focusNode: phoneFocus,
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.number,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.phone),
                    onPressed: (){},
                  ),
                  hintText: message["enterPhone"],
                  labelText: message["enterPhone"],
                  onSubmitted: (value) {
                    addressFocus.unfocus();
                    FocusScope.of(context).requestFocus(addressFocus);
                  },
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter Your Phone Number";
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: addressController,
                  focusNode: addressFocus,
                  textInputAction: TextInputAction.next,
                  maxLines: 3,
                  keyboardType: TextInputType.text,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.location_on),
                    onPressed: (){},
                  ),
                  hintText: message["enterAddress"],
                  labelText: message["enterAddress"],
                  onSubmitted: (value) {
                    cityFocus.unfocus();
                    FocusScope.of(context).requestFocus(cityFocus);
                  },
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter Your Address";
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: cityController,
                  focusNode: cityFocus,
                  textInputAction: TextInputAction.done,
                  keyboardType: TextInputType.text,
                  prefixIcon: IconButton(
                    icon: Icon(Icons.location_city),
                    onPressed: (){},
                  ),
                  hintText: message["enterCity"],
                  labelText: message["enterCity"],
                  validator: (String value){
                    if(value.isEmpty){
                      return "Please Enter Your City";
                    }
                  },
                ),
              ),
              signUpLoading ? 
              Padding(
                padding: EdgeInsets.only(top:15, bottom:15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                  ]
                )
              )
              :
              Container(
                margin: EdgeInsets.only(top: 20),
                child: Button(
                  backgroundColor: config.primaryColor,
                  rounded: true,
                  child: Text(message["createAccount"], style: TextStyle(color: config.primaryTextColor),),
                  onTap: () async {
                    if(_formKey.currentState.validate()){
                      setState(() {
                        signUpLoading = true;
                      });
                      final message = await userApi.addUser(context, emailController.text, nameController.text, passwordController.text, phoneController.text,addressController.text,cityController.text);
                      setState(() {
                        signUpLoading = false;  
                      });
                      showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context){
                          return AlertDialog(
                            title: Text("Alert"),
                            content: Text(message),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("OK"),
                                onPressed: (){
                                  Navigator.of(context).pop();            
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );
                        }
                      );
                    }   
                  },
                )
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Text("By clicking \"Registration\" I agree to Shift-soft", style: TextStyle(color: Colors.grey), textAlign: TextAlign.center,),
              Text("Terms of Service", style: TextStyle(color: Colors.blue), textAlign: TextAlign.center,),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
            ],
          ), 
        ),
      )
    );
  }
}