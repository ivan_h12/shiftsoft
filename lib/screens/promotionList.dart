// packages
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/promotionDetail.dart';
import 'package:shiftsoft/screens/login.dart';


// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/promotionApi.dart';

// models
import 'package:shiftsoft/models/mpromotion.dart';
import 'package:shiftsoft/settings/configuration.dart';

class PromotionList extends StatefulWidget{
  createState(){
    return PromotionListState();
  }
}

class PromotionListState extends State<PromotionList>{
  List<Promotion> _promotionList = [];
  bool promotionListLoading = true;
  ScrollController _controller;
  bool isLoading = false;
  int page = 1;
  
  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","promoPeriod","promoCode","withoutPromoCode","close","insertPromoCodeInCartList","copyCode","copySuccess"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void initState(){
    initPromotionList();
    _controller = ScrollController();
    _controller.addListener(_scrollListener);
  }
  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent && !_controller.position.outOfRange) {
      if(_promotionList.length%c.apiLimit == 0){
        loadMore();
      } else {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  loadMore() async {
    if (!isLoading) {
      setState(() {
        isLoading = true;
      });
      
      _promotionList.addAll(await promotionApi.getPromotionList(context, page));
      if(_promotionList == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message['loginFailed']),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
      await makeAnimation();
      page++;

      setState(() {
        _promotionList = _promotionList;
        isLoading = false;
        page = page;
      });
    }
  }

  makeAnimation() async {
    print("masuk");
    final offsetFromBottom = _controller.position.maxScrollExtent - _controller.offset;
    if (offsetFromBottom < 50) {
      await _controller.animateTo(
        _controller.offset - (50 - offsetFromBottom),
        duration: Duration(milliseconds: 500),
        curve: Curves.easeOut,
      );
    }
  }

  initPromotionList() async {
    setState(() {
      promotionListLoading = true;
    });
    
    _promotionList = await promotionApi.getPromotionList(context, page);
    if(_promotionList == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    page++;

    setState(() {
      _promotionList = _promotionList;
      promotionListLoading = false;
      page = page;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);
    
    final key = GlobalKey<ScaffoldState>();
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: Text("Promo", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: promotionListLoading ? 
          ListView.builder(
            shrinkWrap: true,
            itemCount: 6,
            itemBuilder: (context, index){
              return Container(
                color: Colors.black12,
                padding: EdgeInsets.all(5),
                child: Card(
                  child: Shimmer.fromColors(
                    baseColor: Colors.grey[200],
                    highlightColor: Colors.grey[350],
                    period: Duration(milliseconds: 800),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.width/16*9,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.grey[200],
                        ),
                        ListTile(
                          leading: Icon(Icons.timer),
                          title: Container(width: 50, height: 20, color: Colors.grey[200]),
                          subtitle: Container(width: 100, height: 50, color: Colors.grey[200]),
                        ),
                        ListTile(
                          leading: Icon(Icons.high_quality),
                          title: Container(width: 50, height: 70, color: Colors.grey[200]),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          )
          :
          ListView.builder(
            controller: _controller,
            itemCount: _promotionList.length+1 != 1 ? _promotionList.length+1 : 1,
            itemBuilder: (context, index){
              if(_promotionList.length+1 != 1){
                return index < _promotionList.length ? 
                Container(
                  color: Colors.black12,
                  padding: EdgeInsets.all(5),
                  child: GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => PromotionDetail(mode:3, id:_promotionList[index].id, model: _promotionList[index],type: _promotionList[index].type,)),
                      );
                    },
                    child: Card(
                      key: Key("promotionDetail$index"),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.width/16*9,
                            width: MediaQuery.of(context).size.width,
                            child: ImageBox(CompanyAsset("promotion", _promotionList[index].pic), MediaQuery.of(context).size.height/16*9),
                          ),
                          ListTile(
                            leading: Icon(Icons.timer),
                            title: Text(message['promoPeriod'], style: TextStyle(fontSize: 15, color: Colors.black),),
                            subtitle: Text(DateFormat("dd-MMMM-yyyy").format(_promotionList[index].startAt) + " ~ " + DateFormat("dd-MMMM-yyyy").format(_promotionList[index].endAt), style: TextStyle(fontSize: 12),),
                          ),
                          ListTile(
                            leading: Icon(Icons.high_quality),
                            title: Row(
                              children: <Widget>[
                                _promotionList[index].type != 1 && _promotionList[index].type != 2 ? Text(message['promoCode'], style: TextStyle(fontSize: 15, color: Colors.black),) : Text(message['withoutPromoCode'], style: TextStyle(fontSize: 15, color: Colors.black), textAlign: TextAlign.center,),
                                _promotionList[index].type != 1 && _promotionList[index].type != 2 ? IconButton(
                                  icon: Icon(Icons.info),
                                  onPressed: (){
                                    showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (context){
                                        return AlertDialog(
                                          title: Text(message['promoCode']),
                                          content: ListTile(
                                            leading: Text(message["insertPromoCodeInCartList"]),
                                            trailing: ImageBox("https://dharmamerchantservices.com/wp-content/uploads/2017/07/cropped-visa.png", 50),
                                          ),
                                          actions: <Widget>[
                                            FlatButton(
                                              color: Colors.green,
                                              child: Text(message['close'], style: TextStyle(color: Colors.white),),
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                              },
                                            )
                                          ],
                                        );
                                      }
                                    );
                                  },
                                ): Text(""),
                              ],
                            ),
                            subtitle: Text(_promotionList[index].code, style: TextStyle(color: Colors.red),),
                            trailing: _promotionList[index].type != 1 && _promotionList[index].type != 2 ? RaisedButton(
                              child: Text(message['copyCode']),
                              onPressed: (){
                                Clipboard.setData(ClipboardData(text: _promotionList[index].code));
                                key.currentState.showSnackBar(SnackBar(content: Text(message['copySuccess']),));
                              },
                            ) : null,
                          )
                        ],
                      ),
                    ),
                  ),
                )
                :
                Center(
                  child: Opacity(
                    opacity: isLoading ? 1.0 : 0.0,
                    child: CircularProgressIndicator(),
                  ),
                );
              }else{
                return PlaceholderList(type: "promotion");
              }
            },
          ),
        ),
    );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initPromotionList();
    return null;
  }
}