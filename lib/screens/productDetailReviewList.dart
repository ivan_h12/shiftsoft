// packages
import 'package:flutter/material.dart';
import 'package:flutter_rating/flutter_rating.dart';
import 'package:intl/intl.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:badges/badges.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productReviewAdd.dart';
import 'package:shiftsoft/screens/reportForm.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/productApi.dart';

//models
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/models/mproductreview.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductDetailReviewList extends StatefulWidget{
  int id;
  Product model;

  createState(){
    return ProductDetailReviewListState();
  }
  ProductDetailReviewList({Key key, this.id, this.model}) : super(key:key);
}

class ProductDetailReviewListState extends State<ProductDetailReviewList>{
  List<ProductReview> _productReviewList = [];
  bool productReviewLoading = true;
  int filterReview = 0;
  double rating = 3, totalRating = 0;
  int starCount = 5;
  double rate1=0,rate2=0,rate3=0,rate4=0,rate5=0;

  void setReview(int temp){
    filterReview = temp;
  }

  void initState(){
    initProductReviewList();
    print(widget.model.name);
  }

   Map<String, String> message=new Map();
  List<String> messageList = ["reviews","all","mostHelpfulReview","joinDiscussion","reportDiscussion","cancel","send","loginFailed","by","helped","seeReply"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  initProductReviewList() async{
    setState(() {
      productReviewLoading = true;
    });
    
    _productReviewList = await productApi.getProductReviewList(context, 1926, widget.id);
    if(_productReviewList == null){
      _productReviewList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _productReviewList = _productReviewList;
      rate1 = 0.0;
      rate2 = 0.0;
      rate3 = 0.0;
      rate4 = 0.0;
      rate5 = 0.0;
      totalRating = 0.0;
      if(_productReviewList.length>0){
        print("masuk");
        for(int i=0;i<_productReviewList.length;i++){
          if(_productReviewList[i].rating<=1){
            rate1=rate1+1.0;
          } else if(_productReviewList[i].rating<=2){
            rate2=rate2+1.0;
          } else if(_productReviewList[i].rating<=3){
            rate3=rate3+1.0;
          } else if(_productReviewList[i].rating<=4){
            rate4=rate4+1.0;
          } else if(_productReviewList[i].rating<=5){
            rate5=rate5+1.0;
          }
          totalRating = totalRating + _productReviewList[i].rating.toDouble();
        }
      } 
      productReviewLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['reviews'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          BadgeIconButton(
            icon: Icon(Icons.add,color: Colors.white,),
            itemCount: 0,
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ProductReviewAdd(userId: 1926, productId: widget.model.id, productName: widget.model.name, productPic: widget.model.pic,)));
            },
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productReviewLoading ? 
        Container(
          child: ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context,int index){
              return Column(
                children: <Widget>[
                  Card(
                    child: Row(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:35, width: 70, color: Colors.grey[200]),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:30, width: 80, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                ),
                              ],
                            ),
                            
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: 1,
                                    trailing: Container(color: Colors.grey[200] , width: 50, height: 15,) ,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.grey[200],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: 1,
                                    trailing: Container(color: Colors.grey[200] , width: 50, height: 15,) ,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.grey[200],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: 1,
                                    trailing: Container(color: Colors.grey[200] , width: 50, height: 15,) ,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.grey[200],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: 1,
                                    trailing: Container(color: Colors.grey[200] , width: 50, height: 15,) ,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.grey[200],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                ),
                                Shimmer.fromColors(
                                  baseColor: Colors.grey[200],
                                  highlightColor: Colors.grey[350],
                                  period: Duration(milliseconds: 800),
                                  child: LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: 1,
                                    trailing: Container(color: Colors.grey[200] , width: 50, height: 15,) ,
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.grey[200],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Card(  
                                  child: FlatButton(),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Card(  
                                  child: FlatButton(),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Card(  
                                  child: FlatButton(),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Card(  
                                  child: FlatButton(),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Card(  
                                  child: FlatButton(),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Card(  
                                  child: FlatButton(),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text("Ulasan Paling Membantu"),
                      ],
                    ),
                  ),
                  ListView.builder(
                    itemCount: 2,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (BuildContext context,int index){
                      return Card(
                        child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[200],
                                    highlightColor: Colors.grey[350],
                                    period: Duration(milliseconds: 800),
                                    child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Shimmer.fromColors(
                                    baseColor: Colors.grey[200],
                                    highlightColor: Colors.grey[350],
                                    period: Duration(milliseconds: 800),
                                    child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                                  ),
                                ],
                              ),
                              Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                              ),
                              Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(height:20, width: 95, color: Colors.grey[200], margin: EdgeInsets.only(top: 5, left: 5),),
                              ),
                              Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(left: 5, top: 5),
                                      child: ImageBox("https://ecs7.tokopedia.net/img/cache/700/product-1/2016/4/16/24296143/24296143_c98a84d0-4b06-45e1-872c-6e5b197ce877.jpg", 50),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 5, top: 5),
                                      child: ImageBox("https://ecs7.tokopedia.net/img/cache/700/product-1/2016/4/16/24296143/24296143_c98a84d0-4b06-45e1-872c-6e5b197ce877.jpg", 50),
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 5, top: 5),
                                      child: ImageBox("https://ecs7.tokopedia.net/img/cache/700/product-1/2016/4/16/24296143/24296143_c98a84d0-4b06-45e1-872c-6e5b197ce877.jpg", 50),
                                    ),
                                  ],
                                ),
                              ),
                              Divider(),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: FlatButton(
                                      child: Row(
                                        children: <Widget>[
                                          Icon(Icons.thumb_up),
                                          Text("Terbantu?"),
                                        ],
                                      ),
                                    )
                                  ),  
                                  Expanded(
                                    flex: 1,
                                    child: FlatButton(
                                      child: Row(
                                        children: <Widget>[
                                          Text("Lihat Balasan"),
                                          Icon(Icons.keyboard_arrow_down),  
                                        ],
                                      ),
                                    )
                                  ),
                                ],
                              ),
                            ],
                          )
                        )
                      );
                    },
                  ),
                ],
              );
            },
          )
        )
        :
        Container(
          child: ListView.builder(
            itemCount: 1,
            itemBuilder: (BuildContext context,int index){
              return Column(
                children: <Widget>[
                  Card(
                    child: Row(
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Text(_productReviewList.length != 0 ? (totalRating/_productReviewList.length).toString() : "0.0",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),),
                                Text("/5",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                StarRating(
                                  size: 20.0,
                                  rating: _productReviewList.length != 0 ? (totalRating/_productReviewList.length).toDouble() : 0.0,
                                  color: Colors.yellow,
                                  borderColor: Colors.grey,
                                  starCount: starCount,
                                  
                                ),
                              ],
                            ),
                            Text(_productReviewList.length.toString()+" "+message['reviews']),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 20.0,
                                    rating: 5.0,
                                    color: Colors.yellow,
                                    borderColor: Colors.grey,
                                    starCount: starCount,
                                  ),
                                  LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: rate5 != 0 ? rate5/_productReviewList.length.toDouble() : 0.0,
                                    trailing: Text(rate5.toString()),
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.red,
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 20.0,
                                    rating: 4.0,
                                    color: Colors.yellow,
                                    borderColor: Colors.grey,
                                    starCount: starCount,
                                  ),
                                  LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: rate4 != 0 ? rate4/_productReviewList.length.toDouble() : 0.0,
                                    trailing: Text(rate4.toString()),
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.red,
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 20.0,
                                    rating: 3.0,
                                    color: Colors.yellow,
                                    borderColor: Colors.grey,
                                    starCount: starCount,
                                  ),
                                  LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: rate3 != 0 ? rate3/_productReviewList.length.toDouble() : 0.0,
                                    trailing: Text(rate3.toString()),
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.red,
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 20.0,
                                    rating: 2.0,
                                    color: Colors.yellow,
                                    borderColor: Colors.grey,
                                    starCount: starCount,
                                  ),
                                  LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: rate2 != 0 ? rate2/_productReviewList.length.toDouble() : 0.0,
                                    trailing: Text(rate2.toString()),
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.red,
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  StarRating(
                                    size: 20.0,
                                    rating: 1.0,
                                    color: Colors.yellow,
                                    borderColor: Colors.grey,
                                    starCount: starCount,
                                  ),
                                  LinearPercentIndicator(
                                    width: MediaQuery.of(context).size.width/5,
                                    lineHeight: 15.0,
                                    percent: rate1 != 0 ? rate1/_productReviewList.length.toDouble() : 0.0,
                                    trailing: Text(rate1.toString()),
                                    linearStrokeCap: LinearStrokeCap.butt,
                                    progressColor: Colors.red,
                                  ),
                                ],
                              ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Card(  
                                child: FlatButton(
                                  onPressed: (){
                                    setReview(0);
                                    initProductReviewList();
                                  },
                                  child: Text(message['all']),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 5,
                              child: StarRating(
                                size: 50.0,
                                rating: rating,
                                color: Colors.yellow,
                                borderColor: Colors.grey,
                                starCount: starCount,
                                onRatingChanged: (rating) => setState(
                                  () {
                                    setReview(rating.toInt());
                                    this.rating = rating;
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text(message["mostHelpfulReview"]),
                      ],
                    ),
                  ),
                  ListView.builder(
                    itemCount: _productReviewList.length != 0 ? _productReviewList.length : 1,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (BuildContext context,int index){
                      if(filterReview == 0){
                        return _productReviewList.length != 0 ? Card(
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 5,
                                      child: Text("Rating: "+ _productReviewList[index].rating.toString()),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: IconButton(
                                        icon: Icon(Icons.more_vert),
                                        onPressed: (){
                                          showModalBottomSheet<void> (context: context, builder: (BuildContext context) {
                                            return Container(
                                              padding: EdgeInsets.only(left: 15),
                                              child: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  ListTile(
                                                    leading: Text(message['joinDiscussion']),
                                                  ),
                                                  Divider(),
                                                  InkWell(
                                                    onTap: (){
                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(builder: (context) => ReportForm()),
                                                      );
                                                    },
                                                    child:ListTile(
                                                      leading: Text(message['reportDiscussion']),
                                                    ),
                                                  ),
                                                  Divider(),
                                                  FlatButton(
                                                    child: Text(message['cancel'], style: TextStyle(color: Colors.green),),
                                                    onPressed: (){
                                                      Navigator.pop(context);
                                                    },
                                                  )
                                                ],
                                              ),
                                            );
                                          });
                                        },
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Text(message['by']),
                                    Text(_productReviewList[index].name,style: TextStyle(fontWeight: FontWeight.bold),),
                                  ],
                                ),
                                Text(DateFormat("dd-MMMM-yyyy").format(_productReviewList[index].createAt)),
                                Text(_productReviewList[index].desc),
                                Row(
                                  children: <Widget>[
                                    ImageBox("http://blog.reship.com/wp-content/uploads/2016/06/Best-Product-Review-Sites.jpg", 50),
                                    ImageBox("http://blog.reship.com/wp-content/uploads/2016/06/Best-Product-Review-Sites.jpg", 50),
                                    ImageBox("http://blog.reship.com/wp-content/uploads/2016/06/Best-Product-Review-Sites.jpg", 50),
                                  ],
                                ),
                                Divider(),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: FlatButton(
                                        onPressed: (){},
                                        child: Row(
                                          children: <Widget>[
                                            Icon(Icons.thumb_up),
                                            Text(message['helped']),
                                          ],
                                        ),
                                      )
                                    ),  
                                    Expanded(
                                      flex: 1,
                                      child: FlatButton(
                                        onPressed: (){},
                                        child: Row(
                                          children: <Widget>[
                                            Text(message['seeReply']),
                                            Icon(Icons.keyboard_arrow_down),
                                          ],
                                        ),
                                      )
                                    ),
                                  ],
                                ),
                              ],
                            )
                          )
                        )
                        :
                        PlaceholderList(type: "productreview");
                      } else if (_productReviewList.length !=0 ){
                        if(_productReviewList[index].rating == filterReview){
                          return Card(
                            child: Container(
                              padding: EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 5,
                                        child: Text("Rating: "+ _productReviewList[index].rating.toString()),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: IconButton(
                                          icon: Icon(Icons.more_vert),
                                          onPressed: (){
                                            showModalBottomSheet<void> (context: context, builder: (BuildContext context) {
                                              return Container(
                                                padding: EdgeInsets.only(left: 15),
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[
                                                    ListTile(
                                                      leading: Text(message['joinDiscussion']),
                                                    ),
                                                    Divider(),
                                                    InkWell(
                                                      onTap: (){
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(builder: (context) => ReportForm()),
                                                        );
                                                      },
                                                      child:ListTile(
                                                        leading: Text(message['reportDiscussion']),
                                                      ),
                                                    ),
                                                    Divider(),
                                                    FlatButton(
                                                      child: Text(message['cancel'], style: TextStyle(color: Colors.green),),
                                                      onPressed: (){
                                                        Navigator.pop(context);
                                                      },
                                                    )
                                                  ],
                                                ),
                                              );
                                            });
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Text(message['by']),
                                      Text(_productReviewList[index].name,style: TextStyle(fontWeight: FontWeight.bold),),
                                    ],
                                  ),
                                  Text(DateFormat("dd-MMMM-yyyy").format(_productReviewList[index].createAt)),
                                  Text(_productReviewList[index].desc),
                                  Row(
                                    children: <Widget>[
                                      ImageBox("http://blog.reship.com/wp-content/uploads/2016/06/Best-Product-Review-Sites.jpg", 50),
                                      ImageBox("http://blog.reship.com/wp-content/uploads/2016/06/Best-Product-Review-Sites.jpg", 50),
                                      ImageBox("http://blog.reship.com/wp-content/uploads/2016/06/Best-Product-Review-Sites.jpg", 50),
                                    ],
                                  ),
                                  Divider(),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: (){},
                                          child: Row(
                                            children: <Widget>[
                                              Icon(Icons.thumb_up),
                                              Text(message['helped']),
                                            ],
                                          ),
                                        )
                                      ),  
                                      Expanded(
                                        flex: 1,
                                        child: FlatButton(
                                          onPressed: (){},
                                          child: Row(
                                            children: <Widget>[
                                              Text(message['seeReply']),
                                              Icon(Icons.keyboard_arrow_down),
                                            ],
                                          ),
                                        )
                                      ),
                                    ],
                                  ),
                                ],
                              )
                            )
                          );
                        }else{
                          return PlaceholderList(type: "productreview",);
                        }
                      } else {
                        return PlaceholderList(type: "productreview",);
                      }
                    },
                  ),
                ],
              );
            },
          )
        ),
      ),     
    );
  }
  Future<Null> _refresh() async {
    await initProductReviewList();
    filterReview = 0;
    return null;
  }  
}