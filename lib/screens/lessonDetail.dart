// package
import 'package:flutter/material.dart';

// screens
import './lessonClassroomDetail.dart';
import './lessonStudentList.dart';
import './login.dart';

// widgets
import '../widgets/itemDetail.dart';
import 'package:shiftsoft/widgets/listBox.dart';

// resource
import '../resources/lessonApi.dart';

// models
import '../models/mlesson.dart';
import 'package:shiftsoft/settings/configuration.dart';

class LessonDetail extends StatefulWidget {
  Lesson model;
  int mode, id;
  
  LessonDetail({Key key, this.mode, this.id, this.model}) : super(key:key);

  @override
  _LessonDetailState createState() => _LessonDetailState();
}

class _LessonDetailState extends State<LessonDetail> {
  List<String> data = new List<String>();
  Lesson lessonDetail;
  bool lessonDetailLoading = true;

  List<String> pic = [];

  @override
  void initState(){
    super.initState();
    if (widget.mode == 1) {
      initLessonDetail();
    } else if (widget.mode == 3) {
      lessonDetail = widget.model;
      setState(() {
        lessonDetailLoading = false;
        lessonDetail = lessonDetail;
      });
    }

    setState(() {
     for(int i=0; i<12; i++){
       int a=i+1;
       data.add("Pertemuan "+a.toString());
     } 
    });
  }

  initLessonDetail() async {
    setState(() {
      lessonDetailLoading = true;
    });
    
    lessonDetail = await lessonApi.getLesson(context, widget.id);
    if(lessonDetail == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      lessonDetail = lessonDetail;
      lessonDetailLoading = false;
    });

    pic.add(lessonDetail.pic);
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(lessonDetail.name, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: ItemDetail(
        imageList: pic,
        content: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(lessonDetail.deskripsi),
              ),
              SizedBox(
                height: 100.0,
                  child: InkWell(
                    key: Key("studentlist"),
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LessonStudentList())
                      );
                    },
                    child: CustomScrollView(
                      scrollDirection: Axis.horizontal,
                      slivers: <Widget>[
                        SliverList(
                          delegate: SliverChildBuilderDelegate((context, index) =>
                            Container(
                              alignment: Alignment.center,
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    width: 80.0,
                                    height: 80.0,
                                    decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: NetworkImage("https://via.placeholder.com/150"),
                                        fit: BoxFit.fill,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                      border: Border.all(
                                        color: Colors.white,
                                        width: 4.0,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Text("Made", style: TextStyle(fontSize: 15.0))
                                      ],  
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            childCount: 10,
                          )
                        )
                      ]
                    )
                  )
                )
            ],
          ),
        ),
        list: SliverList(
          delegate: SliverChildBuilderDelegate((context, index) =>
            ListBox(
              childs: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                        Container(
                          margin:  EdgeInsets.only(left: 10.0, right: 7.0, top: 25.0),
                          child: Text(data[index], style: TextStyle(fontSize: 22.0))
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 7.0, right: 7.0, top:25.0),
                          child: Text("1 Januari - 8 Januari", style: TextStyle(fontSize: 15.0, color: Colors.black54))
                        ),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        margin:  EdgeInsets.only(left: 7.0),
                        child: IconButton(
                          key: Key("classroomdetail$index"),
                          icon: Icon(Icons.file_download, color: Colors.redAccent),
                            iconSize: 25.0,
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => LessonClassroomDetail())
                              );
                            },
                        )
                      ),
                      Container(
                          margin:  EdgeInsets.only(bottom: 20.0, top: 17.0),
                          child: Text("Download File", style: TextStyle(fontSize: 15.0))
                        ),
                    ],
                  )
                ],
              )
            ),
            childCount: 12,
          )
        ), 
      ),
    );
  }
}