// package
import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens
import '../widgets/profileBox.dart';
import './profilePage.dart';
import './login.dart';

// widgets
import '../widgets/placeholderList.dart';
import '../widgets/loadingUserList.dart';

// resource
import 'package:shiftsoft/resources/attendanceApi.dart';

// models
import 'package:shiftsoft/models/mattendance.dart';
import 'package:shiftsoft/settings/configuration.dart';

class LessonStudentList extends StatefulWidget{
  createState(){
    return LessonStudentListState();
  }
}

class LessonStudentListState extends State<LessonStudentList>{
  List<Attendance> _attendanceList = [];
  bool attendanceListLoading = true;

  @override
  void initState(){
    setState(() {
      initAttendanceList();
    });
  }

  initAttendanceList() async{
    setState(() {
      attendanceListLoading = true;
    });
    
    _attendanceList = await attendanceApi.getAttendanceList(context, 326);
    if(_attendanceList == null){
      _attendanceList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _attendanceList = _attendanceList;
      attendanceListLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Attendance List", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: attendanceListLoading ? LoadingUserList() :
        Container(
          child: Column(
            children: <Widget>[
              Container(
                child: Expanded(
                  child: SizedBox(
                    height: 800.0,
                    child: ListView.builder(
                      itemCount: _attendanceList.length != 0 ? _attendanceList.length : 1,
                      itemBuilder: (BuildContext context, int index){
                        if(_attendanceList.length != 0){
                          return Hero(
                            tag: "profile$index",
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  onTap: (){
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => ProfilePage(url: CompanyAsset("avatar",  _attendanceList[index].pic), tagHero: "profile$index", idUser: _attendanceList[index].id, mode: 1,))
                                    );
                                  },
                                  contentPadding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 15.0),
                                  leading: ProfileBox(_attendanceList[index].pic, 75.0, _attendanceList[index].name,
                                    Row(
                                      children: <Widget>[
                                        Text("", style: TextStyle(fontSize: 15.0)),
                                      ],
                                    ),
                                  ),
                                ),
                              ],  
                            ),
                          );
                        } else {
                          return PlaceholderList(type: "attendance");
                        }
                      }
                    ),
                  ),
                ),
              )
            ],
          ) 
        ) 
      )
    );
  }

  Future<Null> _refresh() async {
    await initAttendanceList();
    return null;
  }
}