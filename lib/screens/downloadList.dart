// packages
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:open_file/open_file.dart';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:shimmer/shimmer.dart';

// screens

// widgets
import 'package:shiftsoft/widgets/listBox.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/tools/functions.dart';
import '../widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/downloadApi.dart';

// models
import 'package:shiftsoft/models/mdownload.dart';
import 'package:shiftsoft/settings/configuration.dart';

class DownloadList extends StatefulWidget {
  createState() {
    return DownloadListState();
  }
}

class DownloadListState extends State<DownloadList> {
  bool downloadListLoading = true;
  String progressString = "";
  String dirTarget = "";

  List<Download> downloadList;
  List<bool> statusDownloadList;
  List<int> loadingDownloadList = [];
  ScrollController _controller;

  int page = 1;

  Map<String, String> message = new Map();
  List<String> messageList = ["downloadList"];

  void initState() {
    super.initState();

  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    initDownloadList();
  }

  initDownloadList() async {
    setState(() {
      downloadListLoading = true;
    });

    downloadList = await downloadApi.getDownload(context, page);

    statusDownloadList = [];
    Directory dir = await getApplicationDocumentsDirectory();

    downloadList.map((item) async {
      final status = checkFile(dir.path, item.file);
      if (status == true) {
        statusDownloadList.add(true);
      } else {
        statusDownloadList.add(false);
      }
    }).toList();

    page++;

    setState(() {
      downloadList = downloadList;
      statusDownloadList = statusDownloadList;
      downloadListLoading = false;
      page = page;
    });
  }

  Future<bool> checkFile(String path, String fileName) async {
    String dirTarget = "$path/$fileName";
    bool status = await File(dirTarget).exists();
    return status;
  }

  download(int id, String link, String fileName, {bool download = false}) async {
    Dio dio = Dio();
    Directory dir = await getApplicationDocumentsDirectory();
    String dirTarget = "${dir.path}/$fileName";
  
    bool status = await checkFile(dir.path, fileName);

    if (!status || download) {
      String url = link;

      try {
        await dio.download(url, dirTarget,
          onReceiveProgress: (rec, total) {
            print("Rec : $rec, Total : $total");

            loadingDownloadList.add(id);

            print(loadingDownloadList);

            setState(() {
              loadingDownloadList = loadingDownloadList;
              progressString = ((rec/total) * 100).toStringAsFixed(0) + "%";
            });
          }
        );
      } catch(e) {
        print(e);
      }

      loadingDownloadList.removeWhere((item) {
        item = id;
      });
      
      print(loadingDownloadList);

      setState(() {
        loadingDownloadList = loadingDownloadList;
        progressString = "Completed";
        dirTarget = dirTarget;
      });
    }
    await OpenFile.open(dirTarget);
  } 

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      backgroundColor: Color.fromRGBO(229, 229, 229, 1),
      appBar: AppBar(
        title: Text(message["downloadList"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: downloadListLoading ?
        ListView.builder(
          itemCount: 6,
          itemBuilder: (BuildContext context, int index) {
            return Shimmer.fromColors(
              baseColor: Colors.grey[200],
              highlightColor: Colors.grey[350],
              period: Duration(milliseconds: 800),
              child: ListTile(
                title: Container(width: 100, height: 50, color: Colors.grey[200]),
                trailing: Container(width: 50, height: 50, color: Colors.grey[200])
              )
            );
          }
        )
        :
        ListView.builder(
          controller: _controller,
          itemCount: downloadList.length != 0 ? downloadList.length : 1,
          itemBuilder: (BuildContext context, int index) {
            if(downloadList.length != 0) {
              Download item = downloadList[index];
              
              bool status = loadingDownloadList.contains(item.id);

              return index < downloadList.length ?
              ListBox(
                childs: ListTile(
                  key: Key("download$index"),
                  title: Text(item.name),
                  trailing: status ?
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      CircularProgressIndicator(),
                      Padding(padding: EdgeInsets.symmetric(vertical: 5),),
                      Text(progressString)
                    ],
                  )
                  :
                  Container(
                    width: 100,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.file_download),
                          onPressed: () async {
                            await download(item.id, CompanyAsset("download", item.file), item.file, download: true);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.folder_open),
                          onPressed: () async {
                            await download(item.id, CompanyAsset("download", item.file), item.file);
                          },
                        )
                      ],
                    ),
                  )
                )
              )
              :
              Center(
                child: Opacity(
                  opacity: downloadListLoading ? 1.0 : 0.0,
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return PlaceholderList(type: "download");
            }
          }
        ),
      )
    );
  }

  Future<Null> _refresh() async {
    page = 1;
    await initDownloadList();
    return null;
  }
}


// ListBox(
//         childs: Column(
//           children: <Widget>[
//             Button(
//               rounded: true,
//               child: Text("Open PDF"),
//               onTap: () async {
//                 download();
//               },
//             ),
//             downloading ?
//             Column(
//               children: <Widget>[    
//                 Text("Downloading $progressString"),
//                 CircularProgressIndicator()
//               ],
//             )
//             :
//             Text("Download file now!!"),
//           ],
//         )
//       ),