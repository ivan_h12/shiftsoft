// packages
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productDetail.dart';
import 'package:shiftsoft/widgets/productCard.dart';
import 'package:shiftsoft/screens/login.dart';


// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/productWishlistApi.dart';
import 'package:shiftsoft/resources/productApi.dart';

// models
import 'package:shiftsoft/models/mproductwishlist.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductWishlistList extends StatefulWidget{
    createState() {
      return ProductWishlistListState();
    }
}

class ProductWishlistListState extends State<ProductWishlistList>{
  List<Productwishlist> _productWishlist = [];
  bool productWishlistLoading = true;
  Widget appBarTitle = Text("Wishlist",style: TextStyle(color: Colors.white));
  Icon icon = Icon(
    Icons.search,
    color: Colors.white,
  );
  final globalKey = GlobalKey<ScaffoldState>();
  final TextEditingController _controller = TextEditingController();
  bool _isSearching;
  String _searchText = "";
  List<Productwishlist> searchresult = [];

  ProductWishlistState() {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = "";
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }
  
  Map<String, String> message=new Map();
  List<String> messageList = ["buy","loginFailed"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void initState(){
    initProductWishlistList();
    _isSearching = false;
  }

  initProductWishlistList() async {
    setState(() {
      productWishlistLoading = true;
    });
    
    _productWishlist = await productWishlistApi.getProductWishlistList(context);
    if(_productWishlist == null){
      _productWishlist = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message["loginFailed"]),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      _productWishlist = _productWishlist;
      productWishlistLoading = false;
    });
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: appBarTitle,        
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
        actions: <Widget>[
          IconButton(
            icon: icon,
            onPressed: (){
              setState(() {
                if (this.icon.icon == Icons.search) {
                  this.icon = Icon(
                    Icons.close,
                    color: Colors.white,
                  );
                  this.appBarTitle = Container(
                    color: Colors.white,
                    child: TextField(
                      controller: _controller,
                      style: TextStyle(
                        color: Colors.black,
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search, color: Colors.black),
                        hintText: "Search...",
                        hintStyle: TextStyle(color: Colors.black)),
                        onChanged: searchOperation,
                    ),
                  );
                  _handleSearchStart();
                } else {
                  _handleSearchEnd();
                }
              });
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productWishlistLoading ?  
        CustomScrollView(
          slivers: <Widget>[
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 0,
                childAspectRatio: 0.435
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.only(top: 0,right: 0.5),
                    alignment: Alignment.center,
                    child: ProductCard(
                      mHeight: 500,
                      mWidth: 500,
                      url: "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwis2-bVzfHgAhUyg-YKHWa_AuYQjRx6BAgBEAU&url=https%3A%2F%2Fchieftainfabrics.com%2Franges%2Fcasco-smokey-grey%2F&psig=AOvVaw2F3zkCNvOtv2lFocCv4KBB&ust=1552102079437212",
                      imageHeight: 200,
                      imageSize: 200,
                      content: Column(
                        children: <Widget>[
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              margin: EdgeInsets.only(top: 20),
                              width: 35,
                              height: 35,
                              color: Colors.grey[200],
                            ),
                          ),
                          Shimmer.fromColors(
                            baseColor: Colors.grey[200],
                            highlightColor: Colors.grey[350],
                            period: Duration(milliseconds: 800),
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              width: 100,
                              height: 35,
                              color: Colors.grey[200],
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.location_on),
                              Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: Container(
                                  width: 70,
                                  height: 20,
                                  color: Colors.grey[200],
                                ),
                              ),
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              IconButton(
                                icon: Icon(Icons.delete, color: Colors.black,),
                              ),
                              Shimmer.fromColors(
                                baseColor: Colors.grey[200],
                                highlightColor: Colors.grey[350],
                                period: Duration(milliseconds: 800),
                                child: RaisedButton(
                                  child: Text(message['buy']),
                                  onPressed: null,
                                  color: Colors.grey[200],
                              ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  );
                },
                childCount: 2,
              ),
            )   
          ],
        )
        :
        CustomScrollView(
          slivers: <Widget>[
            _productWishlist.length != 0 ?
            SliverGrid(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 0,
                childAspectRatio: 0.475
              ),
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return Container(
                    key: Key("productDetail$index"),
                    margin: EdgeInsets.only(top: 0,right: 0.5),
                    alignment: Alignment.center,
                    child: InkWell(
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => ProductDetail(mode: 1, id: searchresult.length != 0 ? searchresult[index].product.id : _productWishlist[index].product.id, model: searchresult.length != 0 ? searchresult[index].product : _productWishlist[index].product,)));
                      },
                      child: ProductCard(
                        mHeight: 500,
                        mWidth: 500,
                        url: CompanyAsset("product", searchresult.length != 0 ? searchresult[index].product.pic : _productWishlist[index].product.pic),
                        imageHeight: 200,
                        imageWidth: 200,
                        content: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                            searchresult.length != 0 ? searchresult[index].product.name: _productWishlist[index].product.name,
                              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                            Text(
                              searchresult.length != 0 ? numberFormat(searchresult[index].product.price, config.currency) : numberFormat(_productWishlist[index].product.price, config.currency),
                              style:TextStyle(fontSize: 15,color: Colors.grey),
                            ),
                            Text(
                              "Sumbawa Shop"
                            ),
                            Row(
                              children: <Widget>[
                                Icon(Icons.location_on),
                                Text("Surabaya"),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                IconButton(
                                  icon: Icon(Icons.delete,size: 20,),
                                  onPressed: () async {
                                    final messaged = await productApi.removeWishlist(context, searchresult.length != 0 ? searchresult[index].id : _productWishlist[index].id);
                                    if(messaged == null){
                                      showDialog(
                                        context: context,
                                        barrierDismissible: false,
                                        builder: (context){
                                          return AlertDialog(
                                            title: Text("Alert"),
                                            content: Text(message["loginFailed"]),
                                            actions: <Widget>[
                                              FlatButton(
                                                key: Key("ok"),
                                                child: Text("OK"),
                                                onPressed: (){
                                                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                                },
                                              )
                                            ],
                                          );
                                        }
                                      );
                                    }
                                    showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (context){
                                        return AlertDialog(
                                          title: Text("Alert"),
                                          content: Text(messaged),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text("OK"),
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                                initProductWishlistList();
                                                // Navigator.of(context).pop();
                                              },
                                            )
                                          ],
                                        );
                                      }
                                    );
                                  },
                                ),
                                Button(
                                  child: Text(message["buy"]),
                                  onTap: () async {
                                    final messaged = await productApi.addCart(context, searchresult.length != 0 ? searchresult[index].product.id : _productWishlist[index].product.id, 1926, 1, "a");
                                    if(messaged == null){
                                      showDialog(
                                        context: context,
                                        barrierDismissible: false,
                                        builder: (context){
                                          return AlertDialog(
                                            title: Text("Alert"),
                                            content: Text(message["loginFailed"]),
                                            actions: <Widget>[
                                              FlatButton(
                                                key: Key("ok"),
                                                child: Text("OK"),
                                                onPressed: (){
                                                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                                },
                                              )
                                            ],
                                          );
                                        }
                                      );
                                    }
                                    showDialog(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (context){
                                        return AlertDialog(
                                          title: Text("Alert"),
                                          content: Text(messaged),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text("OK"),
                                              onPressed: (){
                                                Navigator.of(context).pop();
                                                Navigator.pop(context, true);
                                              },
                                            )
                                          ],
                                        );
                                      }
                                    );
                                  },
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                childCount: searchresult.length != 0 || _controller.text.isNotEmpty ? searchresult.length : _productWishlist.length,
              ),
            )
            :
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) =>
                Container(
                  child: PlaceholderList(type: "wishlist"),
                ),
                childCount: 1,
              )
            ),
          ],
        ),
      ),
    );
  }
  Future<Null> _refresh() async {
    await initProductWishlistList();
    return null;
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  void _handleSearchEnd() {
    setState(() {
      this.icon = Icon(
        Icons.search,
        color: Colors.white,
      );
      this.appBarTitle = Text(
        "Wishlist",
        style: TextStyle(color: Colors.white),
      );
      _isSearching = false;
      _controller.clear();
      searchresult.clear();
    });
  }

  void searchOperation(String searchText) {
    String data;
    searchresult.clear();
    if (_isSearching != null) {
      for (int i = 0; i < _productWishlist.length; i++) {
        data = _productWishlist[i].product.name;
        if (data.toLowerCase().contains(searchText.toLowerCase())) {
          searchresult.add(_productWishlist[i]);
        }
      }
    }
  }
}