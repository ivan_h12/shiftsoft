// packages
import 'package:flutter/material.dart';

// screens

// widgets
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/userApi.dart';
import 'package:shiftsoft/tools/functions.dart';

//models
import 'package:shiftsoft/settings/configuration.dart';

class ChangePassword extends StatefulWidget {
  String title;
  ChangePassword({Key key, this.title}) : super(key:key);
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  // final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final formKey = GlobalKey<FormState>();
  final oldPass = TextEditingController();
  final newPass = TextEditingController();
  final confirmPass = TextEditingController();

  final FocusNode oldPassFocus = FocusNode();
  final FocusNode newPassFocus = FocusNode();
  final FocusNode confirmPassFocus = FocusNode();

  String _oldPass, _newPass, _confirmPass;
  Map<String, String> message = Map();
  List<String> messageList = ["oldPassword","newPassword","confirmNewPassword", "change"];

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();
   }

  @override
  void dispose(){
    oldPass.dispose();
    newPass.dispose();
    confirmPass.dispose();
    super.dispose();
  }

  void _submit() async {
    final form = formKey.currentState;
    if(form.validate()){
      final message = await userApi.changePassword(context, 2915,oldPass.text, newPass.text);
      if(message != ""){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Gagal"),
              content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: (){
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      } else {
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Sukses"),
              // content: Text(message),
              actions: <Widget>[
                FlatButton(
                  child: Text("OK"),
                  onPressed: (){
                    Navigator.of(context).pop();
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          }
        );
      }
      
    }
  }

  @override   
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title, style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5.0),
        child: Form(
          key: formKey,
          child: ListView(
            children: <Widget>[
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15.0),
                child: Text("Old Password", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(right: 7.0, left: 7.0),
                child: TextFields(
                  obscureText: true,
                  controller: oldPass,
                  focusNode: oldPassFocus,
                  maxLines: 1,
                  textInputAction: TextInputAction.next,
                  hintText: "Old Password",
                  validator: (val) {
                    if (val.length < 1) {
                      return 'Please Input your Old Password';
                    }
                  },
                  onSubmitted: (value) {
                    newPassFocus.unfocus();
                    FocusScope.of(context).requestFocus(newPassFocus);
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: Text("New Password", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(right: 7.0, left: 7.0),
                child: TextFields(
                  obscureText: true,
                  controller: newPass,
                  focusNode: newPassFocus,
                  maxLines: 1,
                  textInputAction: TextInputAction.next,
                  hintText: "New Password",
                  validator: (val) {
                    if (val.length < 1) {
                      return 'Please Input your New Password';
                    }
                  },
                  onSubmitted: (value) {
                    confirmPassFocus.unfocus();
                    FocusScope.of(context).requestFocus(confirmPassFocus);
                  },
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: Text("Confirm New Password", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(right: 7.0, left: 7.0),
                child: TextFields(
                  obscureText: true,
                  controller: confirmPass,
                  focusNode: confirmPassFocus,
                  maxLines: 1,
                  textInputAction: TextInputAction.done,
                  hintText: "Confirm New Password",
                  validator: (val) {
                    if (val != newPass.text) {
                      return 'Password not match';
                    }
                  },
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: Button(
                  child: Text(message["change"],style: TextStyle( color: config.primaryColor),),
                  fill: false,
                  rounded: true,
                  backgroundColor: config.primaryColor,
                  onTap: (){
                    _submit();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}