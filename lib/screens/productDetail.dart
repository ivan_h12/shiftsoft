// packages
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productDetailCommentList.dart';
import 'package:shiftsoft/screens/productDetailReviewList.dart';
import 'package:shiftsoft/screens/login.dart';

// widgets
import 'package:shiftsoft/widgets/itemDetail.dart';
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/productApi.dart';
import 'package:shiftsoft/resources/productCommentApi.dart';
import 'package:shiftsoft/resources/productWishlistApi.dart';

//models
import 'package:shiftsoft/models/mproductcomment.dart';
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mproductreview.dart';
import 'package:shiftsoft/models/mproductwishlist.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/tools/functions.dart';

class ProductDetail extends StatefulWidget{
  int mode, id;
  Product model;
  
  ProductDetail({
    Key key,
    this.mode,
    this.id,
    this.model
  }) : super(key:key);

  createState(){
    return ProductDetailState();
  }
}

class ProductDetailState extends State<ProductDetail>{
  Product productDetail;
  Productwishlist productwishlist;
  List<ProductReview> _productReviewList = [];
  List<ProductComment> _productCommentList = [];
  User user;
  bool wishList = false, cart= false;
  bool checkWishListLoading = true;
  bool checkCartLoading = true;
  bool productDetailLoading = true;
  bool descriptionReadMore = false;

  Map<String, String> message=new Map();
  List<String> messageList = ["reviews","comments","courier","productDescription","readMore","mostHelpfulReview","seeMoreReviews","lastDiscussion","buyNow","addToCart","failed","alreadyInCart","loginFailed"];
  void initState(){
    super.initState();
    user = User(id:1926);
    if(widget.mode == 1) {
      initProductDetail();
    } else if (widget.mode == 3) {
      productDetail = widget.model;
      setState(() {
        productDetailLoading = false;
        productDetail = productDetail;
      });
    }
    
    initCheckWishList();
    initCheckCart();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  initCheckWishList() async {
    wishList = await productApi.checkWishList(context, widget.id, user.id);
    productwishlist = await productWishlistApi.getProductWishlist(context, widget.id, user.id);
    if(wishList == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['failedLogin']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    } else if(wishList){
      if(productwishlist == null){
          showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message['failedLogin']),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
    }

    setState(() {
      wishList = wishList;
      productwishlist = productwishlist;
      checkWishListLoading = false;
    });
  }

  initCheckCart() async {
    cart = await productApi.checkCart(context, widget.id, user.id);
    if(cart == null){
      cart = false;
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['failedLogin']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    setState(() {
      cart = cart;
      checkCartLoading = false;
    });
  }

  initProductDetail() async {
    setState(() {
      productDetailLoading = true;
    });

    productDetail = await productApi.getProduct(context, widget.id);
    productwishlist = await productWishlistApi.getProductWishlist(context, widget.id, user.id);
    _productReviewList = await productApi.getProductReviewList(context, user.id, widget.id);
    
    if(productDetail == null){
      productDetail = null;
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['failedLogin']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }
    if(wishList){
      if(productwishlist == null){
        showDialog(
          context: context,
          barrierDismissible: false,
          builder: (context){
            return AlertDialog(
              title: Text("Alert"),
              content: Text(message['failedLogin']),
              actions: <Widget>[
                FlatButton(
                  key: Key("ok"),
                  child: Text("OK"),
                  onPressed: (){
                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                  },
                )
              ],
            );
          }
        );
      }
    }

    if(_productReviewList == null){
      _productReviewList = [];
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text(message['loginFailed']),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      productDetail = productDetail;
      _productReviewList = _productReviewList;
      productwishlist = productwishlist;
      productDetailLoading = false;
    });

    _productCommentList = await productCommentApi.getProductCommentList(context, widget.id);
    if(_productCommentList == null){
      print("product comment list null");
    }
    setState(() {
      _productCommentList = _productCommentList;
    });
  }
  
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      body: RefreshIndicator(
        onRefresh: _refresh,
        child: productDetailLoading ? 
        ListView.builder(
          key: Key("product_scroll"),
          itemCount: 1,
          itemBuilder: (BuildContext context, int index){
            return Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: Container(height:200, width: 400, color: Colors.grey[200]),
                ),
                Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: ListTile(
                    title: Container(width: 50, height: 20, color: Colors.grey[200]),
                    subtitle: Container(width: 30, height: 20, color: Colors.grey[200]), 
                  ),
                ),
                Divider(),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(width: 100, height: 50, color: Colors.grey[200])
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(width: 100, height: 50, color: Colors.grey[200])
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[200],
                        highlightColor: Colors.grey[350],
                        period: Duration(milliseconds: 800),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(width: 100, height: 50, color: Colors.grey[200])
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Divider(),
                Shimmer.fromColors(
                  baseColor: Colors.grey[200],
                  highlightColor: Colors.grey[350],
                  period: Duration(milliseconds: 800),
                  child: Container(width: MediaQuery.of(context).size.width, height: 500, color: Colors.grey[200])
                ),
              ],
            );
          },
        )
        :
        ItemDetail(
          title: productDetail.name,
          module: "product",
          appBar: true,
          appBarActions: [
            IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: (){
                Navigator.pushNamed(context, "productCartList");
              },
            ),
          ],
          imageList: productDetail.pic.split(","),
          content: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              ListTile(
                title: Text(productDetail.name, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
                subtitle: Text(numberFormat(productDetail.price, config.currency)),
                trailing: checkWishListLoading ?  
                CircularProgressIndicator()
                :
                IconButton(
                  icon: wishList ? Icon(Icons.favorite, color: Colors.red,):Icon(Icons.favorite_border, color: Colors.black,),
                  onPressed: () async {
                    if(!wishList){
                      await productApi.addWishlist(context, productDetail.id, user.id);
                    } else {
                      await productApi.removeWishlist(context, productwishlist.id);
                      print(productwishlist.id);
                    }
                    // await productApi.addWishlist(productDetail.id, user.id);
                    initCheckWishList();
                  },
                ),
              ),
              Divider(),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Icon(Icons.star,size: 18,),
                            Icon(Icons.star,size: 18),
                            Icon(Icons.star,size: 18),
                            Icon(Icons.star,size: 18),
                            Icon(Icons.star,size: 18),
                          ],
                        ),
                        Text(_productReviewList.length.toString()+" " + message['reviews'])
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: FlatButton(
                      key: Key("comment"),
                      onPressed: () async {
                       await Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=> ProductDetailCommentList(title: productDetail.name, id: widget.id, price: productDetail.price, )),
                        );
                        initProductDetail();
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.question_answer,size: 20,),
                          Text(_productCommentList.length.toString() + " " + message['comments'])
                        ],
                      ),
                    )
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.tram,size: 20,),
                        Text("3 "+message['courier'])
                      ],
                    ),
                  )
                ],
              ),
              Divider(),
              ListTile(
                title: Text(message['productDescription'], style: TextStyle(fontWeight: FontWeight.bold)),
                isThreeLine: true,
                subtitle: Text(
                  productDetail.description+"\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                  maxLines: descriptionReadMore ? 1000:10,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              ListTile(
                onTap: () {
                  setState(() {
                    descriptionReadMore = !descriptionReadMore;
                  });
                },
                trailing: ButtonBar(
                  children:<Widget>[
                    Text(message['readMore']),
                    Icon(Icons.keyboard_arrow_right),
                  ],
                ),
              ),
              Divider(),
              Text(message['mostHelpfulReview'],style:TextStyle(fontSize: 20),textAlign: TextAlign.left,),
              Container(
                  margin: EdgeInsets.only(top: 5),
                  height: MediaQuery.of(context).size.width/16*9,
                  child: Swiper(
                  itemBuilder: (BuildContext context,int index){
                    return Column(
                      children: <Widget>[
                        ListTile(
                          leading: ImageBox("https://static.thenounproject.com/png/194977-200.png",50),
                          title: Text("Madeeeeeee"),
                          subtitle: Text("23 Dec 2018, 20:36 WIB ", style: TextStyle(fontSize: 15.0,color: Colors.grey),),
                        ),
                        Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of", style: TextStyle(fontSize: 15.0,color: Colors.grey),),
                      ],
                    );
                  },
                  itemCount: 3,
                  pagination: SwiperPagination(
                      builder: new SwiperCustomPagination(builder:
                        (BuildContext context, SwiperPluginConfig config) {
                          return DotSwiperPaginationBuilder(
                            color: Colors.white,
                            activeColor: Colors.grey[300],
                            size: 10.0,
                            activeSize: 12.0
                          ).build(context, config);
                        }
                      )
                    ),
                ),
              ),
              Divider(),
              ListTile(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductDetailReviewList(id: widget.id, model: productDetail,)));
                },
                trailing: ButtonBar(
                  key: Key("semuaUlasan"),
                  children:<Widget>[
                    Text(message['seeMoreReviews']+" ("+_productReviewList.length.toString()+")" ),
                    IconButton(
                      icon: Icon(Icons.keyboard_arrow_right),
                    ),
                  ],
                ),
              ),
              Divider(),
              Column(
                children: <Widget>[
                  Text(message['lastDiscussion']),
                  ListTile(
                    leading: ImageBox("https://static.thenounproject.com/png/194977-200.png",50),
                    title: Text("Madeeeeeee"),
                    subtitle: Text("23 Dec 2018, 20:36 WIB ", style: TextStyle(fontSize: 15.0,color: Colors.grey),),
                  ),
                  Text("Ready yang hitam Bosque", textAlign: TextAlign.start,),
                  Divider(),
                  ListTile(
                    onTap: () {
                      Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=> ProductDetailCommentList(title: productDetail.name, id: widget.id, price: productDetail.price, )),
                      );
                    },
                    trailing: ButtonBar(
                      children:<Widget>[
                        Text(message['readMore']),
                        IconButton(
                          icon: Icon(Icons.keyboard_arrow_right),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: !productDetailLoading ?
      Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(Icons.comment),
                onPressed: (){},
              ),
            ),
            Expanded(
              flex: 4,
              child: Button(
                child: Text(message['buyNow']),
                rounded: false,
                onTap: !cart ? () async {
                  final messaged = await productApi.addCart(context, productDetail.id, user.id, 1, "a");
                  if (messaged == null){
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Alert"),
                          content: Text(message['loginFailed']),
                          actions: <Widget>[
                            FlatButton(
                              key: Key("ok"),
                              child: Text("OK"),
                              onPressed: (){
                                return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                              },
                            )
                          ],
                        );
                      }
                    );
                  }else {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Alert"),
                          content: Text(messaged),
                          actions: <Widget>[
                            FlatButton(
                              child: Text("OK"),
                              onPressed: (){
                                Navigator.of(context).pop();
                                initCheckCart();
                              },
                            )
                          ],
                        );
                      }
                    );            
                  }
                }
                :
                ()
                {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Alert"),
                          content: Text(message['alreadyInCart']),
                          actions: <Widget>[
                            FlatButton(
                              child: Text("OK"),
                              onPressed: (){
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        );
                      }
                    );
                },
              ),
            ),
            Expanded(
              flex: 5,
              child: checkCartLoading ? 
              Text(message['failed'])
              : 
              Button(
                child: Text(message['addToCart'], style: TextStyle(color: config.primaryTextColor)),
                backgroundColor: config.primaryColor,
                onTap: !cart ? () async {
                  final messaged = await productApi.addCart(context, productDetail.id, user.id, 1, "a");
                  if(messaged == null){
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Alert"),
                          content: Text(message['failedLogin']),
                          actions: <Widget>[
                            FlatButton(
                              key: Key("ok"),
                              child: Text("OK"),
                              onPressed: (){
                                return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                              },
                            )
                          ],
                        );
                      }
                    );
                  }else {
                    showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Alert"),
                          content: Text(messaged),
                          actions: <Widget>[
                            FlatButton(
                              child: Text("OK"),
                              onPressed: (){
                                Navigator.of(context).pop();
                                initCheckCart();
                              },
                            )
                          ],
                        );
                      }
                    );
                  }            
                }
                :
                ()
                {
                  showDialog(
                      context: context,
                      barrierDismissible: false,
                      builder: (context){
                        return AlertDialog(
                          title: Text("Alert"),
                          content: Text(message['alreadyInCart']),
                          actions: <Widget>[
                            FlatButton(
                              child: Text("OK"),
                              onPressed: (){
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        );
                      }
                    );
                } 
              )
            ),
          ],
        ),
      )
      : null,
    );
  }
  Future<Null> _refresh() async {
    await initProductDetail();
    return null;
  }
}