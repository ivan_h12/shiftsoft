// packages
import 'package:flutter/material.dart';
import 'package:shiftsoft/resources/userApi.dart';
import 'package:shiftsoft/tools/functions.dart';

// screens

// widgets
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/textField.dart';


// resources

// models
import 'package:shiftsoft/settings/configuration.dart';

class ForgetPassword extends StatefulWidget{
  createState(){
    return ForgetPasswordState();
  }
}
class ForgetPasswordState extends State<ForgetPassword>{
  final _formKey = GlobalKey<FormState>();
  final emailController = TextEditingController();
  bool forgetPasswordLoading = false;

  Map<String, String> message=new Map();
  List<String> messageList = ["forgotPassword","emailAddress","continue"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }
  
  Widget build(context){
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message["forgotPassword"], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: TextFields(
                  type: 1,
                  controller: emailController,
                  textInputAction: TextInputAction.done,                        
                  hintText: message["emailAddress"],
                  labelText: message["emailAddress"],
                  prefixIcon: IconButton(
                    icon: Icon(Icons.mail),
                    onPressed: (){},
                  ),
                  validator: (value){
                    if(!value.contains("@")){
                      return "Input must be Email";
                    }
                  },
                )
              ),
              forgetPasswordLoading ?
              Padding(
                padding: EdgeInsets.only(top:15, bottom:15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                  ]
                )
              )
              :
              Container(
                margin: EdgeInsets.only(top: 20),
                width: MediaQuery.of(context).size.width,
                child: Button(
                  backgroundColor: config.primaryColor,
                  child: Text(message["continue"], style: TextStyle(color: config.primaryTextColor),),
                  rounded: true,
                  onTap: () async {
                    if(_formKey.currentState.validate()){
                      setState(() {
                        forgetPasswordLoading = true;
                      });
                      final message = await userApi.forgetPassword(context, emailController.text);
                      setState(() {
                        forgetPasswordLoading = false;
                      });
                      showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context){
                          return AlertDialog(
                            title: Text("Alert"),
                            content: Text(message),
                            actions: <Widget>[
                              FlatButton(
                                child: Text("OK"),
                                onPressed: (){
                                  Navigator.of(context).pop();            
                                  Navigator.of(context).pop();
                                },
                              )
                            ],
                          );
                        }
                      );
                    }
                  }
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}