// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/reviewDetailInvoice.dart';

// widgets
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/productApi.dart';

//models
import 'package:shiftsoft/models/mproduct.dart';
import 'package:shiftsoft/models/mproductreview.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductReviewList extends StatefulWidget{
  int id;
  Product model;
  

  createState(){
    return ProductReviewListState();
  }
  ProductReviewList({Key key, this.id, this.model}) : super(key:key);
}

class ProductReviewListState extends State<ProductReviewList>{
  int length = 5;

  // List <String> listTabBar = [];
  
 List <String> listTabBar = new List() ;

  Map<String, String> message=new Map();
  List<String> messageList = ["reviews","all","loginFailed","waitForReview","myReview"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
    // listTabBar[0] = message['waitForReview'];
    // listTabBar[1] = message['myReview'];
    listTabBar.add(message['waitForReview']);
    listTabBar.add(message['myReview']);
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);

    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(message['reviews'], style: TextStyle(color: config.topBarTextColor),),
          backgroundColor: config.topBarBackgroundColor,
          iconTheme: IconThemeData(
            color: config.topBarIconColor
          ),
          bottom: TabBar(
            tabs: List<Widget>.generate(2, (int index){
                return Tab(child: Text(listTabBar[index], key: Key("reviewtab$index"),));
              },
            ),
          ),
        ),
        body: TabBarView(
          children: List<Widget>.generate(2, (int index){
            if ( index == 0) {
              return ListView.builder(
                itemCount: 1,
                itemBuilder: (BuildContext context, int index){
                  return PlaceholderList(paddingTop:100, type:"productreview");
                },
              );
            } else {
              return Stack(
                children: <Widget>[
                  ListView.builder(
                    itemCount: length !=0 ? length : 1,
                    itemBuilder: (BuildContext context, int index){
                      return length !=0 ? Column(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.all(10),
                            child: Card(
                              child: Column(
                                children: <Widget>[
                                  ListTile(
                                    title: Text("24 Jan", style: TextStyle(color: Colors.grey),),
                                    subtitle: Text("INV/213/35dfs/fdfaf", style: TextStyle(color: Colors.black),),
                                  ),
                                  Divider(),
                                  InkWell(
                                    key: Key("reviewdetail$index"),
                                    onTap: (){
                                      Navigator.push(context, MaterialPageRoute(builder: (context) => ReviewDetailInvoice(invoice: "INV/213/35dfs/fdfaf", date: "24 Jan")));
                                    },
                                    child: ButtonBar(
                                      children: <Widget>[
                                        FlatButton(
                                          disabledTextColor: Colors.green,
                                          child: Row(
                                            children: <Widget>[
                                              Text(message['all'],style: TextStyle(),),
                                              Icon(Icons.arrow_forward_ios),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      )
                      :
                      PlaceholderList(type: "productreview",);
                    },
                  ),
                  Positioned(
                    bottom: 10,
                    left: MediaQuery.of(context).size.width/2.5,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(25)),
                      ),
                      color: Colors.white54,
                      onPressed: (){

                      },
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.filter_list),
                          Text("Filter"),
                        ],
                      ),
                    ),
                  )
                ],
              );
            }
          })
        ),
      ),
    );
  }  
}
