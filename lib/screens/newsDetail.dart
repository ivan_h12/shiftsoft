// packages
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:share/share.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:intl/intl.dart';

// screens
import './commentList.dart';
import './login.dart';

// widgets
import '../widgets/itemDetail.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/profileBox.dart';
import 'package:shiftsoft/widgets/button.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/newsApi.dart';

// models
import 'package:shiftsoft/models/mnews.dart';
import 'package:shiftsoft/models/muser.dart';
import 'package:shiftsoft/models/mcomment.dart';
import 'package:shiftsoft/models/mresult.dart';
import 'package:shiftsoft/settings/configuration.dart';

class NewsDetail extends StatefulWidget{
  News model;
  int mode, id;
  
  NewsDetail({Key key, this.mode, this.id, this.model}) : super(key:key);

  createState(){
    return NewsDetailState();
  }
}

class NewsDetailState extends State<NewsDetail>{
  List<Comment> commentList = [];
  News newsDetail;
  User user;
  bool refreshPage = false;
  bool newsDetailLoading = true;
  bool commentListLoading = true;

  Map<String, String> message = new Map();
  List<String> messageList = ["share","writeAComment"];
  
  void initState() {
    super.initState();
    user = User(id:1926);
    
    if (widget.mode == 1) {
      initNewsDetail();
    } else if (widget.mode == 3) {
      newsDetail = widget.model;
      setState(() {
        newsDetailLoading = false;
        newsDetail = newsDetail;
      });
    }

    initCommentList();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      message[item] = messages(context, item.split(","));
    }).toList();

    if (widget.mode == 3) {
      // check viewer
      createNotificationRecipient();
    }
    initCommentList();
   }

   createNotificationRecipient() async {
     Result result = await newsApi.createRecipient(context, widget.id.toString(), user.id.toString());
     print(result.success);
     print(result.message);
     if (result.success == 1) {
       newsDetail.recipients += 1;
       setState(() {
        refreshPage = true;
        newsDetail = newsDetail;
       });
     }
   }

  initNewsDetail() async {
    setState(() {
      newsDetailLoading = true;
    });
    
    newsDetail = await newsApi.getNews(context, widget.id);
    // check viewer
    createNotificationRecipient();

    if(newsDetail == null){
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context){
          return AlertDialog(
            title: Text("Alert"),
            content: Text("Login Gagal"),
            actions: <Widget>[
              FlatButton(
                key: Key("ok"),
                child: Text("OK"),
                onPressed: (){
                  return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                },
              )
            ],
          );
        }
      );
    }

    setState(() {
      newsDetail = newsDetail;
      newsDetailLoading = false;
    });
  }

  
  initCommentList() async{
    setState((){
      commentListLoading = true;
    });

    commentList = await newsApi.getCommentList(context, widget.id);

    setState(() {
      commentList = commentList;
      commentListLoading = false;    
    });
  }

  convertTime(int index){
    final dateNow = DateTime.now();
    final difference = dateNow.difference(commentList[index].createdAt).inMinutes;
    final time = new DateTime.now().subtract(new Duration(minutes: difference));
    return timeago.format(time).toString();
  }

  Widget printComment1(){
    Widget tempWidget = 
      commentList.length > 0 ?
      Column(
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 15.0),
            leading: ProfileBox(CompanyAsset("avatar", commentList[0].user.pic), 75.0, commentList[0].user.name,
              Row(
                children: <Widget>[
                  Text(convertTime(0), style: TextStyle(fontSize: 15.0)),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 35.0, right: 20.0, bottom: 10.0),
            alignment: Alignment(-1.0, -1.0),                            
            child: Text(commentList[0].content),
          ),
        ],  
    )
    :
    Container();
    return tempWidget;
  }

  Widget printComment2(){
    Widget tempWidget = 
      commentList.length > 1 ?
      Column(
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 15.0),
            leading: ProfileBox(CompanyAsset("avatar", commentList[1].user.pic), 75.0, commentList[1].user.name,
              Row(
                children: <Widget>[
                  Text(convertTime(1), style: TextStyle(fontSize: 15.0)),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 35.0, right: 20.0, bottom: 10.0),
            alignment: Alignment(-1.0, -1.0),                            
            child: Text(commentList[1].content),
          ),
        ],  
    )
    :
    Container();
    return tempWidget;
  }
  Widget printComment3(){
    Widget tempWidget = 
      commentList.length > 2 ?
      Column(
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.only(top: 25.0, right: 25.0, left: 25.0, bottom: 15.0),
            leading: ProfileBox(CompanyAsset("avatar", commentList[2].user.pic), 75.0, commentList[2].user.name,
              Row(
                children: <Widget>[
                  Text(convertTime(2), style: TextStyle(fontSize: 15.0)),
                ],
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 35.0, right: 20.0, bottom: 10.0),
            alignment: Alignment(-1.0, -1.0),                            
            child: Text(commentList[2].content),
          ),
        ],  
    )
    :
    Container();
    return tempWidget;
  }
  

  @override
  Widget build(BuildContext context){
    Configuration config = Configuration.of(context);

    return WillPopScope(
      onWillPop: willPopScope,
        child:Scaffold(
        body: Container(
          child: newsDetailLoading ?
          Shimmer.fromColors(
            baseColor: Colors.grey[200],
            highlightColor: Colors.grey[350],
            period: Duration(milliseconds: 800),
            child: Container(
              margin: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(height:50, width: 100, color: Colors.grey[200]),
                  Padding(padding: EdgeInsets.only(top: 20),),
                  Container(height:200, width: 400, color: Colors.grey[200])
                ],
              ),
            )
          )
          :
          Column(
            children: <Widget>[
              Expanded(
                child: SizedBox(
                  height: 1000.0,
                  child: ItemDetail(
                    appBar: true,
                    title: newsDetail.subject,
                    content: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.all(5.0),
                          child: Html(
                            data: ""
                              "<h1>${newsDetail.subject}</h1>"
                            "",
                            onLinkTap: (url) async {
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 5.0),
                          child: Text(DateFormat('E, dd MMM yyyy').format(newsDetail.createdAt)),
                        ),                      
                        Padding(padding: EdgeInsets.only(top:5),),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(left:7),),
                            InkWell(
                              child: Icon(
                                Icons.thumb_up,
                                color: Colors.grey,
                                size: 17
                              ),
                              onTap: () async {
                                Result result = await newsApi.createLike(context, newsDetail.id.toString(), user.id.toString());
                                if (result.success == 1) {
                                  newsDetail.likes += 1;
                                  
                                  setState(() {
                                    refreshPage = true;
                                    newsDetail = newsDetail;
                                  });
                                }
                              }
                            ),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                            Text(newsDetail.likes.toString()),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
                            Icon(
                              Icons.remove_red_eye,
                              color: Colors.grey,
                              size: 17
                            ),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                            Text(newsDetail.recipients.toString()),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 4),),
                            Icon(
                              Icons.comment,
                              color: Colors.grey,
                              size: 17
                            ),
                            Padding(padding: EdgeInsets.symmetric(horizontal: 2),),
                            Text(newsDetail.comments.toString()),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.all(10.0),
                          child: Html(
                            data: ""                            
                              "${newsDetail.content}"
                            "",
                            onLinkTap: (url) async {
                              if (await canLaunch(url)) {
                                await launch(url);
                              } else {
                                throw 'Could not launch $url';
                              }
                            },
                          ),
                        ),
                        commentListLoading ?
                        Shimmer.fromColors(
                          baseColor: Colors.grey[200],
                          highlightColor: Colors.grey[350],
                          period: Duration(milliseconds: 800),
                          child: Container(
                            margin: EdgeInsets.all(10.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(height:50, width: 100, color: Colors.grey[200]),
                                Padding(padding: EdgeInsets.only(top: 20),),
                                Container(height:200, width: 400, color: Colors.grey[200])
                              ],
                            ),
                          )
                        )
                        :
                        Divider(),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Text("Comments", style: TextStyle(fontWeight: FontWeight.bold))
                        ),

                        commentList.length == 0 ?
                        PlaceholderList(type: "productcomment", paddingTop: 50, paddingBottom: 50)
                        :
                        Container(),

                        printComment1(),
                        printComment2(),
                        printComment3(),
                        
                        Container(
                          alignment: Alignment.center,
                          child: Container(
                            width: MediaQuery.of(context).size.width - 10,
                            child: Button(
                              child: Text(message["share"], style: TextStyle(color: config.secondaryTextColor),),
                              backgroundColor: config.secondaryColor,
                              onTap: () {
                                Share.share(
                                  "${newsDetail.subject} \n\n"
                                  "${newsDetail.content}",
                                );
                              },
                              rounded: true,
                            ),
                          )
                        ),
                        Container(
                          alignment: Alignment.center,
                          child: Container(
                            width: MediaQuery.of(context).size.width - 10,
                            child: Button(
                              key: Key("comment${newsDetail.id}"),
                              child: Text(message["writeAComment"], style: TextStyle(color: config.primaryTextColor),),
                              backgroundColor: config.primaryColor,
                              splashColor: Colors.blueGrey,
                              onTap:  () async {
                                bool refresh = await Navigator.push(context, MaterialPageRoute(builder: (context) => CommentList(mode: 1, id: newsDetail.id, model: newsDetail)));
                                if(refresh ?? false) {
                                  initCommentList();
                                }
                              },
                              rounded: true,
                            ),
                          )
                        )
                      ],
                    ),
                    imageList: newsDetail.pic.split(","),
                    module: "news",
                  ),
                ),
              ),
              
            ],
          ),
        )
      )
    );
  }

  Future<bool> willPopScope() {
    Navigator.pop(context, refreshPage);
  }
}
