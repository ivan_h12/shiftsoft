// packages
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'dart:io';
import 'dart:async';
import 'package:image_picker/image_picker.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

// screens
import './login.dart';

// widgets
import 'package:shiftsoft/widgets/textField.dart';
import 'package:shiftsoft/widgets/button.dart';

// resources
import 'package:shiftsoft/resources/subscribeApi.dart';

// models
import 'package:shiftsoft/settings/configuration.dart';

class SubscribeForm extends StatefulWidget { 
  @override
  _SubscribeFormState createState() => _SubscribeFormState();
}

class _SubscribeFormState extends State<SubscribeForm> {
  final _formKey = GlobalKey<FormState>();

  DateTime _date = DateTime.now();
  String monthPeriod;
  List yangDibayar = ["Tagihan 1","Tagihan 2","Tagihan 3"];
  List metodePembayaran = ["Cash", "Transfer", "OVO"];

  List<DropdownMenuItem<String>> _dropDownMenuItemsYangDibayar;
  List<DropdownMenuItem<String>> _dropDownMenuItemsMetodePembayaran;
  String _currentYangDibayar, _currentMetodePembayaran;
  final valueController = TextEditingController();
  final descController = TextEditingController();
  bool submitLoading = false;

  String dateStart, datePaid;
  var tempSubscribe, tempSubscribe1;
  var tempSplit, tempString;

  List<File> temp = [];
  int counter = 0;

  File image;
  picker() async{
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if(img!=null){
      setState(() {
        image = img;
        counter++;
        temp.add(image);
      });
    } 
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showMonthPicker(
      initialDate: _date,
      context: context,
    );

    if(picked != null && picked != _date){
      print(picked);
      setState(() {
        tempSubscribe1 = picked.toString();
        tempSubscribe = tempSubscribe1.split(" ");
        dateStart = tempSubscribe[0];
        monthPeriod = DateFormat("MMMM yyyy").format(DateTime.parse(dateStart)).toString();
        _date = DateTime.now();
      });
    }
  }
  Future<Null> _selectDate2(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: _date,
      firstDate: DateTime(1900),
      lastDate: DateTime(2020),
    );

    if(picked != null && picked != _date){
      setState(() {
        _date = picked;
        tempString = picked.toString();
        tempSplit = tempString.split(" ");
        datePaid = tempSplit[0];
        _date = DateTime.now();
      });
    }
  }

  void dispose(){
    descController.dispose();
    valueController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    monthPeriod = "September 2019";
    _dropDownMenuItemsYangDibayar = getDropDownMenuItemsYangDibayar();
    _dropDownMenuItemsMetodePembayaran = getDropDownMenuItemsMetodePembayaran();
    _currentYangDibayar = _dropDownMenuItemsYangDibayar[0].value;
    _currentMetodePembayaran = _dropDownMenuItemsMetodePembayaran[0].value;
    dateStart = (DateFormat('yyyy-M-dd').format(DateTime.now()).toString());
    datePaid = "2019-01-23";
  }

  List<DropdownMenuItem<String>> getDropDownMenuItemsYangDibayar() {
    List<DropdownMenuItem<String>> items = List();
    for (String da in yangDibayar) {
      items.add(DropdownMenuItem(
          value: da,
          child:Text(da)
      ));
    }
    return items;
  }
  List<DropdownMenuItem<String>> getDropDownMenuItemsMetodePembayaran() {
    List<DropdownMenuItem<String>> items = List();
    for (String da in metodePembayaran) {
      items.add(DropdownMenuItem(
          value: da,
          child:Text(da)
      ));
    }
    return items;
  }

  void changedDropDownItemYangDibayar(String selected) {
    setState(() {
      _currentYangDibayar = selected;
    });
  }
  void changedDropDownItemMetodePembayaran(String selected) {
    setState(() {
      _currentMetodePembayaran = selected;
    });
  }
  String getBase64Image(File img){
    List<int> imagesBytes = img.readAsBytesSync();
    String base64Image = base64Encode(imagesBytes);
    return base64Image;  
  }

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Subscribe Form", style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(5.0),        
        child: Form(
          key: _formKey,
          child: ListView(
            key: Key("subscribeFormScroll"),
            children: <Widget>[
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15),
                child: Text("Periode Bayar", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: InkWell(
                  onTap: (){ _selectDate(context); },
                  key: Key("monthPeriod"),
                  child: TextFields(
                    type: 2,
                    date: "${monthPeriod}",
                    icon: new Icon(Icons.calendar_today),
                  ),
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15),
                child: Text("Tanggal Bayar", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0),
                child: InkWell(
                  onTap: (){ _selectDate2(context); },
                  key: Key("datePaid"),
                  child: TextFields(
                    type: 2,
                    date: "${datePaid}",
                    icon: new Icon(Icons.calendar_today),
                  ),
                ),
              ),
              // Container(
              //   margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 10.0),
              //   child: Text("Pembayaran", style: TextStyle(fontSize: 18.0, color: Colors.grey))
              // ),
              // Container(
              //   margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
              //   child: DropdownButton(
              //     isExpanded: true,
              //     value: _currentYangDibayar,
              //     items: _dropDownMenuItemsYangDibayar,
              //     onChanged: changedDropDownItemYangDibayar,
              //   )
              // ),
              // Container(
              //   margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 10.0),
              //   child: Text("Metode Pembayaran", style: TextStyle(fontSize: 18.0, color: Colors.grey))
              // ),
              // Container(
              //   margin: EdgeInsets.only(left: 7.0, right: 7.0, top: 5.0, bottom: 12.0),
              //   child: DropdownButton(
              //     isExpanded: true,
              //     value: _currentMetodePembayaran,
              //     items: _dropDownMenuItemsMetodePembayaran,
              //     onChanged: changedDropDownItemMetodePembayaran,
              //   )
              // ),
              Container(
              margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 15),
                child: Text("Value", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("textFieldValue"),
                  controller: valueController,
                  keyboardType: TextInputType.number,
                  hintText: "Value",
                ),
              ),
              Container(
                margin:  EdgeInsets.only(left: 7.0, right: 7.0, top: 5),
                child: Text("Description", style: TextStyle(fontSize: 16.0, color: Colors.grey))
              ),
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0),
                child: TextFields(
                  key: Key("textFieldDescription"),
                  controller: descController,
                  maxLines: 3,
                  keyboardType: TextInputType.multiline,
                  hintText: "Description",
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 12),
                height: 75.0,       
                child: ListView.builder(
                  itemCount: counter + 1,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (BuildContext context, int index){
                    return Container(
                      child: Card(
                        child: Container(
                          width: 75,
                          child: index == counter ? 
                          IconButton(
                            key: Key("imagePicker$index"),
                            icon: Icon(Icons.add_a_photo),
                            onPressed: () => picker(),
                          )
                          :
                          Image.file(temp[index]),
                        ),
                      ),
                    );
                  },
                )
              ),
              submitLoading ?
              Padding(
                padding: EdgeInsets.only(top:15, bottom:15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CircularProgressIndicator(),
                  ]
                )
              )
              :
              Container(
                margin: EdgeInsets.only(left: 7.0, right: 7.0, bottom: 10.0),
                width: MediaQuery.of(context).size.width,
                height: 50.0,
                child: Button(
                  key: Key("submit"),
                  rounded: true,
                  child: Text("Submit", style: TextStyle(fontSize: 20.0, color: config.primaryTextColor),),
                  backgroundColor: config.primaryColor,
                  onTap: valueController.text != "" && descController.text != "" ? 
                  ()async{
                    if(_formKey.currentState.validate()){
                      setState(() {
                        submitLoading = true;
                      });
                      final message = await subscribeApi.addSubscribe(context, datePaid, dateStart, "1926", int.parse(valueController.text), descController.text, getBase64Image(image), "picname.jpg");
                      setState(() {
                        submitLoading = false;
                      });
                      if(message == null){
                        showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (context){
                            return AlertDialog(
                              title: Text("Alert"),
                              content: Text("Login Gagal"),
                              actions: <Widget>[
                                FlatButton(
                                  key: Key("ok"),
                                  child: Text("OK"),
                                  onPressed: (){
                                    return Navigator.push(context, MaterialPageRoute(builder: (context) => Login()));
                                  },
                                )
                              ],
                            );
                          }
                        );
                      }
                      showDialog(
                        context: context,
                        barrierDismissible: false,
                        builder: (context){
                          return AlertDialog(
                            title: Text("Alert"),
                            content: Text(message),
                            actions: <Widget>[
                              FlatButton(
                                key: Key("submitOk"),
                                child: Text("OK"),
                                onPressed: (){
                                  Navigator.of(context).pop();            
                                  Navigator.pop(context, true);
                                },
                              )
                            ],
                          );
                        }
                      );
                    }
                  }  
                  :
                  null,
                ),
              )
            ],
          )
        )
        
      ),
    );
  }
}