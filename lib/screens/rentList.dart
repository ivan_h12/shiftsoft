// packages
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:math' as math;

// screens
import 'package:shiftsoft/screens/rentSearch.dart';
import 'package:shiftsoft/screens/rentDetail.dart';
import 'package:shiftsoft/tools/functions.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/photoGalery.dart';

// resources

//models
import 'package:shiftsoft/settings/configuration.dart';

class RentList extends StatefulWidget{
  createState() {
    return RentListState();
  }
}

class RentListState extends State<RentList>{
  List<String> _img=[
    ""
  ];

  List<Widget> bookList=[];
  List<Widget> bookTopList=[];
  final TextEditingController _controller = TextEditingController();
  List<String> _names=[
    "naruto","saske","qaqasi","skra"
  ];

  Map<String, String> message=new Map();
  List<String> messageList = ["loginFailed","addToCart","addToWishlist","rent","available","searchBook","more","topSelling","bestSeller"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  @override
  void initState(){
    initRentList();
  }

  initRentList([String filter =""]) async {
    // setState(() {
    //   productListLoading = true;
    // });
    
    bookList.clear();
    bookTopList.clear();
    for(int i = 0; i < 4; i++){
      
      bookTopList.add(
        InkWell(
          onTap: () { 
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RentDetail(title: _names[i], tagHero: "imagestoplist$i",)),
            );
          },
          child: Container(
            padding: EdgeInsets.only(right: 10, left: 10),
            width: 160.0,
            child: Column(
              children: <Widget>[
                Hero(
                  tag: "imagestoplist$i",
                  child: InkWell (
                    onTap: (){
                      Navigator.push(context, 
                        MaterialPageRoute(
                          builder: (context) => RentDetail(title: _names[i],tagHero: "imagestoplist$i",)
                        )
                      );
                    },
                    child: ImageBox("https://ecs5.tokopedia.net/newimg/product-1/2014/2/12/3249666/3249666_5bd73756-93d0-11e3-b1cf-88bd2523fab8.jpg", 160),
                  )
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Text("This is title\n" + _names[i], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                    ),
                    PopupMenuButton<Choice>(
                      onSelected: _selects,
                      itemBuilder: (BuildContext context){
                        return choices.map((Choice choice) {
                          // choice.index = index;
                          return PopupMenuItem<Choice>(
                            value: choice,
                            child: Text(choice.title),
                          );
                        }).toList();
                      },
                    ),
                  ],
                ),
                i%2 == 0 ? Text('Available') : Text("Rent, 2 month"),
              ],
            ),
          ),
        )
      );
      bookList.add(
        InkWell(
          onTap: () { 
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => RentDetail(title: _names[i], tagHero: "images$i",)),
            );
          },
          child: Container(
            padding: EdgeInsets.only(right: 10, left: 10),
            width: 160.0,
            child: Column(
              children: <Widget>[
                Hero(
                  tag: "images$i",
                  child: InkWell (
                    onTap: (){
                      Navigator.push(context, 
                        MaterialPageRoute(
                          builder: (context) => RentDetail(title: _names[i],tagHero: "images$i",)
                        )
                      );
                    },
                    child: ImageBox("https://ecs5.tokopedia.net/newimg/product-1/2014/2/12/3249666/3249666_5bd73756-93d0-11e3-b1cf-88bd2523fab8.jpg", 160),
                  )
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Text("This is title\n" + _names[i], style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                    ),
                    PopupMenuButton<Choice>(
                      onSelected: _selects,
                      itemBuilder: (BuildContext context){
                        return choices.map((Choice choice) {
                          // choice.index = index;
                          return PopupMenuItem<Choice>(
                            value: choice,
                            child: Text(choice.title),
                          );
                        }).toList();
                      },
                    ),
                  ],
                ),
                i%2 == 0 ? Text('Available') : Text("Rent, 2 month"),
              ],
            ),
          ),
        )
      );
    }
    
    
    setState(() {
      bookTopList = bookTopList;
      bookList = bookList;
      // _productList = _productList;
      // _names.clear();
      // for(int i=0; i<_productList.length; i++){
      //   _names.add(_productList[i].name);
      // }
      // productListLoading = false;
    });
  }
  
  int mCurrentIndex = 0;
  void onTabTapped(int index) async {
    setState(() {
      mCurrentIndex = index;
    });

    if(mCurrentIndex == 0) {
      
    } else if(mCurrentIndex == 1) {
      
    } else if(mCurrentIndex == 2) {
    
    }  
  }

  final key = GlobalKey<ScaffoldState>();

  @override
  void _selects(Choice choice) {
    if(choice.title == "Add To Cart"){
      key.currentState.showSnackBar(SnackBar(content: Text(message['addToCart']+" index-" + choice.index.toString() )));
    }else if(choice.title == "Add To Wishlist"){
      key.currentState.showSnackBar(SnackBar(content: Text(message['addToWishlist']+" index-" + choice.index.toString() )));
    }
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      key: key,
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: config.topBarBackgroundColor,
            iconTheme: IconThemeData(
              color: config.topBarIconColor
            ),
            automaticallyImplyLeading: false,
            floating: false,
            // pinned: true,
            title: Container(    
              // height: MediaQuery.of(context).size.width/16*9,
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.only(left: 10),
              color: config.topBarTextColor,
              child: Form(
                child: TextField(
                  controller: _controller,
                  keyboardType: TextInputType.text,
                  // onChanged: searchOperation,
                  onSubmitted: (_controller){
                    Navigator.push(context, MaterialPageRoute(builder: (context) => RentSearch(searchText: _controller,)));
                  },
                  decoration: InputDecoration(
                    hintText: message['searchBook'],
                  ),
                )
              ),
            ),
          ),
          SliverPersistentHeader(
            delegate: _SliverAppBarDelegate(collapsedHeight: 60.0, expandedHeight: 60.0), pinned: true ,
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) =>
              Container(    
                height: MediaQuery.of(context).size.width/16*9,
                child: InkWell(
                  child: Swiper(
                    itemBuilder: (BuildContext context, int index){
                      return Hero(
                        child:ImageBox(_img[index], 10),
                        tag: "image$index",
                      );
                    },
                    itemCount: _img.length,
                    pagination: _img.length > 1 ? 
                      SwiperPagination(
                        builder: new SwiperCustomPagination(builder:
                          (BuildContext context, SwiperPluginConfig config) {
                            return DotSwiperPaginationBuilder(
                              color: Colors.white,
                              activeColor: Colors.grey[300],
                              size: 10.0,
                              activeSize: 12.0
                            ).build(context, config);
                          }
                        )
                      )
                      :
                      null,
                    control: _img.length > 1 ? SwiperControl():null,
                    controller: _img.length > 1 ? SwiperController():null,
                    loop: _img.length > 1 ? true:false,
                    autoplay: _img.length > 1 ? true : false,
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context)=> PhotoGalery(imageList: _img))
                    );
                  },
                )
              ),
              childCount: 1
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) => 
            Container(
              padding: EdgeInsets.only(top: 10),
              width: MediaQuery.of(context).size.width,
              child: Card(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(message['topSelling'], style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Text("data"),
                      trailing: FlatButton(
                        child: Text(message['more'], style: TextStyle(color: Colors.blue),),
                        onPressed: (){

                        },
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.width/14*9,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: bookTopList,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            childCount: 1,
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate((context, index) => 
            Container(
              padding: EdgeInsets.only(top: 10),
              width: MediaQuery.of(context).size.width,
              child: Card(
                child: Column(
                  children: <Widget>[
                    ListTile(
                      title: Text(message['bestSeller'], style: TextStyle(fontWeight: FontWeight.bold),),
                      subtitle: Text("data"),
                      trailing: FlatButton(
                        child: Text(message['more'], style: TextStyle(color: Colors.blue),),
                        onPressed: (){

                        },
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.width/14*9,
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: bookList,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            childCount: 1,
            ),
          ),
        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   currentIndex: mCurrentIndex,
      //   type: BottomNavigationBarType.fixed,
      //   onTap: onTabTapped,
      //   items: [
      //     BottomNavigationBarItem(
      //       backgroundColor: Colors.blue,
      //       icon: Icon(Icons.sort),
      //       title: Text("Sort"),
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.filter),
      //       title: Text("Filters")
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.grid_on),
      //       title: Text("data")
      //     ),
      //     BottomNavigationBarItem(
      //       icon: Icon(Icons.category),
      //       title: Text("Category")
      //     ),
      //   ],
      // ),
    );
  }
}

class _SliverAppBarDelegate extends SliverPersistentHeaderDelegate {
  _SliverAppBarDelegate({
    @required this.collapsedHeight,
    @required this.expandedHeight,}
      );

  final double expandedHeight;
  final double collapsedHeight;

  @override double get minExtent => collapsedHeight;
  @override double get maxExtent => math.max(expandedHeight, minExtent);

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(
              left: 8.0, top: 8.0, bottom: 8.0, right: 8.0),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              FlatButton(
                onPressed: (){

                },
                child: Column(
                  children: <Widget>[
                    Icon(Icons.arrow_upward),
                    Text("For You"),
                  ],
                ),
              ),
              FlatButton(
                onPressed: (){

                },
                child: Column(
                  children: <Widget>[
                    Icon(Icons.star),
                    Text("Top Selling"),
                  ],
                ),
              ),
              FlatButton(
                onPressed: (){

                },
                child: Column(
                  children: <Widget>[
                    Icon(Icons.add_box),
                    Text("News release"),
                  ],
                ),
              ),
              FlatButton(
                onPressed: (){

                },
                child: Column(
                  children: <Widget>[
                    Icon(Icons.format_shapes),
                    Text("Genres"),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }

  @override
  bool shouldRebuild(_SliverAppBarDelegate oldDelegate) {
    return expandedHeight != oldDelegate.expandedHeight
        || collapsedHeight != oldDelegate.collapsedHeight;
  }
}

class Choice {
  Choice({this.title, this.index});
  String title;
  int index;
}

List<Choice> choices = <Choice>[
  Choice(title: 'Add To Cart'),
  Choice(title: 'Add To Wishlist'),
];