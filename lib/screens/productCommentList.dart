// packages
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shiftsoft/screens/reportForm.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shimmer/shimmer.dart';

// screens
import 'package:shiftsoft/screens/productDetailCommentList.dart';

// widgets
import 'package:shiftsoft/widgets/ImageBox.dart';
import 'package:shiftsoft/widgets/placeholderList.dart';

// resources
import 'package:shiftsoft/resources/productCommentApi.dart';

// models
import 'package:shiftsoft/models/mproductcomment.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ProductCommentList extends StatefulWidget{
  String title;
  int id,price; 
  
  createState(){
    return ProductCommentListState();
  }

  ProductCommentList({this.title,this.id,this.price});
}

class ProductCommentListState extends State<ProductCommentList>{
  int length = 2;
  
  Map<String, String> message=new Map();
  List<String> messageList = ["reply","seller","productDiscussion","user","comments","follow","joinDiscussion","reportDiscussion","cancel","loginFailed","filter"];
  void didChangeDependencies() {
    super.didChangeDependencies();
    messageList.map((item){
      // print(messages(context, item.split(',')));
      message[item] = messages(context, item.split(","));
      // message[item] = item;
    }).toList();
  }

  Widget build(context) {
    Configuration config = Configuration.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(message['productDiscussion'], style: TextStyle(color: config.topBarTextColor),),
        backgroundColor: config.topBarBackgroundColor,
        iconTheme: IconThemeData(
          color: config.topBarIconColor
        ),
      ),
      body: Stack(
        children: <Widget>[
          ListView.builder(
            itemCount: length !=0 ? length : 1,
            itemBuilder: (BuildContext context, int index){
              return length !=0 ? Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  Card(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          leading: ImageBox("", 50),
                          title: Text("PowerBank Super Quick Charge LIMITED EDITION"),
                        ),
                        Divider(),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            ListTile(
                              leading: Container(
                                width: 50,
                                height: 50,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: NetworkImage(""),
                                    fit: BoxFit.fill,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                  border: Border.all(
                                    color: Colors.black,
                                    width: 4.0,
                                  ),
                                ),
                              ),
                              title: Text("Michael Santoso"),
                              subtitle: Text("Kemarin pukul 21:39"),
                              trailing: IconButton(
                                key: Key("vert1"),
                                icon: Icon(Icons.more_vert),
                                onPressed: (){
                                  showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                                    return Container(
                                      padding: EdgeInsets.only(left: 15),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          ListTile(
                                            leading: Text(message['joinDiscussion']),
                                          ),
                                          Divider(),
                                          InkWell(
                                            key: Key("lapor1"),
                                            onTap: (){
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(builder: (context) => ReportForm()),
                                              );
                                            },
                                            child:ListTile(
                                              leading: Text(message['reportDiscussion']),
                                            ),
                                          ),
                                          Divider(),
                                          FlatButton(
                                            key: Key("batal1"),
                                            child: Text(message['cancel'], style: TextStyle(color: Colors.green),),
                                            onPressed: (){
                                              Navigator.pop(context);
                                            },
                                          )
                                        ],
                                      ),
                                    );
                                  });
                                },
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 80),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Ready Bro?"),
                                  Divider(),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      ListTile(
                                        leading: Container(
                                          width: 50,
                                          height: 50,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image: NetworkImage(""),
                                              fit: BoxFit.fill,
                                            ),
                                            borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                            border: Border.all(
                                              color: Colors.black,
                                              width: 4.0,
                                            ),
                                          ),
                                        ),
                                        title: Row(
                                          children: <Widget>[
                                            Text("Edison"),
                                            Card(
                                              color: Colors.green,
                                              child: Container(
                                                padding: EdgeInsets.all(5),
                                                child: Text(message['seller']),
                                              ),
                                            ),
                                          ],
                                        ),
                                        subtitle: Text("Kemarin pukul 21:39"),
                                        trailing: IconButton(
                                          key: Key("vert2"),
                                          icon: Icon(Icons.more_vert),
                                          onPressed: (){
                                            showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
                                              return Container(
                                                padding: EdgeInsets.only(left: 15),
                                                child: Column(
                                                  mainAxisSize: MainAxisSize.min,
                                                  children: <Widget>[
                                                    ListTile(
                                                      leading: Text(message['joinDiscussion']),
                                                    ),
                                                    Divider(),
                                                    InkWell(
                                                      key: Key("lapor2"),
                                                      onTap: (){
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(builder: (context) => ReportForm()),
                                                        );
                                                      },
                                                      child:ListTile(
                                                        leading: Text(message['reportDiscussion']),
                                                      ),
                                                    ),
                                                    Divider(),
                                                    FlatButton(
                                                      key: Key("batal2"),
                                                      child: Text(message['cancel'], style: TextStyle(color: Colors.green),),
                                                      onPressed: (){
                                                        Navigator.pop(context);
                                                      },
                                                    )
                                                  ],
                                                ),
                                              );
                                            });
                                          },
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(left: 80),
                                        child: Text("Ready. Silahkan Dipesan."),
                                      ),
                                      
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Divider(),
                            InkWell(
                              onTap: (){
                                print(MediaQuery.of(context).size.width);
                                // Navigator.push(context, 
                                //   MaterialPageRoute(builder: (context)=> ProductDetailCommentList(title: "Power Bank", id: 1926, price: 50000, )),
                                // );
                              },
                              child: ButtonBar(
                                children: <Widget>[
                                  FlatButton(
                                    disabledTextColor: Colors.green,
                                    child: Row(
                                      children: <Widget>[
                                        Text(message['reply'],style: TextStyle(),),
                                        Icon(Icons.arrow_forward_ios),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              )
              :
              PlaceholderList(type:"productcomment");
            },
          ),
          Positioned(
            bottom: 10,
            left: MediaQuery.of(context).size.width/2.5,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25)),
              ),
              color: Colors.white54,
              onPressed: (){

              },
              child: Row(
                children: <Widget>[
                  Icon(Icons.filter_list),
                  Text(message["filter"]),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}