// packages
import 'package:flutter/material.dart';
import 'package:flutter\_localizations/flutter\_localizations.dart';
import 'DemoLocalizationsDelegate.dart';
import 'package:shared_preferences/shared_preferences.dart';

// screens
import 'package:shiftsoft/screens/splashscreen.dart';
import 'package:shiftsoft/screens/welcomeCarousel.dart';
import 'package:shiftsoft/screens/login.dart';
import 'package:shiftsoft/screens/newsList.dart';
import 'package:shiftsoft/screens/newsDetail.dart';
import 'package:shiftsoft/screens/circleList.dart';
import 'package:shiftsoft/screens/eventDetail.dart';
import 'package:shiftsoft/screens/eventList.dart';
import 'package:shiftsoft/screens/transactionList.dart';
import 'package:shiftsoft/screens/transactionDetail.dart';
import 'package:shiftsoft/screens/downloadList.dart';
import 'package:shiftsoft/screens/assetList.dart';
import 'package:shiftsoft/screens/journeyList.dart';
import 'package:shiftsoft/screens/productCategoryList.dart';
import 'package:shiftsoft/screens/productCategoryDetail.dart';
import 'package:shiftsoft/screens/productWishlistList.dart';
import 'package:shiftsoft/screens/productCartList.dart';
import 'package:shiftsoft/screens/lessonList.dart';
import 'package:shiftsoft/screens/couponList.dart';
import 'package:shiftsoft/screens/fosterChildList.dart';
import 'package:shiftsoft/screens/promotionList.dart';
import 'package:shiftsoft/screens/subscribeList.dart';
import 'package:shiftsoft/screens/nearme.dart';
import 'package:shiftsoft/screens/rentList.dart';
import 'package:shiftsoft/screens/rentWishlistList.dart';
import 'package:shiftsoft/screens/rentCartList.dart';
import 'package:shiftsoft/screens/budgetList.dart';
import 'package:shiftsoft/screens/productReviewList.dart';
import 'package:shiftsoft/screens/productCommentList.dart';
import 'package:shiftsoft/screens/requestList.dart';

// widgets


// resources


// models
import 'package:shiftsoft/settings/configuration.dart';


void main() async {
  if (c.resetOnStart) {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();  
    final SharedPreferences prefs = await _prefs;
    prefs.clear();
  }

  runApp(
    Configuration(
      child: MaterialApp (
        supportedLocales: [  
          const Locale('en', 'US'),
          const Locale('id', 'ID'),
        ],  
        localizationsDelegates: [  
          const DemoLocalizationsDelegate(),  
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate  
        ],  
        localeResolutionCallback: (Locale locale, Iterable<Locale> supportedLocales) {
          for (Locale supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode || supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }  
          }
          return supportedLocales.first;
        },
        title: c.company,
        onGenerateRoute: (RouteSettings settings) {
          List<String> pages = [];
          int mode = 0;
          int id = 0;

          pages = settings.name.split("/");

          //
          if (pages.length > 1) {
            id = int.tryParse(pages[1]);
          }
          if (pages.length > 2) {
            mode = int.tryParse(pages[2]);
          }
          switch (pages[0]) {
            case '':
              return MaterialPageRoute(builder: (context)=> SplashScreen(companyId: settings.arguments));
              break;
            case 'welcomeCarousel':
              return MaterialPageRoute(builder: (context)=> WelcomeCarousel());
              break;
            case 'login':
              return MaterialPageRoute(builder: (context)=> Login());
              break;
            case 'newsList':
              return MaterialPageRoute(builder: (context)=> NewsList());
              break;
            case 'circleList':
              return MaterialPageRoute(builder: (context)=> CircleList(mode: mode, id: id));
              break;
            case 'newsDetail':
              return MaterialPageRoute(builder: (context)=> NewsDetail(mode: mode, id: id, model: settings.arguments));
              break;
            case 'eventList':
              return MaterialPageRoute(builder: (context)=> EventList());
              break;
            case 'eventDetail':
              return MaterialPageRoute(builder: (context)=> EventDetail(mode: mode, id: id, model: settings.arguments));
              break;
            case 'transactionList':
              return MaterialPageRoute(builder: (context)=> TransactionList());
              break;
            case 'transactionDetail':
              return MaterialPageRoute(builder: (context)=> TransactionDetail(mode: mode, id: id, model: settings.arguments,));
              break;
            case 'downloadList':
              return MaterialPageRoute(builder: (context)=> DownloadList());
              break;
            case 'assetList':
              return MaterialPageRoute(builder: (context)=> AssetList());
              break;
            case 'journeyList':
              return MaterialPageRoute(builder: (context)=> JourneyList());
              break;
            case 'productCategoryList':
              return MaterialPageRoute(builder: (context)=> ProductCategoryList());
              break;
            case 'productCategoryDetail':
              return MaterialPageRoute(builder: (context)=> ProductCategoryDetail());
              break;
            case 'productWishlistList':
              return MaterialPageRoute(builder: (context)=> ProductWishlistList());
              break;
            case 'productCartList':
              return MaterialPageRoute(builder: (context)=> ProductCartList());
              break;
            case 'lessonList':
              return MaterialPageRoute(builder: (context)=> LessonList());
              break;
            case 'couponList':
              return MaterialPageRoute(builder: (context)=> CouponList());
              break;
            case 'fosterChildList':
              return MaterialPageRoute(builder: (context)=> FosterChildList());
              break;
            case 'promotionList':
              return MaterialPageRoute(builder: (context)=> PromotionList());
              break;
            case 'subscribeList':
              return MaterialPageRoute(builder: (context)=> SubscribeList());
              break;
            case 'nearMe':
              return MaterialPageRoute(builder: (context)=> NearMe());
              break;
            case 'rentList':
              return MaterialPageRoute(builder: (context)=> RentList());
              break;
            case 'rentWishlistList':
              return MaterialPageRoute(builder: (context)=> RentWishlistList());
              break;
            case 'rentCartList':
              return MaterialPageRoute(builder: (context)=> RentCartList());
              break;
            case 'budgetList':
              return MaterialPageRoute(builder: (context)=> BudgetList());
              break;
            case 'productReviewList':
              return MaterialPageRoute(builder: (context)=> ProductReviewList());
              break;
            case 'productCommentList':
              return MaterialPageRoute(builder: (context)=> ProductCommentList());
              break;
            case 'requestList':
              return MaterialPageRoute(builder: (context)=> RequestList());
              break;
          }
        },
      ),
    )
  );
}
