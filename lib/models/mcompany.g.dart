// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mcompany.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Company _$CompanyFromJson(Map<String, dynamic> json) {
  return Company(
      id: json['ID'] as int ?? 0,
      name: json['Name'] as String ?? '',
      logo: json['Logo'] as String ?? '',
      slug: json['Slug'] as String ?? '',
      hash: json['Hash'] as String ?? '',
      oneSignalAppId: json['oneSignalAppId'] as String ?? '');
}

Map<String, dynamic> _$CompanyToJson(Company instance) => <String, dynamic>{
      'ID': instance.id,
      'Name': instance.name,
      'Logo': instance.logo,
      'Slug': instance.slug,
      'Hash': instance.hash,
      'oneSignalAppId': instance.oneSignalAppId
    };
