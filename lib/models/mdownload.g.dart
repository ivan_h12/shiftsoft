// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mdownload.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Download _$DownloadFromJson(Map<String, dynamic> json) {
  return Download(
      id: json['ID'] as int ?? 0,
      name: json['Name'] as String ?? '',
      file: json['File'] as String ?? '');
}

Map<String, dynamic> _$DownloadToJson(Download instance) => <String, dynamic>{
      'ID': instance.id,
      'Name': instance.name,
      'File': instance.file
    };
