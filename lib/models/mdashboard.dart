class MDashboard {
  int id;
  String name, link, pic, content;

  MDashboard({
    this.id,
    this.name,
    this.link,
    this.pic,
    this.content,
  });

  factory MDashboard.fromJson(Map<String, dynamic> parsedJson) {
    return MDashboard(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      link: parsedJson["Link"],
      content: parsedJson["Content"],
      pic: parsedJson["Pic"],
    );
  }  
}