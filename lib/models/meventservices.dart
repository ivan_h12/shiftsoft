import 'package:json_annotation/json_annotation.dart';
import 'package:shiftsoft/models/muser.dart';

part 'meventservices.g.dart';

@JsonSerializable()
class EventService {
  // int userId, id;
  // String serviceName;
  // User user;

  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'UserID', defaultValue: 0)
  int userId;

  @JsonKey(name: 'ServiceName', defaultValue: "")
  String name;
  
  @JsonKey(name: 'User', defaultValue: null)
  User user;

  EventService({
    this.id,
    this.userId,
    this.name,
    this.user
  });

  factory EventService.fromJson(Map<String, dynamic> parsedJson) => _$EventServiceFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$EventServiceToJson(this);
}