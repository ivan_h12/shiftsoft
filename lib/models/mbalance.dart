class Balance{
  String title, description;
  int value, point;
  DateTime createdAt;

  Balance({
    this.title,
    this.description,
    this.createdAt,
    this.point,
    this.value
  });

  factory Balance.fromJson(Map<String, dynamic> parsedJson) {
    return Balance(
      title: parsedJson["Title"],
      description: parsedJson["Description"],
      value: parsedJson["Value"],
      point: parsedJson["Point"],
      createdAt: DateTime.parse(parsedJson["CreatedAt"]),
    );
  }
}