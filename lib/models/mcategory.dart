import '../models/masset.dart';

class Category{
  int categoryId;
  String categoryName;
  List<Asset> assetList;

  Category({
    this.categoryId,
    this.categoryName,
    this.assetList,
  });

  factory Category.fromJson(Map<String, dynamic> parsedJson) {
    return Category(
      categoryId: parsedJson["ID"],
      categoryName: parsedJson["Name"],
    );
  }  
}
