import 'package:json_annotation/json_annotation.dart';
import 'mrequestType.dart';

part 'mrequest.g.dart';

@JsonSerializable()
class Request {
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  @JsonKey(name: 'Content', defaultValue: "")
  String content;

  @JsonKey(name: 'Pic', defaultValue: "")
  String pic;

  @JsonKey(name: 'CreatedAt')
  DateTime createdAt;

  RequestType requestType;

  Request({
    this.id,
    this.name,
    this.content,
    this.pic,
    this.createdAt
  });

  factory Request.fromJson(Map<String, dynamic> parsedJson) => _$RequestFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$RequestToJson(this);
}