import 'package:json_annotation/json_annotation.dart';
import '../models/mcircle.dart';

part 'mevent.g.dart';

@JsonSerializable()
class Event {
  // int id, rsvp, circleId;
  // String name, place, pic, idEncrypt, content;
  // DateTime startAt, endAt;

  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Rsvp', defaultValue: "")
  int rsvp;

  @JsonKey(name: 'CircleID', defaultValue: "")
  int circleId;
  
  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  @JsonKey(name: 'Place', defaultValue: "")
  String place;

  @JsonKey(name: 'Pic', defaultValue: 0)
  String pic;

  @JsonKey(name: 'IDEncrypt', defaultValue: 0)
  String idEncrypt;

  @JsonKey(name: 'Content', defaultValue: 0)
  String content;

  @JsonKey(name: 'StartAt')
  DateTime startAt;

  @JsonKey(name: 'EndAt')
  DateTime endAt;

  @JsonKey(name: 'Circle', defaultValue: [])
  Circle circle;

  Event({
    this.id,
    this.name,
    this.startAt,
    this.endAt,
    this.place,
    this.content,
    this.pic,
    this.idEncrypt,
    this.rsvp,
    this.circleId,
    this.circle
  });

  factory Event.fromJson(Map<String, dynamic> parsedJson) => _$EventFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$EventToJson(this);
}