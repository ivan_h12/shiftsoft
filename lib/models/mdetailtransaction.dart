import 'package:shiftsoft/models/mproduct.dart';

class DetailTransaction {
  int id, status, price, quantity;
  String description;
  Product product;

  DetailTransaction({
    this.id,
    this.status,
    this.price,
    this.quantity,
    this.description,
  });

  factory DetailTransaction.fromJson(Map<String, dynamic> parsedJson) {
    return DetailTransaction(
      id: parsedJson["ID"],
      status: parsedJson["Status"],
      quantity: parsedJson["Quantity"],
      description: parsedJson["Description"],
      price: parsedJson["Price"],
    );
  }  
}