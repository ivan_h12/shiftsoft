import 'package:json_annotation/json_annotation.dart';

part 'mnews.g.dart';

@JsonSerializable()
class News {
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Subject', defaultValue: "")
  String subject;

  @JsonKey(name: 'Content', defaultValue: "")
  String content;
  
  @JsonKey(name: 'IDEncrypt', defaultValue: "")
  String idEncrypt;

  @JsonKey(name: 'Pic', defaultValue: "")
  String pic;

  @JsonKey(name: 'CountRecipients', defaultValue: 0)
  int recipients;

  @JsonKey(name: 'CountLikes', defaultValue: 0)
  int likes;

  @JsonKey(name: 'CountComments', defaultValue: 0)
  int comments;

  @JsonKey(name: 'CreatedAt')
  DateTime createdAt;

  News({
    this.id,
    this.subject,
    this.content,
    this.idEncrypt,
    this.pic,
    this.recipients,
    this.likes,
    this.comments,
    this.createdAt
  });

  factory News.fromJson(Map<String, dynamic> parsedJson) => _$NewsFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$NewsToJson(this);
}