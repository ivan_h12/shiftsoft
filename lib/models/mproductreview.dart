class ProductReview{
  int id;
  String name, desc;
  DateTime createAt;
  int rating;

  ProductReview({
    this.id,
    this.name,
    this.desc,
    this.createAt,
    this.rating
  });

  factory ProductReview.fromJSON(Map<String, dynamic> parsedJson){
    return ProductReview(
      id: parsedJson["ID"],
      name: parsedJson["User"]["Name"],
      rating: parsedJson["Rating"],
      createAt: DateTime.parse(parsedJson["CreatedAt"]),
      desc: parsedJson["Description"]
    );
  }
}