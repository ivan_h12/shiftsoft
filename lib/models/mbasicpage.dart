class MBasicPage {
  int id;
  String title, pic, content;

  MBasicPage({
    this.id,
    this.title,
    this.pic,
    this.content,
  });

  factory MBasicPage.fromJson(Map<String, dynamic> parsedJson) {
    return MBasicPage(
      id: parsedJson["ID"],
      title: parsedJson["Title"],
      content: parsedJson["Content"],
      pic: parsedJson["Pic"],
    );
  }  
}