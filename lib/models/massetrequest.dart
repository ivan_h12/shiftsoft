import 'muser.dart';
import 'masset.dart';

class AssetRequest{
  int id, status, userId;
  String content, idEncrypt;
  DateTime startAt, endAt;
  User user;
  Asset asset;

  AssetRequest({
    this.id,
    this.status,
    this.content,
    this.startAt,
    this.endAt, 
    this.user,
    this.asset,
    this.userId,
    this.idEncrypt
  });

  factory AssetRequest.fromJson(Map<String, dynamic> parsedJson) {
    return AssetRequest(
      id: parsedJson["ID"],
      status: parsedJson["Status"],
      content: parsedJson["Content"],
      startAt: DateTime.parse(parsedJson["StartAt"]).subtract(Duration(hours: -7)),
      endAt: DateTime.parse(parsedJson["EndAt"]).subtract(Duration(hours: -7)),
      userId: parsedJson["UserID"],
      idEncrypt: parsedJson["IDEncrypt"],
    );
  }  
}

