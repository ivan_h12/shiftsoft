import 'package:json_annotation/json_annotation.dart';

part 'mdownload.g.dart';

@JsonSerializable()
class Download {
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  @JsonKey(name: 'File', defaultValue: "")
  String file;

  Download({
    this.id,
    this.name,
    this.file,
  });

  factory Download.fromJson(Map<String, dynamic> parsedJson) => _$DownloadFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$DownloadToJson(this);
}