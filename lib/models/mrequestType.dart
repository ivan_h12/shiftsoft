import 'package:json_annotation/json_annotation.dart';

part 'mrequestType.g.dart';

@JsonSerializable()
class RequestType {
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  RequestType({
    this.id,
    this.name,
  });

  factory RequestType.fromJson(Map<String, dynamic> parsedJson) => _$RequestTypeFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$RequestTypeToJson(this);
}