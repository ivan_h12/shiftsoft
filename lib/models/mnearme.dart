class Nearme {
  int id;
  String name, place, schedule, pic, gps;

  Nearme({
    this.id,
    this.name,
    this.place,
    this.schedule,
    this.pic,
    this.gps
  });

  factory Nearme.fromJson(Map<String, dynamic> parsedJson) {
    return Nearme(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      place: parsedJson["Place"],
      schedule: parsedJson["Place"],
      pic: parsedJson["Pic"],
      gps: parsedJson["GPS"],
    );
  }  
}