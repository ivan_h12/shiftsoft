class Attendance{
  int id, status;
  String name, pic, idEncrypt;
  DateTime createdAt;

  Attendance({
    this.id,
    this.name,
    this.createdAt,
    this.pic,
    this.idEncrypt,
    this.status
  });

  factory Attendance.fromJson(Map<String, dynamic> parsedJson) {
    return Attendance(
      id: parsedJson["UserID"],
      name: parsedJson["User"]["Name"],
      createdAt: DateTime.parse(parsedJson["CreatedAt"]),
      pic: parsedJson["User"]["Pic"],
      idEncrypt: parsedJson["User"]["IDEncrypt"],
      status: parsedJson["Status"]
    );
  }
}