import 'package:shiftsoft/models/mdetailtransaction.dart';
import 'package:shiftsoft/models/muser.dart';

class Transaction {
  int id, status, totalPrice;
  DateTime dateCreated;
  List<DetailTransaction> detailTransaction;
  User user;

  Transaction({
    this.id,
    this.status,
    this.dateCreated,
    this.totalPrice,
    this.detailTransaction,
    this.user,
  });

  factory Transaction.fromJson(Map<String, dynamic> parsedJson) {
    return Transaction(
      id: parsedJson["ID"],
      status: parsedJson["Status"],
      dateCreated: DateTime.parse(parsedJson["CreatedAt"]),
      totalPrice: parsedJson["TotalPrice"],
    );
  }  
}