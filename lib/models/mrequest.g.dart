// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mrequest.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Request _$RequestFromJson(Map<String, dynamic> json) {
  return Request(
      id: json['ID'] as int ?? 0,
      name: json['Name'] as String ?? '',
      content: json['Content'] as String ?? '',
      pic: json['Pic'] as String ?? '',
      createdAt: json['CreatedAt'] == null
          ? null
          : DateTime.parse(json['CreatedAt'] as String))
    ..requestType = json['requestType'] == null
        ? null
        : RequestType.fromJson(json['requestType'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RequestToJson(Request instance) => <String, dynamic>{
      'ID': instance.id,
      'Name': instance.name,
      'Content': instance.content,
      'Pic': instance.pic,
      'CreatedAt': instance.createdAt?.toIso8601String(),
      'requestType': instance.requestType
    };
