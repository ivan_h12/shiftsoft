class Subscribe {
  int id, status, value;
  DateTime period, paidAt;
  String description;

  Subscribe({
    this.id,
    this.status,
    this.value,
    this.paidAt,
    this.period,
    this.description,
  });

  factory Subscribe.fromJson(Map<String, dynamic> parsedJson) {
    return Subscribe(
      id: parsedJson["ID"],
      status: parsedJson["Status"],
      value: parsedJson["Value"],
      description: parsedJson["Description"],
      period: DateTime.parse(parsedJson["Period"]),
      paidAt: DateTime.parse(parsedJson["PaidAt"]),
    );
  }  
}