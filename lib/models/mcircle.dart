import 'package:json_annotation/json_annotation.dart';
import '../models/muser.dart';

part 'mcircle.g.dart';

@JsonSerializable()
class Circle{
  // int id;
  // String name, pic, gps, schedule, place;

  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  @JsonKey(name: 'Pic', defaultValue: "")
  String pic;

  @JsonKey(name: 'GPS', defaultValue: "")
  String gps;

  @JsonKey(name: 'Schedule', defaultValue: "")
  String schedule;

  @JsonKey(name: 'Place', defaultValue: "")
  String place;
  
  @JsonKey(name: 'Childs', defaultValue: [])
  List<Circle> childs;

  @JsonKey(name: 'Members', defaultValue: [])
  List<User> userList;

  Circle({
    this.id,
    this.name,
    this.pic,
    this.gps,
    this.schedule,
    this.place,
    this.userList,
    this.childs
  });

  factory Circle.fromJson(Map<String, dynamic> parsedJson) => _$CircleFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$CircleToJson(this);
}
