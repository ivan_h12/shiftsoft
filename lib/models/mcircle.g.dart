// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mcircle.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Circle _$CircleFromJson(Map<String, dynamic> json) {
  return Circle(
      id: json['ID'] as int ?? 0,
      name: json['Name'] as String ?? '',
      pic: json['Pic'] as String ?? '',
      gps: json['GPS'] as String ?? '',
      schedule: json['Schedule'] as String ?? '',
      place: json['Place'] as String ?? '',
      userList: (json['Members'] as List)
              ?.map((e) =>
                  e == null ? null : User.fromJson(e as Map<String, dynamic>))
              ?.toList() ??
          [],
      childs: (json['Childs'] as List)
              ?.map((e) =>
                  e == null ? null : Circle.fromJson(e as Map<String, dynamic>))
              ?.toList() ??
          []);
}

Map<String, dynamic> _$CircleToJson(Circle instance) => <String, dynamic>{
      'ID': instance.id,
      'Name': instance.name,
      'Pic': instance.pic,
      'GPS': instance.gps,
      'Schedule': instance.schedule,
      'Place': instance.place,
      'Childs': instance.childs,
      'Members': instance.userList
    };
