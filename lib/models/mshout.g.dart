// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mshout.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Shout _$ShoutFromJson(Map<String, dynamic> json) {
  return Shout(
      id: json['ID'] as int ?? 0,
      status: json['Status'] as int ?? 0,
      title: json['Title'] as String ?? '',
      content: json['Content'] as String ?? '',
      link: json['Link'] as String ?? '',
      createdAt: json['CreatedAt'] == null
          ? null
          : DateTime.parse(json['CreatedAt'] as String),
      updatedAt: json['UpdatedAt'] == null
          ? null
          : DateTime.parse(json['UpdatedAt'] as String));
}

Map<String, dynamic> _$ShoutToJson(Shout instance) => <String, dynamic>{
      'ID': instance.id,
      'Status': instance.status,
      'Title': instance.title,
      'Content': instance.content,
      'Link': instance.link,
      'CreatedAt': instance.createdAt?.toIso8601String(),
      'UpdatedAt': instance.updatedAt?.toIso8601String()
    };
