class Point{
  String roleName;
  int point;
  DateTime createdAt;

  Point({
    this.roleName,
    this.point,
    this.createdAt
  });

  factory Point.fromJson(Map<String, dynamic> parsedJson) {
    return Point(
      roleName: parsedJson["RoleName"],
      point: parsedJson["Point"],
      createdAt: DateTime.parse(parsedJson["CreatedAt"])
    );
  }
}

class PointHeader{
  int currentPoint, totalPoint;
  List<Point> pointList;

  PointHeader({
    this.currentPoint,
    this.pointList,
    this.totalPoint
  });

  factory PointHeader.fromJson(Map<String, dynamic> parsedJson) {
    return PointHeader(
      currentPoint: parsedJson["currentPoint"],
      totalPoint: parsedJson["totalPoint"],
    );
  }
}