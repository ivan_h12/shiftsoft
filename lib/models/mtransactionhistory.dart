class TransactionHistory {
  int id,status;
  String description;
  DateTime createdDate;
  

  TransactionHistory({
    this.id,
    this.description,
    this.createdDate,
    this.status,
  });

  factory TransactionHistory.fromJson(Map<String, dynamic> parsedJson) {
    return TransactionHistory(
      id: parsedJson["ID"],
      description: parsedJson["Description"],
      createdDate: DateTime.parse(parsedJson["CreatedAt"]),
      status: parsedJson["Status"],
    );
  }  
}