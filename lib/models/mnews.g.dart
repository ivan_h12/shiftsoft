// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mnews.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

News _$NewsFromJson(Map<String, dynamic> json) {
  return News(
      id: json['ID'] as int ?? 0,
      subject: json['Subject'] as String ?? '',
      content: json['Content'] as String ?? '',
      idEncrypt: json['IDEncrypt'] as String ?? '',
      pic: json['Pic'] as String ?? '',
      recipients: json['CountRecipients'] as int ?? 0,
      likes: json['CountLikes'] as int ?? 0,
      comments: json['CountComments'] as int ?? 0,
      createdAt: json['CreatedAt'] == null
          ? null
          : DateTime.parse(json['CreatedAt'] as String));
}

Map<String, dynamic> _$NewsToJson(News instance) => <String, dynamic>{
      'ID': instance.id,
      'Subject': instance.subject,
      'Content': instance.content,
      'IDEncrypt': instance.idEncrypt,
      'Pic': instance.pic,
      'CountRecipients': instance.recipients,
      'CountLikes': instance.likes,
      'CountComments': instance.comments,
      'CreatedAt': instance.createdAt?.toIso8601String()
    };
