import 'package:json_annotation/json_annotation.dart';

part 'meventservicesskill.g.dart';

@JsonSerializable()
class EventServiceSkill{
  // int skillID;
  // String skillName;

  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;
  
  EventServiceSkill({
    this.id,
    this.name
  });

  factory EventServiceSkill.fromJson(Map<String, dynamic> parsedJson) => _$EventServiceSkillFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$EventServiceSkillToJson(this);
}
