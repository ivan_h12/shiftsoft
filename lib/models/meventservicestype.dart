import 'package:json_annotation/json_annotation.dart';
import 'package:shiftsoft/models/meventservicesskill.dart';

part 'meventservicestype.g.dart';

@JsonSerializable()
class EventServiceType{
  // int id;
  // String name;

  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;
  
  @JsonKey(name: 'Skills', defaultValue: [])
  List<EventServiceSkill> skills;

  EventServiceType({
    this.id,
    this.name,
    this.skills
  });

  factory EventServiceType.fromJson(Map<String, dynamic> parsedJson) => _$EventServiceTypeFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$EventServiceTypeToJson(this);
}
