class ProductCart {
  int id,price,productID,quantity;
  // double rating;
  String name, pic, description;

  ProductCart({
    this.id,
    this.name,
    this.productID,
    this.price,
    this.quantity,
    this.description,
    // this.rating,
    this.pic,
  });

  factory ProductCart.fromJson(Map<String, dynamic> parsedJson) {
    return ProductCart(
      id: parsedJson["ID"],
      name: parsedJson["Product"]["Name"],
      productID: parsedJson["ProductID"],
      price: parsedJson["Product"]["Price"],
      quantity: parsedJson["Quantity"],
      description: parsedJson["Description"],
      // rating: parsedJson["Rating"],
      pic: parsedJson["Product"]["Pic"],
    );
  }  
}