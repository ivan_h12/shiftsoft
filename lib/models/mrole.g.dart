// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mrole.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Role _$RoleFromJson(Map<String, dynamic> json) {
  return Role(id: json['ID'] as int ?? 0, name: json['Name'] as String ?? '');
}

Map<String, dynamic> _$RoleToJson(Role instance) =>
    <String, dynamic>{'ID': instance.id, 'Name': instance.name};
