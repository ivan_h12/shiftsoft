class Coupon{
  int id, type;
  String name, pic, description,  code;
  DateTime  startAt, endAt;

  Coupon({
    this.id,
    this.name,
    this.pic,
    this.description,
    this.type,
    this.code,
    this.startAt,
    this.endAt
  });

  factory Coupon.fromJson(Map<String, dynamic> parsedJson) {
    return Coupon(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      pic: parsedJson["Pic"],
      description: parsedJson["Description"],
      type: parsedJson["Type"],
      code: parsedJson["Code"],
      startAt: DateTime.parse(parsedJson["StartAt"]),
      endAt: DateTime.parse(parsedJson["EndAt"])
    );
  } 
}