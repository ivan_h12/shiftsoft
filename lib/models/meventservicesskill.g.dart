// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meventservicesskill.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventServiceSkill _$EventServiceSkillFromJson(Map<String, dynamic> json) {
  return EventServiceSkill(
      id: json['ID'] as int ?? 0, name: json['Name'] as String ?? '');
}

Map<String, dynamic> _$EventServiceSkillToJson(EventServiceSkill instance) =>
    <String, dynamic>{'ID': instance.id, 'Name': instance.name};
