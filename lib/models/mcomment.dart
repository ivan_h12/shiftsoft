import '../models/muser.dart';

class Comment{
  int commentId;
  User user;
  String content;
  DateTime createdAt;

  Comment({
    this.commentId,
    this.user,
    this.content,
    this.createdAt
  });

  factory Comment.fromJson(Map<String, dynamic> parsedJson){
    return Comment(
      commentId: parsedJson["ID"],
      content: parsedJson["Content"],
      createdAt: DateTime.parse(parsedJson["CreatedAt"])
    );
  }
}

