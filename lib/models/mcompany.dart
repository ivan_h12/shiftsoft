import 'package:json_annotation/json_annotation.dart';

part 'mcompany.g.dart';

@JsonSerializable() 
class Company {
  // int id;
  // String name, slug, hash, logo, oneSignalAppId;
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  @JsonKey(name: 'Logo', defaultValue: "")
  String logo;

  @JsonKey(name: 'Slug', defaultValue: "")
  String slug;

  @JsonKey(name: 'Hash', defaultValue: "")
  String hash;

  @JsonKey(name: 'oneSignalAppId', defaultValue: "")
  String oneSignalAppId;

  Company({
    this.id,
    this.name,
    this.logo,
    this.slug,
    this.hash,
    this.oneSignalAppId
  });

  factory Company.fromJson(Map<String, dynamic> parsedJson) => _$CompanyFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$CompanyToJson(this);
}