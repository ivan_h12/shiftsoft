// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mevent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
      id: json['ID'] as int ?? 0,
      name: json['Name'] as String ?? '',
      startAt: json['StartAt'] == null
          ? null
          : DateTime.parse(json['StartAt'] as String),
      endAt: json['EndAt'] == null
          ? null
          : DateTime.parse(json['EndAt'] as String),
      place: json['Place'] as String ?? '',
      content: json['Content'] as String ?? 0,
      pic: json['Pic'] as String ?? 0,
      idEncrypt: json['IDEncrypt'] as String ?? 0,
      rsvp: json['Rsvp'] as int ?? '',
      circleId: json['CircleID'] as int ?? '',
      circle: json['Circle'] == null
          ? null
          : Circle.fromJson(json['Circle'] as Map<String, dynamic>) ?? []);
}

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'ID': instance.id,
      'Rsvp': instance.rsvp,
      'CircleID': instance.circleId,
      'Name': instance.name,
      'Place': instance.place,
      'Pic': instance.pic,
      'IDEncrypt': instance.idEncrypt,
      'Content': instance.content,
      'StartAt': instance.startAt?.toIso8601String(),
      'EndAt': instance.endAt?.toIso8601String(),
      'Circle': instance.circle
    };
