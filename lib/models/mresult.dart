class Result {
  int success;
  String message;

  Result({
    this.success,
    this.message,
  });

  factory Result.fromJson(Map<String, dynamic> parsedJson){
    return Result(
      success: parsedJson["success"] ?? 0,
      message: parsedJson["message"] ?? "",
    );
  }
}