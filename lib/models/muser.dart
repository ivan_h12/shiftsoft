import 'package:json_annotation/json_annotation.dart';
import './mcircle.dart';

part 'muser.g.dart';

@JsonSerializable()
class User {
  // int id, status, point, balance;
  // String userName, pic, role, idEncrypt, address, phone, nickname, email, city;
  // DateTime birthday;

  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'RoleID', defaultValue: 0)
  int roleId;

  @JsonKey(name: 'Status', defaultValue: 0)
  int status;

  @JsonKey(name: 'Point', defaultValue: 0)
  int point;

  @JsonKey(name: 'Balance', defaultValue: 0)
  int balance;

  @JsonKey(name: 'CompanyID', defaultValue: 0)
  int companyId;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  @JsonKey(name: 'Nickname', defaultValue: "")
  String nickname;

  @JsonKey(name: 'Pic', defaultValue: "")
  String pic;

  @JsonKey(name: 'IDEncrpyt', defaultValue: "")
  String idEncrypt;

  @JsonKey(name: 'Address', defaultValue: "")
  String address;

  @JsonKey(name: 'Phone', defaultValue: "")
  String phone;

  @JsonKey(name: 'Email', defaultValue: "")
  String email;

  @JsonKey(name: 'City', defaultValue: "")
  String city;

  @JsonKey(name: 'Birhday')
  DateTime birthday;

  @JsonKey(name: 'Circles', defaultValue: null)
  List<Circle> circleList;

  User({
    this.id,
    this.roleId,
    this.name,
    this.nickname,
    this.pic,
    this.idEncrypt,
    this.status,
    this.address,
    this.phone,
    this.point,
    this.balance,
    this.companyId,
    this.email,
    this.city,
    this.birthday,
    this.circleList,
  });

  factory User.fromJson(Map<String, dynamic> parsedJson) => _$UserFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}