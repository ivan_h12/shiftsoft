class Product {
  int id,price,categoryID,quantity;
  // double rating;
  String name, pic, description;

  Product({
    this.id,
    this.name,
    this.categoryID,
    this.price,
    this.quantity,
    this.description,
    // this.rating,
    this.pic,
  });

  factory Product.fromJson(Map<String, dynamic> parsedJson) {
    return Product(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      categoryID: parsedJson["CategoryID"],
      price: parsedJson["Price"],
      quantity: parsedJson["Quantity"],
      description: parsedJson["Description"],
      // rating: parsedJson["Rating"],
      pic: parsedJson["Pic"],
    );
  }  
}