class ProductCategory{
  int id;
  String name;

  ProductCategory({
    this.id,
    this.name
  });

  factory ProductCategory.fromJSON(Map<String, dynamic> parsedJson){
    return ProductCategory(
      id: parsedJson["ID"],
      name: parsedJson["Name"]
    );
  }
}