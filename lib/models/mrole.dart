import 'package:json_annotation/json_annotation.dart';

part 'mrole.g.dart';

@JsonSerializable()
class Role {
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;

  @JsonKey(name: 'Name', defaultValue: "")
  String name;

  Role({
    this.id,
    this.name,
  });

  factory Role.fromJson(Map<String, dynamic> parsedJson) => _$RoleFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$RoleToJson(this);
}