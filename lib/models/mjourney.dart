class Journey{
  int journeyId;
  String name, badgePic;
  DateTime createdAt;
  
  Journey({
    this.journeyId,
    this.name,
    this.createdAt,
    this.badgePic
  });

  factory Journey.fromJson(Map<String, dynamic> parsedJson){
    return Journey(
      journeyId: parsedJson["ID"],
      name: parsedJson["Name"],
      createdAt: DateTime.parse(parsedJson["CreatedAt"]),
      badgePic: parsedJson["Badge"]["Pic"]
    );
  }


}