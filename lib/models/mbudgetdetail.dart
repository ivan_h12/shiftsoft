class BudgetDetails{
  int id, qty, price, total, tax, status, userId;
  String name, content, circleName, pic;

  BudgetDetails({
    this.id,
    this.qty,
    this.price,
    this.total,
    this.name,
    this.content,
    this.circleName,
    this.pic,
    this.tax,
    this.status,
    this.userId
  });

  factory BudgetDetails.fromJson(Map<String, dynamic> parsedJson){
    return BudgetDetails(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      price: parsedJson["Price"],
      qty: parsedJson["Quantity"],
      total: parsedJson["Total"],
      tax: parsedJson["Tax"],
      content: parsedJson["Description"],
      circleName: parsedJson["Circle"]["Name"],
      pic: parsedJson["Pic"],
      status: parsedJson["Status"],
      userId: parsedJson["UserID"]
    );
  }
}