// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meventservices.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventService _$EventServiceFromJson(Map<String, dynamic> json) {
  return EventService(
      id: json['ID'] as int ?? 0,
      userId: json['UserID'] as int ?? 0,
      name: json['ServiceName'] as String ?? '',
      user: json['User'] == null
          ? null
          : User.fromJson(json['User'] as Map<String, dynamic>));
}

Map<String, dynamic> _$EventServiceToJson(EventService instance) =>
    <String, dynamic>{
      'ID': instance.id,
      'UserID': instance.userId,
      'ServiceName': instance.name,
      'User': instance.user
    };
