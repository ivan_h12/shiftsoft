class Lesson{
  int id;
  String name, deskripsi, pic;

  Lesson({
    this.id,
    this.name,
    this.pic,
    this.deskripsi
  });

  factory Lesson.fromJson(Map<String, dynamic> parsedJson){
    return Lesson(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      deskripsi: parsedJson["Description"],
      pic: parsedJson["Pic"]
    );
  }
}