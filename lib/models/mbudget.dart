class Budget{
  int id, total, left, realization, userId;
  String name;
  DateTime periode;
  bool overlimit;

  Budget({
    this.id,
    this.name,
    this.total,
    this.left,
    this.realization,
    this.periode,
    this.overlimit,
    this.userId
  });

  factory Budget.fromJson(Map<String, dynamic> parsedJson) {
    return Budget(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      total: parsedJson["Total"],
      left: parsedJson["BudgetLeft"],
      realization: parsedJson["BudgetRealization"],
      periode: DateTime.parse(parsedJson["Periode"]),
      overlimit: parsedJson["Overlimit"],
      userId: parsedJson["UserID"]
    );
  }  
}