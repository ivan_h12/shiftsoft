import 'package:json_annotation/json_annotation.dart';

part 'mshout.g.dart';

@JsonSerializable()
class Shout {
  // int id, status;
  // String title, content, link;
  // DateTime createDate, updateDate;
  
  @JsonKey(name: 'ID', defaultValue: 0)
  int id;
  
  @JsonKey(name: 'Status', defaultValue: 0)
  int status;

  @JsonKey(name: 'Title', defaultValue: "")
  String title;
  
  @JsonKey(name: 'Content', defaultValue: "")
  String content;
  
  @JsonKey(name: 'Link', defaultValue: "")
  String link;

  @JsonKey(name: 'CreatedAt')
  DateTime createdAt;

  @JsonKey(name: 'UpdatedAt')
  DateTime updatedAt;

  Shout({
    this.id,
    this.status,
    this.title,
    this.content,
    this.link,
    this.createdAt,
    this.updatedAt,
  });
factory Shout.fromJson(Map<String, dynamic> parsedJson) => _$ShoutFromJson(parsedJson);
  Map<String, dynamic> toJson() => _$ShoutToJson(this);
}