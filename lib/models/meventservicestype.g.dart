// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meventservicestype.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventServiceType _$EventServiceTypeFromJson(Map<String, dynamic> json) {
  return EventServiceType(
      id: json['ID'] as int ?? 0, name: json['Name'] as String ?? '');
}

Map<String, dynamic> _$EventServiceTypeToJson(EventServiceType instance) =>
    <String, dynamic>{'ID': instance.id, 'Name': instance.name};
