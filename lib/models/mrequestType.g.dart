// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mrequestType.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestType _$RequestTypeFromJson(Map<String, dynamic> json) {
  return RequestType(
      id: json['ID'] as int ?? 0, name: json['Name'] as String ?? '');
}

Map<String, dynamic> _$RequestTypeToJson(RequestType instance) =>
    <String, dynamic>{'ID': instance.id, 'Name': instance.name};
