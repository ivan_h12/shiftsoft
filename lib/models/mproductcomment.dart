class ProductComment {
  int id, productPrice;
  DateTime createDate;
  String productName,userName, userPic, description;

  ProductComment({
    this.id,
    this.productName,
    this.userName,
    this.createDate,
    this.productPrice,
    this.userPic,
    this.description,
    // this.rating,
    
  });

  factory ProductComment.fromJson(Map<String, dynamic> parsedJson) {
    return ProductComment(
      id: parsedJson["ID"],
      productName: parsedJson["Product"]["Name"],
      userName: parsedJson["User"]["Name"],
      createDate: DateTime.parse(parsedJson["CreatedAt"]),
      productPrice: parsedJson["Product"]["Price"],
      description: parsedJson["Description"],
      // rating: parsedJson["Rating"],
      userPic: parsedJson["User"]["Pic"],
    );
  }  
}