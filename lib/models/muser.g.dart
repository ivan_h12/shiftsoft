// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'muser.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
      id: json['ID'] as int ?? 0,
      roleId: json['RoleID'] as int ?? 0,
      name: json['Name'] as String ?? '',
      nickname: json['Nickname'] as String ?? '',
      pic: json['Pic'] as String ?? '',
      idEncrypt: json['IDEncrpyt'] as String ?? '',
      status: json['Status'] as int ?? 0,
      address: json['Address'] as String ?? '',
      phone: json['Phone'] as String ?? '',
      point: json['Point'] as int ?? 0,
      balance: json['Balance'] as int ?? 0,
      companyId: json['CompanyID'] as int ?? 0,
      email: json['Email'] as String ?? '',
      city: json['City'] as String ?? '',
      birthday: json['Birhday'] == null
          ? null
          : DateTime.parse(json['Birhday'] as String),
      circleList: (json['Circles'] as List)
          ?.map((e) =>
              e == null ? null : Circle.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'ID': instance.id,
      'RoleID': instance.roleId,
      'Status': instance.status,
      'Point': instance.point,
      'Balance': instance.balance,
      'CompanyID': instance.companyId,
      'Name': instance.name,
      'Nickname': instance.nickname,
      'Pic': instance.pic,
      'IDEncrpyt': instance.idEncrypt,
      'Address': instance.address,
      'Phone': instance.phone,
      'Email': instance.email,
      'City': instance.city,
      'Birhday': instance.birthday?.toIso8601String(),
      'Circles': instance.circleList
    };
