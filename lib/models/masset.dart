class Asset {
  int id, userId;
  String name, content, pic;

  Asset({
    this.id,
    this.name,
    this.content,
    this.pic,
    this.userId
  });

  factory Asset.fromJson(Map<String, dynamic> parsedJson) {
    return Asset(
      id: parsedJson["ID"],
      name: parsedJson["Name"],
      pic: parsedJson["Pic"],
      content: parsedJson["Description"],
      userId: parsedJson["UserID"]
    );
  }  
}