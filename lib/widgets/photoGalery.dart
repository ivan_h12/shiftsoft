import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/settings/configuration.dart';

class PhotoGalery extends StatefulWidget {
  List<String> titleList;
  List<String> contentList;
  List<String> imageList;

  PhotoGalery({this.titleList, this.contentList, this.imageList});

  createState() {
    return PhotoGaleryState();
  }
}
class PhotoGaleryState extends State<PhotoGalery> {
  void initState() {
    super.initState();

  }

  int currentIndex = 0;

  Widget build(BuildContext context) {
    int index = -1;
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            PhotoViewGallery(
              enableRotation: true,
              pageOptions: widget.imageList.map((image) {
                index++;
                return PhotoViewGalleryPageOptions(
                  imageProvider: c.testing ? AssetImage('lib/settings/placeholder.png'):NetworkImage(image),
                  heroTag: "image$index",
                );
              }).toList(),
              onPageChanged: (page) {
                if (widget.titleList.length > 0 && widget.contentList.length > 0){
                  setState(() {
                    currentIndex = page;
                  });
                }
              },
            ),
            Positioned(
              bottom: 75,
              left: 25,
              right: 25,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  widget.titleList.length > 0 ?
                  Text(
                    "${widget.titleList[currentIndex]}",
                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
                  )
                  :
                  Text(""),
                  Padding(padding: EdgeInsets.symmetric(vertical: 2),),
                  widget.contentList.length > 0 ?
                  Text(
                    "${widget.contentList[currentIndex]}",
                    style: TextStyle(color: Colors.white),
                  )
                  :
                  Text(""),
                ],
              )
            ),
          ]
        )
      )
    );
  }
}