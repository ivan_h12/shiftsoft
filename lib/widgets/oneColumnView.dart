import 'package:flutter/material.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';

class OneColumnView extends StatelessWidget {
  String imageUrl, title, date;
  Key key;
  List<Widget> widgets;
  VoidCallback onTap;

  OneColumnView({this.key, this.imageUrl, this.title, this.date, this.widgets, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        InkWell(
          key: key,
          child: Container(
            padding: EdgeInsets.fromLTRB(13, 3, 0, 1),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: ImageBox(CompanyAsset("news", imageUrl), 100, fit: BoxFit.cover, border: false),
                  margin: EdgeInsets.only(right: 10),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 18),
                      child: Text(title, overflow: TextOverflow.ellipsis, maxLines: 2, style: TextStyle(fontSize: 15)),
                      width: 210
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(date, style: TextStyle(fontSize: 12))
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 5),
                      child: Row(children: widgets)
                    )
                  ],
                )
              ]
            )
          ),
          onTap: onTap,
        ),
        Divider()
      ],
    );
  }
}