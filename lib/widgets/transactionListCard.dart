import 'package:flutter/material.dart';
import 'package:shiftsoft/models/mtransaction.dart';
import 'package:shiftsoft/screens/transactionDetailHistoryStatus.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart' as config;
import 'package:shiftsoft/widgets/listBox.dart';

class TransactionListCard extends StatelessWidget{
  Transaction model;
  String invoice,date,status,message;
  int userId, transactionId, index;
  TransactionListCard({this.invoice, this.date, this.status, this.userId, this.transactionId, this.model, this.index,this.message});

  @override
    Widget build(BuildContext context) {
      return ListBox(
        
        childs: Container(
          margin: EdgeInsets.only(left: 15,right: 5, top: 5),
          padding: EdgeInsets.all(1),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(invoice,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18.0),textAlign: TextAlign.left,),
              Text(date,style: TextStyle(color: Colors.grey,fontSize: 15.0),textAlign: TextAlign.left),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.all(Radius.circular(10)) 
                    ),
                    child: Text(status,style: TextStyle(color:Colors.black,fontSize: 18.0,),textAlign: TextAlign.left),
                  ), 
                  IconButton(
                    key: Key("transactionlist$index"),
                    icon: Icon(Icons.arrow_forward_ios),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => TransactionDetailHistoryStatus(transactionId: transactionId, userId: userId, model: model,)),
                      );
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      );
    }
}