import 'package:flutter/material.dart';
import 'package:shiftsoft/settings/configuration.dart';

class SSText extends StatelessWidget{
  String text, family, fontFamilyUsed;
  Color color, labelColorUsed;
  FontWeight fontWeight;
  TextAlign align;
  double size, fontSizeUsed;
  int type, maxLines;
  bool caps;
  
  SSText({
    this.text = '',
    this.type = 4,
    this.align = TextAlign.left,
    this.color,
    this.size,
    this.caps = false,
    this.family,
    this.maxLines
  });

  @override
  Widget build(BuildContext context){
    Configuration config = Configuration.of(context);

    switch(type) {
      case 1: // Title with primary background
        this.labelColorUsed = config.primaryTextColor;
        this.fontSizeUsed = 17;
        this.fontFamilyUsed = 'Lato';
        this.fontWeight = FontWeight.w900;
      break;
      case 2: // Title with secondary background
        this.labelColorUsed = config.secondaryTextColor;
        this.fontSizeUsed = 17;
        this.fontFamilyUsed = 'Lato';
        this.fontWeight = FontWeight.w900;
      break;
      case 3: // Title with primary background smaller
        this.labelColorUsed = config.primaryTextColor;
        this.fontSizeUsed = 15;
        this.fontWeight = FontWeight.w700;
      break;
      case 4: // Title with secondary background smaller
        this.labelColorUsed = config.secondaryTextColor;
        this.fontSizeUsed = 15;
        this.fontWeight = FontWeight.w700;
      break;
      case 5: // Greyed text
        this.labelColorUsed = Color(0xFF999999);;
        this.fontSizeUsed = 13;
        this.fontWeight = FontWeight.w500;
      break;
      case 6: // Greyed text smaller caps
        this.labelColorUsed = Color(0xFF888888);
        this.fontSizeUsed = 11;
        this.fontWeight = FontWeight.w500;
      break;
      case 7: // Normal
        this.labelColorUsed = config.secondaryTextColor;
        this.fontSizeUsed = 14;
        this.fontWeight = FontWeight.w500;
      break;
    }
    
    if (caps) this.text = this.text.toUpperCase();
    if (color != null) this.labelColorUsed = this.color;
    if (size != null) this.fontSizeUsed = this.size;
    if (family != null) this.fontFamilyUsed = this.family;

    return Text(
      this.text,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: this.labelColorUsed,
        fontFamily: this.fontFamilyUsed,
        fontWeight: this.fontWeight,
        fontSize: this.fontSizeUsed
      ),
      textAlign: this.align,
      maxLines: this.maxLines,
    );
    
  } 
  
}