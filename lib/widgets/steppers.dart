import 'package:flutter/material.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/settings/configuration.dart';

class Steppers extends StatelessWidget {
  int type;
  String nama, tanggal, gambarNama, gambarLink;
  bool isFirst;
  
  Steppers({
    this.type, 
    this.nama, 
    this.tanggal, 
    this.gambarLink = "", 
    this.gambarNama = "",
    this.isFirst
  });

  @override
  Widget build(BuildContext context) {
    Configuration config = Configuration.of(context);
    if(type == 1){
      return Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 10.0),
            child: Container(
              margin: EdgeInsets.only(left: 50.0, top: 40.0, right: 5.0, bottom: 25.0),
              child: ListTile(
                title: Row(
                  children: <Widget>[
                    Container(
                      width: 180,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: Text(nama, style: TextStyle(fontSize: 17.0)),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 3.0),
                            child: Text(tanggal, style: TextStyle(fontSize: 16.0, color: Colors.black38)),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 30),
                      child: Image.network(CompanyAsset(gambarNama, gambarLink), scale: 7),
                    )
                  ],
                )
              ),
            ),
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 45.0,
            child: Container(
              height: double.infinity,
              width: 1.0,
              color: Colors.black45,
            ),
          ),
          Positioned(
            top: 50.0,
            left: 25.0,
            child: Container(
              height: isFirst ? 30.0 : 20,
              width: 40.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white
              ),
              child: Container(
                margin: EdgeInsets.all(5.0),
                height: 30.0,
                width: 30.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: isFirst ? config.primaryColor : Colors.black26,
                ),
              ),
            ),
          ),
        ],
      );
    } else {
      return Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 50.0),
            child: Card(
              margin: EdgeInsets.only(left: 50.0, top: 25.0, right: 25.0, bottom: 25.0),
              child: ListTile(
                title: Text(tanggal),
                subtitle: Text(nama),
              ),
            ),
          ),
          Positioned(
            top: 0.0,
            bottom: 0.0,
            left: 75.0,
            child: Container(
              height: double.infinity,
              width: 1.0,
              color: Colors.blue,
            ),
          ),
          Positioned(
            top: 50.0,
            left: 55.0,
            child: Container(
              height: 40.0,
              width: 40.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white
              ),
              child: Container(
                margin: EdgeInsets.all(5.0),
                height: 30.0,
                width: 30.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.red
                ),
              ),
            ),
          ),
        ],
      );
    }
  }
}