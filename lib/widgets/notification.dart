import 'package:flutter/material.dart';

class NotificationCard extends StatelessWidget{
  int status;
  String title, content, message, createDate, updateDate;
  final VoidCallback onTap;

  NotificationCard({this.status, this.title, this.message, this.content, this.createDate, this.updateDate, this.onTap});
  Widget build(context){
    return Container(
      padding: EdgeInsets.all(1.5),                  
      child: InkWell(
        child: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 1.5, 0, 1.5),
              child: ListTile(
                leading : status == 2 ? Icon(Icons.notifications_active, color: Colors.blue, size: 45,) : Icon(Icons.notifications, color: Colors.black, size: 45,) ,
                title: Text(title),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(createDate + " - " + updateDate + " " + message, style: TextStyle(fontSize: 10.5,color: Colors.grey)),
                    Text(content, style: TextStyle(color: Colors.black54),),
                  ],  
                )
              ), 
            ),
            Divider(),
          ],
        ),
        onTap: onTap,
      ),
    );
  }
}
