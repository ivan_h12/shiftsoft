import 'package:flutter/material.dart';
import 'package:shiftsoft/settings/configuration.dart';
import 'package:shiftsoft/widgets/carousel.dart';

class ItemDetail extends StatelessWidget{
  Widget content, list;
  String title, module;
  List<String> imageList = [];
  bool pinned, appBar;
  List<Widget> appBarActions;

  ItemDetail({this.title, this.content, this.imageList,  this.list ,this.module, this.pinned = true, this.appBar = false, this.appBarActions});

  @override
  Widget build(BuildContext context){
    Configuration config = Configuration.of(context);

    if (list == null) {
      list = SliverPadding(padding: EdgeInsets.all(0),);
    }

    bool imageExist = false;

    if (imageList.length > 0) {
      if (imageList[0] != "")
        imageExist = true;
    }
    
    return CustomScrollView(
      slivers: <Widget>[
        imageExist || appBar ?
        SliverAppBar(
          title: title != null ? Text(title):null,
          backgroundColor: config.topBarBackgroundColor,
          expandedHeight: MediaQuery.of(context).size.width/16*8,
          automaticallyImplyLeading: appBar,
          floating: true,
          pinned: pinned,
          snap: true,
          flexibleSpace: FlexibleSpaceBar(
            // title: Text(title),
            background: Container(    
              child: Carousel(module, imageList, [], [], [])
            ),
          ),
          actions: appBarActions,
        ) : SliverPadding(padding: EdgeInsets.all(0),),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, index) =>
            content,
            childCount: 1,
          )
        ),
      ]..add(list),
    );
  }
}