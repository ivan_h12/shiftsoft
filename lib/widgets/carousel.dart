import 'package:flutter/material.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/photoGalery.dart';
import 'package:shiftsoft/settings/configuration.dart';

class Carousel extends StatelessWidget {
  List<String> imageList, linkList, titleList, contentList;
  String module;
  bool zoom;

  Carousel(this.module, this.imageList, this.linkList, this.titleList, this.contentList, {this.zoom = true});

  Widget build(context) {
    List<String> imageListAsset = [];

    imageList.map((item) {
      imageListAsset.add(CompanyAsset(module, item, context: context));
    }).toList();

    if (linkList.length != imageList.length) {
      linkList = List<String>.generate(imageList.length, (int index) => "");
    }

    Configuration configuration = Configuration.of(context);

    return Swiper(
      itemBuilder: (BuildContext context,int index){
        final tempYoutubeKey = linkList[index].split("=");
        String youtubeKey = "";
        
        if (tempYoutubeKey.length > 1) {
          youtubeKey = tempYoutubeKey[1];
        }
        return Stack(
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.width/16*9,
              child: imageList[index] == "" ?
              ImageBox(
                "https://img.youtube.com/vi/"+youtubeKey+"/hqdefault.jpg",
                100,
                border: false,
              )
              :
              InkWell(
                child: Hero(
                  child: ImageBox(
                    imageListAsset[index], 
                    100,
                    border: false,
                  ),
                  tag: "image$index",
                ),
                onTap: () {
                  if (zoom) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context)=> PhotoGalery(imageList: imageListAsset, titleList:titleList, contentList:contentList))
                    );
                  }
                },
              )
            ),
            Positioned(
              top: ((MediaQuery.of(context).size.width*9/16)-50)/2,
              left: (MediaQuery.of(context).size.width-50)/2,
              width: 50,
              height: 50,
              child: linkList[index] != "" ?
              FloatingActionButton(
                backgroundColor: configuration.primaryColor,
                elevation: 2,
                onPressed: () async {
                  try {
                    playYoutubeVideo(linkList[index]);
                  } catch (e) {
                    if (await canLaunch(linkList[index])) {
                      await launch(linkList[index]);
                    }
                  }
                },
                child: Icon(
                    tempYoutubeKey.length > 1 ? Icons.play_arrow:Icons.link,
                    color: configuration.primaryTextColor,
                  ),

              )
              : Text("")
            ),
          ],
        );
      },
      itemCount: imageList.length,
      pagination: imageList.length > 1 ?
        SwiperPagination(
          builder: SwiperCustomPagination(builder:
            (BuildContext context, SwiperPluginConfig config) {
              return DotSwiperPaginationBuilder(
                color: configuration.lightGrayColor,
                activeColor: configuration.primaryColor,
                size: 6.0,
                activeSize: 9.0
              ).build(context, config);
            }
          )
        )
        :
        null,
      autoplay: imageList.length > 1,
      loop: imageList.length > 1,
      
    );
  }

  void playYoutubeVideo(String url) {
    FlutterYoutube.playYoutubeVideoByUrl(
      apiKey: "AIzaSyBQUDCIaePCCvQAyisvLlBSys9l4uFqOuU",
      videoUrl: url,
      autoPlay: true,
      // fullScreen: true,
    );   
  }

}