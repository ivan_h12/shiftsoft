import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';
import 'package:shiftsoft/settings/configuration.dart';

class ImageBox extends StatelessWidget{
  String imageUrl;
  double size, width, height;
  BoxFit fit;
  bool border;
  ImageBox(this.imageUrl, this.size, {this.width = 0, this.height = 0, this.fit = BoxFit.fill, this.border = true});

  @override
    Widget build(BuildContext context) {
      final tempImageUrl = imageUrl.split(",");
      imageUrl = tempImageUrl[0];
      
      if (width == 0 && height == 0) {
        width = size;
        height = size;
      }

      return Container(
        width: width,
        height: height,
        child: c.testing ?
        Image(
          image: AssetImage('lib/settings/placeholder.png'),
        )
        :
        CachedNetworkImage(
          alignment: Alignment.center,
          placeholder: Shimmer.fromColors(
            baseColor: Colors.grey[300],
            highlightColor: Colors.white,
              child:Container(
              width: size,
              height: size,
              color: Colors.grey[300]
            )
          ),
          imageUrl: imageUrl,
          fit: fit,
        ),
        decoration: border ? 
          BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            border: Border.all(
              color: Colors.white,
              width: 4.0,
            ),
          )
          :
          null
      );
    }
}