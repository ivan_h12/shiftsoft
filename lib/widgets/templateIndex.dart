import 'package:flutter/material.dart';

class TemplateIndex extends StatelessWidget{
  double mHeight;
  String mText;

  TemplateIndex({this.mHeight, this.mText});

  Widget build(context){
    return Container(
      height: mHeight,
      child: Card(
        color: Colors.white70,
        child: Center(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 25),
                child: Icon(Icons.computer, size: 75,),
              ),
              Text(mText)
            ],
          ) 
        ),
      )
    );
  }
}