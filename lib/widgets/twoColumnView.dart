import 'package:flutter/material.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';
import 'package:shiftsoft/widgets/SSText.dart';

import 'package:shiftsoft/settings/configuration.dart';

class TwoColumnView extends StatelessWidget{
  String imageUrl, title, date;
  Key key;
  List<Widget> widgets;
  VoidCallback onTap;

  TwoColumnView({this.key, this.imageUrl, this.title, this.date, this.widgets, this.onTap});

  @override
  Widget build(BuildContext context) {
    Configuration config = new Configuration();
    double width = MediaQuery.of(context).size.width;
    return InkWell(
      key: key,
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                    child: ImageBox(
                      CompanyAsset("news",imageUrl), 
                      (width-45)/2,
                      border: false,
                      fit: BoxFit.cover
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical:10, horizontal: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children:[
                        SSText(text: title, type: 4,  maxLines: 2, ),
                        Container(
                          margin: EdgeInsets.only(top:5),
                          child: SSText(text: date, type: 6,  maxLines: 2),
                        )
                      ]
                    ),
                  ),
                ],
              ),
              Positioned(
                bottom: 0,
                left: -5,
                child: Container(
                  width: (width-45)/2,
                  child: PhysicalModel(
                    color: config.lighterGrayColor,
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5), bottomRight: Radius.circular(5)),
                    child:Padding(
                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: widgets
                      )
                    ),
                  ),
                ),
              ),
            ]
          )
        ),
      ),
      onTap: onTap
    );
  }
}