import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:shimmer/shimmer.dart';

class ProductCard extends StatelessWidget{
  double imageSize, mWidth, mHeight, imageWidth, imageHeight;
  String url;
  Widget content;

  ProductCard({this.imageSize, this.url, this.content, this.mHeight, this.mWidth, this.imageHeight, this.imageWidth});

  Widget build(context){
    return Container(
      padding: EdgeInsets.all(5.0),                     
      height: mHeight,
      width: mWidth,
      child: Card(
        color: Colors.white,
        child: Container(
          padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Container(
                    height: imageHeight,
                    width: imageWidth,
                    child: CachedNetworkImage(
                      alignment: Alignment.center,
                      placeholder: Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.white,
                          child:Container(
                          width: imageWidth,
                          height: imageHeight,
                          color: Colors.grey[300]
                        )
                      ),
                      imageUrl: url,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Positioned(
                    top: 2.5,
                    right: 5,
                    child: IconButton(
                      onPressed: (){},
                      icon: Icon(Icons.favorite, size: 20,color: Colors.red,),
                    ),
                  )
                ],
              ),
              content
            ],
          ),
        ),
      ),      
    );
  }
}