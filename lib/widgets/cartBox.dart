import 'package:flutter/material.dart';
import 'package:shiftsoft/widgets/imageBox.dart';

class CartBox extends StatelessWidget{
  int type, price, count;
  double imageSize;
  String imageUrl, title;
  bool checked;
  final VoidCallback onChecked, onAdd, onRemove;
  
  CartBox({this.price, this.type, this.count,this.checked,this.imageUrl,this.title,this.imageSize,this.onChecked,this.onAdd,this.onRemove});

  @override
    Widget build(BuildContext context) {
      if (type == 1) { // type 1 button dengan rounded corner
      return Container(
        width: MediaQuery.of(context).size.width,
        // height: MediaQuery.of(context).size.width/16*8,
          margin: EdgeInsets.only(right: 5,),
          alignment: Alignment.center,
          child: Container(
            child: Column(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: IconButton(
                        icon: Icon(Icons.check_circle_outline, color: checked ? Colors.blue : Colors.grey,),
                        onPressed: onChecked,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        padding: EdgeInsets.only(top: 5),
                        width: imageSize,
                        height: imageSize,
                        child: ImageBox(imageUrl,imageSize),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(title, style: TextStyle(fontSize: 12),),
                          Padding(padding: EdgeInsets.all(10),),
                          Text("Rp. " + price.toString(), style:TextStyle(color: Colors.blue),),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Card(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            IconButton(
                              icon:Icon(Icons.add),
                              onPressed: onAdd,
                            ),
                            Text(count.toString()),
                            IconButton(
                              icon:Icon(Icons.remove),
                              onPressed: onRemove,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Divider(),
              ],
            )
          )
        );
      } else { // type 2 button tanpa rounded corner
        return Container(
        width: MediaQuery.of(context).size.width,
        // height: MediaQuery.of(context).size.width/16*8,
          margin: EdgeInsets.only(right: 5,),
          alignment: Alignment.center,
          child: Container(
            child: Column(
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: IconButton(
                        icon: Icon(Icons.check_circle_outline, color: checked ? Colors.blue : Colors.grey,),
                        onPressed: onChecked,
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Container(
                        padding: EdgeInsets.only(top: 5),
                        width: imageSize,
                        height: imageSize,
                        child: ImageBox(imageUrl,imageSize),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(title, style: TextStyle(fontSize: 12),),
                          Padding(padding: EdgeInsets.all(10),),
                          Text("Rp. " + price.toString(), style:TextStyle(color: Colors.blue),),
                        ],
                      ),
                    ),
                  ],
                ),
                Divider(),
              ],
            )
          )
        );
      }
    }
}