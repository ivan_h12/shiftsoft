import 'package:flutter/material.dart';
import 'package:shiftsoft/widgets/imageBox.dart';
import 'package:shiftsoft/tools/functions.dart';

class Grid extends StatelessWidget{
  String imageUrl, title, date, time, place;
  Key key;
  VoidCallback onTap;

  Grid({this.key, this.imageUrl, this.title, this.date, this.time, this.place, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      key: key,
      child: Card(
        elevation: 10,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Container(
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ImageBox(CompanyAsset("event", imageUrl), 175, fit: BoxFit.cover, border: false),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical:5, horizontal: 5),
                    child: Text(title, overflow: TextOverflow.ellipsis, maxLines: 2),
                  ),
                ],
              ),
              Positioned(
                bottom: 60,
                left: 5,
                child: Text(date, style: TextStyle(fontSize: 12, color: Colors.grey))
              ),
              Positioned(
                bottom: 45,
                left: 5,
                child: Text(time, style: TextStyle(fontSize: 12, color: Colors.grey))
              ),
              Positioned(
                bottom: 30,
                left: 5,
                child: Text(place, style: TextStyle(fontSize: 12, color: Colors.grey))
              ),
            ]
          )
        ),
      ),
      onTap: onTap
    );
  }
}