import 'package:flutter/material.dart';

class PlaceholderList extends StatelessWidget {
  PlaceholderList({this.type, this.paddingTop = 150, this.paddingBottom = 0});

  String type;
  IconData icon;
  String text;
  double paddingTop, paddingBottom;

  Widget build(context) {
    type = type.toLowerCase();
    if (type == "notification") {
      icon = Icons.notifications_none;
      text = "no notification yet";
    } else if(type == "spiritualjourney"){
      icon = Icons.notifications_none;
      text = "no spiritual journey yet";
    } else if(type == "budgetlist"){
      icon = Icons.attach_money;
      text = "no budget list yet";
    } else if(type == "budgetsubmission"){
      icon = Icons.attach_money;
      text = "no budget submission yet";
    } else if(type == "budgetrealization"){
      icon = Icons.attach_money;
      text = "no budget realization yet";
    } else if(type == "eventattendancelist"){
      icon = Icons.group;
      text = "no attendance yet";
    } else if(type == "eventattendanceadd"){
      icon = Icons.person;
      text = "no user found";
    } else if(type == "eventservices"){
      icon = Icons.person;
      text = "no user found";
    } else if(type == "eventlist"){
      icon = Icons.calendar_today;
      text = "no event yet";
    } else if(type == "fosterchildlist"){
      icon = Icons.person;
      text = "no foster child yet";
    }else if (type == "cart") {
      icon = Icons.shopping_cart;
      text = "no cart yet";
    } else if (type == "wishlist") {
      icon = Icons.favorite;
      text = "no wishlist yet";
    } else if (type == "transaction") {
      icon = Icons.monetization_on;
      text = "no transaction yet";
    } else if (type == "productcomment") {
      icon = Icons.comment;
      text = "no comment yet";
    } else if (type == "productreview") {
      icon = Icons.content_paste;
      text = "no review yet";
    } else if (type == "requestlist") {
      icon = Icons.receipt;
      text = "no request yet";
    } else if (type == "point"){
      icon = Icons.history;
      text = "no point history yet";
    } else if (type == "balance"){
      icon = Icons.account_balance;
      text = "no balance yet";
    } else if (type == "news"){
      icon = Icons.mail;
      text = "no news yet";
    } else if (type == "transactiondetail"){
      icon = Icons.shopping_cart;
      text = "no transaction detail yet";
    } else if (type == "productcategory"){
      icon = Icons.shopping_cart;
      text = "no product category yet";
    } else if (type == "productcategorydetail"){
      icon = Icons.shopping_cart;
      text = "no product category detail yet";
    } else if (type == "productlist"){
      icon = Icons.shopping_basket;
      text = "no product list yet";
    } else if (type == "coupon"){
      icon = Icons.attach_money;
      text = "no coupon list yet";
    } else if (type == "promotion"){
      icon = Icons.attach_money;
      text = "no promotion list yet";
    } else if (type == "subscribe"){
      icon = Icons.subscriptions;
      text = "no subscribe list yet";
    } else if (type == "rentwishlist"){
      icon = Icons.library_books;
      text = "no rent wishlist yet";
    } else if (type == "rentcartlist"){
      icon = Icons.library_books;
      text = "no rent cartlist yet";
    } else if (type == "nearme"){
      icon = Icons.map;
      text = "no near places found";
    } else if (type == "assetrequest"){
      icon = Icons.library_books;
      text = "no asset request yet";
    } else if(type == "lesson"){
      icon = Icons.library_books;
      text = "no lesson yet";
    } else if(type == "attendance"){
      icon = Icons.people;
      text = "no attendance found";
    } else if(type == "company"){
      icon = Icons.home;
      text = "no company found";
    } else if(type == "download"){
      icon = Icons.file_download;
      text = "no download found";
    } else if(type == "tags"){
      icon = Icons.theaters;
      text = "no tags found";
    }
    
    return Center(
      child: Column(
        children: [
          Padding(padding: EdgeInsets.only(top: paddingTop)),
          Icon(
            icon,  
            size: 40,
            color: Colors.grey,
          ),
          Padding(padding: EdgeInsets.only(top: 10)),
          Text(
            "$text",
            style: TextStyle(
              color: Colors.grey
            ),
          ),
          Padding(padding: EdgeInsets.only(bottom: paddingBottom)),
        ],
      ),
    );
  }
}