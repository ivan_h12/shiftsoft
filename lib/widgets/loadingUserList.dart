// package
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingUserList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 6,
      itemBuilder: (BuildContext context, int index) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[200],
          highlightColor: Colors.grey[350],
          period: Duration(milliseconds: 800),
          child: ListTile(
            contentPadding: EdgeInsets.all(35.0),
            leading: Container(
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(100.0)),
                border: Border.all(
                  color: Colors.white,
                  width: 4.0,
                ),
                color: Colors.grey[200],
              ),
            ),
            title: Container(width: 100, height: 50, color: Colors.grey[200])
          )
        );
      }
    );
  }
}