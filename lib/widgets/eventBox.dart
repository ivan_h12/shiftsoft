import 'package:flutter/material.dart';

class EventBox extends StatelessWidget{
  String namaevent, tgl, jam, tmpt;
  Widget imageUrl;
  
  EventBox(this.imageUrl, this.namaevent, this.tgl, this.jam, this.tmpt);

  @override
    Widget build(BuildContext context) {
      return Container(
        child: Row(
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  child: imageUrl
                )
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Text(namaevent, style: TextStyle(fontSize: 17.0), overflow: TextOverflow.ellipsis, maxLines: 2),
                  margin: EdgeInsets.only(left: 15.0, top: 3.0),
                  width: 210.0,
                ),
                Container(
                  child: Text(tgl, style: TextStyle(fontSize: 14.0, color: Colors.grey)),
                  margin: EdgeInsets.only(left: 15.0)
                ),
                Container(
                  child: Text(jam, style: TextStyle(fontSize: 14.0, color: Colors.grey)),
                  margin: EdgeInsets.only(left: 15.0)
                ),
                Container(
                  child: Text(tmpt, style: TextStyle(fontSize: 14.0, color: Colors.grey)),
                  margin: EdgeInsets.only(left: 15.0)
                )
              ],
            )
          ],
        )
      );
    }
}