// Imports the Flutter Driver API
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';
import 'package:ozzie/ozzie.dart';

void main() {
  group('Counter App', () {
    // First, define the Finders. We can use these to locate Widgets from the
    // test suite. Note: the Strings provided to the `byValueKey` method must
    // be the same as the Strings we used for the Keys in step 1.
    final buttonFinder = find.byType('RaisedButton');

    final counterTextFinder = find.byValueKey('success');
    final welcomeCarousel0 = find.byValueKey("welcome carousel0");
    final welcomeCarousel1 = find.byValueKey("welcome carousel1");
    final welcomeCarousel2 = find.byValueKey("welcome carousel2");
    final dashboard = find.byValueKey("dashboard");
    final login = find.byValueKey('login');

    FlutterDriver driver;
    Ozzie ozzie;

    // Connect to the Flutter driver before running any tests
    setUpAll(() async {
      driver = await FlutterDriver.connect();
      ozzie = Ozzie.initWith(driver, groupName: 'counter');
    });

    // Close the connection to the driver after the tests have completed
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
        await ozzie.generateHtmlReport();
      }
    });

    test('app success start', () async {
      // Use the `driver.getText` method to verify the counter starts at 0.
      expect(await driver.getText(counterTextFinder), "success");
      // await ozzie.takeScreenshot('app success start');
    });

    test('welcoming carousel', () async {
      // First, tap on the button
      await driver.tap(buttonFinder);
      // await ozzie.takeScreenshot('welcoming carousel');
      await driver.tap(welcomeCarousel0);
      await driver.tap(welcomeCarousel1);
      await driver.tap(welcomeCarousel2);

      // Then, verify the counter text has been incremented by 1
      // expect(await driver.getText(counterTextFinder), "1");
    });

    test('login', () async {
      // First, tap on the button
      // await ozzie.takeScreenshot('login');

      // Then, verify the counter text has been incremented by 1
      // expect(await driver.getText(counterTextFinder), "1");
    });

    test('dashboard', () async {
      // First, tap on the button
      await driver.tap(login);
      expect(await driver.getText(dashboard), "success");
      // await ozzie.takeScreenshot('dashboard');

      // Then, verify the counter text has been incremented by 1
      // expect(await driver.getText(counterTextFinder), "1");
    });

        test('shout', () async {
      // sementara tombol backnya click manual
      await driver.tap(find.byValueKey('shout'));
      // await ozzie.takeScreenshot('shout');
      await driver.tap(find.byTooltip('Back'));
    });

    test('point', () async {
      final point = find.byValueKey("point");
      await driver.tap(point);
      //await ozzie.takeScreenshot('point list');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('balance', () async {
      final balance = find.byValueKey("balance");
      await driver.tap(balance);
      //await ozzie.takeScreenshot("balance detail");
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('drawer', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      // await ozzie.takeScreenshot('drawer');
    });

    test('news', () async {
      final news = find.byValueKey("news");
      await driver.tap(news);
      // await ozzie.takeScreenshot('news list');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('news detail', () async {
      final newsdetail = find.byValueKey("newsdetail0");
      await driver.tap(newsdetail);
      // await driver.scroll(find.byValueKey("scroll"), 0, -500, Duration(seconds: 1));
      // await ozzie.takeScreenshot('news detail');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('circle', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.tap(find.byValueKey("circle"));
      // await ozzie.takeScreenshot('circle list');
    });

  
    test('circlemaps', () async {
      final circlemaps = find.byValueKey("circlemaps0");
      await driver.tap(circlemaps);
      // await ozzie.takeScreenshot('Circle Maps');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('userdetail', () async {
      final userdetail = find.byValueKey("userdetail0");
      await driver.tap(userdetail);
      // await ozzie.takeScreenshot('User Detail');
      await driver.tap(find.byTooltip('Back'));
      await driver.tap(find.byTooltip('Back'));
    });

    test('event', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      final event = find.byValueKey("event");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event list');
    });

    test('eventcalendar', () async {
      final event = find.byValueKey("eventcalendar");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event calendar'); 
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('eventform', () async {
      final event = find.byValueKey("eventform");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event form');

      await driver.tap(find.byValueKey("eventname"));
      await driver.enterText("Test event name");
      await driver.waitFor(find.text("Test event name"));

      await driver.tap(find.byValueKey("circle"));
      await driver.tap(find.byValueKey("Marketing Department 2"));

      await driver.tap(find.byValueKey("place"));
      await driver.enterText("Test place");
      await driver.waitFor(find.text("Test place"));

      await driver.scroll(find.byValueKey("scroll"), 0, -1000, Duration(seconds: 1));

      await driver.tap(find.byValueKey("datestart"));
      await driver.tap(find.byValueKey("timestart"));
      await driver.tap(find.byValueKey("dateend"));
      await driver.tap(find.byValueKey("timeend"));

      await driver.tap(find.byValueKey("deskripsi"));
      await driver.enterText("Test deskripsi");
      await driver.waitFor(find.text("Test deskripsi"));

      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('eventdetail', () async {
      final event = find.byValueKey("eventdetail0");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event detail');
    });

    test('eventattendanceadd', () async {
      final event = find.byValueKey("add");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event attendance add');
      final backButton = find.byValueKey('Back');
      await driver.tap(backButton);
    });

    test('eventservices', () async {
      final popup = find.byValueKey("popup");
      await driver.tap(popup);
      final event = find.byValueKey("Services");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event services');
    });

    test('eventservicesform', () async {
      final event = find.byValueKey("eventservicesform");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event services form');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('eventattendancelist', () async {
      final popup = find.byValueKey("popup");
      await driver.tap(popup);
      final event = find.byValueKey("Attendance List");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event attendance list');
    });

    test('eventattendanceform', () async {
      final event = find.byValueKey("attendanceform");
      await driver.tap(event);
      // await ozzie.takeScreenshot('event attendance form');
      await driver.tap(find.byValueKey('Back'));
      await driver.tap(find.byValueKey('Back'));
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('lessonlist', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -500, Duration(seconds: 1));
      final lesson = find.byValueKey("lesson");
      await driver.tap(lesson);
    });

    test('lessondetail', () async {
      final lesson = find.byValueKey("matpel0");
      await driver.tap(lesson);
    });

    test('lessonstudentlist', () async {
      final lesson = find.byValueKey("studentlist");
      await driver.tap(lesson);
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('lessonclassroomdetail', () async {
      final lesson = find.byValueKey("classroomdetail0");
      await driver.tap(lesson);
    });

    test('lessonclassroomattendance', () async {
      final lesson = find.byValueKey("classroomattendance");
      await driver.tap(lesson);
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('asset', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      final asset = find.byValueKey("asset");
      await driver.tap(asset);
      // await ozzie.takeScreenshot('asset list');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('asset detail', () async {
      final expansiontile = find.byValueKey("expansiontile");
      await driver.tap(expansiontile);
      final assetdetail0 = find.byValueKey("assetdetail00");
      await driver.tap(assetdetail0);
      // await ozzie.takeScreenshot('asset detail');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
      // await driver.tap(backButton);
    });

    test('asset request', () async {
      final request = find.byValueKey("request");
      await driver.tap(request);

      var dateStart = find.byValueKey('dateStart');
      await driver.tap(dateStart);

      var timeStart = find.byValueKey('timeStart');
      await driver.tap(timeStart);

      var dateEnd = find.byValueKey('dateEnd');
      await driver.tap(dateEnd);

      var timeEnd = find.byValueKey('timeEnd');
      await driver.tap(timeEnd);

      var contentTextField = find.byValueKey('content-textfield');
      await driver.tap(contentTextField);
      await driver.enterText('Content');
      await driver.waitFor(find.text('Content'));

      // var save = find.byValueKey('save');
      // await driver.tap(save);
      // var ok = find.byValueKey('ok');
      // await driver.tap(ok);

      // await ozzie.takeScreenshot('asset request');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('asset calendar', () async {
      final assetcalendar = find.byValueKey("assetcalendar");
      await driver.tap(assetcalendar);
      // await ozzie.takeScreenshot('asset calendar');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('journey', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      final journey = find.byValueKey("journey");
      await driver.tap(journey);
      // await ozzie.takeScreenshot('spiritual journey');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('journey form', () async {
      final journeyform = find.byValueKey("journeyform");
      await driver.tap(journeyform);
      // await ozzie.takeScreenshot('journey form');

      var nameTextField = find.byValueKey('journeyname');
      await driver.tap(nameTextField);
      await driver.enterText('Spiritual');
      await driver.waitFor(find.text('Spiritual'));

      var startDate = find.byValueKey('startdate');
      await driver.tap(startDate);

      var contentTextField = find.byValueKey('content');
      await driver.tap(contentTextField);
      await driver.enterText('Deskripsi');
      await driver.waitFor(find.text('Deskripsi'));

      // var submit = find.byValueKey('submit');
      // await driver.tap(submit);
      // var ok = find.byValueKey('ok');
      // await driver.tap(ok);

      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('budgetlist', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -1000, Duration(seconds: 1));
      final budget = find.byValueKey("budget");
      await driver.tap(budget);
      // await ozzie.takeScreenshot('budget list');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });
    
    test('budgetdetail', () async {
      final budgetdetail = find.byValueKey("budgetdetail0");
      await driver.tap(budgetdetail);
      // await ozzie.takeScreenshot('budget detail');
      final budgetrealization = find.byValueKey("budgetrealization");
      await driver.tap(budgetrealization);
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('budgetform', () async {
      final budgetform = find.byValueKey("budgetform");
      await driver.tap(budgetform);
      // await ozzie.takeScreenshot('budget form');

      var nameTextField = find.byValueKey('name');
      await driver.tap(nameTextField);
      await driver.enterText('Bola Tenis');
      await driver.waitFor(find.text('Bola Tenis'));

      // var tipe = find.byValueKey('tipe');
      // await driver.tap(tipe);
      // var choosetipe = find.byValueKey('Realization');
      // await driver.tap(choosetipe);

      var circle = find.byValueKey('circle');
      await driver.tap(circle);
      var choosecircle = find.byValueKey('Marketing Department 2');
      await driver.tap(choosecircle);

      var priceTextField = find.byValueKey('price');
      await driver.tap(priceTextField);
      await driver.enterText('15000');
      await driver.waitFor(find.text('15000'));

      var qtyTextField = find.byValueKey('qty');
      await driver.tap(qtyTextField);
      await driver.enterText('3');
      await driver.waitFor(find.text('3'));

      await driver.scroll(find.byValueKey("scroll"), 0, -500, Duration(seconds: 1));

      // var taxTextField = find.byValueKey('tax');
      // await driver.tap(taxTextField);
      // await driver.enterText('10');

      var contentTextField = find.byValueKey('content');
      await driver.tap(contentTextField);
      await driver.enterText('Deskripsi');
      await driver.waitFor(find.text('Deskripsi'));

      var image = find.byValueKey('image');
      await driver.tap(image);
      
      // var submit = find.byValueKey('submit');
      // await driver.tap(submit);
      // var ok = find.byValueKey('ok');
      // await driver.tap(ok);

      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('nearme', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -500, Duration(seconds: 1));
      final nearme = find.byValueKey("nearme");
      await driver.tap(nearme);
      // await ozzie.takeScreenshot('nearme');
      final nearme0 = find.byValueKey("nearme0");
      await driver.tap(nearme0);
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('CommentList', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -1000, Duration(seconds: 1));
      final comment = find.byValueKey("comment_list");
      await driver.tap(comment);
      //await ozzie.takeScreenshot("comment list");

      final more1 = find.byValueKey("vert1");
      await driver.tap(more1);
      final lapor1 = find.byValueKey("lapor1");
      await driver.tap(lapor1);
      //await ozzie.takeScreenshot("lapor1");
      
      final backButton = find.byTooltip('Back');

      await driver.tap(backButton);
      final batal1 = find.byValueKey("batal1");
      await driver.tap(batal1);

      // final more2 = find.byValueKey("vert2");
      // await driver.tap(more2);
      // final lapor2 = find.byValueKey("lapor2");
      // await driver.tap(lapor2);
      //await ozzie.takeScreenshot("lapor2");

      // await driver.tap(backButton);
      // final batal2 = find.byValueKey("batal2");
      // await driver.tap(batal2);

      await driver.tap(backButton);
    });

    test('subscribe', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));      
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -500, Duration(seconds: 1));
      final subscribe = find.byValueKey("subscribe");
      await driver.tap(subscribe);
      //await ozzie.takeScreenshot("subscribe list");
    });

    test('subscribe_form', () async {
      final subscribeForm = find.byValueKey("subscribeForm");
      await driver.tap(subscribeForm);
      
      final monthPick = find.byValueKey("monthPeriod");
      await driver.tap(monthPick);
      
      final datePick = find.byValueKey("datePaid");
      await driver.tap(datePick);

      final textValue = find.byValueKey("textFieldValue");
      await driver.tap(textValue);
      await driver.enterText("100");
      await driver.waitFor(find.text("100"));
      
      final textDescription = find.byValueKey("textFieldDescription");
      await driver.tap(textDescription);
      await driver.enterText("Bayar Lunas Bro!!!");
      await driver.waitFor(find.text("Bayar Lunas Bro!!!"));
      
      final pickImage = find.byValueKey("imagePicker0");
      await driver.tap(pickImage);

      await driver.scroll(find.byValueKey("subscribeFormScroll"), 0, -100, Duration(seconds: 1));
      //await ozzie.takeScreenshot("subscribe form");   

      final submit = find.byValueKey("submit");
      await driver.tap(submit);
      final okButton = find.byValueKey("submitOk");
      await driver.tap(okButton);

      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('RequestList', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));      
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -1000, Duration(seconds: 1));
      final request = find.byValueKey("request_list");
      await driver.tap(request);

      // await ozzie.takeScreenshot("request list");

      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });

    test('productCart', () async {
      final backButton = find.byTooltip('Back');
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -200, Duration(seconds: 1));
      final productCart = find.byValueKey("cart");
      await driver.tap(productCart);
      final wishlist = find.byValueKey("fromWishlist");
      await driver.tap(wishlist);
      await driver.tap(backButton);

      final useCode = find.byValueKey("useCode");
      await driver.tap(useCode);

      final codeValue = find.byValueKey("promoCode");
      await driver.tap(codeValue);
      await driver.enterText("bakulsby");      
      await driver.waitFor(find.text("bakulsby"));

      final use = find.byValueKey("use");
      await driver.tap(use);

      final okButton = find.byValueKey("OK");
      await driver.tap(okButton);
      
      await driver.tap(backButton);
    });

    test('rentWishlist', () async {
      final backButton = find.byTooltip('Back');
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -800, Duration(seconds: 1));
      final rentWishlist = find.byValueKey("rentWishlist");
      await driver.tap(rentWishlist);
        
      // await ozzie.takeScreenshot("rent wishlist");

      await driver.tap(backButton);
    });

    test ('rentCartList', () async {
      final backButton = find.byTooltip('Back');
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -800, Duration(seconds: 1));
      
      final rentCart = find.byValueKey("rentCartList");
      await driver.tap(rentCart);      
      // await ozzie.takeScreenshot("rent cart");
      
      await driver.tap(backButton);

    });
    
    test('productCategory', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -100, Duration(seconds: 1));
      final productCategory = find.byValueKey("productCategory");
      await driver.tap(productCategory);
      //await ozzie.takeScreenshot("product category");
    });

    test('productCategoryDetail', () async {
      final productCategory0 = find.byValueKey("productCategory0");
      await driver.tap(productCategory0);
      //await ozzie.takeScreenshot("product category detail");
    });

    test('productList', () async {
      final productList0 = find.byValueKey("productList0");
      await driver.tap(productList0);
      //await ozzie.takeScreenshot("product list");
    });

    test('productDetail', () async {
      final productDetail2 = find.byValueKey("productDetail2");
      await driver.tap(productDetail2);
      //await ozzie.takeScreenshot("product detail");
    });

    test('productComment', () async {
      final productComment = find.byValueKey("comment");
      await driver.tap(productComment);
      //await ozzie.takeScreenshot("product comment");
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('promotion', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));      
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -500, Duration(seconds: 1));
      final zpromotionList = find.byValueKey("promotion");
      await driver.tap(zpromotionList);
      // await ozzie.takeScreenshot('promotion');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('promotionDetail0', () async {
      final zpromotionList = find.byValueKey("promotionDetail0");
      await driver.tap(zpromotionList);
      // await ozzie.takeScreenshot('promotionDetail0');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('coupon', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -500, Duration(seconds: 1));
      final coupon = find.byValueKey("coupon");
      await driver.tap(coupon);
      // await ozzie.takeScreenshot('coupon');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('couponDetail1', () async {
      final coupon = find.byValueKey("couponDetail1");
      await driver.tap(coupon);
      // await ozzie.takeScreenshot('couponDetail1');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('transaction', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      final transaction = find.byValueKey("transaction");
      await driver.tap(transaction);
      // await ozzie.takeScreenshot('transaction');
      
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('transactionlist0', () async {
      final transaction = find.byValueKey("transactionlist0");
      await driver.tap(transaction);
      // await ozzie.takeScreenshot('transactionlist0');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('transactionDetail', () async {
      final transaction = find.byValueKey("transactionDetail");
      await driver.tap(transaction);
      // await ozzie.takeScreenshot('transactionDetail');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('productDetail0', () async {
      final product = find.byValueKey("productDetail0");
      await driver.tap(product);
      // await ozzie.takeScreenshot('productDetail0');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('productwishlist', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -500, Duration(seconds: 1));
      final productwishlist = find.byValueKey("productwishlist");
      await driver.tap(productwishlist);
      // await ozzie.takeScreenshot('productwishlist');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('productDetail0', () async {
      final product = find.byValueKey("productDetail0");
      await driver.tap(product);
      // await ozzie.takeScreenshot('productDetail0');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
    });

    test('reviewlist', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -800, Duration(seconds: 1));
      final reviewlist = find.byValueKey("reviewlist");
      await driver.tap(reviewlist);
      final reviewtab1 = find.byValueKey("reviewtab1");
      await driver.tap(reviewtab1);
      // await ozzie.takeScreenshot('reviewlist');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('reviewdetail0', () async {
      final reviewdetail0 = find.byValueKey("reviewdetail0");
      await driver.tap(reviewdetail0);
      // await ozzie.takeScreenshot('reviewdetail0');
      // final backButton = find.byTooltip('Back');
      // await driver.tap(backButton);
    });

    test('productReviewAdd', () async {
      final productReviewAdd = find.byValueKey("productReviewAdd");
      await driver.tap(productReviewAdd);
      // await ozzie.takeScreenshot('productReviewAdd');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
      await driver.tap(backButton);
      await driver.tap(backButton);
    });


    test('basic page', () async {
      await driver.tap(find.byTooltip("Open navigation menu"));
      await driver.scroll(find.byValueKey("scroll_sidebar"), 0, -900, Duration(seconds: 1));
      final basicPage = find.byValueKey("basicPage1");
      await driver.tap(basicPage);
      // await ozzie.takeScreenshot('basic page');
      final backButton = find.byTooltip('Back');
      await driver.tap(backButton);
    });
  });
}
